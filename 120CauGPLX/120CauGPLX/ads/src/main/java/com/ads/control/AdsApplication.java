package com.ads.control;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.multidex.MultiDex;
import androidx.multidex.MultiDexApplication;

import com.ads.control.admob.AdsOpenManager;
import com.ads.control.utils.SharedPreferencesUtils;

public class AdsApplication extends MultiDexApplication {
    public AdsOpenManager appOpenManager;
    public static final String TEST_DEVICE_ID_D = "C1C34869AAAE7FEBA739E48E7BF64422";
    public static final String TEST_DEVICE_ID_Q = "57C26666FD0CFEFB7FCB0C4C62741EC4";
    public static final String TEST_DEVICE_ID_T = "28D542283DD7ED7C101E46250CB19F78";

    private static AdsApplication instance;

    public static AdsApplication getInstance() {
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        if (instance == null)
            instance = this;
        //init sharepreference
        SharedPreferencesUtils.init(this);
        //init font size
        if(SharedPreferencesUtils.getInt("font_setting_size",-1) == -1){
            SharedPreferencesUtils.setInt("font_setting_size",4);
        }
        //init admob
        //init open ads
        appOpenManager = new AdsOpenManager(this);
        if (SharedPreferencesUtils.isNightMode()) {
            setTheme(R.style.AppThemeDarkAds);
        } else {
            setTheme(R.style.AppThemeAds);
        }
    }

    @Override
    protected void attachBaseContext(@NonNull Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }
}
