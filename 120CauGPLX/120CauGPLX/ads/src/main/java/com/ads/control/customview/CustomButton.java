package com.ads.control.customview;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;

import com.ads.control.R;
import com.ads.control.utils.SharedPreferencesUtils;
import com.ads.control.utils.Utils;
import com.rey.material.widget.Button;

public class CustomButton extends Button {
    private static final String TAG = "TextView";
    private int currentFontSize = -1;

    public CustomButton(Context context) {
        super(context);
    }

    public CustomButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        setCustomFont(context, attrs);
        setCustomFontSize(context, attrs);
    }

    public CustomButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        setCustomFont(context, attrs);
        setCustomFontSize(context, attrs);
    }

    private void setCustomFont(Context ctx, AttributeSet attrs) {
        TypedArray a = ctx.obtainStyledAttributes(attrs, R.styleable.CustomTextView);
        String customFont = a.getString(R.styleable.CustomTextView_customFont);
        setCustomFont(ctx, customFont);
        a.recycle();
    }

    private void setCustomFontSize(Context ctx, AttributeSet attrs) {
        TypedArray a = ctx.obtainStyledAttributes(attrs, R.styleable.CustomTextView);
        int customFontSize = a.getDimensionPixelSize(R.styleable.CustomTextView_customFontSize,0);
        if(customFontSize > 0) {
            currentFontSize = customFontSize;
            setCustomFontSize(ctx, customFontSize);
        }
        a.recycle();
    }

    public void setCustomFontSize(Context ctx,int fontSize){
        try{
            fontSize = fontSize - Utils.convertDpToPx(ctx,3) + Utils.convertDpToPx(ctx, SharedPreferencesUtils.getInt("font_setting_size") + 1);
            setTextSize(TypedValue.COMPLEX_UNIT_PX, fontSize);
        }catch (Exception e){
            fontSize = fontSize - Utils.convertDpToPx(ctx,3) + Utils.convertDpToPx(ctx,3 + 1);
            setTextSize(TypedValue.COMPLEX_UNIT_PX, fontSize);
        }
    }

    public void changeFont(){
        if(currentFontSize > 0)
            setCustomFontSize(getContext(),currentFontSize);
    }

    public boolean setCustomFont(Context ctx, String fontName) {
        Typeface typeface = null;
        try {
            if(fontName == null){
                fontName = ctx.getString(R.string.regular);
            }
            typeface = Typeface.createFromAsset(ctx.getAssets(), "fonts/" + fontName + ".ttf");
        } catch (Exception e) {
            Log.e(TAG, "Unable to load typeface: "+e.getMessage());
            return false;
        }
        setTypeface(typeface);
        return true;
    }
}