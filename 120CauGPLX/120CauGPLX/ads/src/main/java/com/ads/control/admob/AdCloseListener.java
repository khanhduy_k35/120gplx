package com.ads.control.admob;

public interface AdCloseListener {
    void onAdClosed(boolean hasAds);
}
