package com.ads.control.sound;

import android.util.SparseIntArray;

import com.ads.control.R;


public class SoundFilesPracice implements FileList {

    private static final SparseIntArray SOUND_MAP = new SparseIntArray();

    static {
        SOUND_MAP.put(0, R.raw.sound_right_answer);
        SOUND_MAP.put(1, R.raw.sound_wrong_answer);
        SOUND_MAP.put(2, R.raw.sound_click);
    }

    @Override
    public int getFileCount() {
        return SOUND_MAP.size();
    }

    @Override
    public int getFileId(int note) {
        return SOUND_MAP.get(note);
    }

}
