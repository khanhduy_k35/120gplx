package com.ads.control.model;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.Keep;

import com.google.firebase.database.IgnoreExtraProperties;

@IgnoreExtraProperties
@Keep
public class ConfigAds implements Parcelable {

    public boolean newapp_redirect;
    public String newapp_package;
    public String newapp_name;
    public String newapp_icon;
    public String newapp_des1;
    public String newapp_des2;
    public String newapp_img1;
    public String newapp_img2;
    public String newapp_img3;

    public boolean other_redirect;
    public String other_package;
    public String other_name;
    public String other_icon;
    public String other_des1;
    public String other_des2;
    public String other_img1;
    public String other_img2;
    public String other_img3;
    public int newapp_redirect_percent;
    public int native_distance;
    public boolean load_floor;
    public int inter_distance;


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeByte(this.newapp_redirect ? (byte) 1 : (byte) 0);
        dest.writeString(this.newapp_package);
        dest.writeString(this.newapp_name);
        dest.writeString(this.newapp_icon);
        dest.writeString(this.newapp_des1);
        dest.writeString(this.newapp_des2);
        dest.writeString(this.newapp_img1);
        dest.writeString(this.newapp_img2);
        dest.writeString(this.newapp_img3);
        dest.writeByte(this.other_redirect ? (byte) 1 : (byte) 0);
        dest.writeString(this.other_package);
        dest.writeString(this.other_name);
        dest.writeString(this.other_icon);
        dest.writeString(this.other_des1);
        dest.writeString(this.other_des2);
        dest.writeString(this.other_img1);
        dest.writeString(this.other_img2);
        dest.writeString(this.other_img3);
        dest.writeInt(this.newapp_redirect_percent);
        dest.writeInt(this.native_distance);
        dest.writeByte(this.load_floor ? (byte) 1 : (byte) 0);
        dest.writeInt(this.inter_distance);
    }

    public void readFromParcel(Parcel source) {
        this.newapp_redirect = source.readByte() != 0;
        this.newapp_package = source.readString();
        this.newapp_name = source.readString();
        this.newapp_icon = source.readString();
        this.newapp_des1 = source.readString();
        this.newapp_des2 = source.readString();
        this.newapp_img1 = source.readString();
        this.newapp_img2 = source.readString();
        this.newapp_img3 = source.readString();
        this.other_redirect = source.readByte() != 0;
        this.other_package = source.readString();
        this.other_name = source.readString();
        this.other_icon = source.readString();
        this.other_des1 = source.readString();
        this.other_des2 = source.readString();
        this.other_img1 = source.readString();
        this.other_img2 = source.readString();
        this.other_img3 = source.readString();
        this.newapp_redirect_percent = source.readInt();
        this.native_distance = source.readInt();
        this.load_floor = source.readByte()!=0;
        this.inter_distance = source.readInt();
    }

    public ConfigAds() {
    }

    protected ConfigAds(Parcel in) {
        this.newapp_redirect = in.readByte() != 0;
        this.newapp_package = in.readString();
        this.newapp_name = in.readString();
        this.newapp_icon = in.readString();
        this.newapp_des1 = in.readString();
        this.newapp_des2 = in.readString();
        this.newapp_img1 = in.readString();
        this.newapp_img2 = in.readString();
        this.newapp_img3 = in.readString();
        this.other_redirect = in.readByte() != 0;
        this.other_package = in.readString();
        this.other_name = in.readString();
        this.other_icon = in.readString();
        this.other_des1 = in.readString();
        this.other_des2 = in.readString();
        this.other_img1 = in.readString();
        this.other_img2 = in.readString();
        this.other_img3 = in.readString();
        this.newapp_redirect_percent = in.readInt();
        this.native_distance = in.readInt();
        this.load_floor = in.readByte() != 0;
        this.inter_distance = in.readInt();
    }

    public static final Creator<ConfigAds> CREATOR = new Creator<ConfigAds>() {
        @Override
        public ConfigAds createFromParcel(Parcel source) {
            return new ConfigAds(source);
        }

        @Override
        public ConfigAds[] newArray(int size) {
            return new ConfigAds[size];
        }
    };
}
