package com.ads.control.rate;

import android.app.Activity;
import android.content.Context;
import android.widget.Toast;

import com.ads.control.R;
import com.ads.control.funtion.UtilsApp;
import com.ads.control.utils.SharedPreferencesUtils;


public class Rate {
    static int count = 0;
    public static void Show(final Context mContext, int Style) {
        Show(mContext, Style, true);
    }

    public static void Show(final Context mContext, int Style, boolean isFinish) {
        try {
            if (UtilsApp.isConnectionAvailable(mContext)) {
                if (!SharedPreferencesUtils.getBoolean("show_rate",false)) {
                    DialogRataNew a = new DialogRataNew(mContext);
                    a.show();
                } else if (isFinish) {
                    count++;
                    if(count==1){
                        Toast.makeText(mContext,mContext.getString(R.string.back_again),Toast.LENGTH_SHORT).show();
                    }
                    if(count == 2) {
                        ((Activity) (mContext)).finish();
                        count = -1;
                    }
                }

            } else if (isFinish) {
                ((Activity) (mContext)).finish();
            }

        } catch (Exception e) {
            e.printStackTrace();
            ((Activity) (mContext)).finish();
        }

    }
}
