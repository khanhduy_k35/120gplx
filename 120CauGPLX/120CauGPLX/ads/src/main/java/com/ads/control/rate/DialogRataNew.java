package com.ads.control.rate;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.ads.control.R;
import com.ads.control.funtion.UtilsApp;
import com.ads.control.utils.SharedPreferencesUtils;
import com.google.android.gms.tasks.Task;
import com.google.android.play.core.review.ReviewInfo;
import com.google.android.play.core.review.ReviewManager;
import com.google.android.play.core.review.ReviewManagerFactory;

public class DialogRataNew extends Dialog {
    public a p;
    public final RatingBar q;
    public final ImageView r;
    public final Context s;
    public final TextView t;
    public final TextView u;
    public TextView v;

    /* loaded from: classes.dex */
    public interface a {
        void a();

        void b();

        void c();
    }

    public DialogRataNew(Context context) {
        super(context, R.style.CustomAlertDialog);
        this.s = context;
        setContentView(R.layout.dialog_rating_app);
        setCancelable(true);
        WindowManager.LayoutParams attributes = getWindow().getAttributes();
        attributes.width = -1;
        attributes.height = -2;
        getWindow().setAttributes(attributes);
        getWindow().setSoftInputMode(16);
        this.v = (TextView) findViewById(R.id.tvTitle);
        TextView textView = (TextView) findViewById(R.id.tvContent);
        RatingBar ratingBar = (RatingBar) findViewById(R.id.rtb);
        this.q = ratingBar;
        ImageView imageView = (ImageView) findViewById(R.id.imgIcon);
        this.r = imageView;
        TextView textView2 = (TextView) findViewById(R.id.btnRate);
        this.t = textView2;
        TextView button = (TextView) findViewById(R.id.btn_exit);
        this.u = button;
        textView2.setTextColor(Color.parseColor("#FFFFFF"));
        this.v.setVisibility(View.VISIBLE);
        imageView.setImageResource(R.drawable.ic_star_5);
        textView2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                cancel();
                if(ratingBar.getRating() <= 4.0d){
                    appearGmail(context);
                    SharedPreferencesUtils.setBoolean("show_rate",true);
                }else{
                    ReviewManager manager = ReviewManagerFactory.create(context);
                    Task<ReviewInfo> request = manager.requestReviewFlow();
                    request.addOnCompleteListener(task -> {
                        if (task.isSuccessful()) {
                            // We can get the ReviewInfo object
                            ReviewInfo reviewInfo = task.getResult();
                            Task<Void> flow = manager.launchReviewFlow((AppCompatActivity)context, reviewInfo);
                            flow.addOnCompleteListener(task1 -> {
                                SharedPreferencesUtils.setBoolean("show_rate",true);
                                // The flow has finished. The API does not indicate whether the user
                                // reviewed or not, or even whether the review dialog was shown.
                            });
                        } else {
                            SharedPreferencesUtils.setBoolean("show_rate",false);
                            UtilsApp.RateApp(context);
                        }
                    });
                }
            }
        });
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((Activity) (context)).finish();
            }
        });
        ratingBar.setOnRatingBarChangeListener(new c(this));
        imageView.setImageResource(R.drawable.ic_star_0);
    }

    public class c implements RatingBar.OnRatingBarChangeListener {
        public DialogRataNew a;

        public c(DialogRataNew fVar) {
            this.a = fVar;
        }

        @Override
        public void onRatingChanged(RatingBar ratingBar, float f2, boolean z) {
            ImageView imageView;
            int i2;
            String valueOf = String.valueOf(this.a.q.getRating());
            valueOf.hashCode();
            char c2 = 65535;
            switch (valueOf.hashCode()) {
                case 48563:
                    if (valueOf.equals("1.0")) {
                        c2 = 0;
                        break;
                    }
                    break;
                case 49524:
                    if (valueOf.equals("2.0")) {
                        c2 = 1;
                        break;
                    }
                    break;
                case 50485:
                    if (valueOf.equals("3.0")) {
                        c2 = 2;
                        break;
                    }
                    break;
                case 51446:
                    if (valueOf.equals("4.0")) {
                        c2 = 3;
                        break;
                    }
                    break;
                case 52407:
                    if (valueOf.equals("5.0")) {
                        c2 = 4;
                        break;
                    }
                    break;
            }
            switch (c2) {
                case 0:
                    this.a.v.setVisibility(View.VISIBLE);
                    this.a.u.setVisibility(View.VISIBLE);
                    this.a.t.setTextColor(Color.parseColor("#FFFFFF"));
                    DialogRataNew fVar = this.a;
                    fVar.t.setBackgroundResource(R.drawable.background_button_rate);
                    imageView = this.a.r;
                    i2 = R.drawable.ic_star_1;
                    break;
                case 1:
                    this.a.t.setTextColor(Color.parseColor("#FFFFFF"));
                    this.a.v.setVisibility(View.VISIBLE);
                    this.a.u.setVisibility(View.VISIBLE);
                    DialogRataNew fVar2 = this.a;
                    fVar2.t.setBackgroundResource(R.drawable.background_button_rate);
                    imageView = this.a.r;
                    i2 = R.drawable.ic_star_2;
                    break;
                case 2:
                    this.a.t.setTextColor(Color.parseColor("#FFFFFF"));
                    this.a.v.setVisibility(View.VISIBLE);
                    this.a.u.setVisibility(View.VISIBLE);
                    DialogRataNew fVar3 = this.a;
                    fVar3.t.setBackgroundResource(R.drawable.background_button_rate);
                    imageView = this.a.r;
                    i2 = R.drawable.ic_star_3;
                    break;
                case 3:
                    this.a.t.setTextColor(Color.parseColor("#FFFFFF"));
                    this.a.v.setVisibility(View.VISIBLE);
                    this.a.u.setVisibility(View.VISIBLE);
                    DialogRataNew fVar4 = this.a;
                    fVar4.t.setBackgroundResource(R.drawable.background_button_rate);
                    imageView = this.a.r;
                    i2 = R.drawable.ic_star_4;
                    break;
                case 4:
                    this.a.t.setTextColor(Color.parseColor("#FFFFFF"));
                    this.a.v.setVisibility(View.VISIBLE);
                    DialogRataNew fVar5 = this.a;
                    fVar5.t.setBackgroundResource(R.drawable.background_button_rate);
                    imageView = this.a.r;
                    i2 = R.drawable.ic_star_5;
                    break;
                default:
                    this.a.t.setTextColor(Color.parseColor("#8035C2FD"));
                    this.a.v.setVisibility(View.VISIBLE);
                    DialogRataNew fVar6 = this.a;
                    fVar6.t.setBackgroundResource(R.drawable.background_button_rate_disable);
                    this.a.r.setImageResource(R.drawable.ic_star_0);
                    this.a.u.setVisibility(View.VISIBLE);
                    return;
            }
            imageView.setImageResource(i2);
        }
    }

    public void appearGmail(Context context){
        try {
            Intent intent = new Intent(Intent.ACTION_SEND);
            String[] emails_in_to={context.getString(R.string.email_feedback)};
            intent.putExtra(Intent.EXTRA_EMAIL, emails_in_to );
            intent.putExtra(Intent.EXTRA_SUBJECT,"Camera, Image Translate");
            intent.putExtra(Intent.EXTRA_TEXT, context.getString(R.string.title_email));
            intent.setType("text/html");
            intent.setPackage("com.google.android.gm");
            context.startActivity(intent);
        }catch (Exception e){
            Intent mailer = new Intent(Intent.ACTION_SEND);
            mailer.setType("text/plain");
            mailer.putExtra(Intent.EXTRA_EMAIL, new String[]{context.getString(R.string.email_feedback)});
            mailer.putExtra(Intent.EXTRA_SUBJECT,"Camera, Image Translate");
            mailer.putExtra(Intent.EXTRA_TEXT, context.getString(R.string.title_email));
            context.startActivity(Intent.createChooser(mailer, "Tale Of Word"));
        }
    }

}
