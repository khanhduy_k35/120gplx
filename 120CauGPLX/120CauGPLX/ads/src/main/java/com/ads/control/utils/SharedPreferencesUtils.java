package com.ads.control.utils;
import android.content.Context;
import android.content.SharedPreferences;

import com.ads.control.R;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class SharedPreferencesUtils {
    private static SharedPreferences mPref = null;
    private static String mName = "";
    public enum Cmd {
        INIT("null");

        private String mDefault;

        Cmd(String def) {
            this.mDefault = def;
        }

        public String getDefault() {
            return mDefault;
        }
    }


    public static void init(Context context) {
        mName = context.getString(R.string.share_preference_name);
        mPref = context.getSharedPreferences(mName, Context.MODE_PRIVATE);
    }

    public static void set(Cmd cmd, String data) {
        if (mPref != null) {
            SharedPreferences.Editor edit = mPref.edit();
            edit.putString(cmd.toString(), data);
            edit.commit();
        }
    }

    public static String getCmd(Cmd cmd) {
        if (mPref == null || !mPref.contains(cmd.name())) {
            SharedPreferences.Editor edit = mPref.edit();
            edit.putString(cmd.toString(), cmd.getDefault());
            edit.commit();
        }

        return mPref.getString(cmd.toString(), "null");
    }

    public static void setBoolean(String _key, boolean _value) {
        if (_key == null) {
            return;
        }

        if (mPref != null) {
            SharedPreferences.Editor edit = mPref.edit();
            edit.putBoolean(_key, _value);
            edit.commit();
        }
    }

    public static boolean getBoolean(String _key) {
        if (mPref == null || !mPref.contains(_key)) {
            SharedPreferences.Editor edit = mPref.edit();
            edit.putBoolean(_key, false);
            edit.commit();
        }

        return mPref.getBoolean(_key, false);
    }

    public static boolean getBoolean(String _key, boolean _value) {
        if (mPref == null || !mPref.contains(_key)) {
            SharedPreferences.Editor edit = mPref.edit();
            edit.putBoolean(_key, _value);
            edit.commit();
        }

        return mPref.getBoolean(_key, _value);
    }

    public static void setString(String _key, String _value) {
        if (_key == null) {
            return;
        }

        if (mPref != null) {
            SharedPreferences.Editor edit = mPref.edit();
            edit.putString(_key, _value);
            edit.commit();
        }
    }

    public static String getString(String _key) {
        if (mPref == null || !mPref.contains(_key)) {
            SharedPreferences.Editor edit = mPref.edit();
            edit.putString(_key, null);
            edit.commit();
        }
        return mPref.getString(_key, null);

    }

    public static String getString(String _key, String _value) {
        if (mPref == null || !mPref.contains(_key)) {
            SharedPreferences.Editor edit = mPref.edit();
            edit.putString(_key, _value);
            edit.commit();
        }
        return mPref.getString(_key, _value);

    }

    public static void setInt(String _key, int _value) {
        if (_key == null) {
            return;
        }

        if (mPref != null) {
            SharedPreferences.Editor edit = mPref.edit();
            edit.putInt(_key, _value);
            edit.commit();
        }
    }

    public static int getInt(String _key, int _value) {
        if (mPref == null || !mPref.contains(_key)) {
            SharedPreferences.Editor edit = mPref.edit();
            edit.putInt(_key, _value);
            edit.commit();
        }
        return mPref.getInt(_key, _value);

    }

    public static int getInt(String _key) {
        return mPref.getInt(_key, -1);
    }

    public static void setFloat(String _key, float _value) {
        if (_key == null) {
            return;
        }

        if (mPref != null) {
            SharedPreferences.Editor edit = mPref.edit();
            edit.putFloat(_key, _value);
            edit.commit();
        }
    }

    public static long getLong(String _key, long _value) {
        if (mPref == null || !mPref.contains(_key)) {
            SharedPreferences.Editor edit = mPref.edit();
            edit.putLong(_key, _value);
            edit.commit();
        }
        return mPref.getLong(_key, _value);

    }

    public static void setLong(String _key, long _value) {
        if (_key == null) {
            return;
        }

        if (mPref != null) {
            SharedPreferences.Editor edit = mPref.edit();
            edit.putLong(_key, _value);
            edit.commit();
        }
    }

    public static float getFloat(String _key, float _value) {
        if (mPref == null || !mPref.contains(_key)) {
            SharedPreferences.Editor edit = mPref.edit();
            edit.putFloat(_key, _value);
            edit.commit();
        }
        return mPref.getFloat(_key, _value);
    }


    public static boolean isNightMode(){
        return getBoolean("APP_THEME_DARK",false);
    }

    public static void setNightMode(boolean value){
        setBoolean("APP_THEME_DARK",value);
    }

    public static String getWakeUp() {
        long time = getTimeNotification();
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(time);
        return new SimpleDateFormat("HH:mm", Locale.getDefault()).format(calendar.getTime());
    }

    public static long getTimeNotification(){
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY,21);
        calendar.set(Calendar.MINUTE,0);
        calendar.set(Calendar.SECOND,0);
        return SharedPreferencesUtils.getLong("time_notification",calendar.getTimeInMillis());
    }

    public static void setTimeNotification(long time){
        SharedPreferencesUtils.setLong("time_notification",time);
    }

    public static int getReminderInterval() {
        return getInt("REMINDER_INTERVAL", 720);
    }

    public static boolean showReminder(){
        return SharedPreferencesUtils.getBoolean("show_reminder",true);
    }

    public static void setShowReminder(boolean b){
        SharedPreferencesUtils.setBoolean("show_reminder",b);
    }

    public static void setDayTestExam(long time){
        SharedPreferencesUtils.setLong("day_test_exam",time);
    }

    public static long getDayTestExam(){
        return SharedPreferencesUtils.getLong("day_test_exam",0);
    }

    public static boolean isPro(){
        return SharedPreferencesUtils.getBoolean("isPro", false);
    }

    public static void setIsPro(boolean isPro){
        SharedPreferencesUtils.setBoolean("isPro", isPro);
    }

    public static boolean soundEffect(){
        return SharedPreferencesUtils.getBoolean("sound_effect", true);
    }

    public static void setSoundEffect(boolean soundEffect){
        SharedPreferencesUtils.setBoolean("sound_effect", soundEffect);
    }

    public static boolean showVoidEnable(){
        return SharedPreferencesUtils.getBoolean("sound_enable",true);
    }

    public static void setShowVoidEnable(boolean b){
        SharedPreferencesUtils.setBoolean("sound_enable",b);
    }

    public static String getVoiceCode(){
        return SharedPreferencesUtils.getString("voice_code","en-US-Wavenet-D");
    }

    public static void setVoiceCode(String voiceCode){
        SharedPreferencesUtils.setString("voice_code",voiceCode);
    }
}

