package com.ads.control.google_iab.enums;

public enum SupportState {
    SUPPORTED,
    NOT_SUPPORTED,
    DISCONNECTED
}
