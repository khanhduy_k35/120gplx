package com.ads.control.admob;

import static com.google.android.gms.ads.nativead.NativeAdOptions.ADCHOICES_TOP_RIGHT;
import static com.google.android.gms.ads.nativead.NativeAdOptions.NATIVE_MEDIA_ASPECT_RATIO_ANY;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;

import com.ads.control.R;
import com.ads.control.activity.AdApperActivity;
import com.ads.control.activity.BaseAdsActivity;
import com.ads.control.utils.SharedPreferencesUtils;
import com.ads.control.utils.Utils;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.android.gms.ads.AdError;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdLoader;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.FullScreenContentCallback;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.OnUserEarnedRewardListener;
import com.google.android.gms.ads.VideoOptions;
import com.google.android.gms.ads.interstitial.InterstitialAd;
import com.google.android.gms.ads.interstitial.InterstitialAdLoadCallback;
import com.google.android.gms.ads.nativead.NativeAd;
import com.google.android.gms.ads.nativead.NativeAdOptions;
import com.google.android.gms.ads.nativead.NativeAdView;
import com.google.android.gms.ads.rewarded.RewardItem;
import com.google.android.gms.ads.rewarded.RewardedAd;
import com.google.android.gms.ads.rewarded.RewardedAdLoadCallback;

import java.util.Random;

public class AdmobHelp {
    private static AdmobHelp instance;
    private InterstitialAd mInterstitialAd;
    private AdCloseListener adCloseListener;
    private RewardedAd rewardedAd;
    private AdRewardListener adRewardListener;
    private long TimeLoad = 0;
    private long TimeReload = Utils.TIME_DISTANCE_DISPLAYADS * 1000;
    private boolean initialLayoutComplete = false;

    public long getTimeLoad() {
        return TimeLoad;
    }

    public void setTimeLoad(long timeLoad) {
        TimeLoad = timeLoad;
    }

    public long getTimeReload() {
        return TimeReload;
    }

    public void setTimeReload(long timeReload) {
        TimeReload = timeReload;
    }

    public static AdmobHelp getInstance() {
        if (instance == null) {
            instance = new AdmobHelp();
        }
        return instance;
    }

    private AdmobHelp() {

    }

    public AdCloseListener getAdCloseListener() {
        return adCloseListener;
    }

    public void setAdCloseListener(AdCloseListener adCloseListener) {
        this.adCloseListener = adCloseListener;
    }

    public void init(Activity context) {
        TimeLoad = System.currentTimeMillis() - (Utils.TIME_DISTANCE_DISPLAYADS - 10) * 1000;
        AppLovinHelp.getInstance().init(context);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                loadInterstitialAd(context);
            }
        },3000);
    }

    /**************************** begin inter ads *******************************/
    public void loadReward(Activity context) {
        if (rewardedAd == null) {
            AdRequest adRequest = new AdRequest.Builder().build();
            RewardedAd.load(context, context.getString(R.string.admob_reward_high),
                    adRequest, new RewardedAdLoadCallback() {
                        @Override
                        public void onAdFailedToLoad(@NonNull LoadAdError loadAdError) {
                            // Handle the error.
                            rewardedAd = null;
                            loadRewardMedium(context);
                        }

                        @Override
                        public void onAdLoaded(@NonNull RewardedAd ad) {
                            rewardedAd = ad;
                        }
                    });
        }
    }

    public void loadRewardMedium(Activity context) {
        if (rewardedAd == null) {
            AdRequest adRequest = new AdRequest.Builder().build();
            RewardedAd.load(context, context.getString(R.string.admob_reward_medium),
                    adRequest, new RewardedAdLoadCallback() {
                        @Override
                        public void onAdFailedToLoad(@NonNull LoadAdError loadAdError) {
                            // Handle the error.
                            AppLovinHelp.getInstance().loadReward(context);
                        }

                        @Override
                        public void onAdLoaded(@NonNull RewardedAd ad) {
                            rewardedAd = ad;
                        }
                    });
        }
    }

    public void showReward(Activity activity, AdRewardListener adRewardListenerTemp) {
        this.adRewardListener = adRewardListenerTemp;
        if (rewardedAd != null) {
            rewardedAd.show(activity, new OnUserEarnedRewardListener() {
                @Override
                public void onUserEarnedReward(@NonNull RewardItem rewardItem) {
                    adRewardListener.onAdRewardSuccess();
                }
            });
            rewardedAd.setFullScreenContentCallback(new FullScreenContentCallback() {
                @Override
                public void onAdClicked() {
                    super.onAdClicked();
                }

                @Override
                public void onAdDismissedFullScreenContent() {
                    super.onAdDismissedFullScreenContent();
                    rewardedAd = null;
                }

                @Override
                public void onAdFailedToShowFullScreenContent(@NonNull AdError adError) {
                    super.onAdFailedToShowFullScreenContent(adError);
                    adRewardListener.onAdRewardFailed();
                    rewardedAd = null;
                }

                @Override
                public void onAdImpression() {
                    super.onAdImpression();
                }

                @Override
                public void onAdShowedFullScreenContent() {
                    super.onAdShowedFullScreenContent();
                }
            });
        } else {
            AppLovinHelp.getInstance().showReward(activity, adRewardListener);
        }
    }

    /**************************** end reward ads *******************************/

    /**************************** begin inter ads *******************************/
    private void loadInterstitialAd(final Activity context) {
        if (mInterstitialAd != null)
            return;
        if(!SharedPreferencesUtils.getBoolean("load_floor")){
            AppLovinHelp.getInstance().loadInterstitialAd(context);
            return;
        }
        AdRequest adRequest = new AdRequest.Builder().build();
        InterstitialAd.load(context, context.getString(R.string.admob_inter_high), adRequest, new InterstitialAdLoadCallback() {
            @Override
            public void onAdLoaded(@NonNull InterstitialAd interstitialAd) {
                mInterstitialAd = interstitialAd;
                mInterstitialAd.setFullScreenContentCallback(new FullScreenContentCallback() {
                    @Override
                    public void onAdDismissedFullScreenContent() {
                        // Called when fullscreen content is dismissed.
                        adCloseListener.onAdClosed(true);
                        loadInterstitialAd(context);
                    }

                    @Override
                    public void onAdFailedToShowFullScreenContent(AdError adError) {
                        // Called when fullscreen content failed to show.
                        adCloseListener.onAdClosed(false);
                    }

                    @Override
                    public void onAdShowedFullScreenContent() {
                        mInterstitialAd = null;
                    }
                });
            }

            @Override
            public void onAdFailedToLoad(@NonNull LoadAdError loadAdError) {
                mInterstitialAd = null;
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        loadInterstitialAdMedium(context);
                    }
                },new Random().nextInt(200) + 300);
            }
        });
    }

    private void loadInterstitialAdMedium(final Activity context) {
        if (mInterstitialAd != null)
            return;
        AdRequest adRequest = new AdRequest.Builder().build();
        InterstitialAd.load(context, context.getString(R.string.admob_inter_medium), adRequest, new InterstitialAdLoadCallback() {
            @Override
            public void onAdLoaded(@NonNull InterstitialAd interstitialAd) {
                mInterstitialAd = interstitialAd;
                mInterstitialAd.setFullScreenContentCallback(new FullScreenContentCallback() {
                    @Override
                    public void onAdDismissedFullScreenContent() {
                        // Called when fullscreen content is dismissed.
                        adCloseListener.onAdClosed(true);
                        loadInterstitialAdMedium(context);
                    }

                    @Override
                    public void onAdFailedToShowFullScreenContent(AdError adError) {
                        // Called when fullscreen content failed to show.
                        adCloseListener.onAdClosed(false);
                    }

                    @Override
                    public void onAdShowedFullScreenContent() {
                        mInterstitialAd = null;
                    }
                });
            }

            @Override
            public void onAdFailedToLoad(@NonNull LoadAdError loadAdError) {
                mInterstitialAd = null;
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        AppLovinHelp.getInstance().loadInterstitialAd(context);
                    }
                },new Random().nextInt(200) + 300);
            }
        });
    }

    public void showInterstitialAd(final AdCloseListener adCloseListenerTemp) {
        showInterstitialAd(adCloseListenerTemp, true);
    }

    public void showInterstitialAd(final AdCloseListener adCloseListenerTemp,boolean loading) {
        if(AdsOpenManager.getInstance().getCurrentActivity()!=null && AdsOpenManager.getInstance().getCurrentActivity() instanceof BaseAdsActivity) {
            this.adCloseListener = adCloseListenerTemp;
            if (SharedPreferencesUtils.isPro() || !Utils.isDisplayAds) {
                if (adCloseListener != null) {
                    adCloseListener.onAdClosed(false);
                }
            } else {
                if ((TimeLoad + TimeReload) < System.currentTimeMillis()) {
                    if (canShowInterstitialAd(AdsOpenManager.getInstance().getCurrentActivity())) {
                        if(loading){
                            Intent intent = new Intent(AdsOpenManager.getInstance().getCurrentActivity(), AdApperActivity.class);
                            intent.putExtra("type","inter");
                            ((BaseAdsActivity) AdsOpenManager.getInstance().getCurrentActivity()).startActivity(intent);
                        }else {
                            mInterstitialAd.show(AdsOpenManager.getInstance().getCurrentActivity());
                            TimeLoad = System.currentTimeMillis();
                        }
                    } else if (AppLovinHelp.getInstance().canShowInterstitialAd(AdsOpenManager.getInstance().getCurrentActivity())) {
                        if(loading){
                            Intent intent = new Intent(AdsOpenManager.getInstance().getCurrentActivity(), AdApperActivity.class);
                            intent.putExtra("type","inter");
                            ((BaseAdsActivity)AdsOpenManager.getInstance().getCurrentActivity()).startActivity(intent);
                        }else {
                            AppLovinHelp.getInstance().showInterstitialAd(AdsOpenManager.getInstance().getCurrentActivity(), adCloseListenerTemp);
                        }
                    } else {
                        adCloseListener.onAdClosed(false);
                    }
                } else {
                    adCloseListener.onAdClosed(false);
                }
            }
        }else{
            if (adCloseListenerTemp != null) {
                adCloseListenerTemp.onAdClosed(false);
            }
        }
    }

    public void showInterstitialAdNonLoading(final AdCloseListener adCloseListenerTemp) {
        if (canShowInterstitialAd(AdsOpenManager.getInstance().getCurrentActivity())) {
            mInterstitialAd.show(AdsOpenManager.getInstance().getCurrentActivity());
            TimeLoad = System.currentTimeMillis();
        } else if (AppLovinHelp.getInstance().canShowInterstitialAd(AdsOpenManager.getInstance().getCurrentActivity())) {
            AppLovinHelp.getInstance().showInterstitialAd(AdsOpenManager.getInstance().getCurrentActivity(), adCloseListenerTemp);
        } else {
            adCloseListener.onAdClosed(false);
        }
    }

    private boolean canShowInterstitialAd(Context context) {
        return mInterstitialAd != null && context instanceof Activity;
    }
    /**************************** end inter ads *******************************/


    /**
     * cach dung bat buoc la dung LinearLayout
     * <LinearLayout
     * android:layout_width="match_parent"
     * android:layout_height="wrap_content"
     * android:layout_alignParentBottom="true"
     * android:id="@+id/banner_ads">
     * <include layout="@layout/item_admob_banner"></include>
     * </LinearLayout>
     *
     * @param mActivity
     */
    public void loadBanner(final Activity mActivity) {
        initialLayoutComplete = false;
        if (mActivity == null) {
            return;
        }
        if (SharedPreferencesUtils.isPro() || !Utils.isDisplayAds) {
            RelativeLayout container_ads_banner = mActivity.findViewById(R.id.container_ads_banner);
            if (container_ads_banner != null) {
                container_ads_banner.setVisibility(View.GONE);
            }
        }

        if (!SharedPreferencesUtils.isPro() && Utils.isDisplayAds) {
            final RelativeLayout container_ads_banner = mActivity.findViewById(R.id.container_ads_banner);
            if (container_ads_banner != null) {
                container_ads_banner.getViewTreeObserver().addOnGlobalLayoutListener(
                        new ViewTreeObserver.OnGlobalLayoutListener() {
                            @Override
                            public void onGlobalLayout() {
                                if (!initialLayoutComplete) {
                                    initialLayoutComplete = true;
                                    loadBannerAds(mActivity, container_ads_banner);
                                }
                            }
                        });
            }
        }
    }

    public void loadBannerAds(Activity mActivity, RelativeLayout container_ads_banner) {
        if(!SharedPreferencesUtils.getBoolean("load_floor")){
            loadBannerAdsAll(mActivity,container_ads_banner);
            return;
        }
        ShimmerFrameLayout shimmerFrameLayout = container_ads_banner.findViewById(R.id.shimmerLayout);
        if (shimmerFrameLayout != null)
            shimmerFrameLayout.startShimmer();
        if (Utils.isDisplayAds) {
            final AdView adView = new AdView(mActivity);
            adView.setAdUnitId(mActivity.getString(R.string.admob_banner_high));
            adView.setAdSize(getAdSize(mActivity));

            LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) container_ads_banner.getLayoutParams();
            layoutParams.height = Utils.convertDpToPx(mActivity, getAdSize(mActivity).getHeight());
            container_ads_banner.setLayoutParams(layoutParams);
            container_ads_banner.requestLayout();
            AdRequest adRequest = new AdRequest.Builder().build();
            adView.setAdListener(new AdListener() {
                @Override
                public void onAdLoaded() {
                    super.onAdLoaded();
                    if (mActivity != null) {
                        if (shimmerFrameLayout != null)
                            shimmerFrameLayout.stopShimmer();
                        if (shimmerFrameLayout != null)
                            shimmerFrameLayout.setVisibility(View.GONE);
                        container_ads_banner.removeAllViews();
                        container_ads_banner.addView(adView);
                    }
                }

                @Override
                public void onAdFailedToLoad(LoadAdError i) {
                    super.onAdFailedToLoad(i);
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            loadBannerAdsMedium(mActivity, container_ads_banner);
                        }
                    }, new Random().nextInt(200) +300);
                }
            });
            adView.loadAd(adRequest);
        }
    }

    private void loadBannerAdsMedium(Activity mActivity, RelativeLayout container_ads_banner) {
        ShimmerFrameLayout shimmerFrameLayout = container_ads_banner.findViewById(R.id.shimmerLayout);
        if (shimmerFrameLayout != null)
            shimmerFrameLayout.startShimmer();
        if (Utils.isDisplayAds) {
            final AdView adView = new AdView(mActivity);
            adView.setAdUnitId(mActivity.getString(R.string.admob_banner_medium));
            adView.setAdSize(getAdSize(mActivity));

            LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) container_ads_banner.getLayoutParams();
            layoutParams.height = Utils.convertDpToPx(mActivity, getAdSize(mActivity).getHeight());
            container_ads_banner.setLayoutParams(layoutParams);
            container_ads_banner.requestLayout();
            AdRequest adRequest = new AdRequest.Builder().build();
            adView.setAdListener(new AdListener() {
                @Override
                public void onAdLoaded() {
                    super.onAdLoaded();
                    if (mActivity != null) {
                        if (shimmerFrameLayout != null)
                            shimmerFrameLayout.stopShimmer();
                        if (shimmerFrameLayout != null)
                            shimmerFrameLayout.setVisibility(View.GONE);
                        container_ads_banner.removeAllViews();
                        container_ads_banner.addView(adView);
                    }
                }

                @Override
                public void onAdFailedToLoad(LoadAdError i) {
                    super.onAdFailedToLoad(i);
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            loadBannerAdsAll(mActivity,container_ads_banner);
                        }
                    }, new Random().nextInt(200) +300);
                }
            });
            adView.loadAd(adRequest);
        }
    }

    private void loadBannerAdsAll(Activity mActivity, RelativeLayout container_ads_banner) {
        ShimmerFrameLayout shimmerFrameLayout = container_ads_banner.findViewById(R.id.shimmerLayout);
        if (shimmerFrameLayout != null)
            shimmerFrameLayout.startShimmer();
        if (Utils.isDisplayAds) {
            final AdView adView = new AdView(mActivity);
            adView.setAdUnitId(mActivity.getString(R.string.admob_banner_all));
            adView.setAdSize(getAdSize(mActivity));

            LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) container_ads_banner.getLayoutParams();
            layoutParams.height = Utils.convertDpToPx(mActivity, getAdSize(mActivity).getHeight());
            container_ads_banner.setLayoutParams(layoutParams);
            container_ads_banner.requestLayout();
            AdRequest adRequest = new AdRequest.Builder().build();
            adView.setAdListener(new AdListener() {
                @Override
                public void onAdLoaded() {
                    super.onAdLoaded();
                    if (mActivity != null) {
                        if (shimmerFrameLayout != null)
                            shimmerFrameLayout.stopShimmer();
                        if (shimmerFrameLayout != null)
                            shimmerFrameLayout.setVisibility(View.GONE);
                        container_ads_banner.removeAllViews();
                        container_ads_banner.addView(adView);
                    }
                }

                @Override
                public void onAdFailedToLoad(LoadAdError i) {
                    super.onAdFailedToLoad(i);
                    if(container_ads_banner!=null){
                        try {
                            container_ads_banner.setVisibility(View.GONE);
                        }catch (Exception e){

                        }
                    }
                    if(container_ads_banner.getParent()!=null){
                        ((ViewGroup)container_ads_banner.getParent()).setVisibility(View.GONE);
                    }
                    if(shimmerFrameLayout!=null)
                        shimmerFrameLayout.stopShimmer();
                    if(shimmerFrameLayout!=null)
                        shimmerFrameLayout.setVisibility(View.GONE);
                }
            });
            adView.loadAd(adRequest);
        }
    }



    private AdSize getAdSize(Activity activity) {
        // Step 2 - Determine the screen width (less decorations) to use for the ad width.
        Display display = activity.getWindowManager().getDefaultDisplay();
        DisplayMetrics outMetrics = new DisplayMetrics();
        display.getMetrics(outMetrics);

        float widthPixels = outMetrics.widthPixels;
        float density = outMetrics.density;

        int adWidth = (int) (widthPixels / density);

        // Step 3 - Get adaptive ad size and return for setting on the ad view.
        return AdSize.getCurrentOrientationAnchoredAdaptiveBannerAdSize(activity, adWidth);
    }


    public void openAppShow(Activity appCompatActivity) {
        try {
            //an native
            RelativeLayout container_ads_native = appCompatActivity.findViewById(R.id.container_ads_native);
            if (container_ads_native != null) {
                container_ads_native.setVisibility(View.GONE);
                if (container_ads_native.getParent() != null) {
                    ((ViewGroup) container_ads_native.getParent()).setVisibility(View.GONE);
                }
            }
            //an banner
            RelativeLayout container_ads_banner = appCompatActivity.findViewById(R.id.container_ads_banner);
            if (container_ads_banner != null) {
                container_ads_banner.setVisibility(View.GONE);
            }
        } catch (Exception e) {

        }
    }

    public void openAppHidden(Activity appCompatActivity) {
        try {
            if (SharedPreferencesUtils.isPro() || !Utils.isDisplayAds) {
                RelativeLayout container_ads_native = appCompatActivity.findViewById(R.id.container_ads_native);
                if (container_ads_native != null) {
                    container_ads_native.setVisibility(View.GONE);
                    if (container_ads_native.getParent() != null) {
                        ((ViewGroup) container_ads_native.getParent()).setVisibility(View.GONE);
                    }
                }
                RelativeLayout container_ads_banner = appCompatActivity.findViewById(R.id.container_ads_banner);
                if (container_ads_banner != null) {
                    container_ads_banner.setVisibility(View.GONE);
                }
            } else {
                RelativeLayout container_ads_native = appCompatActivity.findViewById(R.id.container_ads_native);
                if (container_ads_native != null) {
                    container_ads_native.setVisibility(View.VISIBLE);
                    if (container_ads_native.getParent() != null) {
                        ((ViewGroup) container_ads_native.getParent()).setVisibility(View.VISIBLE);
                    }
                }
                RelativeLayout container_ads_banner = appCompatActivity.findViewById(R.id.container_ads_banner);
                if (container_ads_banner != null) {
                    container_ads_banner.setVisibility(View.VISIBLE);
                }
            }
        } catch (Exception e) {

        }
    }

    public void loadNativeActivity(final Activity mActivity) {
        loadNativeActivity(mActivity,false);
    }

    public void loadNativeActivity(final Activity mActivity, boolean isMedium) {
        if (mActivity == null) {
            return;
        }
        if(!SharedPreferencesUtils.getBoolean("load_floor")){
            AppLovinHelp.getInstance().loadNativeActivity(mActivity,isMedium);
            return;
        }
        if (SharedPreferencesUtils.isPro() || !Utils.isDisplayAds) {
            RelativeLayout container_ads_native = mActivity.findViewById(R.id.container_ads_native);
            if (container_ads_native != null) {
                container_ads_native.setVisibility(View.GONE);
            }
            if (container_ads_native.getParent() != null) {
                ((ViewGroup) container_ads_native.getParent()).setVisibility(View.GONE);
            }
        }
        if (!SharedPreferencesUtils.isPro() && Utils.isDisplayAds) {
            RelativeLayout container_ads_native = mActivity.findViewById(R.id.container_ads_native);
            if (container_ads_native != null) {
                ShimmerFrameLayout shimmerFrameLayout = container_ads_native.findViewById(R.id.shimmerLayout);
                View adsLayout = mActivity.getLayoutInflater().inflate(isMedium ? R.layout.native_ads_admob_medium_top : R.layout.native_ads_admob, null);
                NativeAdView nativeAdViewLayout = adsLayout.findViewById(R.id.nativeAdView);
                LinearLayout contentAds = container_ads_native.findViewById(R.id.contentAds);
                if (shimmerFrameLayout != null)
                    shimmerFrameLayout.startShimmer();
                if (Utils.isDisplayAds) {
                    AdLoader.Builder builder = new AdLoader.Builder(mActivity, mActivity.getString(R.string.admob_native_high))
                            .forNativeAd(new NativeAd.OnNativeAdLoadedListener() {
                                @Override
                                public void onNativeAdLoaded(@NonNull NativeAd nativeAd) {
                                    container_ads_native.removeAllViews();
                                    container_ads_native.addView(nativeAdViewLayout);
                                    if (contentAds != null)
                                        contentAds.setVisibility(View.VISIBLE);
                                    if (nativeAdViewLayout != null && nativeAd != null)
                                        Utils.populateUnifiedNativeAdView(nativeAd, nativeAdViewLayout);
                                }
                            });


                    VideoOptions videoOptions =
                            new VideoOptions.Builder().setStartMuted(true).build();

                    NativeAdOptions adOptions =
                            new NativeAdOptions.Builder()
                                    .setVideoOptions(videoOptions)
                                    .setMediaAspectRatio(NATIVE_MEDIA_ASPECT_RATIO_ANY)
                                    .setAdChoicesPlacement(ADCHOICES_TOP_RIGHT).build();

                    builder.withNativeAdOptions(adOptions);

                    AdLoader adLoader = builder.withAdListener(new AdListener() {
                        @Override
                        public void onAdFailedToLoad(LoadAdError loadAdError) {
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    loadNativeMediumActivity(mActivity,isMedium);
                                }
                            },new Random().nextInt(200) + 300);
                        }
                    }).build();
                    adLoader.loadAd(new AdRequest.Builder().build());
                }
            }
        }
    }

    private void loadNativeMediumActivity(final Activity mActivity, boolean isMedium) {
        if (mActivity == null) {
            return;
        }
        if (SharedPreferencesUtils.isPro() || !Utils.isDisplayAds) {
            RelativeLayout container_ads_native = mActivity.findViewById(R.id.container_ads_native);
            if (container_ads_native != null) {
                container_ads_native.setVisibility(View.GONE);
            }
            if (container_ads_native.getParent() != null) {
                ((ViewGroup) container_ads_native.getParent()).setVisibility(View.GONE);
            }
        }
        if (!SharedPreferencesUtils.isPro() && Utils.isDisplayAds) {
            RelativeLayout container_ads_native = mActivity.findViewById(R.id.container_ads_native);
            if (container_ads_native != null) {
                ShimmerFrameLayout shimmerFrameLayout = container_ads_native.findViewById(R.id.shimmerLayout);
                View adsLayout = mActivity.getLayoutInflater().inflate(isMedium ? R.layout.native_ads_admob_medium_top : R.layout.native_ads_admob, null);
                NativeAdView nativeAdViewLayout = adsLayout.findViewById(R.id.nativeAdView);
                LinearLayout contentAds = container_ads_native.findViewById(R.id.contentAds);
                if (shimmerFrameLayout != null)
                    shimmerFrameLayout.startShimmer();
                if (Utils.isDisplayAds) {
                    AdLoader.Builder builder = new AdLoader.Builder(mActivity, mActivity.getString(R.string.admob_native_medium))
                            .forNativeAd(new NativeAd.OnNativeAdLoadedListener() {
                                @Override
                                public void onNativeAdLoaded(@NonNull NativeAd nativeAd) {
                                    container_ads_native.removeAllViews();
                                    container_ads_native.addView(nativeAdViewLayout);
                                    if (contentAds != null)
                                        contentAds.setVisibility(View.VISIBLE);
                                    if (nativeAdViewLayout != null && nativeAd != null)
                                        Utils.populateUnifiedNativeAdView(nativeAd, nativeAdViewLayout);
                                }
                            });


                    VideoOptions videoOptions =
                            new VideoOptions.Builder().setStartMuted(true).build();

                    NativeAdOptions adOptions =
                            new NativeAdOptions.Builder()
                                    .setVideoOptions(videoOptions)
                                    .setMediaAspectRatio(NATIVE_MEDIA_ASPECT_RATIO_ANY)
                                    .setAdChoicesPlacement(ADCHOICES_TOP_RIGHT).build();

                    builder.withNativeAdOptions(adOptions);

                    AdLoader adLoader = builder.withAdListener(new AdListener() {
                        @Override
                        public void onAdFailedToLoad(LoadAdError loadAdError) {
                           new Handler().postDelayed(new Runnable() {
                               @Override
                               public void run() {
                                   AppLovinHelp.getInstance().loadNativeActivity(mActivity,isMedium);
                               }
                           },new Random().nextInt(200) + 300);
                        }
                    }).build();
                    adLoader.loadAd(new AdRequest.Builder().build());
                }
            }
        }
    }
    public void loadNativeFragment(final Activity mActivity, final View rootView){
        loadNativeFragment(mActivity,rootView,false);
    }
    public void loadNativeFragment(final Activity mActivity, final View rootView, boolean isMedium) {
        if (mActivity == null) {
            return;
        }
        if(!SharedPreferencesUtils.getBoolean("load_floor")){
            AppLovinHelp.getInstance().loadNativeFragment(mActivity,rootView,isMedium);
            return;
        }
        if (SharedPreferencesUtils.isPro() || !Utils.isDisplayAds) {
            RelativeLayout container_ads_native = rootView.findViewById(R.id.container_ads_native);
            if (container_ads_native != null) {
                container_ads_native.setVisibility(View.GONE);
            }
            if (container_ads_native.getParent() != null) {
                ((ViewGroup) container_ads_native.getParent()).setVisibility(View.GONE);
            }
        }
        if (!SharedPreferencesUtils.isPro() && Utils.isDisplayAds) {
            RelativeLayout container_ads_native = rootView.findViewById(R.id.container_ads_native);
            if (container_ads_native != null) {
                ShimmerFrameLayout shimmerFrameLayout = container_ads_native.findViewById(R.id.shimmerLayout);
                View adsLayout = mActivity.getLayoutInflater().inflate(isMedium ? R.layout.native_ads_admob_medium_top : R.layout.native_ads_admob, null);
                NativeAdView nativeAdViewLayout = adsLayout.findViewById(R.id.nativeAdView);
                LinearLayout contentAds = container_ads_native.findViewById(R.id.contentAds);
                if (shimmerFrameLayout != null)
                    shimmerFrameLayout.startShimmer();
                if (Utils.isDisplayAds) {
                    AdLoader.Builder builder = new AdLoader.Builder(mActivity, mActivity.getString(R.string.admob_native_high))
                            .forNativeAd(new NativeAd.OnNativeAdLoadedListener() {
                                @Override
                                public void onNativeAdLoaded(@NonNull NativeAd nativeAd) {
                                    container_ads_native.removeAllViews();
                                    container_ads_native.addView(nativeAdViewLayout);
                                    if (contentAds != null)
                                        contentAds.setVisibility(View.VISIBLE);
                                    if (nativeAdViewLayout != null && nativeAd != null)
                                        Utils.populateUnifiedNativeAdView(nativeAd, nativeAdViewLayout);
                                    if (shimmerFrameLayout != null)
                                        shimmerFrameLayout.stopShimmer();
                                    if (shimmerFrameLayout != null)
                                        shimmerFrameLayout.setVisibility(View.GONE);
                                }
                            });


                    VideoOptions videoOptions =
                            new VideoOptions.Builder().setStartMuted(true).build();

                    NativeAdOptions adOptions =
                            new NativeAdOptions.Builder()
                                    .setVideoOptions(videoOptions)
                                    .setMediaAspectRatio(NATIVE_MEDIA_ASPECT_RATIO_ANY)
                                    .setAdChoicesPlacement(ADCHOICES_TOP_RIGHT).build();

                    builder.withNativeAdOptions(adOptions);

                    AdLoader adLoader = builder.withAdListener(new AdListener() {
                        @Override
                        public void onAdFailedToLoad(LoadAdError loadAdError) {
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    loadNativeFragmentMedium(mActivity,rootView,isMedium);
                                }
                            },new Random().nextInt(200) + 300);
                        }
                    }).build();
                    adLoader.loadAd(new AdRequest.Builder().build());
                }
            }
        }
    }

    private void loadNativeFragmentMedium(final Activity mActivity, final View rootView, boolean isMedium) {
        if (mActivity == null) {
            return;
        }
        if (SharedPreferencesUtils.isPro() || !Utils.isDisplayAds) {
            RelativeLayout container_ads_native = rootView.findViewById(R.id.container_ads_native);
            if (container_ads_native != null) {
                container_ads_native.setVisibility(View.GONE);
            }
            if (container_ads_native.getParent() != null) {
                ((ViewGroup) container_ads_native.getParent()).setVisibility(View.GONE);
            }
        }
        if (!SharedPreferencesUtils.isPro() && Utils.isDisplayAds) {
            RelativeLayout container_ads_native = rootView.findViewById(R.id.container_ads_native);
            if (container_ads_native != null) {
                ShimmerFrameLayout shimmerFrameLayout = container_ads_native.findViewById(R.id.shimmerLayout);
                View adsLayout = mActivity.getLayoutInflater().inflate(isMedium ? R.layout.native_ads_admob_medium_top : R.layout.native_ads_admob, null);
                NativeAdView nativeAdViewLayout = adsLayout.findViewById(R.id.nativeAdView);
                LinearLayout contentAds = container_ads_native.findViewById(R.id.contentAds);
                if (shimmerFrameLayout != null)
                    shimmerFrameLayout.startShimmer();
                if (Utils.isDisplayAds) {
                    AdLoader.Builder builder = new AdLoader.Builder(mActivity, mActivity.getString(R.string.admob_native_medium))
                            .forNativeAd(new NativeAd.OnNativeAdLoadedListener() {
                                @Override
                                public void onNativeAdLoaded(@NonNull NativeAd nativeAd) {
                                    container_ads_native.removeAllViews();
                                    container_ads_native.addView(nativeAdViewLayout);
                                    if (contentAds != null)
                                        contentAds.setVisibility(View.VISIBLE);
                                    if (nativeAdViewLayout != null && nativeAd != null)
                                        Utils.populateUnifiedNativeAdView(nativeAd, nativeAdViewLayout);
                                    if (shimmerFrameLayout != null)
                                        shimmerFrameLayout.stopShimmer();
                                    if (shimmerFrameLayout != null)
                                        shimmerFrameLayout.setVisibility(View.GONE);
                                }
                            });


                    VideoOptions videoOptions =
                            new VideoOptions.Builder().setStartMuted(true).build();

                    NativeAdOptions adOptions =
                            new NativeAdOptions.Builder()
                                    .setVideoOptions(videoOptions)
                                    .setMediaAspectRatio(NATIVE_MEDIA_ASPECT_RATIO_ANY)
                                    .setAdChoicesPlacement(ADCHOICES_TOP_RIGHT).build();

                    builder.withNativeAdOptions(adOptions);

                    AdLoader adLoader = builder.withAdListener(new AdListener() {
                        @Override
                        public void onAdFailedToLoad(LoadAdError loadAdError) {
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    AppLovinHelp.getInstance().loadNativeFragment(mActivity,rootView,isMedium);
                                }
                            },new Random().nextInt(200) + 300);
                        }
                    }).build();
                    adLoader.loadAd(new AdRequest.Builder().build());
                }
            }
        }
    }
}