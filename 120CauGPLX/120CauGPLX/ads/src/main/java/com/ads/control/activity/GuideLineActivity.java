package com.ads.control.activity;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.ads.control.R;
import com.ads.control.admob.AdmobHelp;
import com.ads.control.admob.AdsOpenManager;
import com.rey.material.widget.TextView;
import com.tbuonomo.viewpagerdotsindicator.DotsIndicator;

public class GuideLineActivity extends BaseAdsActivity {
    private TextView tvNext;
    private DotsIndicator indicator;
    private ViewPager viewpager;

    private int pos = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_guide_line);
        AdmobHelp.getInstance().loadNativeActivity(this,true);
        tvNext = findViewById(R.id.tvNext);
        indicator = findViewById(R.id.indicator);
        viewpager = findViewById(R.id.viewpager);

        Adapter adapter = new Adapter(GuideLineActivity.this);

        viewpager.setAdapter(adapter);
        indicator.attachTo(viewpager);
        viewpager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (position == 3) {
                    tvNext.setText(getString(R.string.get_start));
                } else {
                    tvNext.setText(getString(R.string.next));
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        tvNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(tvNext.getText().toString().equals(getString(R.string.next))){
                    pos++;
                    viewpager.post(new Runnable() {
                        @Override
                        public void run() {
                            viewpager.setCurrentItem(pos,true);
                        }
                    });
                }else {
                    gotoMain();
                }
            }
        });
    }

    public void gotoMain(){
        if(((BaseAdsActivity) (AdsOpenManager.getInstance().getCurrentActivity())).getDismissAdActivity()!=null){
            ((BaseAdsActivity) (AdsOpenManager.getInstance().getCurrentActivity())).getDismissAdActivity().onDissmiss();
        }
    }

    class Adapter extends PagerAdapter {

        private Context mContext;

        public Adapter(Context context) {
            mContext = context;
        }

        @Override
        public int getCount() {
            return 4;
        }

        @Override
        public Object instantiateItem(ViewGroup collection, int position) {
            View view = (View) LayoutInflater.from(mContext).inflate(R.layout.fragment_guide_line, collection, false);
            collection.addView(view);

            ImageView imvIntro = view.findViewById(R.id.imv_intro);
            TextView tvDetail = view.findViewById(R.id.tv_detail);

            if (position == 0) {
                imvIntro.setImageResource(R.drawable.intro_1);
                tvDetail.setText(getString(R.string.intro_1));
            } else if (position == 1) {
                imvIntro.setImageResource(R.drawable.intro_2);
                tvDetail.setText(getString(R.string.intro_2));
            } else if (position == 2) {
                imvIntro.setImageResource(R.drawable.intro_3);
                tvDetail.setText(getString(R.string.intro_3));
            } else {
                imvIntro.setImageResource(R.drawable.intro_4);
                tvDetail.setText(getString(R.string.intro_4));
            }
            return view;
        }

        @Override
        public void destroyItem(ViewGroup collection, int position, Object view) {
            collection.removeView((View) view);
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return "";
        }

    }
}