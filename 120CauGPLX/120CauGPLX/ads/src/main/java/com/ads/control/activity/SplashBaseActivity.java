package com.ads.control.activity;

import com.ads.control.admob.AdsOpenManager;
import com.google.android.ump.ConsentForm;
import com.google.android.ump.ConsentInformation;
import com.google.android.ump.ConsentRequestParameters;
import com.google.android.ump.UserMessagingPlatform;

import java.util.concurrent.atomic.AtomicBoolean;

public abstract class SplashBaseActivity extends BaseAdsActivity {
    public abstract void animSplash();

    /**
     * nhớ call trong on create splash
     */
    public void onCreateSplash() {
        // Set tag for under age of consent. false means users are not under age
        // of consent.
        initializeMobileAdsSdk();
        animSplash();
    }

    private void initializeMobileAdsSdk() {
        AdsOpenManager.getInstance().initializeMobileAdsSdk();
    }
}