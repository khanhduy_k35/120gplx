package com.ads.control.sound;

interface FileList {
    int getFileCount();
    int getFileId(int note);
}
