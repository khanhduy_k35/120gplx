package com.ads.control.admob;

public interface AdRewardListener {
    void onAdRewardSuccess();
    void onAdRewardFailed();
}
