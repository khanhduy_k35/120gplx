package com.ads.control.admob;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.DefaultLifecycleObserver;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.ProcessLifecycleOwner;

import com.ads.control.AdsApplication;
import com.ads.control.BuildConfig;
import com.ads.control.R;
import com.ads.control.activity.BaseAdsActivity;
import com.ads.control.utils.SharedPreferencesUtils;
import com.ads.control.utils.Utils;
import com.applovin.mediation.MaxAd;
import com.applovin.mediation.MaxAdListener;
import com.applovin.mediation.MaxError;
import com.applovin.mediation.ads.MaxAppOpenAd;
import com.applovin.sdk.AppLovinMediationProvider;
import com.applovin.sdk.AppLovinSdk;
import com.google.android.gms.ads.AdError;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.FullScreenContentCallback;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.RequestConfiguration;
import com.google.android.gms.ads.appopen.AppOpenAd;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Random;

public class AdsOpenManager implements Application.ActivityLifecycleCallbacks, DefaultLifecycleObserver {
    private static AdsOpenManager instance;
    private Activity currentActivity;
    private final AdsApplication myApplication;
    private AppOpenAdManager appOpenManager;
    private boolean initApplovinSuccess = false;


    public static AdsOpenManager getInstance() {
        if(instance!=null)
            return instance;
        else return null;
    }


    public AdsOpenManager(AdsApplication myApplication) {
        this.myApplication = myApplication;
        this.myApplication.registerActivityLifecycleCallbacks(this);
        instance = this;
        ProcessLifecycleOwner.get().getLifecycle().addObserver(this);
    }

    @Override
    public void onStart(@NonNull LifecycleOwner owner) {
        DefaultLifecycleObserver.super.onStart(owner);
        if(currentActivity!=null && !(currentActivity instanceof BaseAdsActivity)){
            return;
        }
        if(currentActivity!=null && currentActivity instanceof BaseAdsActivity && appOpenManager!=null && !currentActivity.getClass().toString().contains("Splash") && !currentActivity.getClass().toString().contains("AdApperActivity")) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    appOpenManager.showAdIfAvailable(currentActivity, new OnShowAdCompleteListener() {
                        @Override
                        public void onFailed() {
                            if(currentActivity instanceof BaseAdsActivity) {
                                AppLovinHelp.getInstance().openAppHidden((AppCompatActivity) currentActivity);
                                AdmobHelp.getInstance().openAppHidden((AppCompatActivity) currentActivity);
                            }
                        }

                        @Override
                        public void onSuccess() {
                            if(currentActivity instanceof BaseAdsActivity) {
                                AppLovinHelp.getInstance().openAppHidden((AppCompatActivity) currentActivity);
                                AdmobHelp.getInstance().openAppHidden((AppCompatActivity) currentActivity);
                            }
                        }

                        @Override
                        public void onShow() {
                            if(currentActivity instanceof BaseAdsActivity) {
                                AppLovinHelp.getInstance().openAppShow((AppCompatActivity) currentActivity);
                                AdmobHelp.getInstance().openAppShow((AppCompatActivity) currentActivity);
                            }
                        }
                    });
                }
            },10);
        }
    }

    public void initializeMobileAdsSdk(){
        if(currentActivity.getClass().toString().contains("Splash")){
            String[] deviceID = myApplication.getResources().getStringArray(R.array.device_id);
            //khởi tạo admob
            MobileAds.initialize(
                    myApplication,
                    new OnInitializationCompleteListener() {
                        @Override
                        public void onInitializationComplete(InitializationStatus initializationStatus) {
                            if(BuildConfig.DEBUG){
                                List<String> testDeviceIds = Arrays.asList(deviceID);
                                RequestConfiguration configuration =
                                        new RequestConfiguration.Builder().setTestDeviceIds(testDeviceIds).build();
                                MobileAds.setRequestConfiguration(configuration);
                            }
                            //load App open
                            initAppOpen();
                        }
                    });
            //khởi tạo applovin backup đấu giá trực tiếp Admob
            AppLovinSdk.getInstance( myApplication ).setMediationProvider(AppLovinMediationProvider.MAX );
            AppLovinSdk.getInstance( myApplication ).initializeSdk( config -> {
                initApplovinSuccess  =true;
            });
        }
    }

    public void initAppOpen(){
        appOpenManager = new AppOpenAdManager(myApplication);
        if(currentActivity!=null) {
            AdmobHelp.getInstance().init(currentActivity);
        }
    }


    public void showAdIfAvailable(
            @NonNull final Activity activity,
            @NonNull OnShowAdCompleteListener onShowAdCompleteListener){
        if (appOpenManager!=null){
            appOpenManager.showAdIfAvailable(activity,onShowAdCompleteListener);
        }else{
            onShowAdCompleteListener.onFailed();
        }
    }

    @Override
    public void onActivityCreated(@NonNull Activity activity, @Nullable Bundle savedInstanceState) {
        currentActivity = activity;
    }

    @Override
    public void onActivityStarted(@NonNull Activity activity) {

    }

    @Override
    public void onActivityResumed(@NonNull Activity activity) {
        currentActivity = activity;
    }

    @Override
    public void onActivityPaused(@NonNull Activity activity) {

    }

    @Override
    public void onActivityStopped(@NonNull Activity activity) {

    }

    @Override
    public void onActivitySaveInstanceState(@NonNull Activity activity, @NonNull Bundle outState) {

    }

    @Override
    public void onActivityDestroyed(@NonNull Activity activity) {

    }

    public interface OnShowAdCompleteListener {
        void onFailed(); /*Không có quảng cáo để hiển thị*/

        void onSuccess(); /**Quảng cáo hiển thị thành công và cần tắt đi**/

        void onShow(); /**Quảng cáo đang hiển thị (cần ẩn các quảng cáo như banner hay native đi)**/
    }

    private class AppOpenAdManager {
        private AppOpenAd appOpenAd = null;
        private MaxAppOpenAd appOpenAdLovin = null;
        private boolean isLoadingAd = false;
        private boolean isShowingAd = false;
        private Context context;
        private long loadTime;
        int typeAds = -1; //1 high,2 medium,3 low
        private OnShowAdCompleteListener onShowAdCompleteListener;
        /** Constructor. */
        public AppOpenAdManager(Context context) {
            this.context = context;
            //load trước id hight
            loadAd(context);
        }

        private void loadAd(Context context) {
            // Do not load ad if there is an unused ad or one is already loading.
            if (isLoadingAd || isAdAvailableAdmob()) {
                return;
            }
            if(!SharedPreferencesUtils.getBoolean("load_floor")){
                isLoadingAd = false;
                //load failed thì load applovin
                if (!isAdAvailableApplovin()) {
                    initApplovin(context);
                }
                return;
            }
            isLoadingAd = true;
            AdRequest request = new AdRequest.Builder().build();
            AppOpenAd.load(
                    context, context.getString(R.string.admob_open_app_high), request,
                    new AppOpenAd.AppOpenAdLoadCallback() {
                        @Override
                        public void onAdLoaded(AppOpenAd ad) {
                            // Called when an app open ad has loaded.
                            appOpenAd = ad;
                            isLoadingAd = false;
                            loadTime = (new Date()).getTime();
                            typeAds = 1;
                        }

                        @Override
                        public void onAdFailedToLoad(LoadAdError loadAdError) {
                            // Called when an app open ad has failed to load.
                            isLoadingAd = false;
                            //load failed thì load medium
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    loadAdsMedium(context);
                                }
                            }, new Random().nextInt(500) + 500);
                        }
                    });
        }

        private void loadAdsMedium(Context context){
            if (isLoadingAd || isAdAvailableAdmob()) {
                return;
            }

            isLoadingAd = true;
            AdRequest request = new AdRequest.Builder().build();
            AppOpenAd.load(
                    context, context.getString(R.string.admob_open_app_medium), request,
                    new AppOpenAd.AppOpenAdLoadCallback() {
                        @Override
                        public void onAdLoaded(AppOpenAd ad) {
                            // Called when an app open ad has loaded.
                            appOpenAd = ad;
                            isLoadingAd = false;
                            loadTime = (new Date()).getTime();
                            typeAds = 2;
                        }

                        @Override
                        public void onAdFailedToLoad(LoadAdError loadAdError) {
                            // Called when an app open ad has failed to load.
                            isLoadingAd = false;
                            //load failed thì load applovin
                            if (!isAdAvailableApplovin()) {
                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        initApplovin(context);
                                    }
                                }, new Random().nextInt(500) + 500);
                            }
                        }
                    });
        }

        private void initApplovin(Context context){
            if(initApplovinSuccess){
                loadOpenApplovin(context);
            }else{
                AppLovinSdk.getInstance( myApplication ).setMediationProvider(AppLovinMediationProvider.MAX );
                AppLovinSdk.getInstance( myApplication ).initializeSdk( config -> {
                    initApplovinSuccess  =true;
                    loadOpenApplovin(context);
                });
            }
        }

        public void loadOpenApplovin(Context context){
            if (isLoadingAd || isAdAvailableApplovin()) {
                return;
            }
            isLoadingAd = true;
            appOpenAdLovin = new MaxAppOpenAd(context.getString(R.string.applovin_open_ads),context);
            appOpenAdLovin.loadAd();
            appOpenAdLovin.setListener(new MaxAdListener() {
                @Override
                public void onAdLoaded(MaxAd maxAd) {
                    isLoadingAd = false;
                    typeAds = 3;
                }

                @Override
                public void onAdDisplayed(MaxAd maxAd) {

                }

                @Override
                public void onAdHidden(MaxAd maxAd) {
                    appOpenAdLovin.loadAd();
                    isShowingAd = false;
                    if(onShowAdCompleteListener!=null){
                        onShowAdCompleteListener.onSuccess();
                    }
                }

                @Override
                public void onAdClicked(MaxAd maxAd) {

                }

                @Override
                public void onAdLoadFailed(String s, MaxError maxError) {
                    isLoadingAd = false;
                }

                @Override
                public void onAdDisplayFailed(MaxAd maxAd, MaxError maxError) {
                    isShowingAd = false;
                    if(onShowAdCompleteListener!=null){
                        onShowAdCompleteListener.onFailed();
                    }
                }
            });
        }

        /** Check if ad exists and can be shown. */
        private boolean isAdAvailableAdmob() {
            if(appOpenAd != null && wasLoadTimeLessThanNHoursAgo(4))
                return true;
            else return false;
        }

        private boolean wasLoadTimeLessThanNHoursAgo(long numHours) {
            long dateDifference = (new Date()).getTime() - this.loadTime;
            long numMilliSecondsPerHour = 3600000;
            return (dateDifference < (numMilliSecondsPerHour * numHours));
        }


        private boolean isAdAvailableApplovin() {
            if (appOpenAdLovin == null || !AppLovinSdk.getInstance( context ).isInitialized() ) return false;

            if(appOpenAdLovin.isReady())
                return true;
            else return false;
        }

        public void showAdIfAvailable(
                @NonNull final Activity activity,
                @NonNull OnShowAdCompleteListener onShow) {
            this.onShowAdCompleteListener = onShow;
            // If the app open ad is already showing, do not show the ad again.
            if (isShowingAd || isLoadingAd) {
                onShowAdCompleteListener.onFailed();
                return;
            }
            if(SharedPreferencesUtils.isPro()) {
                onShowAdCompleteListener.onFailed();
                return;
            }
            if(!Utils.isDisplayAds){
                onShowAdCompleteListener.onFailed();
                return;
            }

            // If the app open ad is not available yet, invoke the callback then load the ad.
            if (!isAdAvailableAdmob() && !isAdAvailableApplovin()) {
                onShowAdCompleteListener.onFailed();
                loadAd(activity);
                return;
            }

            if(isAdAvailableAdmob()) {
                appOpenAd.setFullScreenContentCallback(new FullScreenContentCallback() {
                    @Override
                    public void onAdClicked() {
                        super.onAdClicked();
                    }

                    @Override
                    public void onAdDismissedFullScreenContent() {
                        super.onAdDismissedFullScreenContent();
                        isShowingAd = false;
                        appOpenAd = null;
                        if (onShowAdCompleteListener != null) {
                            onShowAdCompleteListener.onSuccess();
                        }
                        if(typeAds == 1) {
                            loadAd(activity);
                        }else if(typeAds == 2){
                            loadAdsMedium(activity);
                        }
                    }

                    @Override
                    public void onAdFailedToShowFullScreenContent(@NonNull AdError adError) {
                        super.onAdFailedToShowFullScreenContent(adError);
                        isShowingAd = false;
                        appOpenAd = null;
                        if (onShowAdCompleteListener != null) {
                            onShowAdCompleteListener.onFailed();
                        }
                        if(typeAds == 1) {
                            loadAd(activity);
                        }else if(typeAds == 2){
                            loadAdsMedium(activity);
                        }
                    }

                    @Override
                    public void onAdImpression() {
                        super.onAdImpression();
                    }

                    @Override
                    public void onAdShowedFullScreenContent() {
                        super.onAdShowedFullScreenContent();
                    }
                });

                isShowingAd = true;
                appOpenAd.show(activity);
                if (onShowAdCompleteListener != null) {
                    onShowAdCompleteListener.onShow();
                }
            }else if(isAdAvailableApplovin()){
                isShowingAd = true;
                appOpenAdLovin.showAd();
                if(onShowAdCompleteListener!=null){
                    onShowAdCompleteListener.onShow();
                }
            }
        }
    }

    public Activity getCurrentActivity() {
        return currentActivity;
    }

    public void setCurrentActivity(Activity currentActivity) {
        this.currentActivity = currentActivity;
    }
}
