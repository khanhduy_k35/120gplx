package com.ads.control.google_iab.enums;

public enum SkuProductType {
    CONSUMABLE,
    NON_CONSUMABLE,
    SUBSCRIPTION
}
