package com.ads.control.activity;

import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.ads.control.R;
import com.ads.control.admob.AdmobHelp;
import com.ads.control.utils.TimerCustom;


public class AdApperActivity extends BaseAdsActivity {
    private TimerPlay timerPlay = null;
    private String type;

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        type = getIntent().getStringExtra("type");
        setContentView(R.layout.layout_ads_appear);
        Window window = this.getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | 0);
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(Color.TRANSPARENT);
        }
    }

    public void onDestroy() {
        super.onDestroy();
        if (timerPlay != null) {
            timerPlay.cancel();
            timerPlay = null;
        }
        Runtime.getRuntime().gc();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (timerPlay != null) {
            timerPlay.resume();
        }else {
            timerPlay = new TimerPlay(500, 1500);
            timerPlay.start();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (timerPlay != null) {
            timerPlay.pause();
        }
    }


    @Override
    public void onBackPressed() {
    }

    private class TimerPlay extends TimerCustom {
        public TimerPlay(long interval, long duration) {
            super(interval, duration);
        }

        @Override
        protected void onTick() {
        }

        @Override
        protected void onFinish() {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if(type!=null && type.equals("inter")) {
                        AdmobHelp.getInstance().showInterstitialAdNonLoading(AdmobHelp.getInstance().getAdCloseListener());
                        finish();
                    }
                }
            });
        }
    }

}

