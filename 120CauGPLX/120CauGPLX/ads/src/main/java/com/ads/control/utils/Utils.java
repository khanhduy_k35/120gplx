package com.ads.control.utils;

import static android.util.TypedValue.COMPLEX_UNIT_DIP;
import static android.util.TypedValue.applyDimension;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.PorterDuff;
import android.graphics.drawable.LayerDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.Display;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.ads.control.R;
import com.google.android.gms.ads.VideoController;
import com.google.android.gms.ads.nativead.NativeAdView;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.UUID;

public class Utils {
    public static boolean isDisplayAds = true;
    public static int TIME_DISTANCE_DISPLAYADS = 60;
    public static final String ID_STATE = "ID_STATE";

    public static boolean checkConditionalShowAds(Context context){
        return true;
    }

    public static int getColorFromAttr(Context context,int resId){
        TypedValue typedValue = new TypedValue();
        Resources.Theme theme = context.getTheme();
        theme.resolveAttribute(resId, typedValue, true);
        int color = typedValue.data;
        return color;
    }

    public static float dp2px(Resources resources, float dp) {
        final float scale = resources.getDisplayMetrics().density;
        return  dp * scale + 0.5f;
    }

    public static float sp2px(Resources resources, float sp){
        final float scale = resources.getDisplayMetrics().scaledDensity;
        return sp * scale;
    }

    public static String capitalizeString(String string) {
        char[] chars = string.toLowerCase().toCharArray();
        boolean found = false;
        for (int i = 0; i < chars.length; i++) {
            if (!found && Character.isLetter(chars[i])) {
                chars[i] = Character.toUpperCase(chars[i]);
                found = true;
            } else if (Character.isWhitespace(chars[i]) || chars[i]=='.' || chars[i]=='\'') { // You can add other chars here
                found = false;
            }
        }
        return String.valueOf(chars);
    }

    public static int convertDpToPx(Context context, float dp) {
        return (int) applyDimension(COMPLEX_UNIT_DIP, dp, context.getResources().getDisplayMetrics());
    }

    public static int convertPixelsToDp(int px, Context context){
        return (int)(px / ((float) context.getResources().getDisplayMetrics().densityDpi / DisplayMetrics.DENSITY_DEFAULT));
    }

    public static float pixelsToSp(Context context, float px) {
        float scaledDensity = context.getResources().getDisplayMetrics().scaledDensity;
        return px/scaledDensity;
    }

    public static int getWidthScreen(Context context) {
        Display display = ((AppCompatActivity)context).getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        return width;
    }

    public static void populateUnifiedNativeAdView(com.google.android.gms.ads.nativead.NativeAd nativeAd, NativeAdView adView) {
        // Set the media view.
        if(adView.findViewById(R.id.ad_media)!=null) {
            adView.setMediaView((com.google.android.gms.ads.nativead.MediaView) adView.findViewById(R.id.ad_media));
        }

        // Set other ad assets.
        adView.setHeadlineView(adView.findViewById(R.id.ad_headline));
        adView.setBodyView(adView.findViewById(R.id.ad_body));
        adView.setCallToActionView(adView.findViewById(R.id.ad_call_to_action));
        adView.setIconView(adView.findViewById(R.id.ad_app_icon));
        adView.setPriceView(adView.findViewById(R.id.ad_price));
        adView.setStarRatingView(adView.findViewById(R.id.ad_stars));
        adView.setStoreView(adView.findViewById(R.id.ad_store));
        adView.setAdvertiserView(adView.findViewById(R.id.ad_advertiser));

        RatingBar ratingBar = adView.findViewById(R.id.ad_stars);
        if(ratingBar!=null) {
            LayerDrawable stars = (LayerDrawable) ratingBar.getProgressDrawable();
            if(stars!=null) {
                stars.getDrawable(2).setColorFilter(Color.parseColor("#FAA618"), PorterDuff.Mode.SRC_ATOP);
            }
        }

        // The headline and mediaContent are guaranteed to be in every UnifiedNativeAd.
        if(adView.getHeadlineView()!=null && nativeAd.getHeadline()!=null)
            ((TextView) adView.getHeadlineView()).setText(nativeAd.getHeadline());

        if(adView.getMediaView()!=null) {
            if(adView.getMediaView()!=null)
                adView.getMediaView().setMediaContent(nativeAd.getMediaContent());
            if(adView.getMediaView()!=null)
                adView.getMediaView().setImageScaleType(ImageView.ScaleType.CENTER_INSIDE);
        }
        // These assets aren't guaranteed to be in every UnifiedNativeAd, so it's important to
        // check before trying to display them.
        if (nativeAd.getBody() == null) {
            if(adView.getBodyView()!=null)
                adView.getBodyView().setVisibility(View.INVISIBLE);
        } else {
            if(adView.getBodyView()!=null)
                adView.getBodyView().setVisibility(View.VISIBLE);
            if(adView.getBodyView()!=null)
                ((TextView) adView.getBodyView()).setText(nativeAd.getBody());
        }

        if (nativeAd.getCallToAction() == null) {
            if(adView.getCallToActionView()!=null)
                adView.getCallToActionView().setVisibility(View.INVISIBLE);
        } else {
            if(adView.getCallToActionView()!=null)
                adView.getCallToActionView().setVisibility(View.VISIBLE);
            if(adView.getCallToActionView()!=null)
                ((Button) adView.getCallToActionView()).setText(nativeAd.getCallToAction());
        }

        if (nativeAd.getIcon() == null) {
            if(adView.getIconView()!=null)
                adView.getIconView().setVisibility(View.GONE);
        } else {
            if(adView.getIconView()!=null)
                ((ImageView) adView.getIconView()).setImageDrawable(nativeAd.getIcon().getDrawable());
            if(adView.getIconView()!=null)
                adView.getIconView().setVisibility(View.VISIBLE);
        }

        if (nativeAd.getPrice() == null) {
            if(adView.getPriceView()!=null)
                adView.getPriceView().setVisibility(View.INVISIBLE);
        } else {
            if(adView.getPriceView()!=null)
                adView.getPriceView().setVisibility(View.VISIBLE);
            if(adView.getPriceView()!=null)
                ((TextView) adView.getPriceView()).setText(nativeAd.getPrice());
        }

        if (nativeAd.getStore() == null) {
            if(adView.getStoreView()!=null)
                adView.getStoreView().setVisibility(View.INVISIBLE);
        } else {
            if(adView.getStoreView()!=null)
                adView.getStoreView().setVisibility(View.VISIBLE);
            if(adView.getStoreView()!=null)
                ((TextView) adView.getStoreView()).setText(nativeAd.getStore());
        }

        if (nativeAd.getStarRating() == null) {
            if(adView.getStarRatingView()!=null)
                adView.getStarRatingView().setVisibility(View.INVISIBLE);
        } else {
            if(adView.getStarRatingView()!=null)
                ((RatingBar) adView.getStarRatingView()).setRating(nativeAd.getStarRating().floatValue());
            if(adView.getStarRatingView()!=null)
                adView.getStarRatingView().setVisibility(View.VISIBLE);
        }

        if (nativeAd.getAdvertiser() == null) {
            if(adView.getAdvertiserView()!=null)
                adView.getAdvertiserView().setVisibility(View.INVISIBLE);
        } else {
            if(adView.getAdvertiserView()!=null)
                ((TextView) adView.getAdvertiserView()).setText(nativeAd.getAdvertiser());
            if(adView.getAdvertiserView()!=null)
                adView.getAdvertiserView().setVisibility(View.VISIBLE);
        }

        // This method tells the Google Mobile Ads SDK that you have finished populating your
        // native ad view with this native ad.
        if(adView!=null && nativeAd !=null)
            adView.setNativeAd(nativeAd);

        // Get the video controller for the ad. One will always be provided, even if the ad doesn't
        // have a video asset.
        if(nativeAd!=null && nativeAd.getMediaContent()!=null) {
            VideoController vc = nativeAd.getMediaContent().getVideoController();
            if(vc!=null) {
                // Updates the UI to say whether or not this ad has a video asset.
                if (vc.hasVideoContent()) {
                    // Create a new VideoLifecycleCallbacks object and pass it to the VideoController. The
                    // VideoController will call methods on this object when events occur in the video
                    // lifecycle.
                    vc.setVideoLifecycleCallbacks(new VideoController.VideoLifecycleCallbacks() {
                        @Override
                        public void onVideoEnd() {
                            // Publishers should allow native ads to complete video playback before
                            // refreshing or replacing them with another ad in the same UI location.
                            super.onVideoEnd();
                        }
                    });
                } else {

                }
            }
        }
    }

    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public static String getCurrentTime(){
        Date c = Calendar.getInstance().getTime();
        System.out.println("Current time => " + c);

        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        String currentDate = df.format(c);
        return currentDate;
    }

    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        View view = activity.getCurrentFocus();
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }
}
