package com.ads.control.activity;

import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.ads.control.R;
import com.ads.control.admob.AdsOpenManager;
import com.ads.control.utils.SharedPreferencesUtils;
import com.rey.material.widget.ImageView;
import com.rey.material.widget.RelativeLayout;

public class InAppActivity extends BaseAdsActivity {
    private ImageView btn_close;
    private RelativeLayout rltTrial,subMonth,rltOneTime;
    private Animation animClose;
    private TextView tvTitle12,tvTitle6,tvTitle5,tvTitle9,tvTitle2,tv_auto_renew;
    private TextView tvSubMonth4,tvSubMonth1,tvSubMonth2;
    private boolean anim = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestFullScreen();
        setContentView(R.layout.acitivty_inap);
        btn_close = findViewById(R.id.btn_close);
        rltTrial = findViewById(R.id.rltTrial);
        subMonth = findViewById(R.id.subMonth);
        rltOneTime = findViewById(R.id.rltOneTime);
        tvTitle6 = findViewById(R.id.tvTitle6);
        tvTitle5 = findViewById(R.id.tvTitle5);
        tvTitle9 = findViewById(R.id.tvTitle9);
        tvTitle2 = findViewById(R.id.tvTitle2);
        tvTitle12 = findViewById(R.id.tvTitle11);
        tvSubMonth4 = findViewById(R.id.tvSubMonth4);
        tvSubMonth1 = findViewById(R.id.tvSubMonth1);
        tvSubMonth2 = findViewById(R.id.tvSubMonth2);
        tv_auto_renew = findViewById(R.id.tv_auto_renew);
        String pro_feature_1 = getString(R.string.pro_feature_1);
        String pro_feature_2 = getString(R.string.pro_feature_2);
        String pro_feature_3 = getString(R.string.pro_feature_3);
        String pro_feature_4 = getString(R.string.pro_feature_4);
        String pro_feature_5 = getString(R.string.pro_feature_5);
        int maxlength = 0;
        if(maxlength < pro_feature_1.length()){
            maxlength = pro_feature_1.length();
        }
        if(maxlength < pro_feature_2.length()){
            maxlength = pro_feature_2.length();
        }
        if(maxlength < pro_feature_3.length()){
            maxlength = pro_feature_3.length();
        }
        if(maxlength < pro_feature_4.length()){
            maxlength = pro_feature_4.length();
        }
        if(maxlength < pro_feature_5.length()){
            maxlength = pro_feature_5.length();
        }
        if(maxlength == pro_feature_1.length()){
            tvTitle6.setText(R.string.pro_feature_1);
            tvTitle5.setText(R.string.pro_feature_1);
            tvTitle9.setText(R.string.pro_feature_1);
            tvTitle2.setText(R.string.pro_feature_1);
            tvTitle12.setText(R.string.pro_feature_1);
        }
        else if(maxlength == pro_feature_2.length()){
            tvTitle6.setText(R.string.pro_feature_2);
            tvTitle5.setText(R.string.pro_feature_2);
            tvTitle9.setText(R.string.pro_feature_2);
            tvTitle2.setText(R.string.pro_feature_2);
            tvTitle12.setText(R.string.pro_feature_2);
        }else if(maxlength == pro_feature_3.length()){
            tvTitle6.setText(R.string.pro_feature_3);
            tvTitle5.setText(R.string.pro_feature_3);
            tvTitle9.setText(R.string.pro_feature_3);
            tvTitle2.setText(R.string.pro_feature_3);
            tvTitle12.setText(R.string.pro_feature_3);
        }else if(maxlength == pro_feature_4.length()){
            tvTitle6.setText(R.string.pro_feature_4);
            tvTitle5.setText(R.string.pro_feature_4);
            tvTitle9.setText(R.string.pro_feature_4);
            tvTitle2.setText(R.string.pro_feature_4);
            tvTitle12.setText(R.string.pro_feature_4);
        }else if(maxlength == pro_feature_5.length()){
            tvTitle6.setText(R.string.pro_feature_5);
            tvTitle5.setText(R.string.pro_feature_5);
            tvTitle9.setText(R.string.pro_feature_5);
            tvTitle2.setText(R.string.pro_feature_5);
            tvTitle12.setText(R.string.pro_feature_5);
        }
        String start_free_trial = getString(R.string.start_free_trial);
        String subs_month = getString(R.string.subs_month);
        String one_time = getString(R.string.one_time);
        int maxlength2 = 0;
        if(maxlength2 < start_free_trial.length()){
            maxlength2 = start_free_trial.length();
        }
        if(maxlength2 < subs_month.length()){
            maxlength2 = subs_month.length();
        }
        if(maxlength2 < one_time.length()){
            maxlength2 = one_time.length();
        }
        if(maxlength2 == start_free_trial.length()){
            tvSubMonth4.setText(R.string.start_free_trial);
            tvSubMonth1.setText(R.string.start_free_trial);
            tvSubMonth2.setText(R.string.start_free_trial);
        }else if(maxlength2 == subs_month.length()){
            tvSubMonth4.setText(R.string.subs_month);
            tvSubMonth1.setText(R.string.subs_month);
            tvSubMonth2.setText(R.string.subs_month);
        }else if(maxlength2 == one_time.length()){
            tvSubMonth4.setText(R.string.one_time);
            tvSubMonth1.setText(R.string.one_time);
            tvSubMonth2.setText(R.string.one_time);
        }

        anim = getIntent().getBooleanExtra("from_home",false);
        if(anim) {
            animClose = AnimationUtils.loadAnimation(this, R.anim.welcome_fade_in);
            animClose.setDuration(3000l);
            btn_close.startAnimation(this.animClose);
            animClose.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    btn_close.setVisibility(View.VISIBLE);
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });
        }else{
            btn_close.setVisibility(View.VISIBLE);
        }

        btn_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        rltTrial.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buyPro(getString(R.string.sub_key_year),true);
            }
        });
        subMonth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buyPro(getString(R.string.sub_key_month), true);
            }
        });
        rltOneTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buyPro(getString(R.string.pro_key), false);
            }
        });

        String price_year = SharedPreferencesUtils.getString(getString(R.string.sub_key_year) + "_price","14.99$");
        String title_price_year = getString(R.string.auto_renew);
        if(title_price_year.contains("14.99")){
            title_price_year = title_price_year.replace("$", "");
            title_price_year = title_price_year.replace("14.99",price_year);
        }
        tv_auto_renew.setText(title_price_year);
        TextView tvSubMonthPrice = findViewById(R.id.tvSubMonthPrice);
        tvSubMonthPrice.setText(SharedPreferencesUtils.getString(getString(R.string.sub_key_month) + "_price","4.99$"));

        TextView tvOneTimePrice = findViewById(R.id.tvOneTimePrice);
        tvOneTimePrice.setText(SharedPreferencesUtils.getString(getString(R.string.pro_key) + "_price","9.99$"));
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
