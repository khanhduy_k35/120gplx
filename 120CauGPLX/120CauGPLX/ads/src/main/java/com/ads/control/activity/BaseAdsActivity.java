package com.ads.control.activity;

import android.app.Dialog;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.graphics.Point;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.util.TypedValue;
import android.view.Display;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.ads.control.BuildConfig;
import com.ads.control.R;
import com.ads.control.admob.AdsOpenManager;
import com.ads.control.customview.CustomTextView;
import com.ads.control.model.ConfigAds;
import com.ads.control.sound.SoundFilesPracice;
import com.ads.control.sound.SoundPoolManager;
import com.ads.control.utils.SharedPreferencesUtils;
import com.ads.control.utils.Utils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.gms.tasks.Task;
import com.google.android.play.core.review.ReviewInfo;
import com.google.android.play.core.review.ReviewManager;
import com.google.android.play.core.review.ReviewManagerFactory;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.limurse.iap.DataWrappers;
import com.limurse.iap.IapConnector;
import com.limurse.iap.PurchaseServiceListener;
import com.limurse.iap.SubscriptionServiceListener;
import com.rey.material.drawable.RippleDrawable;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Random;

public abstract class BaseAdsActivity extends AppCompatActivity{
    public static IapConnector billingConnector;
    private DatabaseReference mDatabase;
    public static boolean isLoadConfig = false;

    public interface DismissAdActivity{
        public void onDissmiss();
    }
    private static DismissAdActivity dismissAdActivity;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            View decor = getWindow().getDecorView();
            if (SharedPreferencesUtils.isNightMode()) {
                setTheme(R.style.AppThemeDarkAds);
                decor.setSystemUiVisibility(0);
                getWindow().setStatusBarColor(Color.parseColor("#000000"));
                getWindow().setNavigationBarColor(Color.parseColor("#000000"));
            } else {
                setTheme(R.style.AppThemeAds);
                decor.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
                getWindow().setStatusBarColor(Color.parseColor("#ffffff"));
                getWindow().setNavigationBarColor(Color.parseColor("#ffffff"));
            }
        }
        SoundPoolManager.createInstance(getApplicationContext(), new SoundPoolManager.SoundPoolManagerListener() {
            @Override
            public void onItemsLoaded() {
                //SoundPoolManager.getInstance(PracticeActivity.this).playNoteLoop(2);
            }
        }, new SoundFilesPracice());
        if(mDatabase == null)
            mDatabase = FirebaseDatabase.getInstance().getReference();
        //rieng voi tung app co the goi ham nay hay ko
        //setupFullScreen();
        super.onCreate(savedInstanceState);
        if(getClass().toString().contains("HomeActivity") || getClass().toString().contains("InAppActivity") || getClass().toString().contains("IapActivity")){
            checkIAPProversion();
        }
        SharedPreferencesUtils.setBoolean("load_floor",false);
        if(mDatabase!=null && getClass().toString().contains("Splash")){
            if(!isLoadConfig) {
                mDatabase.child("config").addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        try {
                            ConfigAds configAds = dataSnapshot.getValue(ConfigAds.class);
                            isLoadConfig = true;
                            SharedPreferencesUtils.setBoolean("newapp_redirect",configAds.newapp_redirect);
                            SharedPreferencesUtils.setString("newapp_package",configAds.newapp_package);
                            SharedPreferencesUtils.setString("newapp_name",configAds.newapp_name);
                            SharedPreferencesUtils.setString("newapp_icon",configAds.newapp_icon);
                            SharedPreferencesUtils.setString("newapp_des1",configAds.newapp_des1);
                            SharedPreferencesUtils.setString("newapp_des2",configAds.newapp_des2);
                            SharedPreferencesUtils.setString("newapp_img1",configAds.newapp_img1);
                            SharedPreferencesUtils.setString("newapp_img2",configAds.newapp_img2);
                            SharedPreferencesUtils.setString("newapp_img3",configAds.newapp_img3);

                            SharedPreferencesUtils.setBoolean("other_redirect",configAds.other_redirect);
                            SharedPreferencesUtils.setString("other_package",configAds.other_package);
                            SharedPreferencesUtils.setString("other_name",configAds.other_name);
                            SharedPreferencesUtils.setString("other_icon",configAds.other_icon);
                            SharedPreferencesUtils.setString("other_des1",configAds.other_des1);
                            SharedPreferencesUtils.setString("other_des2",configAds.other_des2);
                            SharedPreferencesUtils.setString("other_img1",configAds.other_img1);
                            SharedPreferencesUtils.setString("other_img2",configAds.other_img2);
                            SharedPreferencesUtils.setString("other_img3",configAds.other_img3);
                            SharedPreferencesUtils.setInt("newapp_redirect_percent",configAds.newapp_redirect_percent);
                            SharedPreferencesUtils.setInt("native_distance",configAds.native_distance == 0 ? 1 : configAds.native_distance);
                            SharedPreferencesUtils.setBoolean("load_floor",configAds.load_floor);
                            Utils.TIME_DISTANCE_DISPLAYADS = configAds.inter_distance == 0 ? 45 : configAds.inter_distance;
                        }catch (Exception e){

                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        isLoadConfig = false;
                    }
                });
            }
        }


        if(getClass().toString().contains("HomeActivity") || getClass().toString().contains("MainActivity")){
            if(!checkShowDialogNewApp()) {
                if(checkShowDialogVip()){
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            Intent intent = new Intent(BaseAdsActivity.this, InAppActivity.class);
                            intent.putExtra("from_home",false);
                            startActivity(intent);
                        }
                    },10);
                }
            }
        }
    }

    public DismissAdActivity getDismissAdActivity() {
        return dismissAdActivity;
    }

    public void setDismissAdActivity(DismissAdActivity dismissAdActivity) {
        this.dismissAdActivity = dismissAdActivity;
    }


    public void setupFullScreen(){
        Window window = this.getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | 0);
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(Color.TRANSPARENT);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    protected void setUpToolBar(View v){
        TypedValue tv = new TypedValue();
        if (getTheme().resolveAttribute(android.R.attr.actionBarSize, tv, true))
        {
            int actionBarHeight = TypedValue.complexToDimensionPixelSize(tv.data,getResources().getDisplayMetrics());

            RippleDrawable rippleDrawable = new RippleDrawable.Builder()
                    .cornerRadius(actionBarHeight/2)
                    .backgroundAnimDuration(300)
                    .rippleColor(Utils.getColorFromAttr(this,R.attr.rd_rippleColor))
                    .backgroundColor(Utils.getColorFromAttr(this,R.attr.rd_backgroundColor))
                    .build();
            v.setBackgroundDrawable(rippleDrawable);
        }
    }

    public void requestFullScreen(){
        Window window = getWindow();
        WindowManager.LayoutParams winParams = window.getAttributes();
        winParams.flags &= ~WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS;
        window.setAttributes(winParams);
        window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        getWindow().setStatusBarColor(Color.TRANSPARENT);
    }

    public void checkIAPProversion(){
        try {
            if (billingConnector == null) {
                List<String> nonConsumablesList = Arrays.asList(getString(R.string.pro_key),getString(R.string.pro_1),getString(R.string.pro_3),getString(R.string.pro_5),getString(R.string.pro_10));
                List<String> consumablesList = Arrays.asList("base");
                List<String> subsList = Arrays.asList(getString(R.string.sub_key_month),getString(R.string.sub_key_year),getString(R.string.sub_1),getString(R.string.sub_3),getString(R.string.sub_5),getString(R.string.sub_10));

                billingConnector = new IapConnector(this,nonConsumablesList,consumablesList,subsList, getString(R.string.gg_service_key),true);
                billingConnector.addBillingClientConnectionListener((status, billingResponseCode) -> {
                    Log.e("khanhduy.le","các");
                });

                billingConnector.addPurchaseListener(new PurchaseServiceListener() {
                    @Override
                    public void onPricesUpdated(@NonNull Map<String, DataWrappers.ProductDetails> map) {
                        priceUpdate(map);
                    }

                    @Override
                    public void onProductPurchased(@NonNull DataWrappers.PurchaseInfo purchaseInfo) {
                        processPurchased(purchaseInfo);
                    }

                    @Override
                    public void onProductRestored(@NonNull DataWrappers.PurchaseInfo purchaseInfo) {
                        processPurchased(purchaseInfo);
                    }
                });

                billingConnector.addSubscriptionListener(new SubscriptionServiceListener() {
                    @Override
                    public void onSubscriptionRestored(@NonNull DataWrappers.PurchaseInfo purchaseInfo) {
                        processPurchased(purchaseInfo);
                    }

                    @Override
                    public void onSubscriptionPurchased(@NonNull DataWrappers.PurchaseInfo purchaseInfo) {
                        processPurchased(purchaseInfo);
                    }

                    @Override
                    public void onPricesUpdated(@NonNull Map<String, DataWrappers.ProductDetails> map) {
                        priceUpdate(map);
                    }
                });
            }
        }catch (Exception e){

        }
    }

    public void priceUpdate(@NonNull Map<String, DataWrappers.ProductDetails> map){
        if(map.get(getString(R.string.pro_key))!=null){
            DataWrappers.ProductDetails productDetails = map.get(getString(R.string.pro_key));
            SharedPreferencesUtils.setString(getString(R.string.pro_key) + "_price", productDetails.getPrice());
        }
        if(map.get(getString(R.string.sub_key_month))!=null){
            DataWrappers.ProductDetails productDetails = map.get(getString(R.string.sub_key_month));
            SharedPreferencesUtils.setString(getString(R.string.sub_key_month) + "_price", productDetails.getPrice());
        }
        if(map.get(getString(R.string.sub_key_year))!=null){
            DataWrappers.ProductDetails productDetails = map.get(getString(R.string.sub_key_year));
            SharedPreferencesUtils.setString(getString(R.string.sub_key_year) + "_price", productDetails.getPrice());
        }
        if(map.get(getString(R.string.pro_1))!=null){
            DataWrappers.ProductDetails productDetails = map.get(getString(R.string.pro_1));
            SharedPreferencesUtils.setString(getString(R.string.pro_1) + "_price", productDetails.getPrice());
        }
        if(map.get(getString(R.string.pro_3))!=null){
            DataWrappers.ProductDetails productDetails = map.get(getString(R.string.pro_3));
            SharedPreferencesUtils.setString(getString(R.string.pro_3) + "_price", productDetails.getPrice());
        }
        if(map.get(getString(R.string.pro_5))!=null){
            DataWrappers.ProductDetails productDetails = map.get(getString(R.string.pro_5));
            SharedPreferencesUtils.setString(getString(R.string.pro_5) + "_price", productDetails.getPrice());
        }
        if(map.get(getString(R.string.pro_10))!=null){
            DataWrappers.ProductDetails productDetails = map.get(getString(R.string.pro_10));
            SharedPreferencesUtils.setString(getString(R.string.pro_10) + "_price", productDetails.getPrice());
        }
        if(map.get(getString(R.string.sub_1))!=null){
            DataWrappers.ProductDetails productDetails = map.get(getString(R.string.sub_1));
            SharedPreferencesUtils.setString(getString(R.string.sub_1) + "_price", productDetails.getPrice());
        }
        if(map.get(getString(R.string.sub_3))!=null){
            DataWrappers.ProductDetails productDetails = map.get(getString(R.string.sub_3));
            SharedPreferencesUtils.setString(getString(R.string.sub_3) + "_price", productDetails.getPrice());
        }
        if(map.get(getString(R.string.sub_5))!=null){
            DataWrappers.ProductDetails productDetails = map.get(getString(R.string.sub_5));
            SharedPreferencesUtils.setString(getString(R.string.sub_5) + "_price", productDetails.getPrice());
        }
        if(map.get(getString(R.string.sub_10))!=null){
            DataWrappers.ProductDetails productDetails = map.get(getString(R.string.sub_10));
            SharedPreferencesUtils.setString(getString(R.string.sub_10) + "_price", productDetails.getPrice());
        }
    }

    public void processPurchased(@NonNull DataWrappers.PurchaseInfo purchaseInfo){
        if(purchaseInfo.getSku().equals(getString(R.string.pro_key))){
            if(SharedPreferencesUtils.getString(getString(R.string.pro_key) + "_date_time") == null){
                SharedPreferencesUtils.setString(getString(R.string.pro_key) + "_date_time",Utils.getCurrentTime());
            }
            settingPro();
        }
        if(purchaseInfo.getSku().equals(getString(R.string.sub_key_month))){
            if(SharedPreferencesUtils.getString(getString(R.string.sub_key_month) + "_date_time") == null){
                SharedPreferencesUtils.setString(getString(R.string.sub_key_month) + "_date_time",Utils.getCurrentTime());
            }
            settingPro();
        }
        if(purchaseInfo.getSku().equals(getString(R.string.sub_key_year))){
            if(SharedPreferencesUtils.getString(getString(R.string.sub_key_year) + "_date_time") == null){
                SharedPreferencesUtils.setString(getString(R.string.sub_key_year) + "_date_time",Utils.getCurrentTime());
            }
            settingPro();
        }
        if(purchaseInfo.getSku().equals(getString(R.string.pro_1))||purchaseInfo.getSku().equals(getString(R.string.pro_3))||purchaseInfo.getSku().equals(getString(R.string.pro_5))||
                purchaseInfo.getSku().equals(getString(R.string.pro_10))||purchaseInfo.getSku().equals(getString(R.string.sub_1))||purchaseInfo.getSku().equals(getString(R.string.sub_3))||
                purchaseInfo.getSku().equals(getString(R.string.sub_5))||purchaseInfo.getSku().equals(getString(R.string.sub_10))){
            settingPro();
        }
    }


    public void settingPro(){
        SharedPreferencesUtils.setIsPro(true);
    }

    public void buyPro(boolean isSubs){
        if(isSubs){
            if(billingConnector!=null){
                billingConnector.subscribe(BaseAdsActivity.this, getString(R.string.sub_key_month));
            }
        }else{
            if(billingConnector!=null){
                billingConnector.purchase(BaseAdsActivity.this,getString(R.string.pro_key));
            }
        }
        if(BuildConfig.DEBUG) {
            settingPro();
        }
    }

    public void buyPro(String idProduct,boolean isSub){
        if(billingConnector!=null){
            if (isSub){
                billingConnector.subscribe(BaseAdsActivity.this,idProduct);
            }else{
                billingConnector.purchase(BaseAdsActivity.this,idProduct);
            }

        }
        if(BuildConfig.DEBUG) {
            settingPro();
        }
    }

    public int getWidthScreen(){
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        return width;
    }

    public void showRatingInApp(){
        int count = SharedPreferencesUtils.getInt("count_rating",0);
        if(count % 30 == 5){
            if(count <= 300) {
                SharedPreferencesUtils.setInt("count_rating", count + 1);
                ReviewManager manager = ReviewManagerFactory.create(this);
                Task<ReviewInfo> request = manager.requestReviewFlow();
                request.addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        // We can get the ReviewInfo object
                        ReviewInfo reviewInfo = task.getResult();
                        Task<Void> flow = manager.launchReviewFlow(this, reviewInfo);
                        flow.addOnCompleteListener(task1 -> {
                            // The flow has finished. The API does not indicate whether the user
                            // reviewed or not, or even whether the review dialog was shown.
                        });
                    } else {
                        //show review binh thuong
                    }
                });
            }
        }else{
            SharedPreferencesUtils.setInt("count_rating",count+1);
        }
    }



    public boolean checkShowDialogVip(){
        boolean isShow = false;
        try {
            Date c = Calendar.getInstance().getTime();
            System.out.println("Current time => " + c);

            SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
            String currentDate = df.format(c);
            String preDate = SharedPreferencesUtils.getString("current_date") == null ? "" : SharedPreferencesUtils.getString("current_date");
            if (!preDate.equals(currentDate) && !preDate.isEmpty()) {
                SharedPreferencesUtils.setString("current_date", currentDate);
                if(!SharedPreferencesUtils.isPro()) {
                    isShow = true;
                }
            }
            if(preDate!=null && preDate.isEmpty()){
                SharedPreferencesUtils.setString("current_date", currentDate);
            }
        } catch (Exception e) {

        }
        return isShow;
    }

    public boolean checkShowDialogNewApp(){
        boolean result = false;
        if(SharedPreferencesUtils.isPro())
        {
            result = true;
            return result;
        }
        if(SharedPreferencesUtils.getBoolean("newapp_redirect")){
            showDialogDownloadNewApp();
            result = true;
        }else{
            int percent = SharedPreferencesUtils.getInt("newapp_redirect_percent",-1);
            if(percent > 0){
                Random generator = new Random();
                int ranInf = generator.nextInt(100);
                if(ranInf < percent){
                    showDialogDownloadNewApp();
                    result = true;
                }else {
                    result = false;
                }
            }else{
                result = false;
            }
        }
        return result;
    }

    private void showDialogDownloadNewApp() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Window window = dialog.getWindow();
        window.setBackgroundDrawableResource(android.R.color.transparent);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.customdialog_moreapp);
        final CustomTextView btn_quit = (CustomTextView) dialog.findViewById(R.id.btn_custom_dialog_exit_quit);
        final CustomTextView btn_ok = (CustomTextView) dialog.findViewById(R.id.btn_custom_dialog_exit_ok);
        final CustomTextView tv_app_name = (CustomTextView) dialog.findViewById(R.id.tv_app_name);
        final ImageView iconApp = (ImageView) dialog.findViewById(R.id.icon_app);
        final CustomTextView btn_install = (CustomTextView) dialog.findViewById(R.id.btn_install);
        final CustomTextView tv_description_1 = (CustomTextView) dialog.findViewById(R.id.tv_description_1);
        final CustomTextView tv_description_2 = (CustomTextView) dialog.findViewById(R.id.tv_description_2);
        final ImageView img_ads_1 = (ImageView) dialog.findViewById(R.id.img_ads_1);
        final ImageView img_ads_2 = (ImageView) dialog.findViewById(R.id.img_ads_2);
        final ImageView img_ads_3 = (ImageView) dialog.findViewById(R.id.img_ads_3);

        tv_app_name.setText(SharedPreferencesUtils.getString("newapp_name"));
        if(!SharedPreferencesUtils.getString("newapp_des1","").isEmpty()) {
            tv_description_1.setText(SharedPreferencesUtils.getString("newapp_des1"));
            tv_description_1.setVisibility(View.VISIBLE);
        }else{
            tv_description_1.setVisibility(View.GONE);
        }
        if(!SharedPreferencesUtils.getString("newapp_des2","").isEmpty()) {
            tv_description_2.setText(SharedPreferencesUtils.getString("newapp_des2"));
            tv_description_2.setVisibility(View.VISIBLE);
        }else{
            tv_description_2.setVisibility(View.GONE);
        }
        btn_quit.setVisibility(View.INVISIBLE);

        RequestOptions requestOptions = new RequestOptions();
        requestOptions.placeholder(R.drawable.ads_default_bg);

        Glide.with(this).setDefaultRequestOptions(requestOptions).load(SharedPreferencesUtils.getString("newapp_icon")).into(iconApp);
        Glide.with(this).setDefaultRequestOptions(requestOptions).load(SharedPreferencesUtils.getString("newapp_img1")).into(img_ads_1);
        Glide.with(this).setDefaultRequestOptions(requestOptions).load(SharedPreferencesUtils.getString("newapp_img2")).into(img_ads_2);
        Glide.with(this).setDefaultRequestOptions(requestOptions).load(SharedPreferencesUtils.getString("newapp_img3")).into(img_ads_3);

        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String appPackageName = SharedPreferencesUtils.getString("newapp_package");
                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                } catch (android.content.ActivityNotFoundException anfe) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                }
            }
        });
        btn_install.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String appPackageName = SharedPreferencesUtils.getString("newapp_package");
                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                } catch (android.content.ActivityNotFoundException anfe) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                }
            }
        });
        dialog.show();
    }
}
