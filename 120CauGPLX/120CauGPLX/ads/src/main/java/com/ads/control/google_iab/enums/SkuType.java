package com.ads.control.google_iab.enums;

public enum SkuType {
    INAPP,
    SUBS,
    NONE
}
