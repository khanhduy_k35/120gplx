package com.ads.control.admob;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import androidx.annotation.Nullable;

import com.ads.control.AdsApplication;
import com.ads.control.R;
import com.ads.control.utils.SharedPreferencesUtils;
import com.ads.control.utils.Utils;
import com.applovin.mediation.MaxAd;
import com.applovin.mediation.MaxAdListener;
import com.applovin.mediation.MaxError;
import com.applovin.mediation.MaxReward;
import com.applovin.mediation.MaxRewardedAdListener;
import com.applovin.mediation.ads.MaxInterstitialAd;
import com.applovin.mediation.ads.MaxRewardedAd;
import com.applovin.mediation.nativeAds.MaxNativeAdListener;
import com.applovin.mediation.nativeAds.MaxNativeAdLoader;
import com.applovin.mediation.nativeAds.MaxNativeAdView;
import com.applovin.mediation.nativeAds.MaxNativeAdViewBinder;
import com.applovin.sdk.AppLovinMediationProvider;
import com.applovin.sdk.AppLovinSdk;
import com.facebook.shimmer.ShimmerFrameLayout;

public class AppLovinHelp {
    private static AppLovinHelp instance;
    private MaxInterstitialAd mInterstitialAd;
    private AdCloseListener adCloseListener;
    private AdRewardListener adRewardListener;
    private boolean showInter = false;
    private MaxRewardedAd rewardedAd;
    public static AppLovinHelp getInstance() {
        if (instance == null) {
            instance = new AppLovinHelp();
        }
        return instance;
    }


    public void loadAdsReward(final Activity context){
        if(rewardedAd!=null && rewardedAd.isReady())
            return;
        rewardedAd = MaxRewardedAd.getInstance( context.getString(R.string.applovin_reward_ads), context);
        rewardedAd.setListener(new MaxRewardedAdListener() {
            @Override
            public void onUserRewarded(MaxAd maxAd, MaxReward maxReward) {
                if(adRewardListener!=null)
                    adRewardListener.onAdRewardSuccess();
            }

            @Override
            public void onRewardedVideoStarted(MaxAd maxAd) {

            }

            @Override
            public void onRewardedVideoCompleted(MaxAd maxAd) {

            }

            @Override
            public void onAdLoaded(MaxAd maxAd) {
            }

            @Override
            public void onAdDisplayed(MaxAd maxAd) {

            }

            @Override
            public void onAdHidden(MaxAd maxAd) {
                rewardedAd.loadAd();
            }

            @Override
            public void onAdClicked(MaxAd maxAd) {

            }

            @Override
            public void onAdLoadFailed(String s, MaxError maxError) {
            }

            @Override
            public void onAdDisplayFailed(MaxAd maxAd, MaxError maxError) {
                rewardedAd.loadAd();
                adRewardListener.onAdRewardFailed();
            }
        });

        rewardedAd.loadAd();
    }

    public void loadReward(final Activity context){
        if(AppLovinSdk.getInstance( context ).isInitialized()){
            AppLovinSdk.getInstance(AdsApplication.getInstance()).setMediationProvider(AppLovinMediationProvider.MAX );
            AppLovinSdk.getInstance(AdsApplication.getInstance() ).initializeSdk( config -> {
                loadAdsReward(context);
            });
        }else {
            loadAdsReward(context);
        }
    }

    public void showReward(Activity appCompatActivity,AdRewardListener adRewardListener){
        this.adRewardListener = adRewardListener;
        if(rewardedAd.isReady())
        {
            rewardedAd.showAd();
        }else{
            if(this.adRewardListener!=null)
                this.adRewardListener.onAdRewardFailed();
        }
    }

    private AppLovinHelp() {

    }


    public void init(Activity context) {

    }

    public boolean isShowInter() {
        return showInter;
    }

    public void loadInterstitialAd(final Activity context) {
        if(AppLovinSdk.getInstance( context ).isInitialized()){
            AppLovinSdk.getInstance(AdsApplication.getInstance()).setMediationProvider(AppLovinMediationProvider.MAX );
            AppLovinSdk.getInstance(AdsApplication.getInstance() ).initializeSdk( config -> {
                loadInter(context);
            });
        }else {
            loadInter(context);
        }
    }

    public void loadInter(Activity context){
        if (mInterstitialAd != null && mInterstitialAd.isReady())
            return;

        mInterstitialAd = new MaxInterstitialAd(context.getString(R.string.applovin_inter_ads), context);
        mInterstitialAd.loadAd();
        mInterstitialAd.setListener(new MaxAdListener() {
            @Override
            public void onAdLoaded(MaxAd maxAd) {
            }

            @Override
            public void onAdDisplayed(MaxAd maxAd) {
                showInter = true;
            }

            @Override
            public void onAdHidden(MaxAd maxAd) {
                mInterstitialAd.loadAd();
                if(adCloseListener!=null){
                    adCloseListener.onAdClosed(true);
                }
                showInter = false;
            }

            @Override
            public void onAdClicked(MaxAd maxAd) {

            }

            @Override
            public void onAdLoadFailed(String s, MaxError maxError) {
            }

            @Override
            public void onAdDisplayFailed(MaxAd maxAd, MaxError maxError) {
                // Interstitial ad failed to display. AppLovin recommends that you load the next ad.
                mInterstitialAd.loadAd();
                if(adCloseListener!=null){
                    adCloseListener.onAdClosed(false);
                }
                showInter = false;
            }
        });
    }

    public void showInterstitialAd(Activity activity, final AdCloseListener adCloseListenerTemp) {
        this.adCloseListener = adCloseListenerTemp;
        if(SharedPreferencesUtils.isPro() || !Utils.isDisplayAds){
            if(adCloseListener!=null){
                adCloseListener.onAdClosed(false);
            }
        }else {
            if ((AdmobHelp.getInstance().getTimeLoad() + AdmobHelp.getInstance().getTimeReload()) < System.currentTimeMillis()) {
                if (canShowInterstitialAd(activity)) {
                    mInterstitialAd.showAd();
                    AdmobHelp.getInstance().setTimeLoad(System.currentTimeMillis());
                } else {
                    adCloseListener.onAdClosed(false);
                }
            } else {
                adCloseListener.onAdClosed(false);
            }
        }
    }

    public boolean canShowInterstitialAd(Context context) {
        return mInterstitialAd != null && context instanceof Activity && mInterstitialAd.isReady();
    }

    public void openAppShow(Activity appCompatActivity){
        try{
            //an native
            RelativeLayout container_ads_native = appCompatActivity.findViewById(R.id.container_ads_native);
            if(container_ads_native!=null){
                container_ads_native.setVisibility(View.GONE);
                if(container_ads_native.getParent()!=null){
                    ((ViewGroup)container_ads_native.getParent()).setVisibility(View.GONE);
                }
            }
            //an banner
            RelativeLayout container_ads_banner = appCompatActivity.findViewById(R.id.container_ads_banner);
            if(container_ads_banner!=null){
                container_ads_banner.setVisibility(View.GONE);
            }
        }catch (Exception e){

        }
    }

    public void openAppHidden(Activity appCompatActivity){
        try{
            if(SharedPreferencesUtils.isPro() || !Utils.isDisplayAds){
                RelativeLayout container_ads_native = appCompatActivity.findViewById(R.id.container_ads_native);
                if(container_ads_native!=null){
                    container_ads_native.setVisibility(View.GONE);
                    if(container_ads_native.getParent()!=null){
                        ((ViewGroup)container_ads_native.getParent()).setVisibility(View.GONE);
                    }
                }
                RelativeLayout container_ads_banner = appCompatActivity.findViewById(R.id.container_ads_banner);
                if(container_ads_banner!=null){
                    container_ads_banner.setVisibility(View.GONE);
                }
            }else{
                RelativeLayout container_ads_native = appCompatActivity.findViewById(R.id.container_ads_native);
                if(container_ads_native!=null){
                    container_ads_native.setVisibility(View.VISIBLE);
                    if(container_ads_native.getParent()!=null){
                        ((ViewGroup)container_ads_native.getParent()).setVisibility(View.VISIBLE);
                    }
                }
                RelativeLayout container_ads_banner = appCompatActivity.findViewById(R.id.container_ads_banner);
                if(container_ads_banner!=null){
                    container_ads_banner.setVisibility(View.VISIBLE);
                }
            }
        }catch (Exception e){

        }
    }

    public void loadNativeActivity(final Activity mActivity,boolean medium) {
        if(mActivity == null){
            return;
        }
        if(SharedPreferencesUtils.isPro() || !Utils.isDisplayAds){
            RelativeLayout container_ads_native = mActivity.findViewById(R.id.container_ads_native);
            if(container_ads_native!=null){
                container_ads_native.setVisibility(View.GONE);
            }
            if(container_ads_native.getParent()!=null){
                ((ViewGroup)container_ads_native.getParent()).setVisibility(View.GONE);
            }
        }
        if(!SharedPreferencesUtils.isPro() && Utils.isDisplayAds){
            RelativeLayout container_ads_native = mActivity.findViewById(R.id.container_ads_native);
            if(container_ads_native!=null){
                ShimmerFrameLayout shimmerFrameLayout = container_ads_native.findViewById(R.id.shimmerLayout);
                if(shimmerFrameLayout!=null)
                    shimmerFrameLayout.startShimmer();
                if(Utils.isDisplayAds) {
                    MaxNativeAdLoader  nativeAdLoader = new MaxNativeAdLoader( mActivity.getString(R.string.applovin_native_ads), mActivity );
                    nativeAdLoader.loadAd(createNativeAdView(mActivity,medium));
                    nativeAdLoader.setNativeAdListener(new MaxNativeAdListener() {
                        @Override
                        public void onNativeAdLoaded(@Nullable MaxNativeAdView maxNativeAdView, MaxAd maxAd) {
                            super.onNativeAdLoaded(maxNativeAdView, maxAd);
                            if(shimmerFrameLayout!=null)
                                shimmerFrameLayout.stopShimmer();
                            container_ads_native.removeAllViews();
                            container_ads_native.addView( maxNativeAdView );
                        }

                        @Override
                        public void onNativeAdLoadFailed(String s, MaxError maxError) {
                            super.onNativeAdLoadFailed(s, maxError);
                            if(container_ads_native!=null){
                                try {
                                    container_ads_native.setVisibility(View.GONE);
                                }catch (Exception e){

                                }
                            }
                            if(container_ads_native.getParent()!=null){
                                ((ViewGroup)container_ads_native.getParent()).setVisibility(View.GONE);
                            }
                            if(shimmerFrameLayout!=null)
                                shimmerFrameLayout.stopShimmer();
                        }

                        @Override
                        public void onNativeAdClicked(MaxAd maxAd) {
                            super.onNativeAdClicked(maxAd);
                        }

                        @Override
                        public void onNativeAdExpired(MaxAd maxAd) {
                            super.onNativeAdExpired(maxAd);
                        }
                    });
                }
            }
        }
    }

    public void loadNativeFragment(final Activity mActivity, final View rootView,boolean isMedium) {
        if(mActivity == null){
            return;
        }
        if(SharedPreferencesUtils.isPro() || !Utils.isDisplayAds){
            RelativeLayout container_ads_native = rootView.findViewById(R.id.container_ads_native);
            if(container_ads_native!=null){
                container_ads_native.setVisibility(View.GONE);
            }
            if(container_ads_native.getParent()!=null){
                ((ViewGroup)container_ads_native.getParent()).setVisibility(View.GONE);
            }
        }
        if(!SharedPreferencesUtils.isPro() && Utils.isDisplayAds){
            RelativeLayout container_ads_native = rootView.findViewById(R.id.container_ads_native);
            if(container_ads_native!=null){
                if(container_ads_native!=null){
                    ShimmerFrameLayout shimmerFrameLayout = container_ads_native.findViewById(R.id.shimmerLayout);
                    if(shimmerFrameLayout!=null)
                        shimmerFrameLayout.startShimmer();
                    if(Utils.isDisplayAds) {
                        MaxNativeAdLoader  nativeAdLoader = new MaxNativeAdLoader( mActivity.getString(R.string.applovin_native_ads), mActivity );
                        nativeAdLoader.loadAd(createNativeAdView(mActivity,isMedium));
                        nativeAdLoader.setNativeAdListener(new MaxNativeAdListener() {
                            @Override
                            public void onNativeAdLoaded(@Nullable MaxNativeAdView maxNativeAdView, MaxAd maxAd) {
                                super.onNativeAdLoaded(maxNativeAdView, maxAd);
                                if(shimmerFrameLayout!=null)
                                    shimmerFrameLayout.stopShimmer();
                                container_ads_native.removeAllViews();
                                container_ads_native.addView( maxNativeAdView );
                            }

                            @Override
                            public void onNativeAdLoadFailed(String s, MaxError maxError) {
                                super.onNativeAdLoadFailed(s, maxError);
                                if(container_ads_native!=null){
                                    try {
                                        container_ads_native.setVisibility(View.GONE);
                                    }catch (Exception e){

                                    }
                                }
                                if(container_ads_native.getParent()!=null){
                                    ((ViewGroup)container_ads_native.getParent()).setVisibility(View.GONE);
                                }
                                if(shimmerFrameLayout!=null)
                                    shimmerFrameLayout.stopShimmer();
                            }

                            @Override
                            public void onNativeAdClicked(MaxAd maxAd) {
                                super.onNativeAdClicked(maxAd);
                            }

                            @Override
                            public void onNativeAdExpired(MaxAd maxAd) {
                                super.onNativeAdExpired(maxAd);
                            }
                        });
                    }
                }
            }
        }
    }

    private MaxNativeAdView createNativeAdView(final Activity mActivity,boolean medium)
    {
        if(medium) {
            MaxNativeAdViewBinder binder = new MaxNativeAdViewBinder.Builder(R.layout.native_ads_max_medium)
                    .setTitleTextViewId(R.id.native_ad_title).setBodyTextViewId(R.id.native_ad_body).setAdvertiserTextViewId(R.id.native_ad_sponsored_label).setIconImageViewId(R.id.native_ad_icon).setStarRatingContentViewGroupId(R.id.star_rating_view).setOptionsContentViewGroupId(R.id.ad_choices_container).setCallToActionButtonId(R.id.native_ad_call_to_action)
                    .build();
            return new MaxNativeAdView( binder, mActivity );
        }else{
            MaxNativeAdViewBinder binder = new MaxNativeAdViewBinder.Builder(R.layout.native_ads_max)
                    .setTitleTextViewId(R.id.native_ad_title).setBodyTextViewId(R.id.native_ad_body).setAdvertiserTextViewId(R.id.native_ad_sponsored_label).setIconImageViewId(R.id.native_ad_icon).setStarRatingContentViewGroupId(R.id.star_rating_view).setMediaContentViewGroupId(R.id.native_ad_media).setOptionsContentViewGroupId(R.id.ad_choices_container).setCallToActionButtonId(R.id.native_ad_call_to_action)
                    .build();

            return new MaxNativeAdView(binder, mActivity);
        }
    }
}
