package com.vcarcorp.ontap120tinhhuong.mophonggiaothong.model;

public class VoiceModel {
    private int id;
    private String name;
    private String voice_code;
    private String city;

    public VoiceModel() {
    }

    public VoiceModel(int id, String name, String voice_code, String city) {
        this.id = id;
        this.name = name;
        this.voice_code = voice_code;
        this.city = city;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getVoice_code() {
        return voice_code;
    }

    public void setVoice_code(String voice_code) {
        this.voice_code = voice_code;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

}
