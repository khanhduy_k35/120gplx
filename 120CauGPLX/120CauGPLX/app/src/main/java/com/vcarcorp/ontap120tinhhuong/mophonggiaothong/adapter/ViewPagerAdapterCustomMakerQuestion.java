package com.vcarcorp.ontap120tinhhuong.mophonggiaothong.adapter;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.viewpager2.adapter.FragmentStateAdapter;

import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.marked.MakerQuestionFragment;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.model.QuestionVideo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ViewPagerAdapterCustomMakerQuestion extends FragmentStateAdapter {
    private ArrayList<QuestionVideo> questionVideos;
    private Map<Integer, Fragment> mPageReferenceMap = new HashMap<Integer, Fragment>();

    public ViewPagerAdapterCustomMakerQuestion(FragmentActivity fragmentActivity, ArrayList<QuestionVideo> questionVideos){
        super(fragmentActivity);
        this.questionVideos = questionVideos;
    }

    @NonNull
    @Override
    public Fragment createFragment(int position) {
        MakerQuestionFragment learnQuestionFragment =  MakerQuestionFragment.init(questionVideos.get(position),position);
        mPageReferenceMap.put(position,learnQuestionFragment);
        return learnQuestionFragment;
    }

    @Override
    public int getItemCount() {
        return questionVideos.size();
    }

    public void detroyAllVideo(){
        try {
            for (Map.Entry<Integer, Fragment> entry : mPageReferenceMap.entrySet()) {
                MakerQuestionFragment learnQuestionFragment = (MakerQuestionFragment) entry.getValue();
                learnQuestionFragment.destroyVideo();
            }
        }catch (Exception e){

        }
    }

    public void changeCheckedShowScore(){
        try {
            for (Map.Entry<Integer, Fragment> entry : mPageReferenceMap.entrySet()) {
                MakerQuestionFragment makerQuestionFragment = (MakerQuestionFragment) entry.getValue();
                if(makerQuestionFragment!=null)
                    makerQuestionFragment.changeCheckedShowScore();
            }
        }catch (Exception e){

        }
    }

    public MakerQuestionFragment getFragment(int key) {
        return (MakerQuestionFragment) mPageReferenceMap.get(key);
    }

    public void destroyItem(int position){
        try{
            MakerQuestionFragment learnQuestionFragment = getFragment(position);
            learnQuestionFragment.destroyVideo();
            mPageReferenceMap.remove(position);
        }catch (Exception e){

        }
    }

}
