package com.vcarcorp.ontap120tinhhuong.mophonggiaothong.model;

import android.os.Parcel;
import android.os.Parcelable;

public class Taplo implements Parcelable {
    private int idTaplo;
    private String nameTaplo;
    private String description;

    public Taplo(int idTaplo, String nameTaplo, String description) {
        this.idTaplo = idTaplo;
        this.nameTaplo = nameTaplo;
        this.description = description;
    }

    public int getIdTaplo() {
        return idTaplo;
    }

    public void setIdTaplo(int idTaplo) {
        this.idTaplo = idTaplo;
    }

    public String getNameTaplo() {
        return nameTaplo;
    }

    public void setNameTaplo(String nameTaplo) {
        this.nameTaplo = nameTaplo;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.idTaplo);
        dest.writeString(this.nameTaplo);
        dest.writeString(this.description);
    }

    protected Taplo(Parcel in) {
        this.idTaplo = in.readInt();
        this.nameTaplo = in.readString();
        this.description = in.readString();
    }

    public static final Creator<Taplo> CREATOR = new Creator<Taplo>() {
        @Override
        public Taplo createFromParcel(Parcel source) {
            return new Taplo(source);
        }

        @Override
        public Taplo[] newArray(int size) {
            return new Taplo[size];
        }
    };
}
