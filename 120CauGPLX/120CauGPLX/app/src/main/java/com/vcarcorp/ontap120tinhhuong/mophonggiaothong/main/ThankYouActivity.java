package com.vcarcorp.ontap120tinhhuong.mophonggiaothong.main;

import android.os.Bundle;
import android.os.Handler;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.R;

public class ThankYouActivity extends BaseActivity {

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.act_goodbye);
        Animation loadAnimation = AnimationUtils.loadAnimation(this, R.anim.enter_from_bottom);
        Animation loadAnimation2 = AnimationUtils.loadAnimation(this, R.anim.enter_from_bottom_delay);
        findViewById(R.id.tv_thankyou_container).startAnimation(loadAnimation);
        findViewById(R.id.tv_thankyou_2).startAnimation(loadAnimation2);
        new Handler().postDelayed(new Runnable() {
            public void run() {
                ThankYouActivity.this.finish();
            }
        }, 3000);
    }


    public void onDestroy() {

        super.onDestroy();
        Runtime.getRuntime().gc();
    }
}
