package com.vcarcorp.ontap120tinhhuong.mophonggiaothong.download;

import android.content.Context;

import androidx.annotation.NonNull;

import com.danikula.videocache.HttpProxyCacheServer;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.App;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.database.GPLXDataManager;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.model.QuestionVideo;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.util.AppUtils;
import com.tonyodev.fetch2.Request;

import java.util.ArrayList;
import java.util.List;


public final class Data {

    public static ArrayList<String> sampleUrls = new ArrayList<>();

    private Data() {

    }

    @NonNull
    private static List<Request> getFetchRequests(Context context) {
        GPLXDataManager gplxDataManager = new GPLXDataManager(context);
        ArrayList<QuestionVideo> questionVideos = gplxDataManager.getAllQuestionVideoLink(context);
        try{
            gplxDataManager.close();
        }catch (Exception e){

        }
        final List<Request> requests = new ArrayList<>();
        if(sampleUrls!=null && sampleUrls.size() == 0){
            sampleUrls.add(AppUtils.getServerHuongDan(context));
            sampleUrls.add(AppUtils.getServerImg(context));
            for (int i = 0; i <  questionVideos.size(); i++){
                sampleUrls.add(AppUtils.getServerVideoVipDownload(context,questionVideos.get(i).getId()));
            }
        }
        for (String sampleUrl : sampleUrls) {
            HttpProxyCacheServer cacheServer = App.getCacheServer(context);
            final Request request = new Request(sampleUrl, cacheServer.getCacheFileString(sampleUrl));
            requests.add(request);
        }
        return requests;
    }

    @NonNull
    public static List<Request> getFetchRequestWithGroupId(final int groupId, Context context) {
        final List<Request> requests = getFetchRequests(context);
        for (Request request : requests) {
            request.setGroupId(groupId);
        }
        return requests;
    }
}

