package com.vcarcorp.ontap120tinhhuong.mophonggiaothong.exampractice;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ads.control.admob.AdCloseListener;
import com.ads.control.admob.AdmobHelp;
import com.airbnb.lottie.LottieAnimationView;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.main.GuideExamActivity;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.model.QuestionVideo;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.model.QuestionVideoCategory;
import com.rey.material.widget.FrameLayout;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.R;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.adapter.TestAdapter;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.database.GPLXDataManager;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.main.BaseActivity;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.model.Test;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.util.DialogUtil;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.util.Util;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.util.AppUtils;

import java.util.ArrayList;

/**
 * Created by macbook on 10/31/17.
 */
public class ExamTestCategory extends BaseActivity {
    private GPLXDataManager dataManager;
    private TestAdapter testAdapter;
    private RecyclerView recyclerView;
    private ArrayList<Test> listTestResult;
    private int clickPos = -1;
    private LottieAnimationView animView, animViewReset;
    private ActivityResultLauncher<Intent> launchSomeActivity;

    @SuppressLint("SourceLockedOrientationActivity")
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.exam_category_activity);
        Window window = this.getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | 0);
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(Color.TRANSPARENT);
        }
        launchSomeActivity = registerForActivityResult(
                new ActivityResultContracts.StartActivityForResult(),
                new ActivityResultCallback<ActivityResult>() {
                    @Override
                    public void onActivityResult(ActivityResult result) {
                        if (result.getResultCode() == Activity.RESULT_OK) {
                            process_action(result.getData());
                        }
                    }
                });
        dataManager = new GPLXDataManager(this);
        recyclerView = (RecyclerView) findViewById(R.id.list);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        setUpToolBar((FrameLayout) findViewById(R.id.btn_reset));

        findViewById(R.id.btn_create_random).setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                new DialogUtil.Builder(ExamTestCategory.this)
                        .title("Tạo đề ngẫu nhiên")
                        .content("Tạo đề ngẫu nhiên từ 120 video mô phỏng theo đúng cấu trúc đề thi như thi thật")
                        .positiveText(getResources().getString(R.string.text_ok))
                        .negativeText(getResources().getString(R.string.text_cancel))
                        .onPositive(new DialogUtil.SingleButtonCallback() {
                            @Override
                            public void onClick() {
                                if (listTestResult != null && listTestResult.size() < 14) {
                                    AdmobHelp.getInstance().showInterstitialAd(new AdCloseListener() {
                                        @Override
                                        public void onAdClosed(boolean haveAds) {
                                            makeRandomTest();
                                            if (haveAds) {
                                                checkShowSuggestProDisplayAds();
                                            }
                                        }
                                    });
                                } else {
                                    if (!Util.isPro(ExamTestCategory.this)) {
                                        new DialogUtil.Builder(ExamTestCategory.this)
                                                .title("Tạo đề thi ngẫu nhiên với cấu trúc như đề thi thật")
                                                .content("Bạn cần nâng cấp để sử dụng tính năng này và nhiều tính năng cao cấp khác.")
                                                .doneText(getString(R.string.text_ok))
                                                .onDone(new DialogUtil.SingleButtonCallback() {
                                                    @Override
                                                    public void onClick() {
                                                        buyPro();
                                                    }
                                                })
                                                .show();
                                    } else {
                                        makeRandomTest();
                                    }
                                }
                            }
                        })
                        .show();
            }
        });
        findViewById(R.id.btn_reset).setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                new DialogUtil.Builder(ExamTestCategory.this)
                        .title("Làm lại tất cả các đề thi")
                        .content("Bạn có muốn làm lại tất cả các đề thi từ đầu không?")
                        .positiveText(getResources().getString(R.string.text_ok))
                        .negativeText(getResources().getString(R.string.text_cancel))
                        .onPositive(new DialogUtil.SingleButtonCallback() {
                            @Override
                            public void onClick() {
                                if (!Util.isPro(ExamTestCategory.this)) {
                                    new DialogUtil.Builder(ExamTestCategory.this)
                                            .title("Reset tất cả bài học")
                                            .content("Bạn cần nâng cấp để sử dụng tính năng này và nhiều tính năng cao cấp khác.")
                                            .doneText(getString(R.string.text_ok))
                                            .onDone(new DialogUtil.SingleButtonCallback() {
                                                @Override
                                                public void onClick() {
                                                    buyPro();
                                                }
                                            })
                                            .show();
                                } else {
                                    dataManager.resetAllTest();
                                    dataManager.resetAllTestQuest();
                                    listTestResult = dataManager.getTestResult();
                                    testAdapter.setData(listTestResult);
                                    testAdapter.notifyDataSetChanged();
                                }
                            }
                        })
                        .show();
            }
        });

        setUpToolBar((FrameLayout) findViewById(R.id.btn_back));
        findViewById(R.id.btn_back).setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                onBackPressed();
            }
        });
        setUpToolBar((FrameLayout) findViewById(R.id.btn_guide));
        if (findViewById(R.id.btn_guide) != null) {
            findViewById(R.id.btn_guide).setOnClickListener(new OnSingleClickListener() {
                @Override
                public void onSingleClick(View v) {
                    AdmobHelp.getInstance().showInterstitialAd(new AdCloseListener() {
                        @Override
                        public void onAdClosed(boolean haveAds) {
                            Intent intent = new Intent(ExamTestCategory.this, GuideExamActivity.class);
                            startActivity(intent);
                            if (haveAds) {
                                checkShowSuggestProDisplayAds();
                            }
                        }
                    });
                }
            });
        }

        listTestResult = new ArrayList<>();
        listTestResult = dataManager.getTestResult();
        testAdapter = new TestAdapter(listTestResult, this);
        recyclerView.setAdapter(testAdapter);
        testAdapter.setClickListener(new TestAdapter.ItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                AdmobHelp.getInstance().showInterstitialAd( new AdCloseListener() {
                    @Override
                    public void onAdClosed(boolean haveAds) {
                        clickPos = position;
                        if (listTestResult.get(position).isFinish()) {
                            Intent intent = new Intent(ExamTestCategory.this, ResultActivity.class);
                            Bundle bundle = new Bundle();
                            bundle.putParcelable("test", listTestResult.get(position));
                            intent.putExtras(bundle);
                            launchSomeActivity.launch(intent);
                        } else {
                            Intent i = new Intent(ExamTestCategory.this, ExamQuestActivity.class);
                            Bundle bundle = new Bundle();
                            bundle.putParcelable("test", listTestResult.get(position));
                            i.putExtras(bundle);
                            launchSomeActivity.launch(i);
                        }
                        if (haveAds) {
                            checkShowSuggestProDisplayAds();
                        }
                    }
                });
            }
        });

        if (!AppUtils.checkPortraitMode(this)) {
            android.widget.RelativeLayout rltList = findViewById(R.id.rltList);
            android.widget.RelativeLayout.LayoutParams layoutParams = (android.widget.RelativeLayout.LayoutParams) rltList.getLayoutParams();
            final int width = AppUtils.getWidthScreen(ExamTestCategory.this) > AppUtils.getHeightScreen(ExamTestCategory.this) ? AppUtils.getHeightScreen(ExamTestCategory.this) : AppUtils.getWidthScreen(ExamTestCategory.this);
            rltList.postDelayed(new Runnable() {
                @Override
                public void run() {
                    layoutParams.width = width;
                    rltList.setLayoutParams(layoutParams);
                    rltList.invalidate();
                    rltList.getParent().requestLayout();
                }
            }, 10);
        }
    }

    public void makeRandomTest() {
        Test testResult1 = new Test();
        testResult1.setFinish(false);
        testResult1.setCurrentQuest(0);
        testResult1.setTotalScore(0);
        int result = dataManager.insertTestResult(testResult1);
        if (result > 0) {
            ArrayList<QuestionVideoCategory> categories = dataManager.getAllQuestionCategory();
            ArrayList<QuestionVideo> listTestQuest = new ArrayList<QuestionVideo>();
            for (QuestionVideoCategory categorie : categories) {
                ArrayList<QuestionVideo> temps = categorie.getRamdomQuestion();
                for (QuestionVideo temp : temps) {
                    listTestQuest.add(temp);
                }
            }

            for (QuestionVideo testQuest : listTestQuest) {
                dataManager.insertTestQuest(testQuest, result);
            }

            listTestResult = dataManager.getTestResult();
            testAdapter.setData(listTestResult);
            testAdapter.notifyDataSetChanged();
        }
        recyclerView.scrollToPosition(testAdapter.getItemCount() - 1);
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (clickPos >= 0 && clickPos < listTestResult.size() - 1) {
            dataManager.updateTestByID(listTestResult.get(clickPos));
            testAdapter.notifyItemChanged(this.clickPos);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            dataManager.close();
        } catch (Exception e) {

        }
    }

    public void process_action(Intent data) {
        if (data.getStringExtra("action").equals("restart")) {
            Test test = data.getExtras().getParcelable("test");
            for (int i = 0; i < listTestResult.size(); i++) {
                if (listTestResult.get(i).getIdTest() == test.getIdTest()) {
                    clickPos = i;
                    break;
                }
            }
            dataManager.updateResetTestQuest(test);
            test.setFinish(false);
            test.setCurrentQuest(0);
            test.setCountQuestion(0);
            test.setTotalScore(0);
            dataManager.updateTestFinish(test);

            Intent intent = new Intent(ExamTestCategory.this, ExamQuestActivity.class);
            Bundle bundle = new Bundle();
            intent.putExtra("action", "restart");
            bundle.putParcelable("test", test);
            intent.putExtras(bundle);
            startActivity(intent);
        } else if (data.getStringExtra("action").equals("review")) {
            Test testResult = data.getExtras().getParcelable("test");
            for (int i = 0; i < listTestResult.size(); i++) {
                if (listTestResult.get(i).getIdTest() == testResult.getIdTest()) {
                    clickPos = i;
                    break;
                }
            }
            Intent intent = new Intent(ExamTestCategory.this, ExamQuestActivity.class);
            Bundle bundle = new Bundle();
            intent.putExtra("action", "review");
            bundle.putParcelable("test", testResult);
            intent.putExtras(bundle);
            startActivity(intent);
        } else if (data.getStringExtra("action").equals("next")) {
            Test testResult = data.getExtras().getParcelable("test");
            for (int i = 0; i < listTestResult.size(); i++) {
                if (listTestResult.get(i).getIdTest() == testResult.getIdTest()) {
                    listTestResult.set(i,testResult);
                    clickPos = i;
                    break;
                }
            }
            for (int i = clickPos; i < listTestResult.size(); i++) {
                if (!listTestResult.get(i).isFinish()) {
                    if (listTestResult.get(i).getCountQuestion() > 0) {
                        final int pos = i;
                        new DialogUtil.Builder(ExamTestCategory.this)
                                .title("Làm tiếp đề chưa hoàn thành")
                                .content("Bạn có muốn tiếp tục làm đề thi mô phỏng số " + (i + 1) + " chưa hoàn thành không?")
                                .positiveText(getResources().getString(R.string.text_ok))
                                .negativeText(getResources().getString(R.string.text_cancel))
                                .onPositive(new DialogUtil.SingleButtonCallback() {
                                    @Override
                                    public void onClick() {
                                        Intent intent = new Intent(ExamTestCategory.this, ExamQuestActivity.class);
                                        Bundle bundle = new Bundle();
                                        bundle.putParcelable("test", listTestResult.get(pos));
                                        intent.putExtras(bundle);
                                        launchSomeActivity.launch(intent);
                                    }
                                })
                                .show();
                    } else {
                        final int pos = i;
                        new DialogUtil.Builder(ExamTestCategory.this)
                                .title("Làm đề thi tiếp theo")
                                .content("Bạn có muốn làm đề thi mô phỏng số " + (i + 1) + " không?")
                                .positiveText(getResources().getString(R.string.text_ok))
                                .negativeText(getResources().getString(R.string.text_cancel))
                                .onPositive(new DialogUtil.SingleButtonCallback() {
                                    @Override
                                    public void onClick() {
                                        Intent intent = new Intent(ExamTestCategory.this, ExamQuestActivity.class);
                                        Bundle bundle = new Bundle();
                                        bundle.putParcelable("test", listTestResult.get(pos));
                                        intent.putExtras(bundle);
                                        launchSomeActivity.launch(intent);
                                    }
                                })
                                .show();
                    }

                    break;
                }
            }
        }
    }
}
