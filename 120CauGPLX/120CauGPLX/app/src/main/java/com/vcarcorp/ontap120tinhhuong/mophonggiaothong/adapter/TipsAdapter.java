package com.vcarcorp.ontap120tinhhuong.mophonggiaothong.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.net.Uri;
import android.os.Environment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.rey.material.widget.LinearLayout;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.R;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.model.QuestionVideo;
import com.ads.control.customview.CustomTextView;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.util.OnSingleClickListener;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.util.AppUtils;

import java.io.File;
import java.util.List;

public class TipsAdapter extends RecyclerView.Adapter<TipsAdapter.ViewHolder> {

    private List<QuestionVideo> data;
    private Context context;
    private ItemClickListener mClickListener;

    public TipsAdapter(final List<QuestionVideo> data, Context mContext) {
        this.data = data;
        this.context = mContext;
    }

    @Override
    public ViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
        this.context = parent.getContext();
        return new ViewHolder(LayoutInflater.from(context)
                .inflate(R.layout.item_tips, parent, false));
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, @SuppressLint("RecyclerView") int position) {
        final QuestionVideo questionVideo = data.get(position);
        holder.tv_index.setText(questionVideo.getName());
        holder.tv_tip.setText(questionVideo.getImageDescription());

        File fileImgDetail = new File(context.getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS).toString() + "/img_app/doneimg/" + "TH" + questionVideo.getId() + ".webp");
        if(fileImgDetail.exists()){
            Uri imageUri = Uri.fromFile(fileImgDetail);
            Glide.with(context)
                    .load(imageUri)
                    .into(holder.imgDesAnswer);
        }else{
            Glide.with(context)
                    .load(Uri.parse(AppUtils.getServerImg(context,questionVideo)))
                    .into(holder.imgDesAnswer);
        }
        holder.imgDesAnswer.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                if (mClickListener != null) mClickListener.onImageItemClick(v, position);
            }
        });
        holder.lnTip.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                if (mClickListener != null) mClickListener.onItemClick(v, position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private CustomTextView tv_index, tv_tip;
        private ImageView imgDesAnswer;
        private LinearLayout lnTip;

        public ViewHolder(View v) {
            super(v);
            tv_index = (CustomTextView) v.findViewById(R.id.tv_index);
            tv_tip = (CustomTextView) v.findViewById(R.id.tv_tip);
            imgDesAnswer = (ImageView) v.findViewById(R.id.imgDesAnswer);
            lnTip = v.findViewById(R.id.lnTip);
        }
    }

    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);
        void onImageItemClick(View view, int position);
    }
}