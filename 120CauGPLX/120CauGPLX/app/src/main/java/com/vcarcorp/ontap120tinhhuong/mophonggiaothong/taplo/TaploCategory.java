package com.vcarcorp.ontap120tinhhuong.mophonggiaothong.taplo;

import android.annotation.SuppressLint;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.View;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ads.control.admob.AdmobHelp;
import com.ads.control.customview.DialogUtil;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.R;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.database.DataManagerTaplo;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.main.BaseActivity;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.model.Taplo;

import java.util.ArrayList;

/**
 * Created by macbook on 10/31/17.
 */
public class TaploCategory extends BaseActivity {
    private DataManagerTaplo dataManager;
    private TaploAdapter testAdapter;
    private RecyclerView recyclerView;
    private ArrayList<Taplo> listTestResult;

    @SuppressLint("SourceLockedOrientationActivity")
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_taplo_category);
        AdmobHelp.getInstance().loadBanner(this);
        dataManager = new DataManagerTaplo(this);
        findViewById(R.id.btn_back).setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                onBackPressed();
            }
        });

        recyclerView = (RecyclerView) findViewById(R.id.list);
        recyclerView.setLayoutManager(new GridLayoutManager(this, 6));

        listTestResult = dataManager.getListTaplos();
        testAdapter = new TaploAdapter(listTestResult,this);
        recyclerView.setAdapter(testAdapter);
        testAdapter.setClickListener(new TaploAdapter.ItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                final Taplo taplo = listTestResult.get(position);

                new DialogUtil.Builder(TaploCategory.this)
                        .title(taplo.getNameTaplo())
                        .content(taplo.getDescription())
                        .doneText(getResources().getString(R.string.text_ok))
                        .show();
            }
        });
    }


    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            dataManager.close();
        }catch (Exception e){
        }
    }
}
