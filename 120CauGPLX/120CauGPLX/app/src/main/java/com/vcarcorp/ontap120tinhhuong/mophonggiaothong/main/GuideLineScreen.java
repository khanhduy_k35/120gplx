package com.vcarcorp.ontap120tinhhuong.mophonggiaothong.main;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import androidx.core.widget.NestedScrollView;
import com.rey.material.widget.FrameLayout;
import com.rey.material.widget.RelativeLayout;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.R;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.util.TyperTextView;
import com.ads.control.customview.CustomTextView;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.util.SharedPreferencesUtils;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.util.Util;

/**
 * Created by macbook on 10/31/17.
 */
public class GuideLineScreen extends BaseActivity {
    TyperTextView guide;
    CustomTextView guide1,guide2,guide3,guide4;
    private Animation anim,anim1,anim2,anim3,anim4;
    private RelativeLayout beginUsing;
    private NestedScrollView scrView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.b2_guilde_screen);
        scrView = findViewById(R.id.scrView);
        View padding = findViewById(R.id.padding);
        FrameLayout btn_back = findViewById(R.id.btn_back);
        btn_back.setVisibility(View.GONE);
        setUpToolBar((FrameLayout)findViewById(R.id.btn_back));
        findViewById(R.id.btn_back).setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                onBackPressed();
            }
        });
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            btn_back.setVisibility(View.VISIBLE);
            padding.setVisibility(View.VISIBLE);
        }

        guide = (TyperTextView) findViewById(R.id.guide);
        guide.setTextSize(13 + SharedPreferencesUtils.getInt("font_setting_size"));
        guide1 = (CustomTextView) findViewById(R.id.guide1);
        guide2 = (CustomTextView) findViewById(R.id.guide2);
        guide3 = (CustomTextView) findViewById(R.id.guide3);
        guide4 = (CustomTextView) findViewById(R.id.guide4);
        beginUsing = (RelativeLayout) findViewById(R.id.beginUsing);
        guide1.setVisibility(View.INVISIBLE);
        guide2.setVisibility(View.INVISIBLE);
        guide3.setVisibility(View.INVISIBLE);
        guide4.setVisibility(View.INVISIBLE);
        beginUsing.setVisibility(View.INVISIBLE);

        this.anim = AnimationUtils.loadAnimation(this, R.anim.welcome_fade_in);
        this.anim.setDuration(500L);
        this.guide.setVisibility(View.VISIBLE);
        this.guide.startAnimation(anim);
        this.anim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        guide1.setVisibility(View.VISIBLE);
                        guide1.startAnimation(anim1);
                    }
                },2000);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        this.anim1 = AnimationUtils.loadAnimation(this, R.anim.welcome_fade_in);
        this.anim1.setDuration(500L);
        this.anim1.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        guide2.setVisibility(View.VISIBLE);
                        guide2.startAnimation(anim2);
                    }
                },2000);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        this.anim2 = AnimationUtils.loadAnimation(this, R.anim.welcome_fade_in);
        this.anim2.setDuration(500L);
        this.anim2.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        guide3.setVisibility(View.VISIBLE);
                        guide3.startAnimation(anim3);
                    }
                },2000);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        this.anim3 = AnimationUtils.loadAnimation(this, R.anim.welcome_fade_in);
        this.anim3.setDuration(500L);
        this.anim3.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        guide4.setVisibility(View.VISIBLE);
                        guide4.startAnimation(anim4);
                    }
                },2000);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        this.anim4 = AnimationUtils.loadAnimation(this, R.anim.welcome_fade_in);
        this.anim4.setDuration(500L);
        this.anim4.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        beginUsing.setVisibility(View.VISIBLE);
                        scrView.post(new Runnable() {
                            @Override
                            public void run() {
                                scrView.fullScroll(View.FOCUS_DOWN);
                            }
                        });
                    }
                },2000);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        beginUsing.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle extras = getIntent().getExtras();
                if (extras != null) {
                    GuideLineScreen.this.finish();
                }else{
                    if(SharedPreferencesUtils.isShowProFisrt()) {
                        SharedPreferencesUtils.setBoolean(Util.FINISHGUIDE, true);
                        Intent intent = new Intent(GuideLineScreen.this, UnlockProActivity.class);
                        intent.putExtra("first_start", true);
                        GuideLineScreen.this.startActivity(intent);
                        GuideLineScreen.this.finish();
                    }else{
                        SharedPreferencesUtils.setBoolean(Util.FINISHGUIDE, true);
                        Intent intent = new Intent(GuideLineScreen.this, MainActivity.class);
                        intent.putExtra("first_start", true);
                        GuideLineScreen.this.startActivity(intent);
                        GuideLineScreen.this.finish();
                    }
                }
            }
        });
    }
}
