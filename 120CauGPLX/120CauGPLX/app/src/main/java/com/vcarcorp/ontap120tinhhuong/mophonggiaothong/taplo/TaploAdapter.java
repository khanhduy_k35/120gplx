package com.vcarcorp.ontap120tinhhuong.mophonggiaothong.taplo;

import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import androidx.recyclerview.widget.RecyclerView;

import com.ads.control.customview.CustomTextView;
import com.ads.control.utils.Utils;
import com.bumptech.glide.Glide;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.R;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.model.Taplo;

import java.util.List;

public class TaploAdapter extends RecyclerView.Adapter<TaploAdapter.ViewHolder> {

    private List<Taplo> data;
    private Context context;
    private ItemClickListener mClickListener;
    private RelativeLayout.LayoutParams params;

    public TaploAdapter(final List<Taplo> data, Context mContext) {
        this.data = data;
        this.context = mContext;
        int withItem = Utils.getWidthScreen(mContext) / 6;
        params = new RelativeLayout.LayoutParams(withItem,withItem + Utils.convertDpToPx(context, 35));
    }

    @Override
    public ViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
        this.context = parent.getContext();
        return new ViewHolder(LayoutInflater.from(context)
                .inflate(R.layout.item_taplo, parent, false));
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final Taplo item = data.get(position);
        holder.rltLayout.setLayoutParams(params);
        holder.tvDetail.setText((position + 1) + ": " + item.getNameTaplo());
        Glide.with(context)
                .load(Uri.parse("file:///android_asset/" + "icon_taplo/" + item.getIdTaplo() + ".webp"))
                .into(holder.imageView);

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private RelativeLayout rltLayout;
        private RelativeLayout item_layout;
        private ImageView imageView;
        CustomTextView tvDetail;
        public ViewHolder(View v) {
            super(v);
            imageView = (ImageView) v.findViewById(R.id.icon_taplo);
            rltLayout = (RelativeLayout)v.findViewById(R.id.rltLayout);
            item_layout = (RelativeLayout) v.findViewById(R.id.rltSkill);
            tvDetail = (CustomTextView) v.findViewById(R.id.tv_detail);
            item_layout.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition());
        }
    }
    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }

    public List<Taplo> getData() {
        return data;
    }

    public void setData(List<Taplo> data) {
        this.data = data;
    }
}