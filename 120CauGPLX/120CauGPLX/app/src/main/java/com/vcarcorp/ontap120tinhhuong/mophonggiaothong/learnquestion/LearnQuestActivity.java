package com.vcarcorp.ontap120tinhhuong.mophonggiaothong.learnquestion;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.DecelerateInterpolator;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.widget.SwitchCompat;
import androidx.core.content.ContextCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager2.widget.ViewPager2;

import com.airbnb.lottie.LottieAnimationView;
import com.rey.material.widget.RelativeLayout;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.R;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.adapter.QuestionResultAdapterRecycler;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.adapter.ViewPagerAdapterCustomLearningQuestion;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.database.GPLXDataManager;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.main.SelectSoundActivity;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.model.QuestionVideo;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.main.BaseActivity;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.model.QuestionCategory;
import com.ads.control.customview.CustomTextView;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.util.DialogUtil;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.util.customview.ImageViewButton;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.util.SharedPreferencesUtils;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.util.Util;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.util.AppUtils;
import com.takusemba.spotlight.OnSpotlightEndedListener;
import com.takusemba.spotlight.OnSpotlightStartedListener;
import com.takusemba.spotlight.OnTargetStateChangedListener;
import com.takusemba.spotlight.SimpleTarget;
import com.takusemba.spotlight.Spotlight;

import java.util.ArrayList;

/**
 * Created by duy on 10/23/17.
 */

public class LearnQuestActivity extends BaseActivity {
    private GPLXDataManager dataManager;
    private QuestionCategory questionCategory;
    private QuestionVideo questionVideo;
    private ArrayList<QuestionVideo> questionVideos;
    private ViewPager2 viewPager;
    private RecyclerView recyclerView;
    private QuestionResultAdapterRecycler questionResultAdapterRecycler;
    private ImageView imgNext,imgPrevios,imgBookMark;
    private CustomTextView bottomTitle;
    private int currentIndex,currentIndexFirst;
    private DrawerLayout drawer;
    private android.widget.RelativeLayout nav_view_right;
    private RelativeLayout rltShowScoreSpace;
    private SwitchCompat swShowScoreSpace;
    private ViewPagerAdapterCustomLearningQuestion viewPagerAdapterCustomLearningQuestion;
    private RelativeLayout btn_audio;
    private LottieAnimationView animSound;
    private View viewLine;


    @SuppressLint("SourceLockedOrientationActivity")
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(!AppUtils.checkPortraitMode(this)) {
            setForceDarkTheme();
        }
        setContentView(R.layout.activity_question_learn_screen);
        dataManager = new GPLXDataManager(this);
        Window window = this.getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | 0);
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(Color.TRANSPARENT);
        }
        questionCategory = (QuestionCategory) getIntent().getExtras().getParcelable("question_category");
        questionVideos = dataManager.getAllQuestionByCategory(questionCategory.getQuestionCategoryID(),this);
        questionVideo = (QuestionVideo) getIntent().getExtras().getParcelable("question");
        if(questionVideo!=null){
            for (int i = 0; i < questionVideos.size(); i++) {
                if(questionVideos.get(i).getId() == questionVideo.getId())
                {
                    questionCategory.setCurrentIndex(i);
                    break;
                }
            }
        }
        btn_audio = findViewById(R.id.btn_audio);
        animSound = findViewById(R.id.animSound);
        viewLine = findViewById(R.id.viewLine);
        currentIndexFirst = questionCategory.getCurrentIndex();
        bottomTitle = (CustomTextView)findViewById(R.id.bottomTitle);
        viewPager = (ViewPager2) findViewById(R.id.viewpager);
        recyclerView = (RecyclerView) findViewById(R.id.list);
        imgNext = (ImageView) findViewById(R.id.icon_next);
        imgPrevios = (ImageView) findViewById(R.id.icon_previous);
        imgBookMark = (ImageView)findViewById(R.id.icon_bookmark);
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.setScrimColor(getColor(R.color.shadow_light));
        swShowScoreSpace = findViewById(R.id.swShowScoreSpace);
        rltShowScoreSpace = findViewById(R.id.rltShowScoreSpace);
        setUpToolBar();
        if(Util.isPro(this)){
            if (SharedPreferencesUtils.showVoidEnable()) {
                viewLine.setVisibility(View.GONE);
            } else {
                viewLine.setVisibility(View.VISIBLE);
            }
        }else{
            if(questionCategory.getQuestionCategoryID() > 1){
                viewLine.setVisibility(View.VISIBLE);
            }else {
                if (SharedPreferencesUtils.showVoidEnableFree()) {
                    viewLine.setVisibility(View.GONE);
                } else {
                    viewLine.setVisibility(View.VISIBLE);
                }
            }
        }
        btn_audio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!Util.isPro(LearnQuestActivity.this) && questionCategory.getQuestionCategoryID() > 1){
                    new DialogUtil.Builder(LearnQuestActivity.this)
                            .title("Hướng dẫn học bằng âm thanh")
                            .content("Học qua các giọng đọc tự nhiên đầy cảm xúc, ghi nhớ tình huống rõ hơn. Bạn cần nâng cấp để sử dụng tính năng này và nhiều tính năng cao cấp khác.")
                            .doneText(getString(R.string.text_ok))
                            .onDone(new DialogUtil.SingleButtonCallback() {
                                @Override
                                public void onClick() {
                                    buyPro();
                                }
                            })
                            .show();
                }else{
                    if(Util.isPro(LearnQuestActivity.this)) {
                        SharedPreferencesUtils.setShowVoidEnable(!SharedPreferencesUtils.showVoidEnable());
                        if (SharedPreferencesUtils.showVoidEnable()) {
                            viewLine.setVisibility(View.GONE);
                        } else {
                            viewLine.setVisibility(View.VISIBLE);
                        }
                    }else{
                        SharedPreferencesUtils.setShowVoidEnableFree(!SharedPreferencesUtils.showVoidEnableFree());
                        if (SharedPreferencesUtils.showVoidEnableFree()) {
                            viewLine.setVisibility(View.GONE);
                        } else {
                            viewLine.setVisibility(View.VISIBLE);
                        }
                    }
                }
            }
        });

//        if(!Util.isPro(LearnQuestActivity.this)){
//            if(questionCategory.getQuestionCategoryID() < 2) {
//                swShowScoreSpace.setEnabled(true);
//            }else{
//                swShowScoreSpace.setEnabled(false);
//            }
//        }else{
//            swShowScoreSpace.setEnabled(true);
//        }
        swShowScoreSpace.setChecked(Util.isPro(this) ? SharedPreferencesUtils.showScoreSpace() : SharedPreferencesUtils.showScoreSpaceFree());
        swShowScoreSpace.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(Util.isPro(LearnQuestActivity.this)) {
                    SharedPreferencesUtils.setShowScoreSpace(b);
                }else{
                    SharedPreferencesUtils.setShowScoreSpaceFree(b);
                }
            }
        });
        rltShowScoreSpace.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                if(!Util.isPro(LearnQuestActivity.this) && questionCategory.getQuestionCategoryID() > 1){
//                    new DialogUtil.Builder(LearnQuestActivity.this)
//                            .title("Hiển thị ngay kết quả khi bấm Space.")
//                            .content("Tăng tốc quá trình học nhanh, biết kết quả ngay khi bấm Space. Bạn cần nâng cấp để sử dụng tính năng này và nhiều tính năng cao cấp khác.")
//                            .doneText(getString(R.string.text_ok))
//                            .onDone(new DialogUtil.SingleButtonCallback() {
//                                @Override
//                                public void onClick() {
//                                    buyPro();
//                                }
//                            })
//                            .show();
//                    return;
//                }
                if(!Util.isPro(LearnQuestActivity.this)) {
                    swShowScoreSpace.setChecked(!SharedPreferencesUtils.showScoreSpaceFree());
                }else{
                    swShowScoreSpace.setChecked(!SharedPreferencesUtils.showScoreSpace());
                }
            }
        });

        viewPagerAdapterCustomLearningQuestion = new ViewPagerAdapterCustomLearningQuestion(this,questionVideos,questionCategory);
        viewPager.setAdapter(viewPagerAdapterCustomLearningQuestion);
        currentIndex = questionCategory.getCurrentIndex();
        if (currentIndexFirst > 0) {
            viewPager.postDelayed(new Runnable() {
                @Override
                public void run() {
                    viewPager.setCurrentItem(currentIndexFirst,false);
                }
            },0);
        }
        viewPager.registerOnPageChangeCallback(new ViewPager2.OnPageChangeCallback() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                super.onPageScrolled(position, positionOffset, positionOffsetPixels);
            }

            @Override
            public void onPageSelected(int position) {
                super.onPageSelected(position);
                if(viewPager!=null){
                    ViewPagerAdapterCustomLearningQuestion adapter = (ViewPagerAdapterCustomLearningQuestion) viewPager.getAdapter();
                    if(adapter!=null) {
                        LearnQuestionFragment learnQuestionFragment = adapter.getFragment(currentIndex);
                        if (learnQuestionFragment != null)
                            learnQuestionFragment.stopVideo();
                        adapter.changeCheckedShowScore();
                    }
                }
                currentIndex = position;
                questionResultAdapterRecycler.notifyItemChanged(questionResultAdapterRecycler.getIndexSelection());
                questionResultAdapterRecycler.setIndexSelection(currentIndex);
                questionResultAdapterRecycler.notifyItemChanged(currentIndex);
                questionCategory.setCurrentIndex(position);
                bottomTitle.setText("Tình huống " + questionVideos.get(questionCategory.getCurrentIndex()).getId() + "/" + "120");
                dataManager.updateCategoryCurrenIndex(questionCategory, position);
                if(questionVideos.get(position).isMarked()){
                    imgBookMark.setColorFilter(getColor(R.color.primary));
                }else{
                    imgBookMark.setColorFilter(getColor(R.color.color_text_item_setting_dim));
                }
                if(currentIndex == 0){
                    imgPrevios.setColorFilter(getColor(R.color.color_text_item_setting_dim));
                }else{
                    imgPrevios.setColorFilter(getColor(R.color.primary));
                }
                if(currentIndex == questionVideos.size() - 1){
                    imgNext.setColorFilter(getColor(R.color.color_text_item_setting_dim));
                }else{
                    imgNext.setColorFilter(getColor(R.color.primary));
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {
                super.onPageScrollStateChanged(state);
            }
        });

        if(currentIndex == 0){
            imgPrevios.setColorFilter(getColor(R.color.color_text_item_setting_dim));
        }else{
            imgPrevios.setColorFilter(getColor(R.color.primary));
        }
        if(questionVideos.size() > 0 && currentIndex == questionVideos.size() - 1){
            imgNext.setColorFilter(getColor(R.color.color_text_item_setting_dim));
        }else{
            imgNext.setColorFilter(getColor(R.color.primary));
        }

        if(questionVideos.size() > 0 && questionVideos.get(questionCategory.getCurrentIndex()).isMarked()){
            imgBookMark.setColorFilter(getColor(R.color.primary));
        }else{
            imgBookMark.setColorFilter(getColor(R.color.color_text_item_setting_dim));
        }

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        questionResultAdapterRecycler = new QuestionResultAdapterRecycler(questionVideos,this);
        questionResultAdapterRecycler.setClickListener(new QuestionResultAdapterRecycler.ItemClickListener() {
            @Override
            public void onItemClick(View view, final int position) {
                viewPager.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if(viewPager!=null){
                            ViewPagerAdapterCustomLearningQuestion adapter = (ViewPagerAdapterCustomLearningQuestion) viewPager.getAdapter();
                            if(adapter!=null) {
                                LearnQuestionFragment learnQuestionFragment = adapter.getFragment(currentIndex);
                                if (learnQuestionFragment != null)
                                    learnQuestionFragment.stopVideo();
                            }
                        }
                        currentIndex = position;
                        viewPager.setCurrentItem(position,true);
                    }
                }, 0);
            }
        });
        recyclerView.setAdapter(questionResultAdapterRecycler);

        imgNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (currentIndex < questionVideos.size()-1){
                    if(viewPager!=null){
                        ViewPagerAdapterCustomLearningQuestion adapter = (ViewPagerAdapterCustomLearningQuestion) viewPager.getAdapter();
                        if(adapter!=null) {
                            LearnQuestionFragment learnQuestionFragment = adapter.getFragment(currentIndex);
                            if (learnQuestionFragment != null)
                                learnQuestionFragment.stopVideo();
                        }
                    }
                    currentIndex++;
                    viewPager.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if(viewPager!=null)
                                viewPager.setCurrentItem(currentIndex,true);
                        }
                    }, 10);
                }
            }
        });
        imgPrevios.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(currentIndex > 0){
                    if(viewPager!=null){
                        ViewPagerAdapterCustomLearningQuestion adapter = (ViewPagerAdapterCustomLearningQuestion) viewPager.getAdapter();
                        if(adapter!=null) {
                            LearnQuestionFragment learnQuestionFragment = adapter.getFragment(currentIndex);
                            if (learnQuestionFragment != null)
                                learnQuestionFragment.stopVideo();
                        }
                    }
                    currentIndex --;
                    viewPager.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if(viewPager!=null)
                                viewPager.setCurrentItem(currentIndex,true);
                        }
                    }, 10);
                }
            }
        });

        imgBookMark.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(questionVideos.size() == 0)
                    return;
                if(questionVideos.get(currentIndex).isMarked()) {
                    if(dataManager.updateQuestionMarked(questionVideos.get(currentIndex),false) > 0){
                        questionVideos.get(currentIndex).setMarked(false);
                    }
                }else{
                    if(dataManager.updateQuestionMarked(questionVideos.get(currentIndex),true) > 0){
                        questionVideos.get(currentIndex).setMarked(true);
                    }
                }

                if(questionVideos.get(currentIndex).isMarked()){
                    imgBookMark.setColorFilter(getColor(R.color.primary));
                }else{
                    imgBookMark.setColorFilter(getColor(R.color.color_text_item_setting_dim));
                }
            }
        });

        bottomTitle.setText("Tình huống " + questionVideos.get(questionCategory.getCurrentIndex()).getId() + "/" + "120");

        nav_view_right = findViewById(R.id.nav_view_right);
        if(!AppUtils.checkPortraitMode(this)) {
            final int width = AppUtils.getWidthScreen(LearnQuestActivity.this) < AppUtils.getHeightScreen(LearnQuestActivity.this) ? AppUtils.getHeightScreen(LearnQuestActivity.this) : AppUtils.getWidthScreen(LearnQuestActivity.this);
            nav_view_right.postDelayed(new Runnable() {
                @Override
                public void run() {
                    nav_view_right.getLayoutParams().width = width/2;
                    nav_view_right.requestLayout();
                }
            }, 10);
        }else{
            nav_view_right.postDelayed(new Runnable() {
                @Override
                public void run() {
                    nav_view_right.getLayoutParams().width = AppUtils.getWidthScreen(LearnQuestActivity.this) * 2 / 3;
                    nav_view_right.requestLayout();
                }
            }, 10);
        }
        showDialogGuide();
    }

    @Override
    public void onResume() {
        super.onResume();
        Window window = this.getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | 0);
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(Color.TRANSPARENT);
        }
    }

    public void startAudioAnimation(){
        if(animSound!=null) {
            animSound.playAnimation();
        }
    }

    public void stopAudioAnimation(){
        if(animSound!=null) {
            animSound.pauseAnimation();
            animSound.setFrame(0);
        }
    }

    public void showDialogGuide(){
        if(!SharedPreferencesUtils.getBoolean("DIALOG_LEARN_GUIDE")){
            final Dialog dialog = new Dialog(this);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            Window window = dialog.getWindow();
            window.setBackgroundDrawableResource(android.R.color.transparent);
            dialog.setCanceledOnTouchOutside(false);
            dialog.setCancelable(false);
            dialog.setContentView(R.layout.dialog_guide);
            dialog.findViewById(R.id.btn_ok).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.cancel();
                    dialog.dismiss();
                    SharedPreferencesUtils.setBoolean("DIALOG_LEARN_GUIDE",true);
                    showSpotLight();
                }
            });
            dialog.show();
        }
    }

    public void showSpotLight(){
        if(!SharedPreferencesUtils.getBoolean("GUIDE_BOOKMARK_2")){
            SharedPreferencesUtils.setBoolean("GUIDE_BOOKMARK_2",true);

            SimpleTarget menuTarget = new SimpleTarget.Builder(this)
                    .setPoint(findViewById(R.id.btn_list)) // position of the Target. setPoint(Point point), setPoint(View view) will work too.
                    .setRadius(64f) // radius of the Target
                    .setTitle(getResources().getString(R.string.guide_menu_title)) // title
                    .setDescription(getResources().getString(R.string.guide_menu_content)) // description
                    .setOnSpotlightStartedListener(new OnTargetStateChangedListener<SimpleTarget>() {
                        @Override
                        public void onStarted(SimpleTarget target) {
                            // do something
                        }
                        @Override
                        public void onEnded(SimpleTarget target) {
                            // do something
                        }
                    })
                    .build();

            SimpleTarget bookMarkTarget = new SimpleTarget.Builder(this)
                    .setPoint(imgBookMark) // position of the Target. setPoint(Point point), setPoint(View view) will work too.
                    .setRadius(64f) // radius of the Target
                    .setTitle(getResources().getString(R.string.guide_bookmark_title)) // title
                    .setDescription(getResources().getString(R.string.guide_bookmark_content)) // description
                    .setOnSpotlightStartedListener(new OnTargetStateChangedListener<SimpleTarget>() {
                        @Override
                        public void onStarted(SimpleTarget target) {
                            // do something
                        }
                        @Override
                        public void onEnded(SimpleTarget target) {
                            // do something
                        }
                    })
                    .build();

            ImageViewButton imgPlay = findViewById(R.id.imgPlay);

            SimpleTarget imgPlayTarget = new SimpleTarget.Builder(this)
                    .setPoint(imgPlay) // position of the Target. setPoint(Point point), setPoint(View view) will work too.
                    .setRadius(64f) // radius of the Target
                    .setTitle("Phát video") // title
                    .setDescription("Bấm vào đây để bắt đầu phát bài học và video bắt đầu được phát") // description
                    .setOnSpotlightStartedListener(new OnTargetStateChangedListener<SimpleTarget>() {
                        @Override
                        public void onStarted(SimpleTarget target) {
                            // do something
                        }
                        @Override
                        public void onEnded(SimpleTarget target) {
                            // do something
                        }
                    })
                    .build();

            ImageViewButton imgFull = findViewById(R.id.imgFullScreen);

            SimpleTarget imgFullTarget = new SimpleTarget.Builder(this)
                    .setPoint(imgFull) // position of the Target. setPoint(Point point), setPoint(View view) will work too.
                    .setRadius(64f) // radius of the Target
                    .setTitle("Xem toàn màn hình") // title
                    .setDescription("Bấm vào đây để xem video ở toàn màn hình. Hoặc bật điện thoại ở chế độ tự quay màn hình để xem full màn hình khi quay ngang điện thoại") // description
                    .setOnSpotlightStartedListener(new OnTargetStateChangedListener<SimpleTarget>() {
                        @Override
                        public void onStarted(SimpleTarget target) {
                            // do something
                        }
                        @Override
                        public void onEnded(SimpleTarget target) {
                            // do something
                        }
                    })
                    .build();


            ImageViewButton imgPause = findViewById(R.id.imgPause);

            SimpleTarget imgPauseTarget = new SimpleTarget.Builder(this)
                    .setPoint(imgPause) // position of the Target. setPoint(Point point), setPoint(View view) will work too.
                    .setRadius(64f) // radius of the Target
                    .setTitle("Dừng video") // title
                    .setDescription("Bấm vào đây để dừng phát video, lưu ý khi bạn dừng phát video thì bạn không thể bấm space chọn tình huống.") // description
                    .setOnSpotlightStartedListener(new OnTargetStateChangedListener<SimpleTarget>() {
                        @Override
                        public void onStarted(SimpleTarget target) {
                            // do something
                        }
                        @Override
                        public void onEnded(SimpleTarget target) {
                            // do something
                        }
                    })
                    .build();

            ImageViewButton imgRest = findViewById(R.id.imgReset);

            SimpleTarget imgResetTarget = new SimpleTarget.Builder(this)
                    .setPoint(imgRest) // position of the Target. setPoint(Point point), setPoint(View view) will work too.
                    .setRadius(64f) // radius of the Target
                    .setTitle("Học lại bài học") // title
                    .setDescription("Bấm vào đây để học lại bài học, video sẽ được phát lại từ đầu") // description
                    .setOnSpotlightStartedListener(new OnTargetStateChangedListener<SimpleTarget>() {
                        @Override
                        public void onStarted(SimpleTarget target) {
                            // do something
                        }
                        @Override
                        public void onEnded(SimpleTarget target) {
                            // do something
                        }
                    })
                    .build();

            TextView space = findViewById(R.id.tvSpaceText);
            SimpleTarget spaceTarget = new SimpleTarget.Builder(this)
                    .setPoint(space) // position of the Target. setPoint(Point point), setPoint(View view) will work too.
                    .setRadius(80f) // radius of the Target
                    .setTitle("Chọn tình huống") // title
                    .setDescription("Bấm vào đây để chọn thời điểm bắt đầu có dấu hiệu phát hiện ra tình huống nguy hiểm, lái xe cần xử lý. Lưu ý mỗi bài học chỉ được bấm Space 1 lần.") // description
                    .setOnSpotlightStartedListener(new OnTargetStateChangedListener<SimpleTarget>() {
                        @Override
                        public void onStarted(SimpleTarget target) {
                            // do something
                        }
                        @Override
                        public void onEnded(SimpleTarget target) {
                            // do something
                        }
                    })
                    .build();

            if(AppUtils.checkPortraitMode(this)){
                Spotlight.with(this)
                        .setOverlayColor(ContextCompat.getColor(LearnQuestActivity.this, R.color.bg_splotlight)) // background overlay color
                        .setDuration(1000L) // duration of Spotlight emerging and disappearing in ms
                        .setAnimation(new DecelerateInterpolator(2f)) // animation of Spotlight
                        .setTargets(menuTarget,bookMarkTarget,imgFullTarget,imgPlayTarget,imgPauseTarget,imgResetTarget,spaceTarget) // set targets. see below for more info
                        .setClosedOnTouchedOutside(true) // set if target is closed when touched outside
                        .setOnSpotlightStartedListener(new OnSpotlightStartedListener() { // callback when Spotlight starts
                            @Override
                            public void onStarted() {

                            }
                        })
                        .setOnSpotlightEndedListener(new OnSpotlightEndedListener() { // callback when Spotlight ends
                            @Override
                            public void onEnded() {

                            }
                        })
                        .start(); // start Spotlight
            }else{
                Spotlight.with(this)
                        .setOverlayColor(ContextCompat.getColor(LearnQuestActivity.this, R.color.bg_splotlight)) // background overlay color
                        .setDuration(1000L) // duration of Spotlight emerging and disappearing in ms
                        .setAnimation(new DecelerateInterpolator(2f)) // animation of Spotlight
                        .setTargets(menuTarget,bookMarkTarget,imgPlayTarget,imgPauseTarget,imgResetTarget,spaceTarget) // set targets. see below for more info
                        .setClosedOnTouchedOutside(true) // set if target is closed when touched outside
                        .setOnSpotlightStartedListener(new OnSpotlightStartedListener() { // callback when Spotlight starts
                            @Override
                            public void onStarted() {

                            }
                        })
                        .setOnSpotlightEndedListener(new OnSpotlightEndedListener() { // callback when Spotlight ends
                            @Override
                            public void onEnded() {

                            }
                        })
                        .start(); // start Spotlight
            }
        }
    }

    public void setUpToolBar() {
        TextView timeCounter = findViewById(R.id.timeCounter);
        String title = questionCategory.getQuestName();
        timeCounter.setText(title);
        if(questionVideos.size() > 0)
            bottomTitle.setText("Tình huống " + questionVideos.get(questionCategory.getCurrentIndex()).getId() + "/" + "120");

        com.rey.material.widget.FrameLayout btn_back = findViewById(R.id.btn_back);
        setUpToolBar(btn_back);
        btn_back.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                onBackPressed();
            }
        });
        com.rey.material.widget.FrameLayout btn_list = findViewById(R.id.btn_list);
        setUpToolBar(btn_list);
        btn_list.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                if(drawer!=null){
                    if(!drawer.isDrawerOpen(GravityCompat.START)) drawer.openDrawer(GravityCompat.END);
                    else drawer.closeDrawer(GravityCompat.END);
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else if (drawer.isDrawerOpen(GravityCompat.END)) {
            drawer.closeDrawer(GravityCompat.END);
        } else {
            if(!AppUtils.checkPortraitMode(this)){
                if (android.provider.Settings.System.getInt(getContentResolver(), Settings.System.ACCELEROMETER_ROTATION, 0) == 1){
                    super.onBackPressed();
                }else{
                    requestOritention(false);
                }
            }else {
                super.onBackPressed();
            }
        }
    }

    public void updateQuestion(QuestionVideo question, final int index){
        int result = dataManager.updateQuestionAnswer(question);
        if(result > 0){
            question.setMarked(questionVideos.get(index).isMarked());
            questionVideos.set(index,question);
        }

        questionResultAdapterRecycler.notifyItemChanged(index);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            dataManager.close();
        }catch (Exception e){

        }
        try {
            ViewPagerAdapterCustomLearningQuestion adapter = (ViewPagerAdapterCustomLearningQuestion) viewPager.getAdapter();
            if (adapter != null) {
                adapter.detroyAllVideo();
            }
        }catch (Exception e){

        }
    }

    public void destroyItem(int pos){
        if(viewPagerAdapterCustomLearningQuestion!=null){
            try {
                viewPagerAdapterCustomLearningQuestion.destroyItem(pos);
            }catch (Exception e){

            }
        }
    }

    public void disableViewPagerEvent(boolean action){
        if(viewPager != null){
            viewPager.setUserInputEnabled(action);
        }
    }

    public void requestOritention(boolean islandcape){
        if(islandcape){
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        }else{
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
    }

    public void showSuggestStopScoreAndSpace(){
        if(!SharedPreferencesUtils.getBoolean("show_suggest_stop_score")){
            SharedPreferencesUtils.setBoolean("show_suggest_stop_score",true);
            if(AppUtils.checkPortraitMode(this)){
                new DialogUtil.Builder(LearnQuestActivity.this)
                        .title("Dừng video ở thời điểm 5,4,3,2,1 điểm")
                        .content("Bạn có muốn dùng thử tính năng dừng video ở thời điểm 5,4,3,2,1 điểm giúp bạn nắm bắt thời điểm chính xác và ôn tập tốt hơn không?")
                        .positiveText(getResources().getString(R.string.text_ok))
                        .negativeText(getResources().getString(R.string.text_cancel))
                        .onPositive(new DialogUtil.SingleButtonCallback() {
                            @Override
                            public void onClick() {
                                SharedPreferencesUtils.setShowScoreStopFree(true);
                                if(viewPager!=null){
                                    ViewPagerAdapterCustomLearningQuestion adapter = (ViewPagerAdapterCustomLearningQuestion) viewPager.getAdapter();
                                    if(adapter!=null) {
                                        LearnQuestionFragment learnQuestionFragment = adapter.getFragment(currentIndex);
                                        adapter.changeCheckedShowScore();
                                    }
                                }
                            }
                        })
                        .show();
            }else{
                SwitchCompat swShowScore = findViewById(R.id.swShowScore);
                SimpleTarget swShowScoreTarget = new SimpleTarget.Builder(this)
                        .setPoint(swShowScore) // position of the Target. setPoint(Point point), setPoint(View view) will work too.
                        .setRadius(64f) // radius of the Target
                        .setTitle("Dừng video ở thời điểm 5,4,3,2,1 điểm") // title
                        .setDescription("Bấm vào đây để dừng video ở thời điểm 5,4,3,2,1 điểm giúp bạn nắm bắt thời điểm chính xác và ôn tập tốt hơn.") // description
                        .setOnSpotlightStartedListener(new OnTargetStateChangedListener<SimpleTarget>() {
                            @Override
                            public void onStarted(SimpleTarget target) {
                                // do something
                            }
                            @Override
                            public void onEnded(SimpleTarget target) {
                                // do something
                            }
                        })
                        .build();

                RelativeLayout btnGuide = findViewById(R.id.btn_guide);
                SimpleTarget btnGuideScoreTarget = new SimpleTarget.Builder(this)
                        .setPoint(btnGuide) // position of the Target. setPoint(Point point), setPoint(View view) will work too.
                        .setRadius(64f) // radius of the Target
                        .setTitle("Xem lại mẹo và hướng dẫn") // title
                        .setDescription("Bấm vào đây để xem lại mẹo và hướng dẫn.") // description
                        .setOnSpotlightStartedListener(new OnTargetStateChangedListener<SimpleTarget>() {
                            @Override
                            public void onStarted(SimpleTarget target) {
                                // do something
                            }
                            @Override
                            public void onEnded(SimpleTarget target) {
                                // do something
                            }
                        })
                        .build();

                Spotlight.with(this)
                        .setOverlayColor(ContextCompat.getColor(LearnQuestActivity.this, R.color.bg_splotlight)) // background overlay color
                        .setDuration(1000L) // duration of Spotlight emerging and disappearing in ms
                        .setAnimation(new DecelerateInterpolator(2f)) // animation of Spotlight
                        .setTargets(swShowScoreTarget,btnGuideScoreTarget) // set targets. see below for more info
                        .setClosedOnTouchedOutside(true) // set if target is closed when touched outside
                        .setOnSpotlightStartedListener(new OnSpotlightStartedListener() { // callback when Spotlight starts
                            @Override
                            public void onStarted() {

                            }
                        })
                        .setOnSpotlightEndedListener(new OnSpotlightEndedListener() { // callback when Spotlight ends
                            @Override
                            public void onEnded() {

                            }
                        })
                        .start(); // start Spotlight
            }
        }else{
            if(!SharedPreferencesUtils.getBoolean("show_suggest_stop_space")) {
                SharedPreferencesUtils.setBoolean("show_suggest_stop_space", true);
                new DialogUtil.Builder(LearnQuestActivity.this)
                        .title("Tăng tốc quá trình học")
                        .content("Bạn có muốn dùng thử tính năng hiển thị kết quả ngay sau khi bấm phím Space không?")
                        .positiveText(getResources().getString(R.string.text_ok))
                        .negativeText(getResources().getString(R.string.text_cancel))
                        .onPositive(new DialogUtil.SingleButtonCallback() {
                            @Override
                            public void onClick() {
                                swShowScoreSpace.setChecked(true);
                            }
                        })
                        .show();
            }else if(!SharedPreferencesUtils.getBoolean("show_suggest_change_voice")) {
                SharedPreferencesUtils.setBoolean("show_suggest_change_voice", true);
                new DialogUtil.Builder(LearnQuestActivity.this)
                        .title("Trợ lý giọng nói đầy cảm xúc")
                        .content("Có rất nhiều giọng đọc nam, nữ đủ 3 miền đầy cảm xúc.Bạn có muốn dùng thử tính năng này không?")
                        .positiveText(getResources().getString(R.string.text_ok))
                        .negativeText(getResources().getString(R.string.text_cancel))
                        .onPositive(new DialogUtil.SingleButtonCallback() {
                            @Override
                            public void onClick() {
                                Intent intent = new Intent(LearnQuestActivity.this, SelectSoundActivity.class);
                                startActivity(intent);
                            }
                        })
                        .show();
            }
        }
    }

    public boolean isEnableAudio(){
        if(viewLine ==null)
            return false;
        else
            return (viewLine.getVisibility() == View.VISIBLE) ? false : true;
    }
}
