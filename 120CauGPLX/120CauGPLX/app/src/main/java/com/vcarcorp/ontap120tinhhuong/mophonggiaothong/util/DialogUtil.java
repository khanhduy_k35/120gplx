package com.vcarcorp.ontap120tinhhuong.mophonggiaothong.util;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.UiThread;
import androidx.fragment.app.FragmentActivity;

import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.R;
import com.ads.control.customview.CustomTextView;

public class DialogUtil {
    private Dialog dialog;

    @SuppressLint("InflateParams")
    protected DialogUtil(final Builder builder) {
        dialog = new Dialog(builder.mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Window window = dialog.getWindow();
        window.setBackgroundDrawableResource(android.R.color.transparent);
        dialog.setCanceledOnTouchOutside(builder.canceledOnTouchOutside);
        dialog.setCancelable(builder.cancelable);
        dialog.setContentView(R.layout.ko_exit_dialog);
        dialog.getWindow().setGravity(Gravity.BOTTOM);
        if(AppUtils.checkPortraitMode(builder.mContext)) {
            dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        }else{
            try{
                int width = AppUtils.getWidthScreen((FragmentActivity) builder.mContext) > AppUtils.getHeightScreen((FragmentActivity)builder.mContext) ? AppUtils.getHeightScreen((FragmentActivity)builder.mContext) : AppUtils.getWidthScreen((FragmentActivity)builder.mContext);
                dialog.getWindow().setLayout(width, LinearLayout.LayoutParams.WRAP_CONTENT);
            }catch (Exception e){
                dialog.getWindow().setLayout(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            }
        }
        final CustomTextView btn_quit = (CustomTextView) dialog.findViewById(R.id.btn_cancel);
        final CustomTextView btn_ok = (CustomTextView) dialog.findViewById(R.id.btn_ok);
        final CustomTextView btn_done = (CustomTextView) dialog.findViewById(R.id.btn_done);
        final CustomTextView btn_title = (CustomTextView) dialog.findViewById(R.id.title_dialog);
        final CustomTextView btn_content = (CustomTextView) dialog.findViewById(R.id.content_dialog);
        final LinearLayout ln_done = (LinearLayout) dialog.findViewById(R.id.ln_done);
        final LinearLayout ln_ok_cancel = (LinearLayout) dialog.findViewById(R.id.lnOKCancel);
        final LinearLayout lnCustomContent = dialog.findViewById(R.id.lnCustomContent);
        btn_title.setText(builder.title);
        btn_content.setText(builder.content);

        if(builder.title!=null && !builder.title.isEmpty()){
            btn_title.setVisibility(View.VISIBLE);
            btn_title.setText(builder.title);
        }else{
            btn_title.setVisibility(View.GONE);
        }

        if(builder.content!=null && !builder.content.isEmpty()){
            btn_content.setVisibility(View.VISIBLE);
            btn_content.setText(builder.content);
        }else{
            btn_content.setVisibility(View.GONE);
        }

        if(builder.positiveText!=null && !builder.positiveText.isEmpty()){
            btn_ok.setVisibility(View.VISIBLE);
            btn_ok.setText(builder.positiveText);
        }else{
            btn_ok.setVisibility(View.INVISIBLE);
        }

        if(builder.negativeText!=null && !builder.negativeText.isEmpty()){
            btn_quit.setVisibility(View.VISIBLE);
            btn_quit.setText(builder.negativeText);
        }else{
            btn_quit.setVisibility(View.INVISIBLE);
        }

        if(builder.doneText!=null && !builder.doneText.isEmpty()){
            btn_done.setVisibility(View.VISIBLE);
            btn_done.setText(builder.doneText);
        }else{
            btn_done.setVisibility(View.INVISIBLE);
        }

        if(builder.isHaveDone()){
            ln_done.setVisibility(View.VISIBLE);
            ln_ok_cancel.setVisibility(View.GONE);
        }else{
            ln_ok_cancel.setVisibility(View.VISIBLE);
            ln_done.setVisibility(View.GONE);
        }

        if(builder.customView!=null){
            lnCustomContent.setVisibility(View.VISIBLE);
            lnCustomContent.addView(builder.customView,new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.MATCH_PARENT));
        }else {
            lnCustomContent.setVisibility(View.GONE);
        }


        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(dialog!=null) {
                    if (dialog.isShowing()) {
                        builder.eventBtn = true;
                        dialog.dismiss();
                        dialog.cancel();
                    }
                }
                if(builder.positiveClick!=null){
                    builder.positiveClick.onClick();
                }
            }
        });

        btn_quit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(dialog!=null) {
                    if (dialog.isShowing()) {
                        builder.eventBtn = true;
                        dialog.dismiss();
                        dialog.cancel();
                    }
                }
                if(builder.negativeClick!=null){
                    builder.negativeClick.onClick();
                }
            }
        });

        btn_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(dialog!=null) {
                    if (dialog.isShowing()) {
                        builder.eventBtn = true;
                        dialog.dismiss();
                        dialog.cancel();
                    }
                }
                if(builder.doneClick!=null){
                    builder.doneClick.onClick();
                }
            }
        });

        dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {
                if(builder.onCancelListener!=null){
                    if(!builder.eventBtn)
                        builder.onCancelListener.OnCancel();
                    builder.eventBtn = false;
                }
            }
        });

        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                if(builder.onDismissListener!=null){
                    if(!builder.eventBtn)
                        builder.onDismissListener.OnDismiss();
                    builder.eventBtn = false;
                }
            }
        });
    }

    public void show(){
        if(dialog!=null){
            if(dialog.isShowing())
                dialog.dismiss();
            dialog.show();
        }
    }

    public static class Builder {
        private Context mContext;
        private String title;
        private String content;
        private boolean cancelable = true;
        private boolean canceledOnTouchOutside = true;
        private String positiveText; //text phải
        private String negativeText; //text trái
        private SingleButtonCallback positiveClick;
        private SingleButtonCallback negativeClick;
        private SingleButtonCallback doneClick;
        private OnCancelListener onCancelListener;
        private OnDismissListener onDismissListener;
        private String doneText;
        private boolean isHaveDone;
        private View customView;
        private boolean eventBtn;

        public Builder(@NonNull Context context) {
            this.mContext = context;
            this.positiveText = context.getResources().getString(R.string.text_ok);
            this.negativeText = context.getResources().getString(R.string.text_cancel);
            this.doneText = context.getResources().getString(R.string.text_ok);
            this.isHaveDone = false;
            this.customView = null;
        }

        public Builder title(@NonNull String title) {
            this.title = title;
            return this;
        }

        public Builder content(@NonNull String content) {
            this.content = content;
            return this;
        }

        public Builder cancelable(boolean cancelable){
            this.cancelable = cancelable;
            return this;
        }

        public Builder canceledOnTouchOutside(boolean canceledOnTouchOutside){
            this.canceledOnTouchOutside = canceledOnTouchOutside;
            return this;
        }

        public Builder positiveText(String positiveText){
            this.positiveText = positiveText;
            return this;
        }

        public Builder negativeText(String negativeText){
            this.negativeText = negativeText;
            return this;
        }

        public Builder doneText(String doneText){
            this.isHaveDone = true;
            this.doneText = doneText;
            return this;
        }

        public Builder onPositive(SingleButtonCallback positiveClick){
            this.positiveClick = positiveClick;
            return this;
        }

        public Builder onNegative(SingleButtonCallback negativeClick){
            this.negativeClick = negativeClick;
            return this;
        }

        public Builder onDone(SingleButtonCallback doneClick){
            this.doneClick = doneClick;
            return this;
        }

        public Builder setCustomView(View customView){
            this.customView = customView;
            return this;
        }

        public Builder onDismissListener(OnDismissListener onDismissListener){
            this.onDismissListener = onDismissListener;
            return this;
        }

        public Builder OnCancelListener(OnCancelListener onCancelListener){
            this.onCancelListener = onCancelListener;
            return this;
        }

        public boolean isHaveDone() {
            return isHaveDone;
        }

        public void setHaveDone(boolean haveDone) {
            isHaveDone = haveDone;
        }

        @UiThread
        public DialogUtil build() {
            return new DialogUtil(this);
        }

        @UiThread
        public DialogUtil show() {
            DialogUtil dialog = build();
            dialog.show();
            return dialog;
        }
    }

    public interface SingleButtonCallback {
        void onClick();
    }

    public interface OnDismissListener {
        void OnDismiss();
    }

    public interface OnCancelListener {
        void OnCancel();
    }
}
