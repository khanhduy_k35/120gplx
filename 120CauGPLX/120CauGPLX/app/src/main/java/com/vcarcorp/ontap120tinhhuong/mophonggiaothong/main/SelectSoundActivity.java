package com.vcarcorp.ontap120tinhhuong.mophonggiaothong.main;

import static com.google.android.exoplayer2.ExoPlayer.STATE_BUFFERING;
import static com.google.android.exoplayer2.ExoPlayer.STATE_ENDED;
import static com.google.android.exoplayer2.ExoPlayer.STATE_READY;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import com.danikula.videocache.CacheListener;
import com.danikula.videocache.HttpProxyCacheServer;
import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.rey.material.widget.FrameLayout;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.App;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.R;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.adapter.VoiceAdapter;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.database.AudioDataManager;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.model.VoiceModel;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.util.Util;

import java.io.File;
import java.util.ArrayList;

public class SelectSoundActivity extends BaseActivity {

    private ArrayList<VoiceModel> listVoice;
    private AudioDataManager dataManager;
    private RecyclerView recyclerView;
    private VoiceAdapter voiceAdapter;
    protected ExoPlayer exoPlayerAudio;
    private int previousPos = -1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_sound);
        Window window = this.getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | 0);
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(Color.TRANSPARENT);
        }
        setUpToolBar();
        dataManager =  new AudioDataManager(this);
        recyclerView = (RecyclerView) findViewById(R.id.list);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        listVoice = dataManager.getVoice();
        voiceAdapter = new VoiceAdapter(listVoice,this);

        recyclerView.setAdapter(voiceAdapter);

        voiceAdapter.setClickListener(new VoiceAdapter.ItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                Util.setVoiceCode(listVoice.get(position).getVoice_code());
                voiceAdapter.setVoiceCodeSelect(listVoice.get(position).getVoice_code());
                voiceAdapter.notifyDataSetChanged();
            }

            @Override
            public void onItemPlayClick(View view, int position) {
                if(previousPos!=position){
                    stopAudioAnimation(previousPos);
                    previousPos = position;
                }
                playAudio("https://firebasestorage.googleapis.com/v0/b/tinhhuonggplx.appspot.com/o/audio%2F" + listVoice.get(position).getVoice_code() + "%2F" + listVoice.get(position).getVoice_code() + "_name_1.mp3?alt=media&token=b2257a77-c716-431d-a3f0-086f3e045d76",position);
            }
        });
        voiceAdapter.setVoiceCodeSelect(Util.getVoiceCode());
    }

    public void setUpToolBar(){
        setUpToolBar((FrameLayout)findViewById(R.id.btn_back));
        setUpToolBar((FrameLayout)findViewById(R.id.btn_guide));
        findViewById(R.id.btn_back).setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                onBackPressed();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        destroyAudio();
        try {
            dataManager.close();
        }catch (Exception e){

        }
    }

    private void playAudio(String urlAudio,int pos){
        destroyAudio();
        HttpProxyCacheServer cacheServer = App.getCacheServer(this);
        String proxyVideoUrl = cacheServer.getProxyUrl(urlAudio, true);
        if (cacheServer.isCached(urlAudio)) {
            Log.e("khanhduy.le", "Playing audio Offline");
        } else {
            cacheServer.registerCacheListener(new CacheListener() {
                @Override
                public void onCacheAvailable(File cacheFile, String url, int percentsAvailable) {
                    Log.e("khanhduy.le", String.format("Playing audio Online: Downloaded %d%%", percentsAvailable));
                    if (percentsAvailable == 100) {
                        Log.e("khanhduy.le", "Playing audio Offline");
                    }
                }
            }, urlAudio);
        }
        exoPlayerAudio = ExoPlayerFactory.newSimpleInstance(this,new DefaultTrackSelector(),new DefaultLoadControl());
        String userAgent = com.google.android.exoplayer2.util.Util.getUserAgent(this,"Play Audio");
        ExtractorMediaSource extractorMediaSource = new ExtractorMediaSource(
                Uri.parse(proxyVideoUrl),
                new DefaultDataSourceFactory(SelectSoundActivity.this,userAgent),
                new DefaultExtractorsFactory(),null,null
        );
        exoPlayerAudio.prepare(extractorMediaSource);
        exoPlayerAudio.setPlayWhenReady(true);
        exoPlayerAudio.addListener(new ExoPlayer.EventListener() {
            @Override
            public void onTimelineChanged(Timeline timeline, Object manifest) {

            }

            @Override
            public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {

            }

            @Override
            public void onLoadingChanged(boolean isLoading) {

            }

            @Override
            public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
                if(playWhenReady && playbackState == STATE_READY){
                    startAudioAnimation(pos);
                }
                if(playWhenReady && playbackState == STATE_BUFFERING){
                    startAudioAnimation(pos);
                }
                if(playWhenReady && playbackState == STATE_ENDED){
                    stopAudioAnimation(pos);
                }
            }

            @Override
            public void onPlayerError(ExoPlaybackException error) {

            }

            @Override
            public void onPositionDiscontinuity() {

            }
        });
    }

    public void startAudioAnimation(int pos){
        if(voiceAdapter!=null) {
            voiceAdapter.startAudioAnimation(pos);
            voiceAdapter.notifyItemChanged(pos);
        }
    }

    public void stopAudioAnimation(int pos){
        if(voiceAdapter!=null) {
            voiceAdapter.stopAudioAnimation(pos);
            voiceAdapter.notifyItemChanged(pos);
        }
    }

    protected void destroyAudio(){
        try {
            stopAudioAnimation(-1);
            if(exoPlayerAudio!=null) {
                exoPlayerAudio.setPlayWhenReady(false);
            }
            if(exoPlayerAudio!=null) {
                exoPlayerAudio.release();
                exoPlayerAudio = null;
            }
        }catch (Exception e){

        }
    }
}