package com.vcarcorp.ontap120tinhhuong.mophonggiaothong;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.os.Environment;

import androidx.multidex.MultiDex;

import com.ads.control.AdsApplication;
import com.danikula.videocache.HttpProxyCacheServer;
import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.main.SplashActivity;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.util.MyFileNameGenerator;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.util.SharedPreferencesUtils;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.util.Util;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

public class App extends AdsApplication{
    static App application;
    private HttpProxyCacheServer cacheServer;

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    public static App getInstance() {
        return application;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        application = this;
        FirebaseCrashlytics.getInstance().setCrashlyticsCollectionEnabled(!BuildConfig.DEBUG);
        SharedPreferencesUtils.init(this);
        SharedPreferencesUtils.setBoolean("show_banner_fan_first",false);
        if(SharedPreferencesUtils.getInt("font_setting_size",-1) == -1){
            SharedPreferencesUtils.setInt("font_setting_size",4);
        }
    }

    public static HttpProxyCacheServer getCacheServer(Context context) {
        if(application.cacheServer == null) application.cacheServer = application.buildHttpCacheServer();
        return application.cacheServer;
    }

    private HttpProxyCacheServer buildHttpCacheServer() {
        return new HttpProxyCacheServer.Builder(this)
                .fileNameGenerator(new MyFileNameGenerator(getApplicationContext()))
                .cacheDirectory(getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS))
                .build();
    }
}