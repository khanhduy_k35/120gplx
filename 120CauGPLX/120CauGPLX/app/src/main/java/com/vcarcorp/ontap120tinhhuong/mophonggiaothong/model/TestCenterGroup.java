package com.vcarcorp.ontap120tinhhuong.mophonggiaothong.model;

import java.util.ArrayList;

public class TestCenterGroup {
    public ArrayList<TestCenter> testCenters = new ArrayList<>();
    public String name;

    public TestCenterGroup() {
    }

    public ArrayList<TestCenter> getTestCenters() {
        return testCenters;
    }

    public void setTestCenters(ArrayList<TestCenter> testCenters) {
        this.testCenters = testCenters;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
