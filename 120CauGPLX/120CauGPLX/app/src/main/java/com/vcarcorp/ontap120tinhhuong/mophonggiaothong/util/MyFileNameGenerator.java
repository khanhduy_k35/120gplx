package com.vcarcorp.ontap120tinhhuong.mophonggiaothong.util;

import android.content.Context;
import android.webkit.URLUtil;

import com.danikula.videocache.file.FileNameGenerator;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.main.JNIUtil;

public class MyFileNameGenerator implements FileNameGenerator {
    private Context context;

    public MyFileNameGenerator(Context context){
        this.context = context;
    }

    @Override
    public String generate(String url) {
        String file1 = URLUtil.guessFileName(url, null, null);
        String fileExtension = getExt(file1);
        String fileName = getName(file1);
        String fileReturn = "";
        try {
            fileReturn = correctFileName(AESCrypt.encrypt(JNIUtil.apiKey(context), fileName) + "." + fileExtension);
        }catch (Exception e){
            fileReturn = file1;
        }
        return fileReturn;
    }

    public String generateFileName(String file) {
        if(file.contains("/") && file.endsWith("/"))
            return file;
        String file1 = "";
        if(file.contains("/") && !file.endsWith("/")){
            String[] arr = file.split("/");
            file1 = arr[arr.length - 1];
        }else{
            file1 = file;
        }
        String fileExtension = getExt(file1);
        String fileName = getName(file1);
        String fileReturn = "";
        try {
            fileReturn = correctFileName(AESCrypt.encrypt(JNIUtil.apiKey(context), fileName) + "." + fileExtension);
        }catch (Exception e){
            fileReturn = file;
        }
        return fileReturn;
    }

    public String correctFileName(String filename){
        return filename.replaceAll("[\\\\/:*?\"<>|]", "");
    }

    public String getExt(String filePath){
        int strLength = filePath.lastIndexOf(".");
        if(strLength > 0)
            return filePath.substring(strLength + 1);
        return null;
    }

    public String getName(String filePath){
        int strLength = filePath.lastIndexOf(".");
        if(strLength > 0)
            return filePath.substring(0,strLength);
        return null;
    }
}
