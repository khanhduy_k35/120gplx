package com.vcarcorp.ontap120tinhhuong.mophonggiaothong.moredriver.testcenter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.text.Spannable;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.ads.control.customview.CustomTextView;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.R;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.model.TestCenter;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.util.OnSingleClickListener;

import java.util.ArrayList;
import java.util.Locale;

public class TestCenterAdapter extends RecyclerView.Adapter<TestCenterAdapter.ViewHolder> {

    private ArrayList<TestCenter> data = new ArrayList<>();
    private Context context;
    private ItemClickListener mClickListener;
    private String searchString = "";

    public TestCenterAdapter(Context mContext) {
        this.data = data;
        this.context = mContext;
    }

    public TestCenterAdapter(final ArrayList<TestCenter> data, Context mContext) {
        this.data = data;
        this.context = mContext;
    }

    @Override
    public ViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
        this.context = parent.getContext();
        return new ViewHolder(LayoutInflater.from(context)
                .inflate(R.layout.item_test_center, parent, false));
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, @SuppressLint("RecyclerView") int position) {
        TestCenter testCenter = data.get(position);
        holder.tvPhone.setText(testCenter.getPhoneNumber());
        if (position == data.size() - 1) {
            holder.viewLine.setVisibility(View.GONE);
        } else {
            holder.viewLine.setVisibility(View.VISIBLE);
        }

        String search = testCenter.getName().toLowerCase(Locale.getDefault());
        if (!searchString.equals("") && search.contains(searchString)) {
            int startPos = search.indexOf(searchString);
            int endPos = startPos + searchString.length();
            if (endPos > search.length()) {
                endPos = search.length();
            }
            Spannable spanText = Spannable.Factory.getInstance().newSpannable(testCenter.getName());
            spanText.setSpan(new ForegroundColorSpan(context.getResources().getColor(R.color.red)), startPos, endPos, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

            holder.tvName.setText(spanText, TextView.BufferType.SPANNABLE);
        } else {
            holder.tvName.setText(testCenter.getName());
        }

        search = testCenter.getAddress().toLowerCase(Locale.getDefault());
        if (search!=null && searchString !=null &&!searchString.equals("") && search.contains(searchString)) {
            int startPos = search.indexOf(searchString);
            int endPos = startPos + searchString.length();
            if (endPos > search.length()) {
                endPos = search.length();
            }
            Spannable spanText = Spannable.Factory.getInstance().newSpannable(testCenter.getAddress());
            spanText.setSpan(new ForegroundColorSpan(context.getResources().getColor(R.color.red)), startPos, endPos, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

            holder.tvLocation.setText(spanText, TextView.BufferType.SPANNABLE);
        } else {
            holder.tvLocation.setText(testCenter.getAddress());
        }

    }

    public void setTextFilter(String search) {
        this.searchString = search;
        notifyDataSetChanged();
    }

    public void notifyData(ArrayList<TestCenter> data) {
        this.data = data;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private CustomTextView tvName, tvPhone, tvLocation;
        private LinearLayout lnView;
        private View viewLine;

        public ViewHolder(View v) {
            super(v);
            tvName = (CustomTextView) v.findViewById(R.id.tv_name);
            tvPhone = (CustomTextView) v.findViewById(R.id.tv_phone);
            tvLocation = (CustomTextView) v.findViewById(R.id.tv_location);
            lnView = (LinearLayout) v.findViewById(R.id.lnView);
            viewLine = (View) v.findViewById(R.id.view_line);
            lnView.setOnClickListener(new OnSingleClickListener() {
                @Override
                public void onSingleClick(View v) {
                    mClickListener.onItemClick(data.get(getAdapterPosition()).getPhoneNumber());
                }
            });
        }
    }

    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    public interface ItemClickListener {
        void onItemClick(String phoneNumber);
    }
}
