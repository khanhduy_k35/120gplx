package com.vcarcorp.ontap120tinhhuong.mophonggiaothong.util.customview;

import android.content.Context;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.View;

import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

public class UnobtrusiveDrawerLand extends DrawerLayout {

    public UnobtrusiveDrawerLand(Context context) {
        super(context);
    }

    public UnobtrusiveDrawerLand(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public UnobtrusiveDrawerLand(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        return super.onTouchEvent(ev);
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        View drawer = getChildAt(1);
        if(!isDrawerOpen(drawer)){
            return super.onInterceptTouchEvent(ev);
        }else{
            if (ev.getRawX() < getWidhtScreen() - drawer.getWidth()) {
                try {
                    drawer.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            closeDrawer(GravityCompat.END);
                        }
                    },10);
                }catch (Exception e){

                }
                return true;
            }else if((ev.getRawX() > getWidhtScreen() - drawer.getWidth()) && ev.getRawY() < (getActionbarHeight() + getStatusBarHeight())){
                try {
                    drawer.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            closeDrawer(GravityCompat.END);
                        }
                    },10);
                }catch (Exception e){

                }
                return true;
            }else if((ev.getRawX() > getWidhtScreen() - drawer.getWidth()) && ev.getRawY() > (getHeightScreen() - getActionbarHeight())){
                try {
                    drawer.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            closeDrawer(GravityCompat.END);
                        }
                    },10);
                }catch (Exception e){

                }
                return true;
            }
            else {
                return false;
            }
        }
    }

    public int getActionbarHeight(){
        TypedValue tv = new TypedValue();
        if (getContext().getTheme().resolveAttribute(android.R.attr.actionBarSize, tv, true)) {
            int actionBarHeight = TypedValue.complexToDimensionPixelSize(tv.data, getResources().getDisplayMetrics());
            return actionBarHeight;
        }
        return 0;
    }

    public int getStatusBarHeight() {
        int result = 0;
        int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

    public int getHeightScreen() {
        DisplayMetrics metrics = getContext().getResources().getDisplayMetrics();
        final int w = metrics.heightPixels;
        return w;
    }

    public int getWidhtScreen() {
        DisplayMetrics metrics = getContext().getResources().getDisplayMetrics();
        final int w = metrics.widthPixels;
        return w;
    }
}