package com.vcarcorp.ontap120tinhhuong.mophonggiaothong.util.customview;

import android.content.Context;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.View;
import androidx.drawerlayout.widget.DrawerLayout;

import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.util.Util;

public class UnobtrusiveDrawer extends DrawerLayout {

    public UnobtrusiveDrawer(Context context) {
        super(context);
    }

    public UnobtrusiveDrawer(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public UnobtrusiveDrawer(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        return super.onTouchEvent(ev);
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        View drawer = getChildAt(1);
        if(!isDrawerOpen(drawer)){
            if (ev.getRawX() < getWidhtScreen() - 40) {
                return false;
            } else {
                return true;
            }
        }else{
            if (ev.getRawX() < getWidhtScreen() - drawer.getWidth()) {
                closeDrawer(drawer);
                return true;
            }else if((ev.getRawX() > getWidhtScreen() - drawer.getWidth()) && ev.getRawY() < Util.convertDpToPx(getContext(),80)){
                closeDrawer(drawer);
                return true;
            }else if((ev.getRawX() > getWidhtScreen() - drawer.getWidth()) && ev.getRawY() > (getHeightScreen() - Util.convertDpToPx(getContext(),80))){
                closeDrawer(drawer);
                return true;
            }
            else {
                return false;
            }
        }
    }

    public int getHeightScreen() {
        DisplayMetrics metrics = getContext().getResources().getDisplayMetrics();
        final int w = metrics.heightPixels;
        return w;
    }

    public int getWidhtScreen() {
        DisplayMetrics metrics = getContext().getResources().getDisplayMetrics();
        final int w = metrics.widthPixels;
        return w;
    }
}