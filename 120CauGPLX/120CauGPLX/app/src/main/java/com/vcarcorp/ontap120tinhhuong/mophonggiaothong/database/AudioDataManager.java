package com.vcarcorp.ontap120tinhhuong.mophonggiaothong.database;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.model.VoiceModel;

import java.util.ArrayList;

public class AudioDataManager  extends SQLiteAssetHelper {
    public static String DATABASE_NAME = "audio.db";
    private static final int DATABASE_VERSION = 1;

    public AudioDataManager(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        super.onUpgrade(db, oldVersion, newVersion);
    }

    public ArrayList<VoiceModel> getVoice(){
        ArrayList<VoiceModel> testResults = new ArrayList<>();
        String query = "select * from voice";
        SQLiteDatabase sqLiteDatabase = getWritableDatabase();
        Cursor cursor = sqLiteDatabase.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            do {
                VoiceModel testResult = new VoiceModel();
                testResult.setId(cursor.getInt(0));
                testResult.setName(cursor.getString(1));
                testResult.setVoice_code(cursor.getString(2));
                testResult.setCity(cursor.getString(3));
                testResults.add(testResult);
            } while (cursor.moveToNext());
        }
        try {
            cursor.close();
        } catch (Exception e) {

        }
        try {
            sqLiteDatabase.close();
        }catch (Exception e) {

        }
        return testResults;
    }

}
