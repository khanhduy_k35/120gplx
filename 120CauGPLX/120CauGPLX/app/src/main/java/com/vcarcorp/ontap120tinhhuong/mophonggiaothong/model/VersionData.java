package com.vcarcorp.ontap120tinhhuong.mophonggiaothong.model;

import android.os.Parcel;
import android.os.Parcelable;

public class VersionData implements Parcelable {
    public String date;
    public String version;
    public String link_img;
    public boolean isShowProFisrt;
    public int numberTestFree;
    public int numberQuestFree;
    public boolean adsViettel;
    public boolean adsVina;
    public String main_ver;
    public boolean openLink;
    public boolean onlyLink;
    public String phone;
    public int timeUsing;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getLink_img() {
        return link_img;
    }

    public void setLink_img(String link_img) {
        this.link_img = link_img;
    }

    public boolean isShowProFisrt() {
        return isShowProFisrt;
    }

    public void setShowProFisrt(boolean showProFisrt) {
        isShowProFisrt = showProFisrt;
    }

    public int getNumberTestFree() {
        return numberTestFree;
    }

    public void setNumberTestFree(int numberTestFree) {
        this.numberTestFree = numberTestFree;
    }

    public int getNumberQuestFree() {
        return numberQuestFree;
    }

    public void setNumberQuestFree(int numberQuestFree) {
        this.numberQuestFree = numberQuestFree;
    }

    public boolean isAdsViettel() {
        return adsViettel;
    }

    public void setAdsViettel(boolean adsViettel) {
        this.adsViettel = adsViettel;
    }

    public boolean isAdsVina() {
        return adsVina;
    }

    public void setAdsVina(boolean adsVina) {
        this.adsVina = adsVina;
    }

    public String getMain_ver() {
        return main_ver;
    }

    public void setMain_ver(String main_ver) {
        this.main_ver = main_ver;
    }

    public boolean isOpenLink() {
        return openLink;
    }

    public void setOpenLink(boolean openLink) {
        this.openLink = openLink;
    }

    public boolean isOnlyLink() {
        return onlyLink;
    }

    public void setOnlyLink(boolean onlyLink) {
        this.onlyLink = onlyLink;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public int getTimeUsing() {
        return timeUsing;
    }

    public void setTimeUsing(int timeUsing) {
        this.timeUsing = timeUsing;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.date);
        dest.writeString(this.version);
        dest.writeString(this.link_img);
        dest.writeByte(this.isShowProFisrt ? (byte) 1 : (byte) 0);
        dest.writeInt(this.numberTestFree);
        dest.writeInt(this.numberQuestFree);
        dest.writeByte(this.adsViettel ? (byte) 1 : (byte) 0);
        dest.writeByte(this.adsVina ? (byte) 1 : (byte) 0);
        dest.writeString(this.main_ver);
        dest.writeByte(this.openLink ? (byte) 1 : (byte) 0);
        dest.writeByte(this.onlyLink ? (byte) 1 : (byte) 0);
        dest.writeString(this.phone);
        dest.writeInt(this.timeUsing);
    }

    public void readFromParcel(Parcel source) {
        this.date = source.readString();
        this.version = source.readString();
        this.link_img = source.readString();
        this.isShowProFisrt = source.readByte() != 0;
        this.numberTestFree = source.readInt();
        this.numberQuestFree = source.readInt();
        this.adsViettel = source.readByte() != 0;
        this.adsVina = source.readByte() != 0;
        this.main_ver = source.readString();
        this.openLink = source.readByte() != 0;
        this.onlyLink = source.readByte() != 0;
        this.phone = source.readString();
        this.timeUsing = source.readInt();
    }

    public VersionData() {
    }

    protected VersionData(Parcel in) {
        this.date = in.readString();
        this.version = in.readString();
        this.link_img = in.readString();
        this.isShowProFisrt = in.readByte() != 0;
        this.numberTestFree = in.readInt();
        this.numberQuestFree = in.readInt();
        this.adsViettel = in.readByte() != 0;
        this.adsVina = in.readByte() != 0;
        this.main_ver = in.readString();
        this.openLink = in.readByte() != 0;
        this.onlyLink = in.readByte() != 0;
        this.phone = in.readString();
        this.timeUsing = in.readInt();
    }

    public static final Creator<VersionData> CREATOR = new Creator<VersionData>() {
        @Override
        public VersionData createFromParcel(Parcel source) {
            return new VersionData(source);
        }

        @Override
        public VersionData[] newArray(int size) {
            return new VersionData[size];
        }
    };
}
