package com.vcarcorp.ontap120tinhhuong.mophonggiaothong.main;

import android.content.Context;

public class JNIUtil {
    public static native String apiKey(Context p_context);
    public static native String getServer(Context p_context,int type);
    public static native String getServerVip(Context p_context);

    static {
        System.loadLibrary("gplx-lib");
    }
}
