package com.vcarcorp.ontap120tinhhuong.mophonggiaothong.fragment;

import static com.google.android.exoplayer2.ExoPlayer.STATE_BUFFERING;
import static com.google.android.exoplayer2.ExoPlayer.STATE_ENDED;
import static com.google.android.exoplayer2.ExoPlayer.STATE_READY;

import android.content.Context;
import android.media.tv.AdRequest;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.ads.control.admob.AdmobHelp;
import com.ads.control.utils.Utils;
import com.danikula.videocache.CacheListener;
import com.danikula.videocache.HttpProxyCacheServer;

import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.App;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.R;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.model.QuestionVideo;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.util.Util;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.util.AppUtils;

import java.io.File;

public abstract class BaseFragment extends Fragment {
    protected View containerView;
    protected LayoutInflater inflater;
    protected boolean isCreated;
    protected ExoPlayer exoPlayerAudio;
    protected QuestionVideo questionAudioPlayer;
    public abstract void startAudioAnimation();
    public abstract void stopAudioAnimation();
    private boolean playSoundMota;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        isCreated = true;
        return containerView;
    }

    @Override
    public void onDestroyView() {
        isCreated = false;
        super.onDestroyView();
        if (containerView != null) {
            try {
                ViewGroup parentViewGroup = (ViewGroup) containerView.getParent();
                if (null != parentViewGroup) {
                    parentViewGroup.removeAllViews();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void loadNativeAds(RelativeLayout container_ads_native){
        AdmobHelp.getInstance().loadNativeFragment(getActivity(),container_ads_native);
    }

    public void destroyNativeAds(View container_ads_native){

    }

    protected void playAudioMota(QuestionVideo questionVideo){
        questionAudioPlayer = questionVideo;
        playSoundMota = true;
        if(!Util.isPro(getActivity())){
            if(questionVideo.getScore() == 0){
                playAudio(AppUtils.getServerAudio(getActivity(),questionAudioPlayer) + "temp/" + Util.getVoiceCode() + "_failed.mp3");
            }else if(questionVideo.getScore() == 1){
                playAudio(AppUtils.getServerAudio(getActivity(),questionAudioPlayer) + "temp/" + Util.getVoiceCode() + "_score1.mp3");
            }else if(questionVideo.getScore() == 2){
                playAudio(AppUtils.getServerAudio(getActivity(),questionAudioPlayer) + "temp/" + Util.getVoiceCode() + "_score2.mp3");
            }else if(questionVideo.getScore() == 3){
                playAudio(AppUtils.getServerAudio(getActivity(),questionAudioPlayer) + "temp/" + Util.getVoiceCode() + "_score3.mp3");
            }else if(questionVideo.getScore() == 4){
                playAudio(AppUtils.getServerAudio(getActivity(),questionAudioPlayer) + "temp/" + Util.getVoiceCode() + "_score4.mp3");
            }else if(questionVideo.getScore() == 5){
                playAudio(AppUtils.getServerAudio(getActivity(),questionAudioPlayer)+ "temp/" + Util.getVoiceCode() + "_score5.mp3");
            }
        }else{
            if(questionVideo.getScore() == 0){
                playAudio(AppUtils.getServerAudioVip(getActivity(),questionAudioPlayer) + "temp/" + Util.getVoiceCode() + "_failed.mp3");
            }else if(questionVideo.getScore() == 1){
                playAudio(AppUtils.getServerAudioVip(getActivity(),questionAudioPlayer) + "temp/" + Util.getVoiceCode() + "_score1.mp3");
            }else if(questionVideo.getScore() == 2){
                playAudio(AppUtils.getServerAudioVip(getActivity(),questionAudioPlayer) + "temp/" + Util.getVoiceCode() + "_score2.mp3");
            }else if(questionVideo.getScore() == 3){
                playAudio(AppUtils.getServerAudioVip(getActivity(),questionAudioPlayer) + "temp/" + Util.getVoiceCode() + "_score3.mp3");
            }else if(questionVideo.getScore() == 4){
                playAudio(AppUtils.getServerAudioVip(getActivity(),questionAudioPlayer) + "temp/" + Util.getVoiceCode() + "_score4.mp3");
            }else if(questionVideo.getScore() == 5){
                playAudio(AppUtils.getServerAudioVip(getActivity(),questionAudioPlayer)+ "temp/" + Util.getVoiceCode() + "_score5.mp3");
            }
        }
    }

    protected void playAudioTinhHuongThi(QuestionVideo questionVideo, int pos) {
        questionAudioPlayer = questionVideo;
        if (!Util.isPro(getActivity())) {
            playAudio(AppUtils.getServerAudio(getActivity(),questionAudioPlayer)+ "temp/"  + Util.getVoiceCode() + "_question_" + pos + ".mp3");
        } else {
            playAudio(AppUtils.getServerAudioVip(getActivity(),questionAudioPlayer)+ "temp/"  + Util.getVoiceCode() + "_question_" + pos + ".mp3");
        }
    }

    protected void playAudioScore(int score){
        if(questionAudioPlayer == null)
            return;
        if(!Util.isPro(getActivity())){
            if (score == 1) {
                playAudio(AppUtils.getServerAudio(getActivity(),questionAudioPlayer)  + "temp/" + Util.getVoiceCode() + "_space1.mp3");
            } else if (score == 2) {
                playAudio(AppUtils.getServerAudio(getActivity(),questionAudioPlayer) + "temp/" + Util.getVoiceCode() + "_space2.mp3");
            } else if (score == 3) {
                playAudio(AppUtils.getServerAudio(getActivity(),questionAudioPlayer) + "temp/"  + Util.getVoiceCode() + "_space3.mp3");
            } else if (score == 4) {
                playAudio(AppUtils.getServerAudio(getActivity(),questionAudioPlayer) + "temp/" + Util.getVoiceCode() + "_space4.mp3");
            } else if (score == 5) {
                playAudio(AppUtils.getServerAudio(getActivity(),questionAudioPlayer) + "temp/" + Util.getVoiceCode() + "_space5.mp3");
            }
        }else {
            if (score == 1) {
                playAudio(AppUtils.getServerAudioVip(getActivity(),questionAudioPlayer)  + "temp/" + Util.getVoiceCode() + "_space1.mp3");
            } else if (score == 2) {
                playAudio(AppUtils.getServerAudioVip(getActivity(),questionAudioPlayer) + "temp/" + Util.getVoiceCode() + "_space2.mp3");
            } else if (score == 3) {
                playAudio(AppUtils.getServerAudioVip(getActivity(),questionAudioPlayer) + "temp/"  + Util.getVoiceCode() + "_space3.mp3");
            } else if (score == 4) {
                playAudio(AppUtils.getServerAudioVip(getActivity(),questionAudioPlayer) + "temp/" + Util.getVoiceCode() + "_space4.mp3");
            } else if (score == 5) {
                playAudio(AppUtils.getServerAudioVip(getActivity(),questionAudioPlayer) + "temp/" + Util.getVoiceCode() + "_space5.mp3");
            }
        }
    }

    protected void playAudioTinhhuong(QuestionVideo questionVideo) {
        questionAudioPlayer = questionVideo;
        Log.e("khanhduy.le", "playAudioTinhhuong : " + questionAudioPlayer.getId());
        if (!Util.isPro(getActivity())) {
            playAudio(AppUtils.getServerAudio(getActivity(),questionAudioPlayer) + Util.getVoiceCode() + "/"  + Util.getVoiceCode() + "_name_" + questionVideo.getId() + ".mp3");
        } else {
            playAudio(AppUtils.getServerAudioVip(getActivity(),questionAudioPlayer) + Util.getVoiceCode() + "/"  + Util.getVoiceCode() + "_name_" + questionVideo.getId() + ".mp3");
        }
    }

    private void playAudio(String urlAudio){
        Log.e("khanhduy.le","play audio :" +  urlAudio);
        destroyAudio();
        HttpProxyCacheServer cacheServer = App.getCacheServer(getActivity());
        String proxyVideoUrl = cacheServer.getProxyUrl(urlAudio, true);
        if (cacheServer.isCached(urlAudio)) {
            Log.e("khanhduy.le", "Playing audio Offline");
        } else {
            cacheServer.registerCacheListener(new CacheListener() {
                @Override
                public void onCacheAvailable(File cacheFile, String url, int percentsAvailable) {
                    Log.e("khanhduy.le", String.format("Playing audio Online: Downloaded %d%%", percentsAvailable));
                    if (percentsAvailable == 100) {
                        Log.e("khanhduy.le", "Playing audio Offline");
                    }
                }
            }, urlAudio);
        }
        exoPlayerAudio = ExoPlayerFactory.newSimpleInstance(getContext(),new DefaultTrackSelector(),new DefaultLoadControl());
        String userAgent = com.google.android.exoplayer2.util.Util.getUserAgent(getContext(),"Play Audio");
        ExtractorMediaSource extractorMediaSource = new ExtractorMediaSource(
                Uri.parse(proxyVideoUrl),
                new DefaultDataSourceFactory(getContext(),userAgent),
                new DefaultExtractorsFactory(),null,null
        );
        exoPlayerAudio.prepare(extractorMediaSource);
        exoPlayerAudio.setPlayWhenReady(true);
        exoPlayerAudio.addListener(new ExoPlayer.EventListener() {
            @Override
            public void onTimelineChanged(Timeline timeline, Object manifest) {

            }

            @Override
            public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {

            }

            @Override
            public void onLoadingChanged(boolean isLoading) {

            }

            @Override
            public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
                if(playWhenReady && playbackState == STATE_READY){
                    startAudioAnimation();
                }
                if(playWhenReady && playbackState == STATE_BUFFERING){
                    startAudioAnimation();
                }
                if(playWhenReady && playbackState == STATE_ENDED){
                    stopAudioAnimation();
                    if(playSoundMota) {
                        playSoundMota = false;
                        if (!Util.isPro(getActivity())) {
                            playAudio(AppUtils.getServerAudio(getActivity(),questionAudioPlayer)  + Util.getVoiceCode() + "/" + Util.getVoiceCode() + "_description_" + questionAudioPlayer.getId() + ".mp3");
                        } else {
                            playAudio(AppUtils.getServerAudioVip(getActivity(),questionAudioPlayer)  + Util.getVoiceCode() + "/" + Util.getVoiceCode() + "_description_" + questionAudioPlayer.getId() + ".mp3");
                        }
                    }
                }
            }

            @Override
            public void onPlayerError(ExoPlaybackException error) {

            }

            @Override
            public void onPositionDiscontinuity() {

            }
        });
    }

    protected void destroyAudio(){
        try {
            stopAudioAnimation();
            Log.e("khanhduy.le","destroyAudio : " + questionAudioPlayer.getId());
            if(exoPlayerAudio!=null) {
                exoPlayerAudio.setPlayWhenReady(false);
            }
            if(exoPlayerAudio!=null) {
                exoPlayerAudio.release();
                exoPlayerAudio = null;
            }
        }catch (Exception e){

        }
    }

    protected void pauseAudio(){
        if(exoPlayerAudio!=null){
            Log.e("khanhduy.le","pauseAudio: " + questionAudioPlayer.getId());
             exoPlayerAudio.setPlayWhenReady(false);
            stopAudioAnimation();
        }
    }

    protected void resumerAudio(){
        if(exoPlayerAudio!=null){
            startAudioAnimation();
            Log.e("khanhduy.le","resumerAudio: " + questionAudioPlayer.getId());
            exoPlayerAudio.setPlayWhenReady(true);
        }
    }
}
