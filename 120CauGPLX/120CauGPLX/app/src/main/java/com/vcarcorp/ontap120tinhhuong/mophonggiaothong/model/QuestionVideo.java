package com.vcarcorp.ontap120tinhhuong.mophonggiaothong.model;

import android.os.Parcel;
import android.os.Parcelable;

public class QuestionVideo implements Parcelable {
    private int id;
    private String name;
    private String videoName;
    private int categoryID;
    private int total_length;
    private int begin_pos;
    private int end_pos;
    private String description;
    private int score;
    private int timeSelected;
    private String image;
    private String imageDescription;
    private boolean marked;
    private int timeSelectedMaker;
    private int timeSelectedWrong;
    private int wrong;
    private String videoLink;
    private String explantion;

    public QuestionVideo(){

    }

    public QuestionVideo(int id, String name, String videoName, int categoryID, int total_length, int begin_pos, int end_pos, String description, String image, String imageDescription,String videoLink, String explantion) {
        this.id = id;
        this.name = name;
        this.videoName = videoName;
        this.categoryID = categoryID;
        this.total_length = total_length;
        this.begin_pos = begin_pos;
        this.end_pos = end_pos;
        this.description = description;
        this.image = image;
        this.imageDescription = imageDescription;
        this.videoLink = videoLink;
        this.explantion = explantion;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getVideoName() {
        return videoName;
    }

    public void setVideoName(String videoName) {
        this.videoName = videoName;
    }

    public int getCategoryID() {
        return categoryID;
    }

    public void setCategoryID(int categoryID) {
        this.categoryID = categoryID;
    }

    public int getTotal_length() {
        return total_length;
    }

    public void setTotal_length(int total_length) {
        this.total_length = total_length;
    }

    public int getBegin_pos() {
        return begin_pos;
    }

    public void setBegin_pos(int begin_pos) {
        this.begin_pos = begin_pos;
    }

    public int getEnd_pos() {
        return end_pos;
    }

    public void setEnd_pos(int end_pos) {
        this.end_pos = end_pos;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public int getTimeSelected() {
        return timeSelected;
    }

    public void setTimeSelected(int timeSelected) {
        this.timeSelected = timeSelected;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public int getTimeSelectedMaker() {
        return timeSelectedMaker;
    }

    public void setTimeSelectedMaker(int timeSelectedMaker) {
        this.timeSelectedMaker = timeSelectedMaker;
    }

    public int getTimeSelectedWrong() {
        return timeSelectedWrong;
    }

    public void setTimeSelectedWrong(int timeSelectedWrong) {
        this.timeSelectedWrong = timeSelectedWrong;
    }

    public int getWrong() {
        return wrong;
    }

    public void setWrong(int wrong) {
        this.wrong = wrong;
    }

    public Object clone() {
        try {
            return super.clone();
        } catch (CloneNotSupportedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return this;
        }
    }

    public boolean validateObject(QuestionVideo video) {
        if (!this.name.equals(video.name) || !this.videoName.equals(video.videoName) || (this.total_length != video.total_length) || (this.begin_pos != video.begin_pos) || (this.end_pos != video.end_pos) || !this.description.equals(video.description) || !this.image.equals(video.image) || !this.imageDescription.equals(video.imageDescription)
                || !this.videoLink.equals(video.videoLink) || !this.explantion.equals(video.explantion)) {
            return false;

        } else {
            return true;
        }
    }

    public boolean isMarked() {
        return marked;
    }

    public void setMarked(boolean marked) {
        this.marked = marked;
    }

    public String getImageDescription() {
        return imageDescription;
    }

    public void setImageDescription(String imageDescription) {
        this.imageDescription = imageDescription;
    }

    public String getVideoLink() {
        return videoLink;
    }

    public void setVideoLink(String videoLink) {
        this.videoLink = videoLink;
    }

    public String getExplantion() {
        return explantion;
    }

    public void setExplantion(String explantion) {
        this.explantion = explantion;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.name);
        dest.writeString(this.videoName);
        dest.writeInt(this.categoryID);
        dest.writeInt(this.total_length);
        dest.writeInt(this.begin_pos);
        dest.writeInt(this.end_pos);
        dest.writeString(this.description);
        dest.writeInt(this.score);
        dest.writeInt(this.timeSelected);
        dest.writeString(this.image);
        dest.writeString(this.imageDescription);
        dest.writeByte(this.marked ? (byte) 1 : (byte) 0);
        dest.writeInt(this.timeSelectedMaker);
        dest.writeInt(this.timeSelectedWrong);
        dest.writeInt(this.wrong);
        dest.writeString(this.videoLink);
        dest.writeString(this.explantion);
    }

    public void readFromParcel(Parcel source) {
        this.id = source.readInt();
        this.name = source.readString();
        this.videoName = source.readString();
        this.categoryID = source.readInt();
        this.total_length = source.readInt();
        this.begin_pos = source.readInt();
        this.end_pos = source.readInt();
        this.description = source.readString();
        this.score = source.readInt();
        this.timeSelected = source.readInt();
        this.image = source.readString();
        this.imageDescription = source.readString();
        this.marked = source.readByte() != 0;
        this.timeSelectedMaker = source.readInt();
        this.timeSelectedWrong = source.readInt();
        this.wrong = source.readInt();
        this.videoLink = source.readString();
        this.explantion = source.readString();
    }

    protected QuestionVideo(Parcel in) {
        this.id = in.readInt();
        this.name = in.readString();
        this.videoName = in.readString();
        this.categoryID = in.readInt();
        this.total_length = in.readInt();
        this.begin_pos = in.readInt();
        this.end_pos = in.readInt();
        this.description = in.readString();
        this.score = in.readInt();
        this.timeSelected = in.readInt();
        this.image = in.readString();
        this.imageDescription = in.readString();
        this.marked = in.readByte() != 0;
        this.timeSelectedMaker = in.readInt();
        this.timeSelectedWrong = in.readInt();
        this.wrong = in.readInt();
        this.videoLink = in.readString();
        this.explantion = in.readString();
    }

    public static final Creator<QuestionVideo> CREATOR = new Creator<QuestionVideo>() {
        @Override
        public QuestionVideo createFromParcel(Parcel source) {
            return new QuestionVideo(source);
        }

        @Override
        public QuestionVideo[] newArray(int size) {
            return new QuestionVideo[size];
        }
    };
}
