package com.vcarcorp.ontap120tinhhuong.mophonggiaothong.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.RecyclerView;

import com.airbnb.lottie.LottieAnimationView;
import com.bumptech.glide.Glide;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.R;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.main.SelectSoundActivity;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.model.VoiceModel;
import com.ads.control.customview.CustomTextView;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.util.OnSingleClickListener;

import java.util.ArrayList;
import java.util.List;

public class VoiceAdapter extends RecyclerView.Adapter<VoiceAdapter.ViewHolder> {
    //recycle
    private ItemClickListener mClickListener;
    private ArrayList<VoiceModel> voiceModels;
    private Context mContext;
    private String voiceCodeSelect;
    private boolean startAnim;
    private int posAnim;

    public VoiceAdapter(final ArrayList<VoiceModel> data, Context mContext) {
        this.voiceModels = data;
        this.mContext = mContext;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        this.mContext = parent.getContext();
        return new VoiceAdapter.ViewHolder(LayoutInflater.from(mContext)
                .inflate(R.layout.voice_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position, @NonNull List<Object> payloads) {
        super.onBindViewHolder(holder, position, payloads);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, @SuppressLint("RecyclerView") int position) {
        final VoiceModel item = voiceModels.get(position);
        holder.tvName.setText(item.getName());
        holder.tvCity.setText(item.getCity());
        Glide.with(((SelectSoundActivity) mContext))
                .load(Uri.parse("file:///android_asset/icon/" + item.getVoice_code().replace("-fhg", "") + ".png"))
                .into(holder.imgThumb);
        if (item.getVoice_code().equals(this.voiceCodeSelect)) {
            holder.imgTick.setVisibility(View.VISIBLE);
        } else {
            holder.imgTick.setVisibility(View.INVISIBLE);
        }
        holder.animSound.pauseAnimation();
        if(startAnim && posAnim == position){
            holder.animSound.setVisibility(View.VISIBLE);
            holder.animSound.playAnimation();
        }else{
            holder.animSound.setVisibility(View.GONE);
        }
        holder.rltView.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                if (mClickListener != null) mClickListener.onItemClick(v, position);
            }
        });

        holder.icPlay.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                if (mClickListener != null) mClickListener.onItemPlayClick(v, position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return voiceModels.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private com.rey.material.widget.RelativeLayout rltView;
        private CustomTextView tvName, tvCity;
        private ImageView imgThumb;
        private AppCompatImageView imgTick;
        private CustomTextView icPlay;
        private LottieAnimationView animSound;

        public ViewHolder(View view) {
            super(view);
            imgThumb = (ImageView) view.findViewById(R.id.img_thumb);
            imgTick = (AppCompatImageView) view.findViewById(R.id.ic_tick);
            icPlay = (CustomTextView) view.findViewById(R.id.ic_play);
            tvName = (CustomTextView) view.findViewById(R.id.tv_name);
            tvCity = (CustomTextView) view.findViewById(R.id.tv_city);
            rltView =  (com.rey.material.widget.RelativeLayout)view.findViewById(R.id.rltClass1);
            animSound = view.findViewById(R.id.animSound);
        }
    }
    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);
        void onItemPlayClick(View view, int position);
    }

    public ArrayList<VoiceModel> getQuestionCategories() {
        return voiceModels;
    }

    public void setVoice(ArrayList<VoiceModel> questionCategories) {
        this.voiceModels = questionCategories;
    }

    public void setVoiceCodeSelect(String voiceCodeSelect) {
        this.voiceCodeSelect = voiceCodeSelect;
    }

    public void startAudioAnimation(int pos){
        startAnim = true;
        posAnim = pos;
    }

    public void stopAudioAnimation(int pos){
        startAnim = false;
        posAnim = pos;
    }
}
