package com.vcarcorp.ontap120tinhhuong.mophonggiaothong.database;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.main.JNIUtil;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.model.QuestionVideo;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.model.QuestionVideoCategory;
import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.model.QuestionCategory;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.model.Test;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.model.TestQuest;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.util.AESCrypt;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.util.SharedPreferencesUtils;

import java.util.ArrayList;

/**
 * Created by duy on 10/23/17.
 */

public class GPLXDataManager extends SQLiteAssetHelper {
    public static String DATABASE_NAME = "120_caugplx.db";
    private static final int DATABASE_VERSION = 3;

    public GPLXDataManager(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        super.onUpgrade(db, oldVersion, newVersion);
    }

    public int countTotalTest() {
        int totalQuest = 0;
        String query = "select count(*) from ZTEST;";
        SQLiteDatabase sqLiteDatabase = getWritableDatabase();
        Cursor cursor = sqLiteDatabase.rawQuery(query, null);
        if (cursor != null && cursor.moveToFirst()) {
            totalQuest = cursor.getInt(0);
        }
        try {
            cursor.close();
        } catch (Exception e) {

        }
        try {
            sqLiteDatabase.close();
        } catch (Exception e) {

        }
        return totalQuest;
    }

    public ArrayList<Test> getTestResult(){
        ArrayList<Test> testResults = new ArrayList<>();
        String query = "select * from ZTEST";
        SQLiteDatabase sqLiteDatabase = getWritableDatabase();
        Cursor cursor = sqLiteDatabase.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            do {
                Test testResult = new Test();
                testResult.setIdTest(cursor.getInt(0));
                testResult.setCurrentQuest(cursor.getInt(1));
                testResult.setFinish(cursor.getInt(2) == 0 ? false : true);
                testResult.setTotalScore(cursor.getInt(3));
                testResults.add(testResult);
            } while (cursor.moveToNext());
        }
        try {
            cursor.close();
        } catch (Exception e) {

        }
        try {
            sqLiteDatabase.close();
        }catch (Exception e) {

        }
        for(int i = 0; i < testResults.size(); i ++){
            Test testResult = testResults.get(i);
            testResult.setCountQuestion(countTestQuestionChecked(testResult));
        }
        return testResults;
    }

    public int countTestQuestionChecked(Test testResult){
        int totalQuest = 0;
        String query = "select count(*) from ZTESTQUEST where ZANSWER > 0 and ISLEARNED = 1 and TESTID = " + testResult.getIdTest();
        SQLiteDatabase sqLiteDatabase = getWritableDatabase();
        Cursor cursor = sqLiteDatabase.rawQuery(query, null);
        if (cursor != null && cursor.moveToFirst()) {
            totalQuest = cursor.getInt(0);
        }
        try {
            cursor.close();
        } catch (Exception e) {

        }
        try {
            sqLiteDatabase.close();
        } catch (Exception e) {

        }
        return totalQuest;
    }

    public int insertTestResult(Test testResult) {
        ContentValues args = new ContentValues();
        args.put("ZCURRENT_QUEST", testResult.getCurrentQuest());
        args.put("ISFINISH",testResult.isFinish() ? 1 : 0);
        args.put("TOTAL_POINT_CORRECT",testResult.getTotalScore());
        SQLiteDatabase mDb = getWritableDatabase();
        int result = (int)mDb.insert("ZTEST","IDTEST",args);
        try {
            mDb.close();
        } catch (Exception e) {

        }
        return result;
    }

    public int resetAllTestQuest(){
        ContentValues args = new ContentValues();
        args.put("ZANSWER", "");
        SQLiteDatabase mDb = getWritableDatabase();
        int result = (int)mDb.update("ZTESTQUEST",args, "ZTESTQUESTID" + ">0", new String[] {});
        try {
            mDb.close();
        } catch (Exception e) {

        }
        return result;
    }

    public int resetAllTest(){
        ContentValues args = new ContentValues();
        args.put("ZCURRENT_QUEST", 0);
        args.put("ISFINISH", 0);
        args.put("TOTAL_POINT_CORRECT", 0);
        SQLiteDatabase mDb = getWritableDatabase();
        int result = (int)mDb.update("ZTEST",args, "IDTEST" + ">0", new String[] {});
        try {
            mDb.close();
        } catch (Exception e) {

        }
        return result;
    }

    public ArrayList<QuestionVideo> getAllQuestionVideoLink(Context context){
        ArrayList<QuestionVideo> listResult = new ArrayList<QuestionVideo>();
        SQLiteDatabase localSQLiteDatabase = getWritableDatabase();
        String query = "SELECT Z_PK,ZVIDEO_LINK FROM ZQUESTION";

        Cursor cursor = localSQLiteDatabase.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            do {
                QuestionVideo question = new QuestionVideo();
                question.setId(cursor.getInt(0));
                try{
                    question.setVideoLink(AESCrypt.decrypt(JNIUtil.apiKey(context),cursor.getString(1)));
                }catch (Exception e){
                    question.setVideoLink(cursor.getString(1));
                }
                listResult.add(question);
            } while (cursor.moveToNext());
        }
        try {
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            localSQLiteDatabase.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return listResult;
    }

    public ArrayList<QuestionVideo> getAllQuestionVideo(Context context){
        ArrayList<QuestionVideo> listResult = new ArrayList<QuestionVideo>();
        SQLiteDatabase localSQLiteDatabase = getWritableDatabase();
        String query = "SELECT * FROM ZQUESTION";

        Cursor cursor = localSQLiteDatabase.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            do {
                QuestionVideo question = new QuestionVideo();
                question.setId(cursor.getInt(0));
                question.setName(cursor.getString(1));
                question.setVideoName(cursor.getString(2));
                question.setCategoryID(cursor.getInt(3));
                question.setDescription(cursor.getString(4));
                try {
                    question.setTotal_length(Integer.parseInt(AESCrypt.decrypt(JNIUtil.apiKey(context),cursor.getString(5))));
                }catch ( Exception e){
                    question.setTotal_length(Integer.parseInt(cursor.getString(5)));
                }
                try {
                    question.setBegin_pos(Integer.parseInt(AESCrypt.decrypt(JNIUtil.apiKey(context),cursor.getString(6))));
                }catch ( Exception e){
                    question.setBegin_pos(Integer.parseInt(cursor.getString(6)));
                }
                try {
                    question.setEnd_pos(Integer.parseInt(AESCrypt.decrypt(JNIUtil.apiKey(context),cursor.getString(7))));
                }catch ( Exception e){
                    question.setEnd_pos(Integer.parseInt(cursor.getString(7)));
                }
                try {
                    question.setImage(AESCrypt.decrypt(JNIUtil.apiKey(context),cursor.getString(8)));
                }catch ( Exception e){
                    question.setImage(cursor.getString(8));
                }
                question.setScore(cursor.getInt(9));
                question.setTimeSelected(cursor.getInt(10));
                question.setMarked(cursor.getInt(11) > 0 ? true : false);
                try {
                    question.setImageDescription(AESCrypt.decrypt(JNIUtil.apiKey(context),cursor.getString(12)));
                }catch ( Exception e){
                    question.setImageDescription(cursor.getString(12));
                }
                question.setWrong(cursor.getInt(16));
                try{
                    question.setVideoLink(AESCrypt.decrypt(JNIUtil.apiKey(context),cursor.getString(17)));
                }catch (Exception e){
                    question.setVideoLink(cursor.getString(17));
                }
                try{
                    question.setExplantion(AESCrypt.decrypt(JNIUtil.apiKey(context),cursor.getString(18)));
                }catch (Exception e){
                    question.setExplantion(cursor.getString(18));
                }
                listResult.add(question);
            } while (cursor.moveToNext());
        }
        try {
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            localSQLiteDatabase.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return listResult;
    }

    public void updateQuestionVideo(QuestionVideo questionVideo,Context context){
        ContentValues args = new ContentValues();
        args.put("ZNAME",questionVideo.getName());
        args.put("ZVIDEONAME", questionVideo.getVideoName());
        args.put("ZDESCRIPTION", questionVideo.getDescription());
        try {
            args.put("TOTAL_LENGTH", AESCrypt.encrypt(JNIUtil.apiKey(context),questionVideo.getTotal_length() + ""));
            args.put("BEGIN_POS",  AESCrypt.encrypt(JNIUtil.apiKey(context),questionVideo.getBegin_pos() + ""));
            args.put("END_POS",  AESCrypt.encrypt(JNIUtil.apiKey(context),questionVideo.getEnd_pos() + ""));
            args.put("ZIMGAE_IMAGE",  AESCrypt.encrypt(JNIUtil.apiKey(context),questionVideo.getImage()));
            args.put("ZIMAGEDESCRIPTION",  AESCrypt.encrypt(JNIUtil.apiKey(context),questionVideo.getImageDescription()));
            args.put("ZVIDEO_LINK",  AESCrypt.encrypt(JNIUtil.apiKey(context),questionVideo.getVideoLink()));
            args.put("EXPLAINTION",  AESCrypt.encrypt(JNIUtil.apiKey(context),questionVideo.getExplantion()));
        }catch (Exception e){

        }
        SQLiteDatabase mDb = getWritableDatabase();
        mDb.update("ZQUESTION", args, "Z_PK" + "=?", new String[] {String.valueOf(questionVideo.getId())});
        try {
            mDb.close();
        }catch (Exception e) {

        }
    }

    public ArrayList<QuestionVideoCategory> getAllQuestionCategory(){
        ArrayList<QuestionVideoCategory> listResult = new ArrayList<QuestionVideoCategory>();
        SQLiteDatabase localSQLiteDatabase = getWritableDatabase();
        String query = "SELECT * FROM ZQUESTIONCATEGORY";
        Cursor cursor = localSQLiteDatabase.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            do {
                QuestionVideoCategory question = new QuestionVideoCategory();
                question.setId(cursor.getInt(0));
                question.setName(cursor.getString(1));
                question.setDesc(cursor.getString(2));
                question.setCount(cursor.getInt(3));
                listResult.add(question);
            } while (cursor.moveToNext());
        }
        try {
            cursor.close();
        } catch (Exception e) {

        }
        try {
            localSQLiteDatabase.close();
        } catch (Exception e) {

        }

        for (QuestionVideoCategory questionVideoCategory : listResult) {
            questionVideoCategory.setListQuestion(getAllQuestionByCategory(questionVideoCategory));
            questionVideoCategory.setNumberQuestion(getNumberQuestion(questionVideoCategory));
        }

        return listResult;
    }

    public ArrayList<QuestionVideo> getAllQuestionByCategory(QuestionVideoCategory category){
        ArrayList<QuestionVideo> listResult = new ArrayList<QuestionVideo>();
        SQLiteDatabase localSQLiteDatabase = getWritableDatabase();
        String query = "SELECT * FROM ZQUESTION Where ZQUESTION.ZCATEGORY_ID = "+ category.getId();
        Cursor cursor = localSQLiteDatabase.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            do {
                QuestionVideo question = new QuestionVideo();
                question.setId(cursor.getInt(0));
                question.setName(cursor.getString(1));
                question.setVideoName(cursor.getString(2));
                listResult.add(question);
            } while (cursor.moveToNext());
        }
        try {
            cursor.close();
        } catch (Exception e) {

        }
        try {
            localSQLiteDatabase.close();
        } catch (Exception e) {

        }
        return listResult;
    }

    public int getNumberQuestion(QuestionVideoCategory category){
        SQLiteDatabase localSQLiteDatabase = getWritableDatabase();
        String query = "SELECT ZNUMBER_QUESTION FROM ZTESTQUESTPERTYPE Where ZTESTQUESTPERTYPE.ZCATEGORY_ID = "+ category.getId();
        Cursor cursor = localSQLiteDatabase.rawQuery(query, null);
        int number = 0;
        if (cursor.moveToFirst()) {
            do {
                number = cursor.getInt(0);
            } while (cursor.moveToNext());
        }
        try {
            cursor.close();
        } catch (Exception e) {

        }
        try {
            localSQLiteDatabase.close();
        } catch (Exception e) {

        }
        return number;
    }

    public ArrayList<QuestionVideo> getAllQuestionByCategory(int categoryId,Context context){
        ArrayList<QuestionVideo> listResult = new ArrayList<QuestionVideo>();
        SQLiteDatabase localSQLiteDatabase = getWritableDatabase();
        String query = "select * from ZQUESTION where ZCATEGORY_ID = " + categoryId + ";";
        Cursor cursor = localSQLiteDatabase.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            do {
                try {
                    QuestionVideo question = new QuestionVideo();
                    question.setId(cursor.getInt(0));
                    question.setName(cursor.getString(1));
                    question.setVideoName(cursor.getString(2));
                    question.setCategoryID(cursor.getInt(3));
                    question.setDescription(cursor.getString(4));
                    question.setTotal_length(Integer.parseInt(AESCrypt.decrypt(JNIUtil.apiKey(context),cursor.getString(5))));
                    question.setBegin_pos(Integer.parseInt(AESCrypt.decrypt(JNIUtil.apiKey(context),cursor.getString(6))));
                    question.setEnd_pos(Integer.parseInt(AESCrypt.decrypt(JNIUtil.apiKey(context),cursor.getString(7))));
                    question.setImage(AESCrypt.decrypt(JNIUtil.apiKey(context),cursor.getString(8)));
                    question.setScore(cursor.getInt(9));
                    question.setTimeSelected(cursor.getInt(10));
                    question.setMarked(cursor.getInt(11) > 0 ? true : false);
                    question.setImageDescription(AESCrypt.decrypt(JNIUtil.apiKey(context),cursor.getString(12)));
                    question.setWrong(cursor.getInt(16));
                    try{
                        question.setVideoLink(AESCrypt.decrypt(JNIUtil.apiKey(context),cursor.getString(17)));
                    }catch (Exception e){
                        question.setVideoLink(cursor.getString(17));
                    }
                    try{
                        question.setExplantion(AESCrypt.decrypt(JNIUtil.apiKey(context),cursor.getString(18)));
                    }catch (Exception e){
                        question.setExplantion(cursor.getString(18));
                    }
                    listResult.add(question);
                }catch (Exception e){

                }
            } while (cursor.moveToNext());
        }
        try {
            cursor.close();
        } catch (Exception e) {

        }
        try {
            localSQLiteDatabase.close();
        } catch (Exception e) {

        }
        return listResult;
    }

    public int updateQuestionAnswer(QuestionVideo questionVideo){
        ContentValues args = new ContentValues();
        args.put("SCORED", questionVideo.getScore());
        args.put("TIME_SELECTED", questionVideo.getTimeSelected());
        SQLiteDatabase mDb = getWritableDatabase();
        int result =  mDb.update("ZQUESTION", args, "Z_PK" + "=?", new String[] {String.valueOf(questionVideo.getId())});
        try {
            mDb.close();
        }catch (Exception e) {
        }
        return result;
    }

    public void updateAllQuestion(){
        ContentValues args = new ContentValues();
        args.put("SCORED",0);
        args.put("TIME_SELECTED", 0);
        SQLiteDatabase mDb = getWritableDatabase();
        int result =  mDb.update("ZQUESTION", args, "Z_PK" + ">?", new String[] {String.valueOf(-1)});
        try {
            mDb.close();
        }catch (Exception e) {

        }


        args = new ContentValues();
        args.put("ZCURRENT_INDEX", 0);
        mDb = getWritableDatabase();
        result =  mDb.update("ZQUESTIONCATEGORY", args, "Z_PK" + ">?", new String[] {String.valueOf(-1)});
        try {
            mDb.close();
        }catch (Exception e) {

        }
    }

    public int updateQuestionMarked(QuestionVideo question,boolean marked){
        ContentValues args = new ContentValues();
        int markedValue = marked ? ((SharedPreferencesUtils.getInt("MARKED_VALUE",0) + 1)) : 0;
        if(markedValue > 0){
            SharedPreferencesUtils.setInt("MARKED_VALUE",(SharedPreferencesUtils.getInt("MARKED_VALUE",0) + 1));
        }
        args.put("MARKED", markedValue);
        SQLiteDatabase mDb = getWritableDatabase();
        int result = (int)mDb.update("ZQUESTION",args, "Z_PK" + "=?", new String[] {String.valueOf(question.getId())});
        try {
            mDb.close();
        } catch (Exception e) {

        }
        return result;
    }

    public void getQuestionCategoryById(QuestionCategory questCates){
        SQLiteDatabase localSQLiteDatabase = getWritableDatabase();
        String query = "select Z_PK,Z_NAME,Z_DESC,Z_COUNT,Z_PASS,ZCURRENT_INDEX from ZQUESTIONCATEGORY where Z_PK = " + questCates.getQuestionCategoryID() + " order by Z_PK asc;";
        Cursor cursor = localSQLiteDatabase.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            questCates.setQuestionCategoryID(cursor.getInt(0));
            questCates.setQuestName(cursor.getString(1));
            questCates.setQuestDes(cursor.getString(2));
            questCates.setNumberQuest(cursor.getInt(3));
            questCates.setNumberPass(cursor.getInt(4));
            questCates.setTotalScore(getSumScoreQuestByCategory(questCates.getQuestionCategoryID()));
            questCates.setCurrentIndex(cursor.getInt(5));
        }
        try {
            cursor.close();
        } catch (Exception e) {

        }
        try {
            localSQLiteDatabase.close();
        } catch (Exception e) {

        }
    }

    public QuestionCategory getQuestionCategoryById(int questCatesID){
        SQLiteDatabase localSQLiteDatabase = getWritableDatabase();
        String query = "select Z_PK,Z_NAME,Z_DESC,Z_COUNT,Z_PASS,ZCURRENT_INDEX from ZQUESTIONCATEGORY where Z_PK = " + questCatesID + " order by Z_PK asc;";
        Cursor cursor = localSQLiteDatabase.rawQuery(query, null);
        QuestionCategory questCates = new QuestionCategory();
        if (cursor.moveToFirst()) {
            questCates.setQuestionCategoryID(cursor.getInt(0));
            questCates.setQuestName(cursor.getString(1));
            questCates.setQuestDes(cursor.getString(2));
            questCates.setNumberQuest(cursor.getInt(3));
            questCates.setNumberPass(cursor.getInt(4));
            questCates.setTotalScore(getSumScoreQuestByCategory(questCates.getQuestionCategoryID()));
            questCates.setCurrentIndex(cursor.getInt(5));
        }
        try {
            cursor.close();
        } catch (Exception e) {

        }
        try {
            localSQLiteDatabase.close();
        } catch (Exception e) {

        }
        return questCates;
    }

    public ArrayList<QuestionCategory> getQuestionCategory(){
        ArrayList<QuestionCategory> listResult = new ArrayList<QuestionCategory>();
        SQLiteDatabase localSQLiteDatabase = getWritableDatabase();
        String query = "select Z_PK,Z_NAME,Z_DESC,Z_COUNT,Z_PASS,ZCURRENT_INDEX from ZQUESTIONCATEGORY order by Z_PK asc;";
        Cursor cursor = localSQLiteDatabase.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            do {
                QuestionCategory questionCategory = new QuestionCategory();
                questionCategory.setQuestionCategoryID(cursor.getInt(0));
                questionCategory.setQuestName(cursor.getString(1));
                questionCategory.setQuestDes(cursor.getString(2));
                questionCategory.setNumberQuest(cursor.getInt(3));
                questionCategory.setNumberPass(cursor.getInt(4));
                questionCategory.setTotalScore(getSumScoreQuestByCategory(questionCategory.getQuestionCategoryID()));
                questionCategory.setCurrentIndex(cursor.getInt(5));
                listResult.add(questionCategory);
            } while (cursor.moveToNext());
        }
        try {
            cursor.close();
        } catch (Exception e) {

        }
        try {
            localSQLiteDatabase.close();
        } catch (Exception e) {

        }
        return listResult;
    }

    public void updateCategoryCurrenIndex(QuestionCategory category,int index){
        ContentValues args = new ContentValues();
        args.put("ZCURRENT_INDEX", index);
        SQLiteDatabase mDb = getWritableDatabase();
        int result =  mDb.update("ZQUESTIONCATEGORY", args, "Z_PK" + "=?", new String[] {String.valueOf(category.getQuestionCategoryID())});
        try {
            mDb.close();
        }catch (Exception e) {

        }
    }

    public int getSumScoreQuestByCategory(int categoryQuest){
        int totalQuest = 0;
        String query = "SELECT sum(SCORED) from ZQUESTION WHERE ZCATEGORY_ID = " + categoryQuest + ";";
        SQLiteDatabase sqLiteDatabase = getWritableDatabase();
        Cursor cursor = sqLiteDatabase.rawQuery(query, null);
        if (cursor != null && cursor.moveToFirst()) {
            totalQuest = cursor.getInt(0);
        }
        try {
            cursor.close();
        } catch (Exception e) {

        }
        try {
            sqLiteDatabase.close();
        } catch (Exception e) {

        }
        return totalQuest;
    }

    public ArrayList<TestQuest> getAllTestQuestByTest(Test test, Context context){
        ArrayList<TestQuest> listResult = new ArrayList<TestQuest>();
        SQLiteDatabase localSQLiteDatabase = getWritableDatabase();
        String query = "SELECT * from ZTESTQUEST INNER join ZQUESTION on ZTESTQUEST.ZQUESTIONID = ZQUESTION.Z_PK WHERE ZTESTQUEST.TESTID = "+ test.getIdTest() +";";
        Cursor cursor = localSQLiteDatabase.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            do {
                try {
                    TestQuest testQuest = new TestQuest();
                    testQuest.setId(cursor.getInt(0));
                    testQuest.setTestID(cursor.getInt(1));
                    testQuest.setQuestionID(cursor.getInt(2));
                    testQuest.setAnswer(cursor.getInt(3));
                    testQuest.setScore(cursor.getInt(4));
                    testQuest.setLearned(cursor.getInt(5) == 1 ? true : false);

                    QuestionVideo question = new QuestionVideo();
                    question.setId(cursor.getInt(0 + 6));
                    question.setName(cursor.getString(1 + 6));
                    question.setVideoName(cursor.getString(2 + 6));
                    question.setCategoryID(cursor.getInt(3 + 6));
                    question.setDescription(cursor.getString(4 + 6));
                    question.setTotal_length(Integer.parseInt(AESCrypt.decrypt(JNIUtil.apiKey(context),cursor.getString(5 + 6))));
                    question.setBegin_pos(Integer.parseInt(AESCrypt.decrypt(JNIUtil.apiKey(context),cursor.getString(6 + 6))));
                    question.setEnd_pos(Integer.parseInt(AESCrypt.decrypt(JNIUtil.apiKey(context),cursor.getString(7 + 6))));
                    question.setImage(AESCrypt.decrypt(JNIUtil.apiKey(context),cursor.getString(8 + 6)));
                    question.setScore(cursor.getInt(9 + 6));
                    question.setTimeSelected(cursor.getInt(10 + 6));
                    question.setMarked(cursor.getInt(11 + 6) > 0 ? true : false);
                    question.setImageDescription(AESCrypt.decrypt(JNIUtil.apiKey(context),cursor.getString(12 + 6)));

                    try{
                        question.setVideoLink(AESCrypt.decrypt(JNIUtil.apiKey(context),cursor.getString(17 + 6)));
                    }catch (Exception e){
                        question.setVideoLink(cursor.getString(17 + 6));
                    }
                    try{
                        question.setExplantion(AESCrypt.decrypt(JNIUtil.apiKey(context),cursor.getString(18 + 6)));
                    }catch (Exception e){
                        question.setExplantion(cursor.getString(18 + 6));
                    }

                    testQuest.setQuestionVideo(question);
                    listResult.add(testQuest);
                }catch (Exception e){

                }
            } while (cursor.moveToNext());
        }
        try {
            cursor.close();
        } catch (Exception e) {

        }
        try {
            localSQLiteDatabase.close();
        } catch (Exception e) {

        }
        return listResult;
    }

    public void updateTestCurrenIndex(Test test,int index){
        ContentValues args = new ContentValues();
        args.put("ZCURRENT_QUEST", index);
        SQLiteDatabase mDb = getWritableDatabase();
        int result =  mDb.update("ZTEST", args, "IDTEST" + "=?", new String[] {String.valueOf(test.getIdTest())});
        try {
            mDb.close();
        }catch (Exception e) {

        }
    }

    public void updateTestFinish(Test test){
        ContentValues args = new ContentValues();
        args.put("ZCURRENT_QUEST", test.getCurrentQuest());
        args.put("ISFINISH", test.isFinish() ? 1 : 0);
        args.put("TOTAL_POINT_CORRECT", getSumScoreQuestByTest(test));
        test.setTotalScore(getSumScoreQuestByTest(test));
        SQLiteDatabase mDb = getWritableDatabase();
        int result =  mDb.update("ZTEST", args, "IDTEST" + "=?", new String[] {String.valueOf(test.getIdTest())});
        try {
            mDb.close();
        }catch (Exception e) {

        }
    }

    public void updateResetTestQuest(Test test){
        ContentValues args = new ContentValues();
        args.put("ZANSWER", 0);
        args.put("ZSCORE", 0);
        args.put("ISLEARNED", 0);
        SQLiteDatabase mDb = getWritableDatabase();
        int result =  mDb.update("ZTESTQUEST", args, "TESTID" + "=?", new String[] {String.valueOf(test.getIdTest())});
        try {
            mDb.close();
        }catch (Exception e) {

        }
    }

    public int getSumScoreQuestByTest(Test test){
        int totalQuest = 0;
        String query = "SELECT sum(ZSCORE) from ZTESTQUEST WHERE TESTID = " + test.getIdTest() + ";";
        SQLiteDatabase sqLiteDatabase = getWritableDatabase();
        Cursor cursor = sqLiteDatabase.rawQuery(query, null);
        if (cursor != null && cursor.moveToFirst()) {
            totalQuest = cursor.getInt(0);
        }
        try {
            cursor.close();
        } catch (Exception e) {

        }
        try {
            sqLiteDatabase.close();
        } catch (Exception e) {

        }
        return totalQuest;
    }

    public int updateQuestionAnswerTest(TestQuest testQuest){
        ContentValues args = new ContentValues();
        args.put("ZSCORE", testQuest.getScore());
        args.put("ZANSWER", testQuest.getAnswer());
        args.put("ISLEARNED",testQuest.isLearned() ? 1 : 0);
        SQLiteDatabase mDb = getWritableDatabase();
        int result =  mDb.update("ZTESTQUEST", args, "ZTESTQUESTID" + "=?", new String[] {String.valueOf(testQuest.getId())});
        try {
            mDb.close();
        }catch (Exception e) {
        }
        return result;
    }

    public int countTotalQuestionMarked(){
        int result = -1;
        SQLiteDatabase localSQLiteDatabase = getWritableDatabase();
        Cursor cursor = localSQLiteDatabase.rawQuery("SELECT count(*)" + " from ZQUESTION WHERE ZQUESTION.MARKED > 0", new String[]{});
        if (cursor.moveToFirst()) {
            result = cursor.getInt(0);
        }
        try {
            cursor.close();
        } catch (Exception e) {

        }
        try {
            localSQLiteDatabase.close();
        }catch (Exception e) {

        }
        return result;
    }

    public ArrayList<QuestionVideo> getQuestionByMaker(Context context) {
        ArrayList<QuestionVideo> listResult = new ArrayList<QuestionVideo>();
        SQLiteDatabase localSQLiteDatabase = getWritableDatabase();
        String query = "select * from ZQUESTION where MARKED > 0 ORDER by MARKED desc;";
        Cursor cursor = localSQLiteDatabase.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            do {
                try {
                    QuestionVideo question = new QuestionVideo();
                    question.setId(cursor.getInt(0));
                    question.setName(cursor.getString(1));
                    question.setVideoName(cursor.getString(2));
                    question.setCategoryID(cursor.getInt(3));
                    question.setDescription(cursor.getString(4));
                    question.setTotal_length(Integer.parseInt(AESCrypt.decrypt(JNIUtil.apiKey(context), cursor.getString(5))));
                    question.setBegin_pos(Integer.parseInt(AESCrypt.decrypt(JNIUtil.apiKey(context), cursor.getString(6))));
                    question.setEnd_pos(Integer.parseInt(AESCrypt.decrypt(JNIUtil.apiKey(context), cursor.getString(7))));
                    question.setImage(AESCrypt.decrypt(JNIUtil.apiKey(context), cursor.getString(8)));
                    question.setScore(cursor.getInt(9));
                    question.setTimeSelected(cursor.getInt(10));
                    question.setMarked(cursor.getInt(11) > 0 ? true : false);
                    question.setImageDescription(AESCrypt.decrypt(JNIUtil.apiKey(context), cursor.getString(12)));
                    question.setTimeSelectedMaker(cursor.getInt(14));
                    question.setWrong(cursor.getInt(16));
                    try{
                        question.setVideoLink(AESCrypt.decrypt(JNIUtil.apiKey(context),cursor.getString(17)));
                    }catch (Exception e){
                        question.setVideoLink(cursor.getString(17));
                    }
                    try{
                        question.setExplantion(AESCrypt.decrypt(JNIUtil.apiKey(context),cursor.getString(18)));
                    }catch (Exception e){
                        question.setExplantion(cursor.getString(18));
                    }
                    listResult.add(question);
                } catch (Exception e) {

                }
            } while (cursor.moveToNext());
        }
        return listResult;
    }

    public void updateTestByID(Test test){
        SQLiteDatabase localSQLiteDatabase = getWritableDatabase();
        String query = "SELECT * from ZTEST where IDTEST = "+ test.getIdTest() +";";
        Cursor cursor = localSQLiteDatabase.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            test.setIdTest(cursor.getInt(0));
            test.setCurrentQuest(cursor.getInt(1));
            test.setFinish(cursor.getInt(2) == 0 ? false : true);
            test.setTotalScore(cursor.getInt(3));
        }
        test.setCountQuestion(countTestQuestionChecked(test));
        try {
            cursor.close();
        } catch (Exception e) {

        }
        try {
            localSQLiteDatabase.close();
        } catch (Exception e) {

        }
    }

    public int updateQuestionAnswerMaker(QuestionVideo questionVideo){
        ContentValues args = new ContentValues();
        args.put("TIME_SELECTED_MAKER", questionVideo.getTimeSelectedMaker());
        SQLiteDatabase mDb = getWritableDatabase();
        int result =  mDb.update("ZQUESTION", args, "Z_PK" + "=?", new String[] {String.valueOf(questionVideo.getId())});
        try {
            mDb.close();
        }catch (Exception e) {
        }
        return result;
    }

    public void updateWrong(QuestionVideo questionVideo){
        if(existsColumnInTable("ZQUESTION","WRONG")) {
            ContentValues args = new ContentValues();
            args.put("WRONG", questionVideo.getWrong());
            SQLiteDatabase mDb = getWritableDatabase();
            mDb.update("ZQUESTION", args, "Z_PK = ?", new String[]{String.valueOf(questionVideo.getId())});
            try {
                mDb.close();
            } catch (Exception e) {

            }
        }
    }

    public boolean existsColumnInTable(String inTable, String columnToCheck) {
        SQLiteDatabase mDb = getWritableDatabase();
        Cursor mCursor = null;
        try {
            // Query 1 row
            mCursor = mDb.rawQuery("SELECT * FROM " + inTable + " LIMIT 0", null);

            // getColumnIndex() gives us the index (0 to ...) of the column - otherwise we get a -1
            if (mCursor.getColumnIndex(columnToCheck) != -1)
                return true;
            else
                return false;

        } catch (Exception Exp) {
            // Something went wrong. Missing the database? The table?
            Log.d("... - existsColumnInTable", "When checking whether a column exists in the table, an error occurred: " + Exp.getMessage());
            return false;
        } finally {
            try{
                mDb.close();
                if (mCursor != null) mCursor.close();
            }catch (Exception e){

            }
        }
    }



    public int countTotalQuestionWrong(){
        int result = -1;
        try {
            SQLiteDatabase localSQLiteDatabase = getWritableDatabase();
            Cursor cursor = localSQLiteDatabase.rawQuery("SELECT count(*)" + " from ZQUESTION WHERE ZQUESTION.WRONG > 0", new String[]{});
            if (cursor.moveToFirst()) {
                result = cursor.getInt(0);
            }
            try {
                cursor.close();
            } catch (Exception e) {

            }
            try {
                localSQLiteDatabase.close();
            }catch (Exception e) {

            }
        }catch (Exception e){
            result = 0;
        }
        return result;
    }

    public ArrayList<QuestionVideo> getQuestionByWrong(Context context) {
        ArrayList<QuestionVideo> listResult = new ArrayList<QuestionVideo>();
        SQLiteDatabase localSQLiteDatabase = getWritableDatabase();
        String query = "select * from ZQUESTION where WRONG > 0 ORDER by WRONG desc;";
        Cursor cursor = localSQLiteDatabase.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            do {
                try {
                    QuestionVideo question = new QuestionVideo();
                    question.setId(cursor.getInt(0));
                    question.setName(cursor.getString(1));
                    question.setVideoName(cursor.getString(2));
                    question.setCategoryID(cursor.getInt(3));
                    question.setDescription(cursor.getString(4));
                    question.setTotal_length(Integer.parseInt(AESCrypt.decrypt(JNIUtil.apiKey(context), cursor.getString(5))));
                    question.setBegin_pos(Integer.parseInt(AESCrypt.decrypt(JNIUtil.apiKey(context), cursor.getString(6))));
                    question.setEnd_pos(Integer.parseInt(AESCrypt.decrypt(JNIUtil.apiKey(context), cursor.getString(7))));
                    question.setImage(AESCrypt.decrypt(JNIUtil.apiKey(context), cursor.getString(8)));
                    question.setScore(cursor.getInt(9));
                    question.setTimeSelected(cursor.getInt(10));
                    question.setMarked(cursor.getInt(11) > 0 ? true : false);
                    question.setImageDescription(AESCrypt.decrypt(JNIUtil.apiKey(context), cursor.getString(12)));
                    question.setTimeSelectedWrong(cursor.getInt(15));
                    question.setWrong(cursor.getInt(16));
                    try{
                        question.setVideoLink(AESCrypt.decrypt(JNIUtil.apiKey(context),cursor.getString(17)));
                    }catch (Exception e){
                        question.setVideoLink(cursor.getString(17));
                    }
                    try{
                        question.setExplantion(AESCrypt.decrypt(JNIUtil.apiKey(context),cursor.getString(18)));
                    }catch (Exception e){
                        question.setExplantion(cursor.getString(18));
                    }
                    listResult.add(question);
                } catch (Exception e) {

                }
            } while (cursor.moveToNext());
        }
        return listResult;
    }

    public int updateQuestionAnswerWrong(QuestionVideo questionVideo){
        ContentValues args = new ContentValues();
        args.put("TIME_SELECTED_WRONG", questionVideo.getTimeSelectedWrong());
        SQLiteDatabase mDb = getWritableDatabase();
        int result =  mDb.update("ZQUESTION", args, "Z_PK" + "=?", new String[] {String.valueOf(questionVideo.getId())});
        try {
            mDb.close();
        }catch (Exception e) {
        }
        return result;
    }

    public QuestionVideo getQuestionVideoByID(Context context,int id){
        QuestionVideo question = new QuestionVideo();
        SQLiteDatabase localSQLiteDatabase = getWritableDatabase();
        String query = "SELECT * FROM ZQUESTION WHERE Z_PK = " + id + ";";

        Cursor cursor = localSQLiteDatabase.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            question.setId(cursor.getInt(0));
            question.setName(cursor.getString(1));
            question.setVideoName(cursor.getString(2));
            question.setCategoryID(cursor.getInt(3));
            question.setDescription(cursor.getString(4));
            try {
                question.setTotal_length(Integer.parseInt(AESCrypt.decrypt(JNIUtil.apiKey(context),cursor.getString(5))));
            }catch ( Exception e){
                question.setTotal_length(Integer.parseInt(cursor.getString(5)));
            }
            try {
                question.setBegin_pos(Integer.parseInt(AESCrypt.decrypt(JNIUtil.apiKey(context),cursor.getString(6))));
            }catch ( Exception e){
                question.setBegin_pos(Integer.parseInt(cursor.getString(6)));
            }
            try {
                question.setEnd_pos(Integer.parseInt(AESCrypt.decrypt(JNIUtil.apiKey(context),cursor.getString(7))));
            }catch ( Exception e){
                question.setEnd_pos(Integer.parseInt(cursor.getString(7)));
            }
            try {
                question.setImage(AESCrypt.decrypt(JNIUtil.apiKey(context),cursor.getString(8)));
            }catch ( Exception e){
                question.setImage(cursor.getString(8));
            }
            question.setScore(cursor.getInt(9));
            question.setTimeSelected(cursor.getInt(10));
            question.setMarked(cursor.getInt(11) > 0 ? true : false);
            try {
                question.setImageDescription(AESCrypt.decrypt(JNIUtil.apiKey(context),cursor.getString(12)));
            }catch ( Exception e){
                question.setImageDescription(cursor.getString(12));
            }
            try{
                question.setVideoLink(AESCrypt.decrypt(JNIUtil.apiKey(context),cursor.getString(17)));
            }catch (Exception e){
                try{
                    question.setVideoLink(cursor.getString(17));
                }catch (Exception ex){

                }
            }
            try{
                question.setExplantion(AESCrypt.decrypt(JNIUtil.apiKey(context),cursor.getString(18)));
            }catch (Exception e){
                try{
                    question.setExplantion(cursor.getString(18));
                }catch (Exception ex){

                }
            }
        }
        try {
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            localSQLiteDatabase.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return question;
    }

    public QuestionVideo getRandomQuestionVideo(Context context){
        QuestionVideo question = new QuestionVideo();
        SQLiteDatabase localSQLiteDatabase = getWritableDatabase();
        String query = "SELECT * FROM ZQUESTION ORDER BY RANDOM() LIMIT 1";

        Cursor cursor = localSQLiteDatabase.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            question.setId(cursor.getInt(0));
            question.setName(cursor.getString(1));
            question.setVideoName(cursor.getString(2));
            question.setCategoryID(cursor.getInt(3));
            question.setDescription(cursor.getString(4));
            try {
                question.setTotal_length(Integer.parseInt(AESCrypt.decrypt(JNIUtil.apiKey(context),cursor.getString(5))));
            }catch ( Exception e){
                question.setTotal_length(Integer.parseInt(cursor.getString(5)));
            }
            try {
                question.setBegin_pos(Integer.parseInt(AESCrypt.decrypt(JNIUtil.apiKey(context),cursor.getString(6))));
            }catch ( Exception e){
                question.setBegin_pos(Integer.parseInt(cursor.getString(6)));
            }
            try {
                question.setEnd_pos(Integer.parseInt(AESCrypt.decrypt(JNIUtil.apiKey(context),cursor.getString(7))));
            }catch ( Exception e){
                question.setEnd_pos(Integer.parseInt(cursor.getString(7)));
            }
            try {
                question.setImage(AESCrypt.decrypt(JNIUtil.apiKey(context),cursor.getString(8)));
            }catch ( Exception e){
                question.setImage(cursor.getString(8));
            }
            question.setScore(cursor.getInt(9));
            question.setTimeSelected(cursor.getInt(10));
            question.setMarked(cursor.getInt(11) > 0 ? true : false);
            try {
                question.setImageDescription(AESCrypt.decrypt(JNIUtil.apiKey(context),cursor.getString(12)));
            }catch ( Exception e){
                question.setImageDescription(cursor.getString(12));
            }
            try{
                question.setVideoLink(AESCrypt.decrypt(JNIUtil.apiKey(context),cursor.getString(17)));
            }catch (Exception e){
                try{
                    question.setVideoLink(cursor.getString(17));
                }catch (Exception ex){

                }
            }
            try{
                question.setExplantion(AESCrypt.decrypt(JNIUtil.apiKey(context),cursor.getString(18)));
            }catch (Exception e){
                try{
                    question.setExplantion(cursor.getString(18));
                }catch (Exception ex){

                }
            }
        }
        try {
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            localSQLiteDatabase.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return question;
    }

    public int countTotalQuestionCorrect(Test test){
        int totalQuest = 0;
        String query = "select count(*) from ZTESTQUEST where ZSCORE > 0 and ISLEARNED = 1 and TESTID = " + test.getIdTest();
        SQLiteDatabase sqLiteDatabase = getWritableDatabase();
        Cursor cursor = sqLiteDatabase.rawQuery(query, null);
        if (cursor != null && cursor.moveToFirst()) {
            totalQuest = cursor.getInt(0);
        }
        try {
            cursor.close();
        } catch (Exception e) {

        }
        try {
            sqLiteDatabase.close();
        } catch (Exception e) {

        }
        return totalQuest;
    }
//-----------------------------------------------------------------------------

    //ma hoa table
    //command theo quy tac tablename#id#columname....
    @SuppressLint("Range")
    public void encryptTable(String command){
        //begin load data
        String []commands = command.split("#");
        String tableName = commands[0];
        String tabbleId = commands[1];
        String tableColumn = "";
        Log.e("encrypt","command: " + command);
        for(int i = 2 ; i < commands.length ; i++){
            tableColumn += commands[i] + ",";
        }
        tableColumn = tableColumn.substring(0,tableColumn.length() - 1);

        SQLiteDatabase localSQLiteDatabase = getWritableDatabase();
        Cursor cursor = localSQLiteDatabase.rawQuery("select " + tabbleId + "," + tableColumn + " from " + tableName, null);
        ArrayList<ArrayList<String>> listDataOriginal = new ArrayList<ArrayList<String>>();
        if (cursor.moveToFirst()) {
            do {
                ArrayList<String> data = new ArrayList<>();
                data.add(String.valueOf(cursor.getInt(0)));
                for(int i = 2 ; i < commands.length ; i++){
                    data.add(cursor.getString(cursor.getColumnIndex(commands[i])) == null ? "" : cursor.getString(cursor.getColumnIndex(commands[i])));
                }

                listDataOriginal.add(data);
            } while (cursor.moveToNext());
        }
        try {
            cursor.close();
        } catch (Exception e) {

        }
        try {
            localSQLiteDatabase.close();
        }catch (Exception e) {

        }
        Log.e("encrypt","size update: " + listDataOriginal.size());
        for(int i = 0; i < listDataOriginal.size(); i++){
            ArrayList<String> data = listDataOriginal.get(i);
            ContentValues args = new ContentValues();
            for(int j = 2 ; j < commands.length ; j++){
                try {
                    if(data.get(j - 1).isEmpty()){
                        args.put(commands[j],"");
                    }else {
                        args.put(commands[j], AESCrypt.encrypt("TheoryTest1@#$", data.get(j - 1)));
                    }
                }catch (Exception e){
                    Log.e("encrypt","AESCrypt faile: " + String.valueOf(data.get(0)));
                }
            }

            SQLiteDatabase mDb = getWritableDatabase();
            int result =  mDb.update(tableName, args, tabbleId + " = ?",new String[] {String.valueOf(data.get(0))});
            try {
                mDb.close();
            }catch (Exception e) {
                Log.e("encrypt","encrypt faile: " + String.valueOf(data.get(0)));
            }
        }
        Log.e("encrypt","done encrypt: " + tableName);
    }

    public int insertTestQuest(QuestionVideo questionVideo, int testid) {
        ContentValues args = new ContentValues();
        args.put("TESTID", testid);
        args.put("ZQUESTIONID", questionVideo.getId());
        SQLiteDatabase mDb = getWritableDatabase();
        int result = (int)mDb.insert("ZTESTQUEST","IDTEST",args);
        try {
            mDb.close();
        } catch (Exception e) {

        }
        return result;
    }

    public Test getTestWithID(int idTest){
        Test testResult = new Test();
        String query = "select * from ZTEST where ZTEST.IDTEST = " + idTest;
        SQLiteDatabase sqLiteDatabase = getWritableDatabase();
        Cursor cursor = sqLiteDatabase.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            do {
                testResult.setIdTest(cursor.getInt(0));
                testResult.setCurrentQuest(cursor.getInt(1));
                testResult.setFinish(cursor.getInt(2) == 0 ? false : true);
                testResult.setTotalScore(cursor.getInt(3));
            } while (cursor.moveToNext());
        }
        try {
            cursor.close();
        } catch (Exception e) {

        }
        try {
            sqLiteDatabase.close();
        }catch (Exception e) {

        }
        testResult.setCountQuestion(countTestQuestionChecked(testResult));
        return testResult;
    }

    public int getCountQuestCheckedLearn(){
        int totalQuest = 0;
        String query = "SELECT count(Z_PK) from ZQUESTION WHERE TIME_SELECTED > 0;";
        SQLiteDatabase sqLiteDatabase = getWritableDatabase();
        Cursor cursor = sqLiteDatabase.rawQuery(query, null);
        if (cursor != null && cursor.moveToFirst()) {
            totalQuest = cursor.getInt(0);
        }
        try {
            cursor.close();
        } catch (Exception e) {

        }
        try {
            sqLiteDatabase.close();
        } catch (Exception e) {

        }
        return totalQuest;
    }

    public int getCountQuestCheckTest(){
        int totalQuest = 0;
        String query = "SELECT count(ZTESTQUESTID) from ZTESTQUEST WHERE ZANSWER > 0;";
        SQLiteDatabase sqLiteDatabase = getWritableDatabase();
        Cursor cursor = sqLiteDatabase.rawQuery(query, null);
        if (cursor != null && cursor.moveToFirst()) {
            totalQuest = cursor.getInt(0);
        }
        try {
            cursor.close();
        } catch (Exception e) {

        }
        try {
            sqLiteDatabase.close();
        } catch (Exception e) {

        }
        return totalQuest;
    }

    public int getTotalQuestCorrect() {
        int totalQuest = 0;
        String query = "SELECT count(Z_PK) from ZQUESTION WHERE  SCORED >= 3;";
        SQLiteDatabase sqLiteDatabase = getReadableDatabase();
        Cursor cursor = sqLiteDatabase.rawQuery(query, null);
        if (cursor != null && cursor.moveToFirst()) {
            totalQuest = cursor.getInt(0);
        }
        try {
            cursor.close();
        } catch (Exception e) {

        }
        try {
            sqLiteDatabase.close();
        } catch (Exception e) {

        }
        return totalQuest;
    }

    public int getTotalDefaultTestPass() {
        int totalQuest = 0;
        String query = "select count(*) from ZTEST where TOTAL_POINT_CORRECT >= 35 and IDTEST <= 12;";
        SQLiteDatabase sqLiteDatabase = getReadableDatabase();
        Cursor cursor = sqLiteDatabase.rawQuery(query, null);
        if (cursor != null && cursor.moveToFirst()) {
            totalQuest = cursor.getInt(0);
        }
        try {
            cursor.close();
        } catch (Exception e) {

        }
        try {
            sqLiteDatabase.close();
        } catch (Exception e) {

        }
        return totalQuest;
    }

    public int getTotalDefaultTestRandom() {
        int totalQuest = 0;
        String query = "select count(*) from ZTEST where TOTAL_POINT_CORRECT >= 35 and IDTEST > 12;";
        SQLiteDatabase sqLiteDatabase = getReadableDatabase();
        Cursor cursor = sqLiteDatabase.rawQuery(query, null);
        if (cursor != null && cursor.moveToFirst()) {
            totalQuest = cursor.getInt(0);
        }
        try {
            cursor.close();
        } catch (Exception e) {

        }
        try {
            sqLiteDatabase.close();
        } catch (Exception e) {

        }
        return totalQuest;
    }
}