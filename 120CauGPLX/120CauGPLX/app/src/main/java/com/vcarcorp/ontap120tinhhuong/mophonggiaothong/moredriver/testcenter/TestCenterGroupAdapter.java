package com.vcarcorp.ontap120tinhhuong.mophonggiaothong.moredriver.testcenter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.text.Spannable;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ads.control.customview.CustomTextView;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.R;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.model.TestCenter;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.model.TestCenterGroup;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class TestCenterGroupAdapter extends RecyclerView.Adapter<TestCenterGroupAdapter.ViewHolder> {

    private List<TestCenterGroup> data = new ArrayList<>();
    private List<TestCenterGroup> dataList = new ArrayList<>();
    private Context context;
    private ItemClickListener mClickListener;
    private String searchString = "";

    public TestCenterGroupAdapter(final List<TestCenterGroup> data, Context mContext) {
        this.data = data;
        this.dataList.addAll(this.data);
        this.context = mContext;
    }

    @Override
    public ViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
        this.context = parent.getContext();
        return new ViewHolder(LayoutInflater.from(context)
                .inflate(R.layout.item_test_center_group, parent, false));
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, @SuppressLint("RecyclerView") int position) {
        TestCenterGroup testCenterGroup = dataList.get(position);
        holder.testCenterAdapter.notifyData(testCenterGroup.getTestCenters());
        holder.testCenterAdapter.setTextFilter(searchString);
        String search = testCenterGroup.getName().toLowerCase(Locale.getDefault());
        if (search!=null && searchString !=null &&!searchString.equals("") && search.contains(searchString)) {
            int startPos = search.indexOf(searchString);
            int endPos = startPos + searchString.length();
            if (endPos > search.length()) {
                endPos = search.length();
            }
            Spannable spanText = Spannable.Factory.getInstance().newSpannable(testCenterGroup.getName());
            spanText.setSpan(new ForegroundColorSpan(context.getResources().getColor(R.color.red)), startPos, endPos, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            holder.tvProvince.setText(spanText, TextView.BufferType.SPANNABLE);
        } else {
            holder.tvProvince.setText(testCenterGroup.getName());
        }
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }


    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        searchString = charText;
        dataList.clear();
        ArrayList<TestCenterGroup> list = new ArrayList<TestCenterGroup>();
        if (charText.length() != 0) {
            for (TestCenterGroup temp : data) {
                if (temp.getName().toLowerCase(Locale.getDefault()).contains(charText)) {
                    list.add(temp);
                } else {
                    ArrayList<TestCenter> listTestCenter = new ArrayList<TestCenter>();
                    for (TestCenter testCenter: temp.getTestCenters()) {
                        if (testCenter.getName().toLowerCase(Locale.getDefault()).contains(charText) ||
                                testCenter.getAddress().toLowerCase(Locale.getDefault()).contains(charText)) {
                            listTestCenter.add(testCenter);
                        }
                    }
                    if (listTestCenter.size() > 0) {
                        TestCenterGroup testCenterGroup = new TestCenterGroup();
                        testCenterGroup.setName(temp.getName());
                        testCenterGroup.setTestCenters(listTestCenter);
                        list.add(testCenterGroup);
                    }
                }
            }
            dataList.addAll(list);
        } else {
            dataList.addAll(data);
        }
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private CustomTextView tvProvince;
        private RecyclerView recyclerView;
        private TestCenterAdapter testCenterAdapter;

        public ViewHolder(View v) {
            super(v);
            tvProvince = (CustomTextView) v.findViewById(R.id.tv_province);
            recyclerView = (RecyclerView) v.findViewById(R.id.recycle_view);
            recyclerView.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
            testCenterAdapter = new TestCenterAdapter(context);
            recyclerView.setAdapter(testCenterAdapter);
            testCenterAdapter.setClickListener(new TestCenterAdapter.ItemClickListener() {
                @Override
                public void onItemClick(String phoneNumber) {
                    mClickListener.onItemClick(phoneNumber);
                }
            });
        };
    }

    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    public interface ItemClickListener {
        void onItemClick(String phone);
    }
}
