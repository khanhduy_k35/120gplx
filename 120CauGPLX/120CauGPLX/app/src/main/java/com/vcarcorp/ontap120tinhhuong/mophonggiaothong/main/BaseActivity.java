package com.vcarcorp.ontap120tinhhuong.mophonggiaothong.main;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import android.os.Environment;
import android.os.SystemClock;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ads.control.activity.BaseAdsActivity;
import com.ads.control.google_iab.BillingConnector;
import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.Task;
import com.google.android.play.core.review.ReviewInfo;
import com.google.android.play.core.review.ReviewManager;
import com.google.android.play.core.review.ReviewManagerFactory;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.BuildConfig;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.database.GPLXDataManager;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.model.QuestionVideo;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.model.VersionData;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.util.AESCrypt;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;
import com.rey.material.drawable.RippleDrawable;
import com.rey.material.widget.FrameLayout;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.R;
import com.ads.control.customview.CustomTextView;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.util.DialogUtil;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.util.MyFileNameGenerator;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.util.SharedPreferencesUtils;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.util.Util;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.util.AppUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public abstract class BaseActivity extends BaseAdsActivity {
    private DatabaseReference mDatabase;
    public static BillingConnector billingConnector;
    public static boolean isShowSuggestProRemoveAds = false;
    public static FirebaseAnalytics mFirebaseAnalytics;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        setTheme();
        super.onCreate(savedInstanceState);
        Window window = this.getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | 0);
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(Color.TRANSPARENT);
        }
        if(mFirebaseAnalytics == null){
            mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        }
        mDatabase = FirebaseDatabase.getInstance("https://caugplx-default-rtdb.asia-southeast1.firebasedatabase.app").getReference();
        if(getClass().toString().contains(SplashActivity.class.toString())) {
            loadDataFromFirebase();
        }
        if(getClass().toString().contains(MainActivity.class.toString())) {
            loadDataDetail();
        }
        resetAllSettingNonPro();
    }

    public void resetAllSettingNonPro(){
        if(!Util.isPro(this)){
            SharedPreferencesUtils.setShowScoreStop(false);
            SharedPreferencesUtils.setShowDetail(false);
            SharedPreferencesUtils.setShowReminder(true);
        }
    }

    public void setTheme(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            View decor = getWindow().getDecorView();
            if (SharedPreferencesUtils.isNightMode()) {
                setTheme(R.style.AppThemeDark);
                decor.setSystemUiVisibility(0);
                getWindow().setStatusBarColor(Color.parseColor("#000000"));
                getWindow().setNavigationBarColor(Color.parseColor("#000000"));
            } else {
                //setTheme(R.style.AppThemeDark);
                // We want to change tint color to white again.
                // You can also record the flags in advance so that you can turn UI back completely if
                // you have set other flags before, such as translucent or full screen.
                setTheme(R.style.AppThemeLight);
                decor.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
                getWindow().setStatusBarColor(Color.parseColor("#f2f2f2"));
                getWindow().setNavigationBarColor(Color.parseColor("#f2f2f2"));
            }
        }
    }

    public void setForceDarkTheme(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            View decor = getWindow().getDecorView();
            setTheme(R.style.AppThemeDark);
            decor.setSystemUiVisibility(0);
            getWindow().setStatusBarColor(Color.parseColor("#000000"));
            getWindow().setNavigationBarColor(Color.parseColor("#000000"));
        }
    }

    protected void setUpToolBar(FrameLayout v){
        if(v!=null) {
            TypedValue tv = new TypedValue();
            if (getTheme().resolveAttribute(android.R.attr.actionBarSize, tv, true)) {
                int actionBarHeight = TypedValue.complexToDimensionPixelSize(tv.data, getResources().getDisplayMetrics());

                RippleDrawable rippleDrawable = new RippleDrawable.Builder()
                        .cornerRadius(actionBarHeight / 2)
                        .backgroundAnimDuration(300)
                        .rippleColor(Color.parseColor("#114c4c4c"))
                        .backgroundColor(Color.parseColor("#074c4c4c"))
                        .build();
                v.setBackgroundDrawable(rippleDrawable);
            }
        }
    }

    @Override
    public void setContentView(@LayoutRes int layoutResID) {
        super.setContentView(layoutResID);
    }

    public abstract class OnSingleClickListener implements View.OnClickListener {
        private static final long MIN_CLICK_INTERVAL = 500;
        private long mLastClickTime;
        public abstract void onSingleClick(View v);

        @Override
        public final void onClick(View v) {
            long currentClickTime= SystemClock.uptimeMillis();
            long elapsedTime=currentClickTime-mLastClickTime;
            mLastClickTime=currentClickTime;
            if(elapsedTime <= MIN_CLICK_INTERVAL)
                return;
            onSingleClick(v);
        }
    }

    public void showDialogDownloadNewApp(){
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Window window = dialog.getWindow();
        window.setBackgroundDrawableResource(android.R.color.transparent);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.customdialog_moreapp);
        final TextView btn_quit = (TextView) dialog.findViewById(R.id.btn_custom_dialog_exit_quit);
        final TextView btn_ok = (TextView) dialog.findViewById(R.id.btn_custom_dialog_exit_ok);
        final CustomTextView tv_app_name = (CustomTextView) dialog.findViewById(R.id.tv_app_name);
        final ImageView iconApp = (ImageView)dialog.findViewById(R.id.icon_app);
        final com.rey.material.widget.TextView btn_install = (com.rey.material.widget.TextView)dialog.findViewById(R.id.btn_install);
        final CustomTextView tv_description_1 = (CustomTextView) dialog.findViewById(R.id.tv_description_1);
        final CustomTextView tv_description_2 = (CustomTextView) dialog.findViewById(R.id.tv_description_2);
        final ImageView img_ads_1 = (ImageView) dialog.findViewById(R.id.img_ads_1);
        final ImageView img_ads_2 = (ImageView) dialog.findViewById(R.id.img_ads_2);
        final ImageView img_ads_3 = (ImageView) dialog.findViewById(R.id.img_ads_3);

        Typeface type = Typeface.createFromAsset(getAssets(), "fonts/RobotoCondensed-Bold.ttf");
        btn_quit.setTypeface(type);
        tv_app_name.setTypeface(type);
        btn_install.setTypeface(type);
        btn_ok.setTypeface(type);

        type = Typeface.createFromAsset(getAssets(), "fonts/RobotoCondensed-Regular.ttf");
        tv_description_1.setTypeface(type);
        tv_description_2.setTypeface(type);
        tv_app_name.setText(SharedPreferencesUtils.getString("newapp_name"));
        tv_description_1.setText(SharedPreferencesUtils.getString("newapp_des1"));
        tv_description_2.setText(SharedPreferencesUtils.getString("newapp_des2"));
        btn_quit.setVisibility(View.INVISIBLE);
        iconApp.setVisibility(View.GONE);
        Glide.with(this).load(SharedPreferencesUtils.getString("newapp_img1")).into(img_ads_1);
        Glide.with(this).load(SharedPreferencesUtils.getString("newapp_img2")).into(img_ads_2);
        Glide.with(this).load(SharedPreferencesUtils.getString("newapp_img3")).into(img_ads_3);

        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String appPackageName = SharedPreferencesUtils.getString("newapp_package");
                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                } catch (android.content.ActivityNotFoundException anfe) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                }
            }
        });
        btn_install.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String appPackageName = SharedPreferencesUtils.getString("newapp_package");
                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                } catch (android.content.ActivityNotFoundException anfe) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                }
            }
        });
        dialog.show();
    }

    public void showDialogDownloadMoreApp(final boolean isExit){
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Window window = dialog.getWindow();
        window.setBackgroundDrawableResource(android.R.color.transparent);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.customdialog_moreapp);
        dialog.getWindow().setGravity(Gravity.BOTTOM);
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        final TextView btn_quit = (TextView) dialog.findViewById(R.id.btn_custom_dialog_exit_quit);
        final TextView btn_ok = (TextView) dialog.findViewById(R.id.btn_custom_dialog_exit_ok);
        final CustomTextView tv_app_name = (CustomTextView) dialog.findViewById(R.id.tv_app_name);
        final ImageView iconApp = (ImageView)dialog.findViewById(R.id.icon_app);
        final com.rey.material.widget.TextView btn_install = (com.rey.material.widget.TextView)dialog.findViewById(R.id.btn_install);
        final CustomTextView tv_description_1 = (CustomTextView) dialog.findViewById(R.id.tv_description_1);
        final CustomTextView tv_description_2 = (CustomTextView) dialog.findViewById(R.id.tv_description_2);
        final ImageView img_ads_1 = (ImageView) dialog.findViewById(R.id.img_ads_1);
        final ImageView img_ads_2 = (ImageView) dialog.findViewById(R.id.img_ads_2);
        final ImageView img_ads_3 = (ImageView) dialog.findViewById(R.id.img_ads_3);
        tv_app_name.setText(SharedPreferencesUtils.getString("other_name"));
        tv_description_1.setText(SharedPreferencesUtils.getString("other_des1"));
        tv_description_2.setText(SharedPreferencesUtils.getString("other_des2"));
        btn_quit.setVisibility(View.VISIBLE);
        iconApp.setVisibility(View.VISIBLE);
        Glide.with(this).load(SharedPreferencesUtils.getString("other_img1")).into(img_ads_1);
        Glide.with(this).load(SharedPreferencesUtils.getString("other_img2")).into(img_ads_2);
        Glide.with(this).load(SharedPreferencesUtils.getString("other_img3")).into(img_ads_3);
        Glide.with(this).load(SharedPreferencesUtils.getString("other_icon")).into(iconApp);

        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
                dialog.dismiss();
                final String appPackageName = SharedPreferencesUtils.getString("other_package");
                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                } catch (android.content.ActivityNotFoundException anfe) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                }
                finish();
            }
        });
        btn_install.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.cancel();
                dialog.dismiss();
                final String appPackageName = SharedPreferencesUtils.getString("other_package");
                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                } catch (android.content.ActivityNotFoundException anfe) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                }
                finish();
            }
        });
        if(isExit){
            btn_quit.setText("Exit");
        }else{
            btn_quit.setText("Install Later");
        }
        btn_quit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
                dialog.dismiss();
                if(isExit) {
                    finish();
                }
            }
        });

        dialog.show();
    }

    public boolean isPackageInstalled(String packagename, PackageManager packageManager) {
        try {
            packageManager.getPackageInfo(packagename, 0);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }

    public void openRating() {
        final String appPackageName = getPackageName();
        try {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
        } catch (android.content.ActivityNotFoundException anfe) {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
        }
    }

    public void loadDataFromFirebase(){
        mDatabase = FirebaseDatabase.getInstance("https://caugplx-default-rtdb.asia-southeast1.firebasedatabase.app").getReference();
        if(mDatabase!=null){
            mDatabase.child("version_120_mophong").addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    try {
                        VersionData versionData = dataSnapshot.getValue(VersionData.class);
                        SharedPreferencesUtils.setString("link_img",versionData.getLink_img());
                        SharedPreferencesUtils.setShowProFisrt(versionData.isShowProFisrt());
                        SharedPreferencesUtils.setNumberQuestFree(versionData.getNumberQuestFree());
                        SharedPreferencesUtils.setNumberTestFree(versionData.getNumberTestFree());
                        SharedPreferencesUtils.setShowAds3g4GVietetel(versionData.isAdsViettel());
                        SharedPreferencesUtils.setShowAds3g4GVina(versionData.isAdsVina());
                        SharedPreferencesUtils.setShowAds3g4G(versionData.isAdsViettel() || versionData.isAdsVina());
                        SharedPreferencesUtils.setMainver(versionData.getMain_ver());
                        SharedPreferencesUtils.setOpenLink(versionData.isOpenLink());
                        SharedPreferencesUtils.setOnlyLink(versionData.isOnlyLink());
                        SharedPreferencesUtils.setPhoneVT(versionData.getPhone());
                        SharedPreferencesUtils.setLong("time_using_setup",versionData.getTimeUsing() * 1000);
                        Log.e("khanhduy.le","lay settings ve");
                        if (!SharedPreferencesUtils.getString("version", "").equals(versionData.getVersion())) {
                            SharedPreferencesUtils.setBoolean("load_data",true);
                            SharedPreferencesUtils.setString("version", versionData.getVersion());
                            SharedPreferencesUtils.setString("date", versionData.getDate());
                        }else{
                            SharedPreferencesUtils.setBoolean("load_data",false);
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }
    }

    private void loadDataDetail() {
        if(SharedPreferencesUtils.getBoolean("load_data",false)) {
            if (mDatabase != null) {
                mDatabase.child("data_120_mophong/list_question").addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        Log.e("khanhduy.le","load loadDataDetail from firebase");
                        try {
                            ArrayList<QuestionVideo> videos = new ArrayList<QuestionVideo>();
                            Gson gson = new Gson();
                            for (DataSnapshot childDataSnapshot : dataSnapshot.getChildren()) {
                                String value = (String) childDataSnapshot.getValue();
                                QuestionVideo video = gson.fromJson(AESCrypt.decrypt(JNIUtil.apiKey(BaseActivity.this), value), QuestionVideo.class);
                                videos.add(video);
                            }
                            processData(videos);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
            }
        }
    }

    private void processData(ArrayList<QuestionVideo> questionVideos) {
        Collections.sort(questionVideos, new Comparator<QuestionVideo>() {
            @Override
            public int compare(QuestionVideo questionVideo, QuestionVideo t1) {
                if(questionVideo.getId() > t1.getId())
                    return 1;
                else if(questionVideo.getId() == t1.getId())
                    return 0;
                else
                    return -1;
            }
        });
        GPLXDataManager theoryDB = new GPLXDataManager(this);
        ArrayList<QuestionVideo> videos = theoryDB.getAllQuestionVideo(this);
        boolean isDeleteCache = false;
        String valueUpdate = "";
        for (int i = 0; i < questionVideos.size(); i++) {
            for (int j = 0; j < videos.size(); j++) {
                QuestionVideo videoData = videos.get(j);
                if (videoData.getId() == questionVideos.get(i).getId()){
                    if (videoData.validateObject(questionVideos.get(i)) == false) {
                        theoryDB.updateQuestionVideo(questionVideos.get(i),this);
                        if(!isDeleteCache){
                            try {
                                Glide.get(this).clearMemory();
                                deleteCache(this);
                                isDeleteCache = true;
                                SharedPreferencesUtils.setUpdateQuestionID("");
                            }catch (Exception e){

                            }
                        }
                        valueUpdate += questionVideos.get(i).getId() + ", ";
                        removeCacheOld(questionVideos.get(i).getId());
                        break;
                    }
                }
            }
        }
        if(valueUpdate.endsWith(", ")){
            valueUpdate = valueUpdate.substring(0,valueUpdate.length() - 2);
        }
        if(!valueUpdate.equals("")){
            SharedPreferencesUtils.setUpdateQuestionID(valueUpdate);
        }
        try {
            theoryDB.close();
        }catch (Exception e){

        }
    }

    public void removeCacheOld(int idQuestion){
        try{
            File fileImgDetail = new File(getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS).toString() + "/img_app/doneimg/" + "TH" + idQuestion + ".webp");
            if(fileImgDetail.exists()){
                fileImgDetail.delete();
            }
            File f = new File(getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS).toString() + "/" + new MyFileNameGenerator(this).generateFileName("hn_female_maiphuong_vdts_48k-fhg_description_"+ idQuestion +".mp3"));
            if(f.exists()){
                f.delete();
            }
            f = new File(getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS).toString() + "/" + new MyFileNameGenerator(this).generateFileName("hn_female_maiphuong_vdts_48k-fhg_name_"+ idQuestion +".mp3"));
            if(f.exists()){
                f.delete();
            }
            f = new File(getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS).toString() + "/" + new MyFileNameGenerator(this).generateFileName("hn_female_ngochuyen_full_48k-fhg_description_"+ idQuestion +".mp3"));
            if(f.exists()){
                f.delete();
            }
            f = new File(getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS).toString() + "/" + new MyFileNameGenerator(this).generateFileName("hn_female_ngochuyen_full_48k-fhg_name_"+ idQuestion +".mp3"));
            if(f.exists()){
                f.delete();
            }
            f = new File(getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS).toString() + "/" + new MyFileNameGenerator(this).generateFileName("hn_male_manhdung_news_48k-fhg_description_"+ idQuestion +".mp3"));
            if(f.exists()){
                f.delete();
            }
            f = new File(getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS).toString() + "/" + new MyFileNameGenerator(this).generateFileName("hn_male_manhdung_news_48k-fhg_name_"+ idQuestion +".mp3"));
            if(f.exists()){
                f.delete();
            }
            f = new File(getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS).toString() + "/" + new MyFileNameGenerator(this).generateFileName("hn_male_phuthang_news65dt_44k-fhg_description_"+ idQuestion +".mp3"));
            if(f.exists()){
                f.delete();
            }
            f = new File(getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS).toString() + "/" + new MyFileNameGenerator(this).generateFileName("hn_male_phuthang_news65dt_44k-fhg_name_"+ idQuestion +".mp3"));
            if(f.exists()){
                f.delete();
            }
            f = new File(getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS).toString() + "/" + new MyFileNameGenerator(this).generateFileName("hn_male_thanhlong_talk_48k-fhg_description_"+ idQuestion +".mp3"));
            if(f.exists()){
                f.delete();
            }
            f = new File(getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS).toString() + "/" + new MyFileNameGenerator(this).generateFileName("hn_male_thanhlong_talk_48k-fhg_name_"+ idQuestion +".mp3"));
            if(f.exists()){
                f.delete();
            }
            f = new File(getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS).toString() + "/" + new MyFileNameGenerator(this).generateFileName("hue_female_huonggiang_full_48k-fhg_description_"+ idQuestion +".mp3"));
            if(f.exists()){
                f.delete();
            }
            f = new File(getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS).toString() + "/" + new MyFileNameGenerator(this).generateFileName("hue_female_huonggiang_full_48k-fhg_name_"+ idQuestion +".mp3"));
            if(f.exists()){
                f.delete();
            }
            f = new File(getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS).toString() + "/" + new MyFileNameGenerator(this).generateFileName("hue_male_duyphuong_full_48k-fhg_description_"+ idQuestion +".mp3"));
            if(f.exists()){
                f.delete();
            }
            f = new File(getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS).toString() + "/" + new MyFileNameGenerator(this).generateFileName("hue_male_duyphuong_full_48k-fhg_name_"+ idQuestion +".mp3"));
            if(f.exists()){
                f.delete();
            }
            f = new File(getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS).toString() + "/" + new MyFileNameGenerator(this).generateFileName("sg_female_lantrinh_vdts_48k-fhg_description_"+ idQuestion +".mp3"));
            if(f.exists()){
                f.delete();
            }
            f = new File(getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS).toString() + "/" + new MyFileNameGenerator(this).generateFileName("sg_female_lantrinh_vdts_48k-fhg_name_"+ idQuestion +".mp3"));
            if(f.exists()){
                f.delete();
            }
            f = new File(getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS).toString() + "/" + new MyFileNameGenerator(this).generateFileName("sg_female_thaotrinh_full_48k-fhg_description_"+ idQuestion +".mp3"));
            if(f.exists()){
                f.delete();
            }
            f = new File(getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS).toString() + "/" + new MyFileNameGenerator(this).generateFileName("sg_female_thaotrinh_full_48k-fhg_name_"+ idQuestion +".mp3"));
            if(f.exists()){
                f.delete();
            }
            f = new File(getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS).toString() + "/" + new MyFileNameGenerator(this).generateFileName("sg_male_minhhoang_full_48k-fhg_description_"+ idQuestion +".mp3"));
            if(f.exists()){
                f.delete();
            }
            f = new File(getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS).toString() + "/" + new MyFileNameGenerator(this).generateFileName("sg_male_minhhoang_full_48k-fhg_name_"+ idQuestion +".mp3"));
            if(f.exists()){
                f.delete();
            }
            f = new File(getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS).toString() + "/" + new MyFileNameGenerator(this).generateFileName("sg_male_trungkien_vdts_48k-fhg_description_"+ idQuestion +".mp3"));
            if(f.exists()){
                f.delete();
            }
            f = new File(getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS).toString() + "/" + new MyFileNameGenerator(this).generateFileName("sg_male_trungkien_vdts_48k-fhg_name_"+ idQuestion +".mp3"));
            if(f.exists()){
                f.delete();
            }
            f = new File(getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS).toString() + "/" + new MyFileNameGenerator(this).generateFileName(""+ idQuestion +".mp4"));
            if(f.exists()){
                f.delete();
            }
            f = new File(getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS).toString() + "/" + new MyFileNameGenerator(this).generateFileName("TH"+ idQuestion +".webp"));
            if(f.exists()){
                f.delete();
            }
        }catch (Exception e){

        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    public void showRatingInApp(){
        int count = SharedPreferencesUtils.getInt("count_rating",0);
        if(count % 10 == 5){
            SharedPreferencesUtils.setInt("count_rating",count+1);
            ReviewManager manager = ReviewManagerFactory.create(this);
            Task<ReviewInfo> request = manager.requestReviewFlow();
            request.addOnCompleteListener(task -> {
                if (task.isSuccessful()) {
                    // We can get the ReviewInfo object
                    ReviewInfo reviewInfo = task.getResult();
                    Task <Void> flow = manager.launchReviewFlow(this, reviewInfo);
                    flow.addOnCompleteListener(task1 -> {
                        // The flow has finished. The API does not indicate whether the user
                        // reviewed or not, or even whether the review dialog was shown.
                    });
                } else {
                    //show review binh thuong
                }
            });
        }else{
            SharedPreferencesUtils.setInt("count_rating",count+1);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onResume() {
        try {
            super.onResume();
        } catch (IllegalStateException e) {

        }
        if (Util.isPro(this)) {
            try {
                View container_ads = findViewById(R.id.container_ads);
                if (container_ads != null) {
                    container_ads.setVisibility(View.GONE);
                }
                RelativeLayout container_ads_native = findViewById(R.id.container_ads_native);
                if(container_ads_native!=null){
                    container_ads_native.setVisibility(View.GONE);
                }
            } catch (Exception e) {

            }
        }
        if(isShowSuggestProRemoveAds){
            isShowSuggestProRemoveAds = false;
            new DialogUtil.Builder(BaseActivity.this)
                    .title("Xoá quảng cáo làm phiền")
                    .content("Quảng cáo xuất hiện sẽ làm phiền đến bạn và ảnh hưởng đến quá trình học tập. Bạn có muốn xoá quảng cáo và sử dụng nhiều tính năng cao cấp khác không?")
                    .doneText(getString(R.string.text_ok))
                    .onDone(new DialogUtil.SingleButtonCallback() {
                        @Override
                        public void onClick() {
                            buyPro();
                        }
                    })
                    .show();
        }
        if(!SharedPreferencesUtils.getString("show_dialog_update","").equals(SharedPreferencesUtils.getUpdateQuestionID())){
            View customView = getLayoutInflater().inflate(R.layout.custom_view_update, null);
            new DialogUtil.Builder(BaseActivity.this)
                    .title("Cập nhật câu hỏi theo phiên bản mới nhất " + SharedPreferencesUtils.getMainVer() + " Tổng Cục Đường Bộ Việt Nam")
                    .setCustomView(AppUtils.checkPortraitMode(BaseActivity.this) ? customView : null)
                    .content(Util.isPro(BaseActivity.this)? "Tình huống " + SharedPreferencesUtils.getUpdateQuestionID() + " đã được cập nhật lại đáp án và điểm căn. Bạn vui lòng tải lại và học lại các câu hỏi này để thi có kết quả tốt nhất. Chúng tôi rất xin lỗi về sự bất tiện này" : "Tình huống " + SharedPreferencesUtils.getUpdateQuestionID() + " đã được cập nhật lại đáp án và điểm căn. Bạn vui lòng học lại các câu hỏi này để thi có kết quả tốt nhất. Chúng tôi rất xin lỗi về sự bất tiện này")
                    .doneText("Đã hiểu")
                    .onDone(new DialogUtil.SingleButtonCallback() {
                        @Override
                        public void onClick() {
                        }
                    })
                    .show();
            SharedPreferencesUtils.setString("show_dialog_update",SharedPreferencesUtils.getUpdateQuestionID());
        }
    }

    public void showDialogUpdateQuestion(){
        View customView = getLayoutInflater().inflate(R.layout.custom_view_update, null);
        new DialogUtil.Builder(BaseActivity.this)
                .title("Cập nhật câu hỏi theo phiên bản mới nhất " + SharedPreferencesUtils.getMainVer() + " Tổng Cục Đường Bộ Việt Nam")
                .setCustomView(AppUtils.checkPortraitMode(BaseActivity.this) ? customView : null)
                .content(Util.isPro(BaseActivity.this)? "Tình huống " + SharedPreferencesUtils.getUpdateQuestionID() + " đã được cập nhật lại đáp án và điểm căn. Bạn vui lòng tải lại và học lại các câu hỏi này để thi có kết quả tốt nhất. Chúng tôi rất xin lỗi về sự bất tiện này" : "Tình huống " + SharedPreferencesUtils.getUpdateQuestionID() + " đã được cập nhật lại đáp án và điểm căn. Bạn vui lòng học lại các câu hỏi này để thi có kết quả tốt nhất. Chúng tôi rất xin lỗi về sự bất tiện này")
                .doneText("Đã hiểu")
                .onDone(new DialogUtil.SingleButtonCallback() {
                    @Override
                    public void onClick() {
                    }
                })
                .show();
        SharedPreferencesUtils.setString("show_dialog_update",SharedPreferencesUtils.getUpdateQuestionID());
    }

    public void buyPro(){
        Intent intent = new Intent(BaseActivity.this, UnlockProActivity.class);
        intent.putExtra("first_start",false);
        BaseActivity.this.startActivity(intent);
    }

    @Override
    public void settingPro() {
        super.settingPro();
        Util.setIsPro(this);
        try {
            com.rey.material.widget.FrameLayout btnRemoveAds = findViewById(R.id.btn_platium);
            if(btnRemoveAds!=null){
                btnRemoveAds.setVisibility(View.GONE);
            }
            RelativeLayout container_native = findViewById(R.id.container_ads_native);
            if(container_native!=null){
                container_native.setVisibility(View.GONE);
            }
        }catch (Exception e){

        }
    }

    public void showSuggestShowScoreStop(){
        if(!Util.isPro(BaseActivity.this)) {
            new DialogUtil.Builder(BaseActivity.this)
                    .title("Dừng video chính xác tại thời điểm 5,4,3,2,1 điểm.")
                    .content("Bạn có muốn video dừng ở thời điểm chính xác 5,4,3,2,1 để bạn căn chuẩn thời gian bấm Space chính xác. Bạn cần nâng cấp để sử dụng tính năng này và nhiều tính năng cao cấp khác.")
                    .doneText(getString(R.string.text_ok))
                    .onDone(new DialogUtil.SingleButtonCallback() {
                        @Override
                        public void onClick() {
                            buyPro();
                        }
                    })
                    .show();
        }
    }

    public void checkShowSuggestProDisplayAds(){
        if(!Util.isPro(this)) {
            if (SharedPreferencesUtils.getInt("count_show_ads", 0) % 11 == 10) {
                isShowSuggestProRemoveAds = true;
            }
            SharedPreferencesUtils.setInt("count_show_ads", SharedPreferencesUtils.getInt("count_show_ads", 0) + 1);
        }
    }

    public void logFirebase(Bundle bundle){
        if(mFirebaseAnalytics!=null){
            try {
                mFirebaseAnalytics.logEvent("load_video_error",bundle);
            }catch (Exception e){

            }
        }
    }

    public static void deleteCache(Context context) {
        try {
            File dir = context.getCacheDir();
            deleteDir(dir);
        } catch (Exception e) { e.printStackTrace();}
    }

    public static boolean deleteDir(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
            return dir.delete();
        } else if(dir!= null && dir.isFile()) {
            return dir.delete();
        } else {
            return false;
        }
    }

    public static void deleteFiles(Context context) {
        try {
            File dir = context.getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS);
            deleteDirFile(dir);
        } catch (Exception e) { e.printStackTrace();}
    }

    public static boolean deleteDirFile(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDirFile(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
            return dir.delete();
        } else if(dir!= null && dir.isFile()) {
            return dir.delete();
        } else {
            return false;
        }
    }

    public void buyProDialog(){
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Window window = dialog.getWindow();
        window.setBackgroundDrawableResource(android.R.color.transparent);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_vip_upgrade);
        com.rey.material.widget.RelativeLayout btn_close = dialog.findViewById(R.id.btn_close);
        btn_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.cancel();
                dialog.dismiss();
            }
        });
        com.rey.material.widget.RelativeLayout btn_upgrade_now = dialog.findViewById(R.id.btn_upgrade_now);
        btn_upgrade_now.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.cancel();
                dialog.dismiss();
                buyPro(false);
            }
        });
        dialog.show();
    }
}
