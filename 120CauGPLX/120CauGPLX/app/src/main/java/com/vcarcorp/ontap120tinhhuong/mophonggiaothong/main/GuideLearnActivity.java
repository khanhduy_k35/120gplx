package com.vcarcorp.ontap120tinhhuong.mophonggiaothong.main;

import android.os.Bundle;
import android.view.View;

import com.rey.material.widget.FrameLayout;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.R;

public class GuideLearnActivity extends BaseActivity {

    private FrameLayout btn_back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        setContentView(R.layout.guide_learn_activity);
        initValue();
        initIdView();
        initView();
        initActionView();
    }

    private void initValue() {
    }

    private void initIdView() {
        btn_back = findViewById(R.id.btn_back);
    }

    private void initView() {
        setUpToolBar(btn_back);
    }


    private void initActionView() {
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }
}
