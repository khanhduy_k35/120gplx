package com.vcarcorp.ontap120tinhhuong.mophonggiaothong.tips;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ads.control.admob.AdCloseListener;
import com.ads.control.admob.AdmobHelp;
import com.rey.material.widget.FrameLayout;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.R;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.adapter.TipsAdapter;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.database.GPLXDataManager;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.learnquestion.LearnQuestActivity;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.main.BaseActivity;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.main.MainActivity;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.model.QuestionCategory;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.model.QuestionVideo;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.util.Util;

import java.util.ArrayList;

/**
 * Created by macbook on 10/31/17.
 */
public class TipsActivity extends BaseActivity {
    private GPLXDataManager dataManager;
    private TipsAdapter tipsAdapter;
    private RecyclerView recyclerView;
    private ArrayList<QuestionVideo> listTips = new ArrayList<>();
    private int clickPos = -1;

    @SuppressLint("SourceLockedOrientationActivity")
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        setContentView(R.layout.tips_activity);
        dataManager = new GPLXDataManager(this);
        recyclerView = (RecyclerView) findViewById(R.id.list);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        setUpToolBar((FrameLayout)findViewById(R.id.btn_back));
        findViewById(R.id.btn_back).setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                onBackPressed();
            }
        });

        listTips = dataManager.getAllQuestionVideo(TipsActivity.this);
        tipsAdapter = new TipsAdapter(listTips,this);
        tipsAdapter.setClickListener(new TipsAdapter.ItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                AdmobHelp.getInstance().showInterstitialAd( new AdCloseListener() {
                    @Override
                    public void onAdClosed(boolean haveAds) {
                        QuestionCategory questionCategory = dataManager.getQuestionCategoryById(listTips.get(position).getCategoryID());
                        Intent i = new Intent(TipsActivity.this, LearnQuestActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putParcelable("question_category", questionCategory);
                        bundle.putParcelable("question",listTips.get(position));
                        i.putExtras(bundle);
                        startActivity(i);
                        if(haveAds) {
                            checkShowSuggestProDisplayAds();
                        }
                    }
                });
            }

            @Override
            public void onImageItemClick(View view, int position) {
                Util.showDialogTipForQuestion(TipsActivity.this, listTips.get(position));
            }
        });

        recyclerView.setAdapter(tipsAdapter);
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            dataManager.close();
        }catch (Exception e){

        }
    }
}
