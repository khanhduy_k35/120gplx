package com.vcarcorp.ontap120tinhhuong.mophonggiaothong.download;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.R;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.database.GPLXDataManager;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.learnquestion.LearnQuestActivity;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.model.QuestionCategory;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.model.QuestionVideo;
import com.ads.control.customview.CustomTextView;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.util.AppUtils;
import com.tonyodev.fetch2.Download;
import com.tonyodev.fetch2.Status;

import java.util.ArrayList;
import java.util.List;

public final class FileAdapter extends RecyclerView.Adapter<FileAdapter.ViewHolder> {

    @NonNull
    private final List<DownloadData> downloads = new ArrayList<>();
    @NonNull
    private final ActionListener actionListener;

    FileAdapter(@NonNull final ActionListener actionListener) {
        this.actionListener = actionListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(@NonNull final ViewGroup parent, int viewType) {
        final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.download_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        holder.actionButton.setOnClickListener(null);
        holder.actionButton.setEnabled(true);
        final DownloadData downloadData = downloads.get(position);
        Status status = downloadData.download.getStatus();
        final Context context = holder.itemView.getContext();
        int idVideo = 0;
        if(position > 1){
            idVideo = position - 1;
            holder.titleTextView.setText("Nội dung tình huống " + idVideo);
        }else if(position == 1){
            holder.titleTextView.setText("Hình ảnh gợi ý + giải thích");
        }
        else{
            holder.titleTextView.setText("Giọng đọc hướng dẫn");
        }


        int progress = downloadData.download.getProgress();
        if (progress == -1) { // Download progress is undermined at the moment.
            progress = 0;
        }
        if(downloadData.isCompleted){
            progress = 100;
            status = Status.COMPLETED;
        }
        holder.statusTextView.setText(getStatusString(status));
        holder.progressBar.setProgress(progress);
        holder.progressTextView.setText(context.getString(R.string.percent_progress, progress));

        if (downloadData.eta == -1) {
            holder.timeRemainingTextView.setText("");
        } else {
            holder.timeRemainingTextView.setText(AppUtils.getETAString(context, downloadData.eta));
        }

        if (downloadData.downloadedBytesPerSecond == 0) {
            holder.downloadedBytesPerSecondTextView.setText("");
        } else {
            holder.downloadedBytesPerSecondTextView.setText(AppUtils.getDownloadSpeedString(context, downloadData.downloadedBytesPerSecond));
        }

        switch (status) {
            case COMPLETED: {
                holder.actionButton.setText(R.string.view);
                if(position <= 1){
                    holder.actionButton.setVisibility(View.INVISIBLE);
                }else{
                    holder.actionButton.setVisibility(View.VISIBLE);
                }
                int finalIdVideo = idVideo;
                holder.actionButton.setOnClickListener(view -> {
                    GPLXDataManager gplxDataManager = new GPLXDataManager(context);
                    QuestionVideo questionVideo = gplxDataManager.getQuestionVideoByID(context, finalIdVideo);
                    QuestionCategory questionCategory = gplxDataManager.getQuestionCategoryById(questionVideo.getCategoryID());
                    Intent i = new Intent(context, LearnQuestActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putParcelable("question_category", questionCategory);
                    bundle.putParcelable("question",questionVideo);
                    i.putExtras(bundle);
                    try{
                        gplxDataManager.close();
                    }catch (Exception e){

                    }
                    context.startActivity(i);
                });
                break;
            }
            case FAILED: {
                holder.actionButton.setText(R.string.retry);
                holder.actionButton.setOnClickListener(view -> {
                    holder.actionButton.setEnabled(false);
                    actionListener.onRetryDownload(downloadData.download.getId());
                });
                break;
            }
            case PAUSED: {
                holder.actionButton.setText(R.string.resume);
                holder.actionButton.setOnClickListener(view -> {
                    holder.actionButton.setEnabled(false);
                    actionListener.onResumeDownload(downloadData.download.getId());
                });
                break;
            }
            case DOWNLOADING:
            case QUEUED: {
                holder.actionButton.setText(R.string.pause);
                holder.actionButton.setOnClickListener(view -> {
                    holder.actionButton.setEnabled(false);
                    actionListener.onPauseDownload(downloadData.download.getId());
                });
                break;
            }
            case ADDED: {
                holder.actionButton.setText(R.string.download);
                holder.actionButton.setOnClickListener(view -> {
                    holder.actionButton.setEnabled(false);
                    actionListener.onResumeDownload(downloadData.download.getId());
                });
                break;
            }
            default: {
                break;
            }
        }
    }

    public void addDownload(@NonNull final Download download,boolean isCompleted) {
        boolean found = false;
        DownloadData data = null;
        for (int i = 0; i < downloads.size(); i++) {
            final DownloadData downloadData = downloads.get(i);
            if (downloadData.id == download.getId()) {
                data = downloadData;
                found = true;
                break;
            }
        }
        if (!found) {
            final DownloadData downloadData = new DownloadData();
            downloadData.id = download.getId();
            downloadData.download = download;
            downloadData.isCompleted = isCompleted;
            downloads.add(downloadData);
        } else {
            data.download = download;
            data.isCompleted = isCompleted;
        }
    }

    public void addDownload(@NonNull final Download download) {
        boolean found = false;
        DownloadData data = null;
        int dataPosition = -1;
        for (int i = 0; i < downloads.size(); i++) {
            final DownloadData downloadData = downloads.get(i);
            if (downloadData.id == download.getId()) {
                data = downloadData;
                dataPosition = i;
                found = true;
                break;
            }
        }
        if (!found) {
            final DownloadData downloadData = new DownloadData();
            downloadData.id = download.getId();
            downloadData.download = download;
            downloads.add(downloadData);
            notifyItemInserted(downloads.size() - 1);
        } else {
            data.download = download;
            notifyItemChanged(dataPosition);
        }
    }

    @Override
    public int getItemCount() {
        return downloads.size();
    }

    public int update(@NonNull final Download download, long eta, long downloadedBytesPerSecond) {
        int pos = -1;
        for (int position = 0; position < downloads.size(); position++) {
            final DownloadData downloadData = downloads.get(position);
            if (downloadData.id == download.getId()) {
                pos = position;
                switch (download.getStatus()) {
                    case REMOVED:
                    case DELETED: {
                        downloads.remove(position);
                        break;
                    }
                    default: {
                        downloadData.download = download;
                        downloadData.eta = eta;
                        downloadData.downloadedBytesPerSecond = downloadedBytesPerSecond;
                    }
                }
                return pos;
            }
        }
        return pos;
    }

    public int update(@NonNull final Download download, long eta, long downloadedBytesPerSecond,boolean isComplete) {
        int pos = -1;
        for (int position = 0; position < downloads.size(); position++) {
            final DownloadData downloadData = downloads.get(position);
            if (downloadData.id == download.getId()) {
                pos = position;
                switch (download.getStatus()) {
                    case REMOVED:
                    case DELETED: {
                        downloads.remove(position);
                        break;
                    }
                    default: {
                        downloadData.download = download;
                        downloadData.eta = eta;
                        downloadData.downloadedBytesPerSecond = downloadedBytesPerSecond;
                        downloadData.isCompleted = isComplete;
                    }
                }
                return pos;
            }
        }
        return pos;
    }

    private String getStatusString(Status status) {
        switch (status) {
            case COMPLETED:
                return "Đã tải xong";
            case DOWNLOADING:
                return "Đang tải";
            case FAILED:
                return "Tải lỗi";
            case PAUSED:
                return "Tạm dừng";
            case QUEUED:
                return "Đang chờ tải";
            case REMOVED:
                return "Đã xoá";
            case NONE:
                return "Không chờ tải";
            default:
                return "Unknown";
        }
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public final CustomTextView titleTextView;
        final CustomTextView statusTextView;
        public final ProgressBar progressBar;
        public final CustomTextView progressTextView;
        public final Button actionButton;
        final CustomTextView timeRemainingTextView;
        final CustomTextView downloadedBytesPerSecondTextView;

        ViewHolder(View itemView) {
            super(itemView);
            titleTextView = itemView.findViewById(R.id.titleTextView);
            statusTextView = itemView.findViewById(R.id.status_TextView);
            progressBar = itemView.findViewById(R.id.progressBar);
            actionButton = itemView.findViewById(R.id.actionButton);
            progressTextView = itemView.findViewById(R.id.progress_TextView);
            timeRemainingTextView = itemView.findViewById(R.id.remaining_TextView);
            downloadedBytesPerSecondTextView = itemView.findViewById(R.id.downloadSpeedTextView);
        }

    }

    public static class DownloadData {
        public int id;
        @Nullable
        public Download download;
        long eta = -1;
        long downloadedBytesPerSecond = 0;
        boolean isCompleted;

        @Override
        public int hashCode() {
            return id;
        }

        @Override
        public String toString() {
            if (download == null) {
                return "";
            }
            return download.toString();
        }

        @Override
        public boolean equals(Object obj) {
            return obj == this || obj instanceof DownloadData && ((DownloadData) obj).id == id;
        }
    }

}
