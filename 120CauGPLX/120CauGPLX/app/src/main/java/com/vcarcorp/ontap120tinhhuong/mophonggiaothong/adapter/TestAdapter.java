package com.vcarcorp.ontap120tinhhuong.mophonggiaothong.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Configuration;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.RecyclerView;

import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.R;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.model.Test;
import com.ads.control.customview.CustomTextView;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.util.customview.DonutProgress;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.util.OnSingleClickListener;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.util.SharedPreferencesUtils;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.util.Util;

import java.util.List;

public class TestAdapter extends RecyclerView.Adapter<TestAdapter.ViewHolder> {

    private List<Test> data;
    private Context context;
    private ItemClickListener mClickListener;
    private RelativeLayout.LayoutParams params;
    private RelativeLayout.LayoutParams paramsImage;
    private int totalQuest;

    public TestAdapter(final List<Test> data, Context mContext) {
        this.data = data;
        this.context = mContext;
        int withItem = (Util.getWidthScreen(mContext) - (mContext.getResources().getDimensionPixelSize(R.dimen.layout_margin_16dp)) * 3) / 3;
        params = new RelativeLayout.LayoutParams(withItem, withItem + mContext.getResources().getDimensionPixelSize(R.dimen.layout_margin_16dp));
        paramsImage = new RelativeLayout.LayoutParams(withItem / 2, withItem / 2);
        paramsImage.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE);
        this.totalQuest = 10;
    }

    @Override
    public ViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
        this.context = parent.getContext();
        return new ViewHolder(LayoutInflater.from(context)
                .inflate(R.layout.theory_item_test, parent, false));
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, @SuppressLint("RecyclerView") int position) {
        final Test item = data.get(position);
        Configuration config = this.context.getResources().getConfiguration();
        if (config.smallestScreenWidthDp >= 380) {
            holder.textViewTitle.setText("Đề thi mô phỏng số " + (position + 1));
        }else{
            holder.textViewTitle.setText("Đề thi số " + (position + 1));
        }
        holder.arcProgress2.setText((position + 1) < 10 ? ("0" + (position + 1)) : ((position + 1) + ""));
        holder.arcProgress.setText("");
        if (item.isFinish()) {
            if (item.getTotalScore() >= 35) {
                holder.txtAction.setText(context.getString(R.string.pass));
                holder.txtAction.setTextColor(context.getColor(R.color.colorPrimary));
                holder.ic_arrow.setColorFilter(context.getColor(R.color.colorPrimary));
            } else {
                holder.txtAction.setText(context.getString(R.string.faile));
                holder.txtAction.setTextColor(context.getColor(R.color.colorRedPrimary));
                holder.ic_arrow.setColorFilter(context.getColor(R.color.colorRedPrimary));
            }
            holder.textViewSumQuest.setText(String.format("Điểm thi: %d/%d", item.getTotalScore(), totalQuest * 5));
            holder.arcProgress2.setMax(50);
            holder.arcProgress.setMax(50);
            holder.arcProgress2.setProgress(item.getTotalScore());
            holder.arcProgress.setProgress(item.getTotalScore());
            holder.arcProgress.setVisibility(View.VISIBLE);
            holder.arcProgress2.setVisibility(View.VISIBLE);
            holder.arcProgress.setFinishedStrokeColor(context.getColor(R.color.colorPrimary));
            holder.arcProgress2.setFinishedStrokeColor(context.getColor(R.color.white_20));
        } else {
            if (item.getCountQuestion() > 0) {
                holder.txtAction.setText(context.getString(R.string.do_continue));
                holder.txtAction.setTextColor(context.getColor(R.color.orange));
                holder.ic_arrow.setColorFilter(context.getColor(R.color.orange));
                holder.textViewSumQuest.setText(String.format("Đã đánh dấu %d/%d video", item.getCountQuestion(), totalQuest));
                holder.arcProgress2.setMax(totalQuest);
                holder.arcProgress.setMax(totalQuest);
                holder.arcProgress2.setProgress(item.getCountQuestion());
                holder.arcProgress.setProgress(item.getCountQuestion());
                holder.arcProgress.setVisibility(View.VISIBLE);
                holder.arcProgress2.setVisibility(View.VISIBLE);
                if (SharedPreferencesUtils.isNightMode()) {
                    holder.arcProgress.setFinishedStrokeColor(context.getColor(R.color.white));
                    holder.arcProgress2.setFinishedStrokeColor(context.getColor(R.color.white));
                    holder.arcProgress.setUnfinishedStrokeColor(context.getColor(R.color.white_20));
                    holder.arcProgress2.setUnfinishedStrokeColor(context.getColor(R.color.white_20));
                } else {
                    holder.arcProgress.setFinishedStrokeColor(context.getColor(R.color.white));
                    holder.arcProgress2.setFinishedStrokeColor(context.getColor(R.color.white));
                    holder.arcProgress.setUnfinishedStrokeColor(context.getColor(R.color.white_20));
                    holder.arcProgress2.setUnfinishedStrokeColor(context.getColor(R.color.white_20));
                }
            } else {
                holder.txtAction.setText(context.getString(R.string.do_test));
                holder.txtAction.setTextColor(context.getColor(R.color.colorPrimary));
                holder.ic_arrow.setColorFilter(context.getColor(R.color.colorPrimary));
                holder.textViewSumQuest.setText(String.format("%d video", totalQuest));
                holder.arcProgress2.setMax(totalQuest);
                holder.arcProgress.setMax(totalQuest);
                holder.arcProgress2.setProgress(0);
                holder.arcProgress.setProgress(0);
                holder.arcProgress.setVisibility(View.VISIBLE);
                holder.arcProgress2.setVisibility(View.VISIBLE);
                holder.arcProgress.setFinishedStrokeColor(context.getColor(R.color.colorPrimary));
                holder.arcProgress2.setFinishedStrokeColor(context.getColor(R.color.colorRedPrimary));
                holder.arcProgress.setUnfinishedStrokeColor(context.getColor(R.color.white_20));
                holder.arcProgress2.setUnfinishedStrokeColor(context.getColor(R.color.white_20));
            }
        }
        if (position % 18 == 0) {
            holder.item_layout.setBackgroundResource(R.drawable.background_row_101);
        } else if (position % 18 == 1) {
            holder.item_layout.setBackgroundResource(R.drawable.background_row_102);
        } else if (position % 18 == 2) {
            holder.item_layout.setBackgroundResource(R.drawable.background_row_103);
        } else if (position % 18 == 3) {
            holder.item_layout.setBackgroundResource(R.drawable.background_row_104);
        } else if (position % 18 == 4) {
            holder.item_layout.setBackgroundResource(R.drawable.background_row_105);
        } else if (position % 18 == 5) {
            holder.item_layout.setBackgroundResource(R.drawable.background_row_106);
        } else if (position % 18 == 6) {
            holder.item_layout.setBackgroundResource(R.drawable.background_row_107);
        } else if (position % 18 == 7) {
            holder.item_layout.setBackgroundResource(R.drawable.background_row_108);
        } else if (position % 18 == 8) {
            holder.item_layout.setBackgroundResource(R.drawable.background_row_109);
        } else if (position % 18 == 9) {
            holder.item_layout.setBackgroundResource(R.drawable.background_row_110);
        } else if (position % 18 == 10) {
            holder.item_layout.setBackgroundResource(R.drawable.background_row_111);
        } else if (position % 18 == 11) {
            holder.item_layout.setBackgroundResource(R.drawable.background_row_112);
        } else if (position % 18 == 12) {
            holder.item_layout.setBackgroundResource(R.drawable.background_row_113);
        } else if (position % 18 == 13) {
            holder.item_layout.setBackgroundResource(R.drawable.background_row_114);
        } else if (position % 18 == 14) {
            holder.item_layout.setBackgroundResource(R.drawable.background_row_115);
        } else if (position % 18 == 15) {
            holder.item_layout.setBackgroundResource(R.drawable.background_row_116);
        } else if (position % 18 == 16) {
            holder.item_layout.setBackgroundResource(R.drawable.background_row_117);
        } else if (position % 18 == 17) {
            holder.item_layout.setBackgroundResource(R.drawable.background_row_118);
        }
        holder.rltClick.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                if (mClickListener != null) mClickListener.onItemClick(v, position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        RelativeLayout item_layout;
        com.rey.material.widget.RelativeLayout rltClick;
        DonutProgress arcProgress;
        DonutProgress arcProgress2;
        AppCompatImageView ic_arrow;
        TextView textViewTitle;
        TextView txtAction;
        TextView textViewSumQuest;

        public ViewHolder(View v) {
            super(v);
            arcProgress = (DonutProgress) v.findViewById(R.id.progress1);
            arcProgress2 = (DonutProgress) v.findViewById(R.id.progress2);
            item_layout = (RelativeLayout) v.findViewById(R.id.chartView);
            textViewTitle = (CustomTextView) v.findViewById(R.id.textViewTitle);
            txtAction = (CustomTextView) v.findViewById(R.id.txtAction);
            textViewSumQuest = (CustomTextView) v.findViewById(R.id.textViewSumQuest);
            ic_arrow = (AppCompatImageView) v.findViewById(R.id.ic_arrow);
            rltClick = (com.rey.material.widget.RelativeLayout) v.findViewById(R.id.rltClick);
        }
    }

    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }

    public List<Test> getData() {
        return data;
    }

    public void setData(List<Test> data) {
        this.data = data;
    }
}