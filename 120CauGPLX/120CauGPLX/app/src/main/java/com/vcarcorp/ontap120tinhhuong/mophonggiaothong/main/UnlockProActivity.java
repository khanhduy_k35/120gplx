package com.vcarcorp.ontap120tinhhuong.mophonggiaothong.main;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import androidx.appcompat.widget.AppCompatImageView;

import com.rey.material.widget.FrameLayout;
import com.rey.material.widget.RelativeLayout;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.R;
import com.ads.control.customview.CustomTextView;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.util.DialogUtil;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.util.SharedPreferencesUtils;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.util.Util;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.util.AppUtils;

import java.util.Random;

public class UnlockProActivity extends BaseActivity {

    private FrameLayout btn_close;
    private RelativeLayout rltButtonAction;
    private CustomTextView tvPackage1, tvPackage2;
    private CustomTextView tvUnlock;
    private boolean first_start = false;
    private RelativeLayout rltPro;
    private RelativeLayout rltSub;
    private AppCompatImageView imgCheckPro, imgCheckSub;
    private CustomTextView tvBtnTitle;
    private boolean isChooseSub;
    private CustomTextView app_content;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getIntent() != null && getIntent().getExtras() != null) {
            Bundle extras = getIntent().getExtras();
            first_start = extras.getBoolean("first_start");
        }
        setContentView(R.layout.open_unlock_vip);
        Window window = this.getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | 0);
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(Color.TRANSPARENT);
        }
        initValue();
        initIdView();
        initView();
        initActionView();
    }

    private void initValue() {
    }

    private void initIdView() {
        btn_close = findViewById(R.id.btnClose);
        rltButtonAction = findViewById(R.id.rltButtonAction);
        tvPackage1 = findViewById(R.id.tvPackage1);
        tvPackage2 = findViewById(R.id.tvPackage2);
        tvUnlock = findViewById(R.id.tvUnlock);
        rltPro = findViewById(R.id.rltProClick);
        rltSub = findViewById(R.id.rltSubClick);
        imgCheckPro = findViewById(R.id.imgCheckPro);
        imgCheckSub = findViewById(R.id.imgCheckSub);
        tvBtnTitle = findViewById(R.id.tvBtnTitle);
        app_content = findViewById(R.id.app_content);
    }

    private void initView() {
        setUpToolBar(btn_close);

        SpannableString spannableStr = new SpannableString("Mua 1 lần và mãi mãi: 29.999đ (chỉ bằng giá 1 tô phở) sử dụng tất cả các tính năng.");
        int beginIndex = "Mua 1 lần và mãi mãi: 29.999đ (chỉ bằng giá 1 tô phở) sử dụng tất cả các tính năng.".indexOf("29.999đ");
        ForegroundColorSpan backgroundColorSpan = new ForegroundColorSpan(Color.parseColor("#00C6C1"));
        spannableStr.setSpan(backgroundColorSpan, beginIndex, beginIndex + 7, Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        StyleSpan styleSpanItalic = new StyleSpan(Typeface.BOLD);
        spannableStr.setSpan(styleSpanItalic, beginIndex, beginIndex + 7, Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        tvPackage1.setText(spannableStr);

        spannableStr = new SpannableString("3 ngày dùng thử miễn phí: Sau giai đoạn dùng thử 49.999đ/năm. Huỷ bất cứ lúc nào.");
        beginIndex = "3 ngày dùng thử miễn phí: Sau giai đoạn dùng thử 49.999đ/năm. Huỷ bất cứ lúc nào.".indexOf("49.999đ");
        backgroundColorSpan = new ForegroundColorSpan(Color.parseColor("#00C6C1"));
        spannableStr.setSpan(backgroundColorSpan, beginIndex, beginIndex + 7, Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        styleSpanItalic = new StyleSpan(Typeface.BOLD);
        spannableStr.setSpan(styleSpanItalic, beginIndex, beginIndex + 7, Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        tvPackage2.setText(spannableStr);
        if(Util.isPro(this)){
            tvBtnTitle.setText("Tiếp tục sử dụng ứng dụng");
            tvUnlock.setVisibility(View.INVISIBLE);
            app_content.setText("Chúc mừng bạn đã mở khoá thành công toàn bộ các tính năng cao cấp dưới đây.");
        }else{
            app_content.setText("Mở khoá tất cả các tính năng");
            tvBtnTitle.setText("Mở khoá toàn bộ tính năng");
            tvUnlock.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private void initActionView() {

        rltButtonAction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Util.isPro(UnlockProActivity.this)) {
                    new DialogUtil.Builder(UnlockProActivity.this)
                            .cancelable(false)
                            .canceledOnTouchOutside(false)
                            .title("Chúc mừng bạn")
                            .content("Chúc mừng bạn đã nâng cấp thành công và mở khoá tất cả các tính năng cao cấp của ứng dụng. Chúng tôi sẽ khởi động lại ứng dụng để bạn sử dụng được tất cả các tính năng cao cấp. Chúc bạn thi thật tốt và an toàn trên mọi chặng đường.")
                            .doneText(getString(R.string.text_ok))
                            .onDone(new DialogUtil.SingleButtonCallback() {
                                @Override
                                public void onClick() {
                                    Intent intent = new Intent(UnlockProActivity.this, MainActivity.class);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    startActivity(intent);
                                }
                            })
                            .show();
                } else {
                    if (isChooseSub) {
                        buyPro(true);
                    } else {
                        buyPro(false);
                    }
                }
            }
        });
        rltPro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isChooseSub = false;
                imgCheckPro.setImageResource(R.drawable.ic_settting_check);
                imgCheckPro.setColorFilter(AppUtils.getColorFromAttr(UnlockProActivity.this, R.attr.color_control_selected));
                imgCheckSub.setImageResource(R.drawable.ic_uncheck);
                imgCheckSub.setColorFilter(AppUtils.getColorFromAttr(UnlockProActivity.this, R.attr.color_control_normal));
                rltPro.setBackgroundResource(R.drawable.background_button_pro_selected);
                rltSub.setBackgroundResource(R.drawable.background_button_pro);
                if (isChooseSub) {
                    tvBtnTitle.setText("Bắt đầu dùng thử miễn phí");
                } else {
                    tvBtnTitle.setText("Mở khoá toàn bộ tính năng");
                }
            }
        });
        imgCheckPro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isChooseSub = false;
                imgCheckPro.setImageResource(R.drawable.ic_settting_check);
                imgCheckPro.setColorFilter(AppUtils.getColorFromAttr(UnlockProActivity.this, R.attr.color_control_selected));
                imgCheckSub.setImageResource(R.drawable.ic_uncheck);
                imgCheckSub.setColorFilter(AppUtils.getColorFromAttr(UnlockProActivity.this, R.attr.color_control_normal));
                rltPro.setBackgroundResource(R.drawable.background_button_pro_selected);
                rltSub.setBackgroundResource(R.drawable.background_button_pro);
                if (isChooseSub) {
                    tvBtnTitle.setText("Bắt đầu dùng thử miễn phí");
                } else {
                    tvBtnTitle.setText("Mở khoá toàn bộ tính năng");
                }
            }
        });
        rltSub.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isChooseSub = true;
                imgCheckSub.setImageResource(R.drawable.ic_settting_check);
                imgCheckSub.setColorFilter(AppUtils.getColorFromAttr(UnlockProActivity.this, R.attr.color_control_selected));
                imgCheckPro.setImageResource(R.drawable.ic_uncheck);
                imgCheckPro.setColorFilter(AppUtils.getColorFromAttr(UnlockProActivity.this, R.attr.color_control_normal));
                rltSub.setBackgroundResource(R.drawable.background_button_pro_selected);
                rltPro.setBackgroundResource(R.drawable.background_button_pro);
                if (isChooseSub) {
                    tvBtnTitle.setText("Bắt đầu dùng thử miễn phí");
                } else {
                    tvBtnTitle.setText("Mở khoá toàn bộ tính năng");
                }
            }
        });
        imgCheckSub.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isChooseSub = true;
                imgCheckSub.setImageResource(R.drawable.ic_settting_check);
                imgCheckSub.setColorFilter(AppUtils.getColorFromAttr(UnlockProActivity.this, R.attr.color_control_selected));
                imgCheckPro.setImageResource(R.drawable.ic_uncheck);
                imgCheckPro.setColorFilter(AppUtils.getColorFromAttr(UnlockProActivity.this, R.attr.color_control_normal));
                rltSub.setBackgroundResource(R.drawable.background_button_pro_selected);
                rltPro.setBackgroundResource(R.drawable.background_button_pro);
                if (isChooseSub) {
                    tvBtnTitle.setText("Bắt đầu dùng thử miễn phí");
                } else {
                    tvBtnTitle.setText("Mở khoá toàn bộ tính năng");
                }
            }
        });
        btn_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(Util.isPro(UnlockProActivity.this)){
                    if (Util.isPro(UnlockProActivity.this)) {
                        new DialogUtil.Builder(UnlockProActivity.this)
                                .cancelable(false)
                                .canceledOnTouchOutside(false)
                                .title("Chúc mừng bạn")
                                .content("Chúc mừng bạn đã nâng cấp thành công và mở khoá tất cả các tính năng cao cấp của ứng dụng. Chúng tôi sẽ khởi động lại ứng dụng để bạn sử dụng được tất cả các tính năng cao cấp. Chúc bạn thi thật tốt và an toàn trên mọi chặng đường.")
                                .doneText(getString(R.string.text_ok))
                                .onDone(new DialogUtil.SingleButtonCallback() {
                                    @Override
                                    public void onClick() {
                                        Intent intent = new Intent(UnlockProActivity.this, MainActivity.class);
                                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        startActivity(intent);
                                    }
                                })
                                .show();
                    }
                }else {
                    closeVip();
                }
            }
        });
        tvUnlock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                closeVip();
            }
        });
    }

    public void closeVip() {
        String title = "";
        String content = "";
        int previous = SharedPreferencesUtils.getInt("key_dialog");
        boolean isRandom = false;
        while (!isRandom) {
            int random = new Random().nextInt(100000) % 5;
            if (previous != random) {
                previous = random;
                SharedPreferencesUtils.setInt("key_dialog", random);
                isRandom = true;
            }
        }
        if (previous == 0) {
            title = "Sử dụng không đầy đủ các tính năng";
            content = "Bạn ơi học bằng lái xe cả chục triệu tiếc gì vài chục ngàn ủng hộ phần mềm của chúng tôi. Trong quá trình bạn học sẽ có quảng cáo xuất hiện do chúng tôi cũng cần phải duy trì cuộc sống. Chúng tôi rất xin lỗi về sự bất tiện này!";
        } else if (previous == 1) {
            title = "Quảng cáo có thể xuất hiện";
            content = "Chúng tôi rất xin lỗi bạn nếu như quảng cáo xuất hiện và làm bạn mất tập trung trong quá trình học tập. Chúng tôi làm việc và làm ra ứng dụng nhưng cũng cần ăn, cần uống, cần phải duy trì cuộc sống nên rất xin lỗi nếu điều này làm phiền đến bạn mong bạn không đánh giá thấp những nỗ lực và cố gắng của chúng tôi.";
        } else if (previous == 2) {
            title = "Tải video để học ngoại tuyến";
            content = "Bạn sẽ không tải video để học ngoại tuyến được và không sử dụng được 1 số tính năng cao cấp của ứng dụng. Sẽ có quảng cáo xuất hiện trong quá trình học. Chúng tôi rất xin lỗi về sự bất tiện này!";
        } else if (previous == 3) {
            title = "Tạo đề thi ngẫu nhiên như thật";
            content = "Bạn sẽ không tạo được đề thi ngẫu nhiên giống như thi thật và không sử dụng được 1 số tính năng cao cấp của ứng dụng. Sẽ có quảng cáo xuất hiện trong quá trình học. Chúng tôi rất xin lỗi về sự bất tiện này!";
        } else if (previous == 4) {
            title = "Hiển thị điểm dừng 5,4,3,2,1";
            content = "Video sẽ không dừng đúng tại thời điểm 5,4,3,2,1 điểm và bạn không sử dụng được 1 số tính năng cao cấp của ứng dụng. Sẽ có quảng cáo xuất hiện trong quá trình học. Chúng tôi rất xin lỗi về sự bất tiện này!";
        }

        if (first_start) {
            new DialogUtil.Builder(UnlockProActivity.this)
                    .title(title)
                    .content(content)
                    .doneText(getString(R.string.text_ok))
                    .onDone(new DialogUtil.SingleButtonCallback() {
                        @Override
                        public void onClick() {
                            Intent intent = new Intent(UnlockProActivity.this, MainActivity.class);
                            UnlockProActivity.this.startActivity(intent);
                            UnlockProActivity.this.finish();
                        }
                    })
                    .show();

        } else {
            new DialogUtil.Builder(UnlockProActivity.this)
                    .title(title)
                    .content(content)
                    .doneText(getString(R.string.text_ok))
                    .onDone(new DialogUtil.SingleButtonCallback() {
                        @Override
                        public void onClick() {
                            UnlockProActivity.this.finish();
                        }
                    })
                    .show();
        }

    }

    @Override
    public void settingPro() {
        super.settingPro();
        Toast.makeText(UnlockProActivity.this, "Chúc mừng bạn đã nâng cấp thành công và mở khoá tất cả các tính năng cao cấp của ứng dụng.", Toast.LENGTH_LONG).show();
        if(Util.isPro(this)){
            tvBtnTitle.setText("Tiếp tục sử dụng ứng dụng");
            tvUnlock.setVisibility(View.INVISIBLE);
            app_content.setText("Chúc mừng bạn đã mở khoá thành công toàn bộ các tính năng cao cấp dưới đây.");
        }else{
            app_content.setText("Mở khoá tất cả các tính năng");
            tvBtnTitle.setText("Mở khoá toàn bộ tính năng");
            tvUnlock.setVisibility(View.VISIBLE);
        }
        SharedPreferencesUtils.setShowDetail(SharedPreferencesUtils.showDetailFree());
        SharedPreferencesUtils.setShowScoreSpace(SharedPreferencesUtils.showScoreSpaceFree());
        SharedPreferencesUtils.setShowScoreStop(SharedPreferencesUtils.showScoreStopFree());
        SharedPreferencesUtils.setShowVoidEnable(SharedPreferencesUtils.showVoidEnableFree());
    }

    @Override
    public void onBackPressed() {
        if (Util.isPro(UnlockProActivity.this)) {
            new DialogUtil.Builder(UnlockProActivity.this)
                    .cancelable(false)
                    .canceledOnTouchOutside(false)
                    .title("Chúc mừng bạn")
                    .content("Chúc mừng bạn đã nâng cấp thành công và mở khoá tất cả các tính năng cao cấp của ứng dụng. Chúng tôi sẽ khởi động lại ứng dụng để bạn sử dụng được tất cả các tính năng cao cấp. Chúc bạn thi thật tốt và an toàn trên mọi chặng đường.")
                    .doneText(getString(R.string.text_ok))
                    .onDone(new DialogUtil.SingleButtonCallback() {
                        @Override
                        public void onClick() {
                            Intent intent = new Intent(UnlockProActivity.this, MainActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                        }
                    })
                    .show();
        }else {
            super.onBackPressed();
        }
    }
}
