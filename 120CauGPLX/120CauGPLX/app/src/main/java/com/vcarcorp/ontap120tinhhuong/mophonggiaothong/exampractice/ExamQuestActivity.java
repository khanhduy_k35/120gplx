package com.vcarcorp.ontap120tinhhuong.mophonggiaothong.exampractice;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.DecelerateInterpolator;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.widget.SwitchCompat;
import androidx.core.content.ContextCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager2.widget.ViewPager2;

import com.ads.control.admob.AdCloseListener;
import com.ads.control.admob.AdmobHelp;
import com.airbnb.lottie.LottieAnimationView;
import com.rey.material.widget.RelativeLayout;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.R;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.adapter.QuestionResultTestAdapterRecycler;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.adapter.ViewPagerAdapterCustomTestQuestion;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.database.GPLXDataManager;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.main.BaseActivity;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.model.Test;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.model.TestQuest;
import com.ads.control.customview.CustomTextView;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.util.DialogUtil;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.util.customview.ImageViewButton;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.util.SharedPreferencesUtils;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.util.Util;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.util.AppUtils;
import com.takusemba.spotlight.OnSpotlightEndedListener;
import com.takusemba.spotlight.OnSpotlightStartedListener;
import com.takusemba.spotlight.OnTargetStateChangedListener;
import com.takusemba.spotlight.SimpleTarget;
import com.takusemba.spotlight.Spotlight;

import java.util.ArrayList;

/**
 * Created by duy on 10/23/17.
 */

public class ExamQuestActivity extends BaseActivity {
    private GPLXDataManager dataManager;
    private Test test;
    private ArrayList<TestQuest> testQuests;
    private ViewPager2 viewPager;
    private RecyclerView recyclerView;
    private QuestionResultTestAdapterRecycler questionResultTestAdapterRecycler;
    private ImageView imgNext, imgPrevios, imgBookMark;
    private CustomTextView bottomTitle;
    private int currentIndex, currentIndexFirst;
    private DrawerLayout drawer;
    private android.widget.RelativeLayout nav_view_right;
    private ViewPagerAdapterCustomTestQuestion viewPagerAdapterCustomTestQuestion;
    private CustomTextView tvRightBtn;
    private RelativeLayout rltRightButton;
    private ActivityResultLauncher<Intent> launchSomeActivity;
    private RelativeLayout rltShowDetail;
    private SwitchCompat swShowDetail;
    private RelativeLayout btn_audio;
    private LottieAnimationView animSound;
    private View viewLine;

    @SuppressLint("SourceLockedOrientationActivity")
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (!AppUtils.checkPortraitMode(this)) {
            setForceDarkTheme();
        }
        launchSomeActivity = registerForActivityResult(
                new ActivityResultContracts.StartActivityForResult(),
                new ActivityResultCallback<ActivityResult>() {
                    @Override
                    public void onActivityResult(ActivityResult result) {
                        if (result.getResultCode() == Activity.RESULT_OK) {
                            processAction(result.getData());
                        }
                    }
                });

        setContentView(R.layout.activity_question_test_screen);
        Window window = this.getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | 0);
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(Color.TRANSPARENT);
        }
        dataManager = new GPLXDataManager(this);
        test = (Test) getIntent().getExtras().getParcelable("test");
        testQuests = dataManager.getAllTestQuestByTest(test, this);
        currentIndexFirst = test.getCurrentQuest();
        bottomTitle = (CustomTextView) findViewById(R.id.bottomTitle);
        viewPager = (ViewPager2) findViewById(R.id.viewpager);
        recyclerView = (RecyclerView) findViewById(R.id.list);
        imgNext = (ImageView) findViewById(R.id.icon_next);
        imgPrevios = (ImageView) findViewById(R.id.icon_previous);
        imgBookMark = (ImageView) findViewById(R.id.icon_bookmark);
        tvRightBtn = findViewById(R.id.tvRightBtn);
        rltRightButton = findViewById(R.id.rltRightButton);
        swShowDetail = findViewById(R.id.swShowDetail);
        rltShowDetail = findViewById(R.id.rltShowDetail);
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.setScrimColor(getColor(R.color.shadow_light));
        btn_audio = findViewById(R.id.btn_audio);
        animSound = findViewById(R.id.animSound);
        viewLine = findViewById(R.id.viewLine);

        setUpToolBar();

        if (Util.isPro(this)) {
            if (SharedPreferencesUtils.showVoidEnable()) {
                viewLine.setVisibility(View.GONE);
            } else {
                viewLine.setVisibility(View.VISIBLE);
            }
        } else {
            if (test.getIdTest() > 2) {
                viewLine.setVisibility(View.VISIBLE);
            } else {
                if (SharedPreferencesUtils.showVoidEnableFree()) {
                    viewLine.setVisibility(View.GONE);
                } else {
                    viewLine.setVisibility(View.VISIBLE);
                }
            }
        }
        btn_audio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!Util.isPro(ExamQuestActivity.this) && test.getIdTest() > 2) {
                    new DialogUtil.Builder(ExamQuestActivity.this)
                            .title("Hướng dẫn học bằng âm thanh")
                            .content("Học qua các giọng đọc tự nhiên đầy cảm xúc, ghi nhớ tình huống rõ hơn. Bạn cần nâng cấp để sử dụng tính năng này và nhiều tính năng cao cấp khác.")
                            .doneText(getString(R.string.text_ok))
                            .onDone(new DialogUtil.SingleButtonCallback() {
                                @Override
                                public void onClick() {
                                    buyPro();
                                }
                            })
                            .show();
                } else {
                    if (Util.isPro(ExamQuestActivity.this)) {
                        SharedPreferencesUtils.setShowVoidEnable(!SharedPreferencesUtils.showVoidEnable());
                        if (SharedPreferencesUtils.showVoidEnable()) {
                            viewLine.setVisibility(View.GONE);
                        } else {
                            viewLine.setVisibility(View.VISIBLE);
                        }
                    } else {
                        SharedPreferencesUtils.setShowVoidEnableFree(!SharedPreferencesUtils.showVoidEnableFree());
                        if (SharedPreferencesUtils.showVoidEnableFree()) {
                            viewLine.setVisibility(View.GONE);
                        } else {
                            viewLine.setVisibility(View.VISIBLE);
                        }
                    }
                }
            }
        });


        if (!Util.isPro(ExamQuestActivity.this)) {
            if (test.getIdTest() <= 2) {
                swShowDetail.setEnabled(true);
            } else {
                swShowDetail.setEnabled(false);
            }
        } else {
            swShowDetail.setEnabled(true);
        }
        if (!Util.isPro(ExamQuestActivity.this)) {
            swShowDetail.setChecked(SharedPreferencesUtils.showDetailFree());
        } else {
            swShowDetail.setChecked(SharedPreferencesUtils.showDetail());
        }
        swShowDetail.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (Util.isPro(ExamQuestActivity.this)) {
                    SharedPreferencesUtils.setShowDetail(b);
                } else {
                    SharedPreferencesUtils.setShowDetailFree(b);
                }
                reloadShowDetail();
            }
        });
        rltShowDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!Util.isPro(ExamQuestActivity.this) && test.getIdTest() > 2) {
                    new DialogUtil.Builder(ExamQuestActivity.this)
                            .title("Hiển thị chi tiết các trường hợp khi thi thử")
                            .content("Bạn cần nâng cấp để sử dụng tính năng này và nhiều tính năng cao cấp khác.")
                            .doneText(getString(R.string.text_ok))
                            .onDone(new DialogUtil.SingleButtonCallback() {
                                @Override
                                public void onClick() {
                                    buyPro();
                                }
                            })
                            .show();
                    return;
                }
                if (Util.isPro(ExamQuestActivity.this)) {
                    swShowDetail.setChecked(!SharedPreferencesUtils.showDetail());
                } else {
                    swShowDetail.setChecked(!SharedPreferencesUtils.showDetailFree());
                }
                reloadShowDetail();
            }
        });

        viewPagerAdapterCustomTestQuestion = new ViewPagerAdapterCustomTestQuestion(this, testQuests, test);
        viewPager.setAdapter(viewPagerAdapterCustomTestQuestion);
        currentIndex = test.getCurrentQuest();
        if (currentIndexFirst > 0) {
            viewPager.postDelayed(new Runnable() {
                @Override
                public void run() {
                    viewPager.setCurrentItem(currentIndexFirst, false);
                }
            }, 0);
        }
        viewPager.registerOnPageChangeCallback(new ViewPager2.OnPageChangeCallback() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                super.onPageScrolled(position, positionOffset, positionOffsetPixels);
            }

            @Override
            public void onPageSelected(int position) {
                super.onPageSelected(position);
                if (viewPager != null) {
                    ViewPagerAdapterCustomTestQuestion adapter = (ViewPagerAdapterCustomTestQuestion) viewPager.getAdapter();
                    if (adapter != null) {
                        ExamQuestFragment examQuestFragment = adapter.getFragment(currentIndex);
                        if (examQuestFragment != null)
                            examQuestFragment.stopVideo();
                        adapter.changeCheckedShowScore();
                    }
                }
                currentIndex = position;
                questionResultTestAdapterRecycler.notifyItemChanged(questionResultTestAdapterRecycler.getIndexSelection());
                questionResultTestAdapterRecycler.setIndexSelection(currentIndex);
                questionResultTestAdapterRecycler.notifyItemChanged(currentIndex);
                test.setCurrentQuest(position);
                try {
                    if (Util.isPro(ExamQuestActivity.this)) {
                        if (SharedPreferencesUtils.showDetail()) {
                            bottomTitle.setText("(" + (test.getCurrentQuest() + 1) + "/" + testQuests.size() + ")" + " " + testQuests.get(currentIndex).getQuestionVideo().getName());
                        } else {
                            bottomTitle.setText("Tình huống " + (test.getCurrentQuest() + 1) + "/" + testQuests.size());
                        }
                    } else {
                        if (SharedPreferencesUtils.showDetailFree() && test.getIdTest() <= 2) {
                            bottomTitle.setText("(" + (test.getCurrentQuest() + 1) + "/" + testQuests.size() + ")" + " " + testQuests.get(currentIndex).getQuestionVideo().getName());
                        } else {
                            bottomTitle.setText("Tình huống " + (test.getCurrentQuest() + 1) + "/" + testQuests.size());
                        }
                    }
                } catch (Exception e) {

                }
                dataManager.updateTestCurrenIndex(test, position);
                if (testQuests.get(position).getQuestionVideo().isMarked()) {
                    imgBookMark.setColorFilter(getColor(R.color.primary));
                } else {
                    imgBookMark.setColorFilter(getColor(R.color.color_text_item_setting_dim));
                }
                if (currentIndex == 0) {
                    imgPrevios.setColorFilter(getColor(R.color.color_text_item_setting_dim));
                } else {
                    imgPrevios.setColorFilter(getColor(R.color.primary));
                }
                if (currentIndex == testQuests.size() - 1) {
                    imgNext.setColorFilter(getColor(R.color.color_text_item_setting_dim));
                } else {
                    imgNext.setColorFilter(getColor(R.color.primary));
                }
                if (viewPager != null) {
                    ViewPagerAdapterCustomTestQuestion adapter = (ViewPagerAdapterCustomTestQuestion) viewPager.getAdapter();
                    ExamQuestFragment examQuestFragment = adapter.getFragment(currentIndex);
                    if (examQuestFragment != null)
                        examQuestFragment.enableNextBtn(currentIndex < (testQuests.size() - 1) ? true : false);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {
                super.onPageScrollStateChanged(state);
            }
        });

        if (currentIndex == 0) {
            imgPrevios.setColorFilter(getColor(R.color.color_text_item_setting_dim));
        } else {
            imgPrevios.setColorFilter(getColor(R.color.primary));
        }
        if (currentIndex == testQuests.size() - 1) {
            imgNext.setColorFilter(getColor(R.color.color_text_item_setting_dim));
        } else {
            imgNext.setColorFilter(getColor(R.color.primary));
        }

        if (testQuests != null && testQuests.size() > 0 && testQuests.get(test.getCurrentQuest()).getQuestionVideo().isMarked()) {
            imgBookMark.setColorFilter(getColor(R.color.primary));
        } else {
            imgBookMark.setColorFilter(getColor(R.color.color_text_item_setting_dim));
        }

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        questionResultTestAdapterRecycler = new QuestionResultTestAdapterRecycler(testQuests, this, test);
        questionResultTestAdapterRecycler.setClickListener(new QuestionResultTestAdapterRecycler.ItemClickListener() {
            @Override
            public void onItemClick(View view, final int position) {
                viewPager.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (viewPager != null) {
                            ViewPagerAdapterCustomTestQuestion adapter = (ViewPagerAdapterCustomTestQuestion) viewPager.getAdapter();
                            if (adapter != null) {
                                ExamQuestFragment examQuestFragment = adapter.getFragment(currentIndex);
                                if (examQuestFragment != null)
                                    examQuestFragment.stopVideo();
                            }
                        }
                        currentIndex = position;
                        if (viewPager != null)
                            viewPager.setCurrentItem(position, true);
                    }
                }, 0);
            }
        });
        recyclerView.setAdapter(questionResultTestAdapterRecycler);

        imgNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (currentIndex < testQuests.size() - 1) {
                    if (viewPager != null) {
                        ViewPagerAdapterCustomTestQuestion adapter = (ViewPagerAdapterCustomTestQuestion) viewPager.getAdapter();
                        if (adapter != null) {
                            ExamQuestFragment examQuestFragment = adapter.getFragment(currentIndex);
                            if (examQuestFragment != null)
                                examQuestFragment.stopVideo();
                        }
                    }
                    currentIndex++;
                    viewPager.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if (viewPager != null)
                                viewPager.setCurrentItem(currentIndex, true);
                        }
                    }, 10);
                }
            }
        });
        imgPrevios.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (currentIndex > 0) {
                    if (viewPager != null) {
                        ViewPagerAdapterCustomTestQuestion adapter = (ViewPagerAdapterCustomTestQuestion) viewPager.getAdapter();
                        if (adapter != null) {
                            ExamQuestFragment examQuestFragment = adapter.getFragment(currentIndex);
                            if (examQuestFragment != null)
                                examQuestFragment.stopVideo();
                        }
                    }
                    currentIndex--;
                    viewPager.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if (viewPager != null)
                                viewPager.setCurrentItem(currentIndex, true);
                        }
                    }, 10);
                }
            }
        });

        imgBookMark.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (testQuests.size() == 0)
                    return;
                if (testQuests.get(currentIndex).getQuestionVideo().isMarked()) {
                    if (dataManager.updateQuestionMarked(testQuests.get(currentIndex).getQuestionVideo(), false) > 0) {
                        testQuests.get(currentIndex).getQuestionVideo().setMarked(false);
                    }
                } else {
                    if (dataManager.updateQuestionMarked(testQuests.get(currentIndex).getQuestionVideo(), true) > 0) {
                        testQuests.get(currentIndex).getQuestionVideo().setMarked(true);
                    }
                }

                if (testQuests.get(currentIndex).getQuestionVideo().isMarked()) {
                    imgBookMark.setColorFilter(getColor(R.color.primary));
                } else {
                    imgBookMark.setColorFilter(getColor(R.color.color_text_item_setting_dim));
                }
            }
        });

        try {
            if (Util.isPro(this)) {
                if (SharedPreferencesUtils.showDetail()) {
                    bottomTitle.setText("(" + (test.getCurrentQuest() + 1) + "/" + testQuests.size() + ")" + " " + testQuests.get(currentIndex).getQuestionVideo().getName());
                } else {
                    bottomTitle.setText("Tình huống " + (test.getCurrentQuest() + 1) + "/" + testQuests.size());
                }
            } else {
                if (SharedPreferencesUtils.showDetailFree() && test.getIdTest() <= 2) {
                    bottomTitle.setText("(" + (test.getCurrentQuest() + 1) + "/" + testQuests.size() + ")" + " " + testQuests.get(currentIndex).getQuestionVideo().getName());
                } else {
                    bottomTitle.setText("Tình huống " + (test.getCurrentQuest() + 1) + "/" + testQuests.size());
                }
            }
        } catch (Exception e) {

        }

        nav_view_right = findViewById(R.id.nav_view_right);
        if (!AppUtils.checkPortraitMode(this)) {
            final int width = AppUtils.getWidthScreen(ExamQuestActivity.this) < AppUtils.getHeightScreen(ExamQuestActivity.this) ? AppUtils.getHeightScreen(ExamQuestActivity.this) : AppUtils.getWidthScreen(ExamQuestActivity.this);
            nav_view_right.postDelayed(new Runnable() {
                @Override
                public void run() {
                    nav_view_right.getLayoutParams().width = width / 2;
                    nav_view_right.requestLayout();
                    if (test.isFinish()) {
                        drawer.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                drawer.openDrawer(GravityCompat.END);
                            }
                        }, 10);
                    }
                }
            }, 10);
        } else {
            nav_view_right.postDelayed(new Runnable() {
                @Override
                public void run() {
                    nav_view_right.getLayoutParams().width = AppUtils.getWidthScreen(ExamQuestActivity.this) * 2 / 3;
                    nav_view_right.requestLayout();
                    if (test.isFinish()) {
                        drawer.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                drawer.openDrawer(GravityCompat.END);
                            }
                        }, 10);
                    }
                }
            }, 10);
        }


        if (!test.isFinish()) {
            tvRightBtn.setText("Kết thúc bài thi");
        } else {
            tvRightBtn.setText("Làm lại bài thi");
        }
        rltRightButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (test.isFinish()) {
                    new DialogUtil.Builder(ExamQuestActivity.this)
                            .title("Làm lại bài thi")
                            .content("Bạn có muốn làm lại bài thi này không ?")
                            .positiveText(getResources().getString(R.string.text_ok))
                            .negativeText(getResources().getString(R.string.text_cancel))
                            .onPositive(new DialogUtil.SingleButtonCallback() {
                                @Override
                                public void onClick() {
                                    restartTest();
                                }
                            })
                            .show();
                } else {
                    //check xem đã hoàn thành tất cả các video chưa
                    int countCheck = 0;
                    for (int i = 0; i < testQuests.size(); i++) {
                        if (testQuests.get(i).isLearned()) {
                            countCheck++;
                        }
                    }
                    int currentIndexTemp = -1;
                    for (int i = 0; i < testQuests.size(); i++) {
                        TestQuest detail = testQuests.get(i);
                        if (!detail.isLearned()) {
                            currentIndexTemp = i;
                            break;
                        }
                    }

                    if (countCheck < testQuests.size()) {
                        final int currentIndexTempNext = currentIndexTemp;
                        if (currentIndexTempNext == currentIndex) {
                            new DialogUtil.Builder(ExamQuestActivity.this)
                                    .title("Bạn chưa hoàn thành video này")
                                    .content("Bạn vui lòng hoàn thành phát hiện tình huống ở tất cả các video để kết thúc bài thi và xem đáp án giải thích chi tiết.")
                                    .doneText("Đồng ý")
                                    .show();
                        } else {
                            if (!testQuests.get(currentIndex).isLearned()) {
                                new DialogUtil.Builder(ExamQuestActivity.this)
                                        .title("Bạn chưa hoàn thành video này")
                                        .content("Bạn vui lòng hoàn thành phát hiện tình huống ở tất cả các video để kết thúc bài thi và xem đáp án giải thích chi tiết.")
                                        .doneText("Đồng ý")
                                        .show();
                            } else {
                                new DialogUtil.Builder(ExamQuestActivity.this)
                                        .title("Kết thúc bài thi")
                                        .content("Bạn vui lòng hoàn thành phát hiện tình huống ở tất cả các video để kết thúc bài thi và xem đáp án giải thích chi tiết.")
                                        .doneText("Chuyển đến video chưa làm")
                                        .onDone(new DialogUtil.SingleButtonCallback() {
                                            @Override
                                            public void onClick() {
                                                currentIndex = currentIndexTempNext;
                                                viewPager.post(new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        viewPager.setCurrentItem(currentIndex, true);
                                                        questionResultTestAdapterRecycler.notifyItemChanged(questionResultTestAdapterRecycler.getIndexSelection());
                                                        questionResultTestAdapterRecycler.setIndexSelection(currentIndex);
                                                        questionResultTestAdapterRecycler.notifyItemChanged(currentIndex);
                                                        if (drawer != null) {
                                                            drawer.postDelayed(new Runnable() {
                                                                @Override
                                                                public void run() {
                                                                    drawer.openDrawer(GravityCompat.END);
                                                                }
                                                            }, 10);
                                                        }
                                                    }
                                                });
                                            }
                                        }).show();
                            }
                        }
                    } else {
                        //ket thuc bai thi
                        new DialogUtil.Builder(ExamQuestActivity.this)
                                .title("Kết thúc bài thi")
                                .content("Bạn có muốn kết thúc bài thi và xem đáp án giải thích chi tiết và mẹo thi từng trường hợp không.")
                                .positiveText(getResources().getString(R.string.text_ok))
                                .negativeText(getResources().getString(R.string.text_cancel))
                                .onPositive(new DialogUtil.SingleButtonCallback() {
                                    @Override
                                    public void onClick() {
                                        finishTest();
                                    }
                                })
                                .show();
                    }
                }
            }
        });
        showDialogGuide();

        SharedPreferencesUtils.setInt("CURRENT_TEST", test.getIdTest());

    }

    public void startAudioAnimation() {
        if (animSound != null) {
            animSound.playAnimation();
        }
    }

    public void stopAudioAnimation() {
        if (animSound != null) {
            animSound.pauseAnimation();
            animSound.setFrame(0);
        }
    }

    public void reloadShowDetail() {
        questionResultTestAdapterRecycler.notifyDataSetChanged();
        try {
            if (Util.isPro(this)) {
                if (SharedPreferencesUtils.showDetail()) {
                    bottomTitle.setText("(" + (test.getCurrentQuest() + 1) + "/" + testQuests.size() + ")" + " " + testQuests.get(currentIndex).getQuestionVideo().getName());
                } else {
                    bottomTitle.setText("Tình huống " + (test.getCurrentQuest() + 1) + "/" + testQuests.size());
                }
            } else {
                if (SharedPreferencesUtils.showDetailFree() && test.getIdTest() <= 2) {
                    bottomTitle.setText("(" + (test.getCurrentQuest() + 1) + "/" + testQuests.size() + ")" + " " + testQuests.get(currentIndex).getQuestionVideo().getName());
                } else {
                    bottomTitle.setText("Tình huống " + (test.getCurrentQuest() + 1) + "/" + testQuests.size());
                }
            }
        } catch (Exception e) {

        }
    }

    public void showDialogGuide() {
        if (!SharedPreferencesUtils.getBoolean("DIALOG_LEARN_GUIDE_EXAM")) {
            final Dialog dialog = new Dialog(this);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            Window window = dialog.getWindow();
            window.setBackgroundDrawableResource(android.R.color.transparent);
            dialog.setCanceledOnTouchOutside(false);
            dialog.setCancelable(false);
            dialog.setContentView(R.layout.dialog_guide);
            dialog.findViewById(R.id.btn_ok).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.cancel();
                    dialog.dismiss();
                    SharedPreferencesUtils.setBoolean("DIALOG_LEARN_GUIDE_EXAM", true);
                    showSpotLight();
                }
            });
            dialog.show();
        }
    }

    public void showSpotLight() {
        if (!SharedPreferencesUtils.getBoolean("GUIDE_BOOKMARK_EXAM")) {
            SharedPreferencesUtils.setBoolean("GUIDE_BOOKMARK_EXAM", true);

            SimpleTarget menuTarget = new SimpleTarget.Builder(this)
                    .setPoint(findViewById(R.id.btn_list)) // position of the Target. setPoint(Point point), setPoint(View view) will work too.
                    .setRadius(64f) // radius of the Target
                    .setTitle(getResources().getString(R.string.guide_menu_title)) // title
                    .setDescription(getResources().getString(R.string.guide_menu_content)) // description
                    .setOnSpotlightStartedListener(new OnTargetStateChangedListener<SimpleTarget>() {
                        @Override
                        public void onStarted(SimpleTarget target) {
                            // do something
                        }

                        @Override
                        public void onEnded(SimpleTarget target) {
                            // do something
                        }
                    })
                    .build();

            SimpleTarget bookMarkTarget = new SimpleTarget.Builder(this)
                    .setPoint(imgBookMark) // position of the Target. setPoint(Point point), setPoint(View view) will work too.
                    .setRadius(64f) // radius of the Target
                    .setTitle(getResources().getString(R.string.guide_bookmark_title)) // title
                    .setDescription(getResources().getString(R.string.guide_bookmark_content)) // description
                    .setOnSpotlightStartedListener(new OnTargetStateChangedListener<SimpleTarget>() {
                        @Override
                        public void onStarted(SimpleTarget target) {
                            // do something
                        }

                        @Override
                        public void onEnded(SimpleTarget target) {
                            // do something
                        }
                    })
                    .build();

            ImageViewButton imgFull = findViewById(R.id.imgFullScreen);

            SimpleTarget imgFullTarget = new SimpleTarget.Builder(this)
                    .setPoint(imgFull) // position of the Target. setPoint(Point point), setPoint(View view) will work too.
                    .setRadius(64f) // radius of the Target
                    .setTitle("Xem toàn màn hình") // title
                    .setDescription("Bấm vào đây để xem video ở toàn màn hình. Hoặc bật điện thoại ở chế độ tự quay màn hình để xem full màn hình khi quay ngang điện thoại") // description
                    .setOnSpotlightStartedListener(new OnTargetStateChangedListener<SimpleTarget>() {
                        @Override
                        public void onStarted(SimpleTarget target) {
                            // do something
                        }

                        @Override
                        public void onEnded(SimpleTarget target) {
                            // do something
                        }
                    })
                    .build();

            ImageViewButton imgPlay = findViewById(R.id.imgPlay);

            SimpleTarget imgPlayTarget = new SimpleTarget.Builder(this)
                    .setPoint(imgPlay) // position of the Target. setPoint(Point point), setPoint(View view) will work too.
                    .setRadius(64f) // radius of the Target
                    .setTitle("Phát video") // title
                    .setDescription("Bấm vào đây để bắt đầu phát bài thi và video bắt đầu được phát") // description
                    .setOnSpotlightStartedListener(new OnTargetStateChangedListener<SimpleTarget>() {
                        @Override
                        public void onStarted(SimpleTarget target) {
                            // do something
                        }

                        @Override
                        public void onEnded(SimpleTarget target) {
                            // do something
                        }
                    })
                    .build();

            ImageViewButton imgPause = findViewById(R.id.imgPause);

            SimpleTarget imgPauseTarget = new SimpleTarget.Builder(this)
                    .setPoint(imgPause) // position of the Target. setPoint(Point point), setPoint(View view) will work too.
                    .setRadius(64f) // radius of the Target
                    .setTitle("Dừng video") // title
                    .setDescription("Bấm vào đây để dừng phát video, lưu ý khi bạn dừng phát video thì bạn không thể bấm space chọn tình huống.") // description
                    .setOnSpotlightStartedListener(new OnTargetStateChangedListener<SimpleTarget>() {
                        @Override
                        public void onStarted(SimpleTarget target) {
                            // do something
                        }

                        @Override
                        public void onEnded(SimpleTarget target) {
                            // do something
                        }
                    })
                    .build();

            ImageViewButton imgNext = findViewById(R.id.imgNext);

            SimpleTarget imgResetTarget = new SimpleTarget.Builder(this)
                    .setPoint(imgNext) // position of the Target. setPoint(Point point), setPoint(View view) will work too.
                    .setRadius(64f) // radius of the Target
                    .setTitle("Bài học tiếp theo") // title
                    .setDescription("Bấm vào đây để chuyển đến video tiếp theo của bài thi") // description
                    .setOnSpotlightStartedListener(new OnTargetStateChangedListener<SimpleTarget>() {
                        @Override
                        public void onStarted(SimpleTarget target) {
                            // do something
                        }

                        @Override
                        public void onEnded(SimpleTarget target) {
                            // do something
                        }
                    })
                    .build();

            TextView space = findViewById(R.id.tvSpaceText);
            SimpleTarget spaceTarget = new SimpleTarget.Builder(this)
                    .setPoint(space) // position of the Target. setPoint(Point point), setPoint(View view) will work too.
                    .setRadius(80f) // radius of the Target
                    .setTitle("Chọn tình huống") // title
                    .setDescription("Bấm vào đây để chọn thời điểm bắt đầu có dấu hiệu phát hiện ra tình huống nguy hiểm, lái xe cần xử lý. Lưu ý mỗi video của bài thi chỉ được bấm Space 1 lần.") // description
                    .setOnSpotlightStartedListener(new OnTargetStateChangedListener<SimpleTarget>() {
                        @Override
                        public void onStarted(SimpleTarget target) {
                            // do something
                        }

                        @Override
                        public void onEnded(SimpleTarget target) {
                            // do something
                        }
                    })
                    .build();

            if (AppUtils.checkPortraitMode(this)) {
                Spotlight.with(this)
                        .setOverlayColor(ContextCompat.getColor(ExamQuestActivity.this, R.color.bg_splotlight)) // background overlay color
                        .setDuration(1000L) // duration of Spotlight emerging and disappearing in ms
                        .setAnimation(new DecelerateInterpolator(2f)) // animation of Spotlight
                        .setTargets(menuTarget, bookMarkTarget, imgFullTarget, imgPlayTarget, imgPauseTarget, imgResetTarget, spaceTarget) // set targets. see below for more info
                        .setClosedOnTouchedOutside(true) // set if target is closed when touched outside
                        .setOnSpotlightStartedListener(new OnSpotlightStartedListener() { // callback when Spotlight starts
                            @Override
                            public void onStarted() {

                            }
                        })
                        .setOnSpotlightEndedListener(new OnSpotlightEndedListener() { // callback when Spotlight ends
                            @Override
                            public void onEnded() {

                            }
                        })
                        .start(); // start Spotlight
            } else {
                Spotlight.with(this)
                        .setOverlayColor(ContextCompat.getColor(ExamQuestActivity.this, R.color.bg_splotlight)) // background overlay color
                        .setDuration(1000L) // duration of Spotlight emerging and disappearing in ms
                        .setAnimation(new DecelerateInterpolator(2f)) // animation of Spotlight
                        .setTargets(menuTarget, bookMarkTarget, imgPlayTarget, imgPauseTarget, imgResetTarget, spaceTarget) // set targets. see below for more info
                        .setClosedOnTouchedOutside(true) // set if target is closed when touched outside
                        .setOnSpotlightStartedListener(new OnSpotlightStartedListener() { // callback when Spotlight starts
                            @Override
                            public void onStarted() {

                            }
                        })
                        .setOnSpotlightEndedListener(new OnSpotlightEndedListener() { // callback when Spotlight ends
                            @Override
                            public void onEnded() {

                            }
                        })
                        .start(); // start Spotlight
            }
        }
    }


    public void setUpToolBar() {
        TextView timeCounter = findViewById(R.id.timeCounter);
        String title = test.getTestName();
        timeCounter.setText(title);
        try {
            if (Util.isPro(this)) {
                if (SharedPreferencesUtils.showDetail()) {
                    bottomTitle.setText("(" + (test.getCurrentQuest() + 1) + "/" + testQuests.size() + ")" + " " + testQuests.get(currentIndex).getQuestionVideo().getName());
                } else {
                    bottomTitle.setText("Tình huống " + (test.getCurrentQuest() + 1) + "/" + testQuests.size());
                }
            } else {
                if (SharedPreferencesUtils.showDetailFree() && test.getIdTest() <= 2) {
                    bottomTitle.setText("(" + (test.getCurrentQuest() + 1) + "/" + testQuests.size() + ")" + " " + testQuests.get(currentIndex).getQuestionVideo().getName());
                } else {
                    bottomTitle.setText("Tình huống " + (test.getCurrentQuest() + 1) + "/" + testQuests.size());
                }
            }
        } catch (Exception e) {

        }

        com.rey.material.widget.FrameLayout btn_back = findViewById(R.id.btn_back);
        setUpToolBar(btn_back);
        btn_back.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                onBackPressed();
            }
        });
        com.rey.material.widget.FrameLayout btn_list = findViewById(R.id.btn_list);
        setUpToolBar(btn_list);
        btn_list.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                if (drawer != null) {
                    if (!drawer.isDrawerOpen(GravityCompat.START))
                        drawer.openDrawer(GravityCompat.END);
                    else drawer.closeDrawer(GravityCompat.END);
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else if (drawer.isDrawerOpen(GravityCompat.END)) {
            drawer.closeDrawer(GravityCompat.END);
        } else {
            if (!test.isFinish()) {
                new DialogUtil.Builder(this)
                        .title("Tạm dừng bài thi")
                        .content("Bạn chưa hoàn thành chọn tất cả các video, bạn có muốn tạm dừng bài thi này không?")
                        .onPositive(new DialogUtil.SingleButtonCallback() {
                            @Override
                            public void onClick() {
                                if (!AppUtils.checkPortraitMode(ExamQuestActivity.this)) {
                                    if (android.provider.Settings.System.getInt(getContentResolver(), Settings.System.ACCELEROMETER_ROTATION, 0) == 1) {
                                        ExamQuestActivity.this.finish();
                                    } else {
                                        requestOritention(false);
                                    }
                                } else {
                                    ExamQuestActivity.this.finish();
                                }
                            }
                        })
                        .show();
            } else {
                if (!AppUtils.checkPortraitMode(this)) {
                    if (android.provider.Settings.System.getInt(getContentResolver(), Settings.System.ACCELEROMETER_ROTATION, 0) == 1) {
                        super.onBackPressed();
                    } else {
                        requestOritention(false);
                    }
                } else {
                    super.onBackPressed();
                }
            }
        }
    }

    public void finishTest() {
        AdmobHelp.getInstance().showInterstitialAd( new AdCloseListener() {
            @Override
            public void onAdClosed(boolean haveAds) {
                test.setCurrentQuest(0);
                test.setFinish(true);
                dataManager.updateTestFinish(test);
                tvRightBtn.setText("Làm lại bài thi");
                //open drawer
                if (drawer != null) {
                    drawer.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            drawer.openDrawer(GravityCompat.END);
                        }
                    }, 10);
                }
                runOnUiThread(new Runnable() {
                    public void run() {
                        if (questionResultTestAdapterRecycler != null) {
                            questionResultTestAdapterRecycler.setTest(test);
                        }
                    }
                });

                Intent intent = new Intent(ExamQuestActivity.this, ResultActivity.class);
                Bundle bundle = new Bundle();
                bundle.putParcelable("test", test);
                intent.putExtras(bundle);
                launchSomeActivity.launch(intent);
                if (haveAds) {
                    checkShowSuggestProDisplayAds();
                }
            }
        });
    }

    public void updateQuestion(TestQuest testQuest, final int index) {
        int result = dataManager.updateQuestionAnswerTest(testQuest);
        if (result > 0) {
            testQuest.getQuestionVideo().setMarked(testQuests.get(index).getQuestionVideo().isMarked());
            testQuests.set(index, testQuest);
        }
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                questionResultTestAdapterRecycler.notifyItemChanged(index);
            }
        });
        int countLearn = 0;
        for (int i = 0; i < testQuests.size(); i++) {
            if (testQuests.get(i).getAnswer() > 0 && testQuests.get(i).isLearned())
                countLearn++;
        }
        if (countLearn == testQuests.size()) {
            new DialogUtil.Builder(ExamQuestActivity.this)
                    .title("Kết thúc bài thi")
                    .content("Chúc mừng bạn đã hoàn thành tất cả các video. Bạn có muốn kết thúc bài thi và xem đáp án giải thích chi tiết.")
                    .positiveText(getResources().getString(R.string.text_ok))
                    .negativeText(getResources().getString(R.string.text_cancel))
                    .onPositive(new DialogUtil.SingleButtonCallback() {
                        @Override
                        public void onClick() {
                            finishTest();
                        }
                    })
                    .show();
        }
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            dataManager.close();
        } catch (Exception e) {

        }
        try {
            ViewPagerAdapterCustomTestQuestion adapter = (ViewPagerAdapterCustomTestQuestion) viewPager.getAdapter();
            if (adapter != null) {
                adapter.detroyAllVideo();
            }
        } catch (Exception e) {

        }
    }

    public void destroyItem(int pos) {
        if (viewPagerAdapterCustomTestQuestion != null) {
            try {
                viewPagerAdapterCustomTestQuestion.destroyItem(pos);
            } catch (Exception e) {

            }
        }
    }

    public void disableViewPagerEvent(boolean action) {
        if (viewPager != null) {
            viewPager.setUserInputEnabled(action);
        }
    }

    public Test getTest() {
        return test;
    }

    public void nextVideo() {
        if (currentIndex < testQuests.size() - 1) {
            if (viewPager != null) {
                ViewPagerAdapterCustomTestQuestion adapter = (ViewPagerAdapterCustomTestQuestion) viewPager.getAdapter();
                if (adapter != null) {
                    ExamQuestFragment examQuestFragment = adapter.getFragment(currentIndex);
                    if (examQuestFragment != null)
                        examQuestFragment.stopVideo();
                }
            }
            currentIndex++;
            viewPager.postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (viewPager != null)
                        viewPager.setCurrentItem(currentIndex, true);
                }
            }, 10);
        }
    }

    public void processAction(Intent data) {
        if (data.getStringExtra("action").equals("next")) {
            Test testResult = data.getExtras().getParcelable("test");
            Intent dataNew = new Intent();
            dataNew.putExtra("action", "next");
            Bundle bundle = new Bundle();
            bundle.putParcelable("test", testResult);
            dataNew.putExtras(bundle);
            setResult(Activity.RESULT_OK, dataNew);
            finish();
        } else if (data.getStringExtra("action").equals("restart")) {
            String action = "restart";
            restartTest();
        } else if (data.getStringExtra("action").equals("review")) {
            ViewPagerAdapterCustomTestQuestion adapter = (ViewPagerAdapterCustomTestQuestion) viewPager.getAdapter();
            if (adapter != null)
                adapter.checkResult();
        }
    }

    public void restartTest() {
        dataManager.updateResetTestQuest(test);
        test.setFinish(false);
        test.setCurrentQuest(0);
        test.setCountQuestion(0);
        test.setTotalScore(0);
        dataManager.updateTestFinish(test);
        for (int i = 0; i < testQuests.size(); i++) {
            testQuests.get(i).setLearned(false);
            testQuests.get(i).setScore(0);
            testQuests.get(i).setAnswer(0);
        }
        runOnUiThread(new Runnable() {
            public void run() {
                if (questionResultTestAdapterRecycler != null) {
                    questionResultTestAdapterRecycler.setTest(test);
                }
            }
        });
        if (!test.isFinish()) {
            tvRightBtn.setText("Kết thúc bài thi");
        } else {
            tvRightBtn.setText("Làm lại bài thi");
        }
        currentIndex = 0;
        viewPagerAdapterCustomTestQuestion = new ViewPagerAdapterCustomTestQuestion(this,testQuests,test);
        viewPager.setAdapter(viewPagerAdapterCustomTestQuestion);
    }

    public void requestOritention(boolean islandcape) {
        if (islandcape) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        } else {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
    }

    public boolean isEnableAudio() {
        if (viewLine == null)
            return false;
        else
            return (viewLine.getVisibility() == View.VISIBLE) ? false : true;
    }
}
