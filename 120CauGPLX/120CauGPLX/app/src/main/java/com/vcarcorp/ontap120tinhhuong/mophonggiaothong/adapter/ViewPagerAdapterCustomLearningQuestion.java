package com.vcarcorp.ontap120tinhhuong.mophonggiaothong.adapter;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.viewpager2.adapter.FragmentStateAdapter;

import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.learnquestion.LearnQuestionFragment;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.model.QuestionCategory;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.model.QuestionVideo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ViewPagerAdapterCustomLearningQuestion extends FragmentStateAdapter {
    private ArrayList<QuestionVideo> questionVideos;
    private QuestionCategory questionCategory;
    private Map<Integer, Fragment> mPageReferenceMap = new HashMap<Integer, Fragment>();

    public ViewPagerAdapterCustomLearningQuestion(FragmentActivity fragmentActivity,ArrayList<QuestionVideo> questionVideos, QuestionCategory questionCategory){
        super(fragmentActivity);
        this.questionVideos = questionVideos;
        this.questionCategory = questionCategory;
    }

    @NonNull
    @Override
    public Fragment createFragment(int position) {
        LearnQuestionFragment learnQuestionFragment =  LearnQuestionFragment.init(questionVideos.get(position),questionCategory,position);
        mPageReferenceMap.put(position,learnQuestionFragment);
        return learnQuestionFragment;
    }

    @Override
    public int getItemCount() {
        return questionVideos.size();
    }

    public void detroyAllVideo(){
        try {
            for (Map.Entry<Integer, Fragment> entry : mPageReferenceMap.entrySet()) {
                LearnQuestionFragment learnQuestionFragment = (LearnQuestionFragment) entry.getValue();
                if(learnQuestionFragment!=null)
                    learnQuestionFragment.destroyVideo();
            }
        }catch (Exception e){

        }
    }

    public void changeCheckedShowScore(){
        try {
            for (Map.Entry<Integer, Fragment> entry : mPageReferenceMap.entrySet()) {
                LearnQuestionFragment learnQuestionFragment = (LearnQuestionFragment) entry.getValue();
                if(learnQuestionFragment!=null)
                    learnQuestionFragment.changeCheckedShowScore();
            }
        }catch (Exception e){

        }
    }

    public LearnQuestionFragment getFragment(int key) {
        return (LearnQuestionFragment) mPageReferenceMap.get(key);
    }

    public void destroyItem(int position){
        try{
            LearnQuestionFragment learnQuestionFragment = getFragment(position);
            learnQuestionFragment.destroyVideo();
            mPageReferenceMap.remove(position);
        }catch (Exception e){

        }
    }

}
