package com.vcarcorp.ontap120tinhhuong.mophonggiaothong.reminder;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.util.Log;
import android.widget.RemoteViews;

import androidx.core.app.NotificationCompat;
import com.google.firebase.database.annotations.NotNull;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.R;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.main.SplashActivity;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.util.SharedPreferencesUtils;

import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Random;

public class ReminderNotifierReceiver extends BroadcastReceiver {
    private static final String TAG = "ReminderNotifierReceive";
    public static final int ID_NOTIFIER = 12345;
    private Map<String, String> currentHourMap = new HashMap<>();
    public static final String OPEN_APP = "write_diary";
    public static SimpleDateFormat currentDF = new SimpleDateFormat("HH:mm", Locale.ENGLISH);

    @Override
    public void onReceive(Context context, Intent intent) {
        currentDF = new SimpleDateFormat("HH:mm", Locale.ENGLISH);
        currentHourMap.put("Current", ReminderNotifierReceiver.currentDF.format(System.currentTimeMillis()));
        Log.e(TAG, "onReceive: ");
        if(SharedPreferencesUtils.showReminder()) {
            notifi(context);
        }
    }

    public void notifi(@NotNull Context context) {
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        Intent repeating_intent = new Intent(context,
                SplashActivity.class);
        repeating_intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);

        PendingIntent pendingIntent = PendingIntent.getActivity(context, 12, repeating_intent,   PendingIntent.FLAG_IMMUTABLE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(String.valueOf(ID_NOTIFIER), "Notification", NotificationManager.IMPORTANCE_HIGH);
            channel.enableVibration(true);
            channel.setLightColor(Color.BLUE);
            channel.enableLights(true);
            channel.setShowBadge(true);

            if (notificationManager != null) {
                notificationManager.createNotificationChannel(channel);
            }
        }

        RemoteViews notificationLayout =
                new RemoteViews(context.getPackageName(), R.layout.notification_custom_view);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(context, String.valueOf(ID_NOTIFIER))
                .setCustomContentView(notificationLayout)
                .setFullScreenIntent(pendingIntent,true)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setAutoCancel(true)
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setContentTitle(context.getResources().getString(R.string.app_name))
                .setContentText("Học ngay 120 tình huống")
                .setVisibility(NotificationCompat.VISIBILITY_PUBLIC);
        Random r = new Random();
        int i = r.nextInt(4) % 4;
        if(i == 0){
            // Lam de thi dang lam do
            notificationLayout.setTextViewText(R.id.tv_notifier_title,"Bạn có một đề thi đang làm dở");
            notificationLayout.setTextViewText(R.id.tv_notifier_content,"Hoàn thành nốt đề thi nào!");
        }else if(i == 1){
            // Vao man cac cau sai
            notificationLayout.setTextViewText(R.id.tv_notifier_title,"Bạn có 1 tình huống làm sai nhiều lần");
            notificationLayout.setTextViewText(R.id.tv_notifier_content,"Hãy đọc kỹ mẹo và làm lại tình huống này!");
        }else if(i == 2){
            // vao danh sach chu de
            notificationLayout.setTextViewText(R.id.tv_notifier_title,"Bạn còn nhiều tình huống chưa làm?");
            notificationLayout.setTextViewText(R.id.tv_notifier_content,"Học các tình huống để còn lấy bằng lái xe nào!");
        }else if(i == 3){
            // vao danh sach test
            notificationLayout.setTextViewText(R.id.tv_notifier_title,"Làm đề thi mô phỏng như thật");
            notificationLayout.setTextViewText(R.id.tv_notifier_content,"Thử làm đề thi mô phỏng xem thế nào nhé!");
        }
        repeating_intent = new Intent(context,
                SplashActivity.class);
        repeating_intent.setAction(OPEN_APP);
        repeating_intent.putExtra("ACTION_APP", i);
        repeating_intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        pendingIntent = PendingIntent.getActivity(context, 12, repeating_intent,  PendingIntent.FLAG_IMMUTABLE);
        notificationLayout.setOnClickPendingIntent(R.id.btn_drink_notifier, pendingIntent);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            builder.setChannelId(String.valueOf(ID_NOTIFIER));
        }
        if (notificationManager != null) {
            notificationManager.notify(ID_NOTIFIER, builder.build());
        }
        AlarmManagerSetup.scheduleAlarmReceiverDiaryReminder(context);
    }
}