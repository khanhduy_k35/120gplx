package com.vcarcorp.ontap120tinhhuong.mophonggiaothong.moredriver.testcenter;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ads.control.admob.AdmobHelp;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.R;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.database.DataManagerTestCenter;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.main.BaseActivity;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.model.TestCenterGroup;

import java.util.ArrayList;

public class TestCenterActivity extends BaseActivity {

    private DataManagerTestCenter dataManagerTestCenter;
    private ArrayList<TestCenterGroup> testCenterGroups = new ArrayList<>();
    private RecyclerView recyclerView;
    private TestCenterGroupAdapter testCenterGroupAdapter;
    private EditText edtSearch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_center);
        AdmobHelp.getInstance().loadBanner(this);
        dataManagerTestCenter = new DataManagerTestCenter(TestCenterActivity.this);
        testCenterGroups = dataManagerTestCenter.getListTestCenter();
        recyclerView = (RecyclerView) findViewById(R.id.recycle_view);
        edtSearch = (EditText) findViewById(R.id.edt_search);

        recyclerView.setLayoutManager(new LinearLayoutManager(TestCenterActivity.this, LinearLayoutManager.VERTICAL, false));
        testCenterGroupAdapter = new TestCenterGroupAdapter(testCenterGroups, TestCenterActivity.this);
        recyclerView.setAdapter(testCenterGroupAdapter);
        testCenterGroupAdapter.setClickListener(new TestCenterGroupAdapter.ItemClickListener() {
            @Override
            public void onItemClick(String phone) {
                Intent dialIntent = new Intent(Intent.ACTION_DIAL);
                dialIntent.setData(Uri.parse("tel:" + phone));
                startActivity(dialIntent);
            }
        });
        findViewById(R.id.btn_back).setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                onBackPressed();
            }
        });

        findViewById(R.id.btn_platinum).setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                buyPro();
            }
        });
        edtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                testCenterGroupAdapter.filter(charSequence.toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }
}