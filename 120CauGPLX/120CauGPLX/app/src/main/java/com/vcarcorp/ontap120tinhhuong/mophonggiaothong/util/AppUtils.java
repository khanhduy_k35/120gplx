package com.vcarcorp.ontap120tinhhuong.mophonggiaothong.util;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.util.DisplayMetrics;
import android.util.TypedValue;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;

import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.R;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.main.JNIUtil;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.model.QuestionVideo;

import java.text.DecimalFormat;
import java.util.Random;

/**
 * Created by bruce on 14-11-6.
 */
public final class AppUtils {

    private AppUtils() {
    }

    public static float dp2px(Resources resources, float dp) {
        final float scale = resources.getDisplayMetrics().density;
        return dp * scale + 0.5f;
    }

    public static float sp2px(Resources resources, float sp) {
        final float scale = resources.getDisplayMetrics().scaledDensity;
        return sp * scale;
    }

    /**
     * This method converts dp unit to equivalent pixels, depending on device density.
     *
     * @param dp      A value in dp (density independent pixels) unit. Which we need to convert into pixels
     * @param context Context to get resources and device specific display metrics
     * @return A float value to represent px equivalent to dp depending on device density
     */
    public static int convertDpToPx(Context context, int dp) {
        return Math.round(dp * ((float) context.getResources().getDisplayMetrics().densityDpi / DisplayMetrics.DENSITY_DEFAULT));
    }

    public static int pxFromDp(Context context, int dp) {
        return Math.round(dp * ((float) context.getResources().getDisplayMetrics().densityDpi / DisplayMetrics.DENSITY_DEFAULT));
    }

    /**
     * This method converts device specific pixels to density independent pixels.
     *
     * @param px      A value in px (pixels) unit. Which we need to convert into db
     * @param context Context to get resources and device specific display metrics
     * @return A float value to represent dp equivalent to px value
     */
    public static float convertPixelsToDp(float px, Context context) {
        return px / ((float) context.getResources().getDisplayMetrics().densityDpi / DisplayMetrics.DENSITY_DEFAULT);
    }

    public static int getColorFromAttr(Context context, int resId) {
        TypedValue typedValue = new TypedValue();
        Resources.Theme theme = context.getTheme();
        theme.resolveAttribute(resId, typedValue, true);
        int color = typedValue.data;
        return color;
    }

    public static int getWidthScreen(FragmentActivity context) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        context.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int width = displayMetrics.widthPixels;
        return width;
    }

    public static int getHeightScreen(FragmentActivity context) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        context.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int width = displayMetrics.heightPixels;
        return width;
    }

    public static boolean checkPortraitMode(Context context) {
        int orientation = context.getResources().getConfiguration().orientation;
        if (orientation == Configuration.ORIENTATION_LANDSCAPE) {
            return false;
        } else {
            return true;
        }
    }

    public static String getServerVideo(Context context, int idVideo) {
        String linkVideo = "";
        if (SharedPreferencesUtils.getString("video_link_" + idVideo) == null) {
            int random = new Random().nextInt(6);
            linkVideo = JNIUtil.getServer(context, random);
            try {
                SharedPreferencesUtils.setString("video_link_" + idVideo, AESCrypt.encrypt(JNIUtil.apiKey(context), linkVideo));
            } catch (Exception e) {

            }
        } else {
            if (!SharedPreferencesUtils.getString("video_link_" + idVideo).isEmpty()) {
                try {
                    linkVideo = AESCrypt.decrypt(JNIUtil.apiKey(context), SharedPreferencesUtils.getString("video_link_" + idVideo));
                } catch (Exception e) {

                }
            } else {
                int random = new Random().nextInt(6);
                linkVideo = JNIUtil.getServer(context, random);
                try {
                    SharedPreferencesUtils.setString("video_link_" + idVideo, AESCrypt.encrypt(JNIUtil.apiKey(context), linkVideo));
                } catch (Exception e) {

                }
            }
        }
        return linkVideo + idVideo + ".mp4";
    }

    public static String getServerVideoVip(Context context, int idVideo) {
        return JNIUtil.getServerVip(context) + idVideo + ".mp4";
    }

    public static String getServerAudio(Context context,QuestionVideo questionVideo){
        String linkVideo = "";
        if (SharedPreferencesUtils.getString("img_link_" + questionVideo.getId()) == null) {
            int random = new Random().nextInt(6);
            linkVideo = JNIUtil.getServer(context, random);
            try {
                SharedPreferencesUtils.setString("img_link_" + questionVideo.getId(), AESCrypt.encrypt(JNIUtil.apiKey(context), linkVideo));
            } catch (Exception e) {

            }
        } else {
            if (!SharedPreferencesUtils.getString("img_link_" + questionVideo.getId()).isEmpty()) {
                try {
                    linkVideo = AESCrypt.decrypt(JNIUtil.apiKey(context), SharedPreferencesUtils.getString("img_link_" + questionVideo.getId()));
                } catch (Exception e) {

                }
            } else {
                int random = new Random().nextInt(6);
                linkVideo = JNIUtil.getServer(context, random);
                try {
                    SharedPreferencesUtils.setString("img_link_" + questionVideo.getId(), AESCrypt.encrypt(JNIUtil.apiKey(context), linkVideo));
                } catch (Exception e) {

                }
            }
        }
        return linkVideo.replace("DoneVideo/","audio/audio/");
    }

    public static String getServerAudioVip(Context context,QuestionVideo questionVideo){
        return JNIUtil.getServerVip(context).replace("DoneVideo/","audio/audio/");
    }


    public static String getServerImg(Context context,QuestionVideo questionVideo){
        if(Util.isPro(context)){
            return JNIUtil.getServerVip(context).replace("DoneVideo/", "doneimg/") + "TH" + questionVideo.getId() + ".webp";
        }
        String linkVideo = "";
        if (SharedPreferencesUtils.getString("img_link_" + questionVideo.getId()) == null) {
            int random = new Random().nextInt(6);
            linkVideo = JNIUtil.getServer(context, random);
            try {
                SharedPreferencesUtils.setString("img_link_" + questionVideo.getId(), AESCrypt.encrypt(JNIUtil.apiKey(context), linkVideo));
            } catch (Exception e) {

            }
        } else {
            if (!SharedPreferencesUtils.getString("img_link_" + questionVideo.getId()).isEmpty()) {
                try {
                    linkVideo = AESCrypt.decrypt(JNIUtil.apiKey(context), SharedPreferencesUtils.getString("img_link_" + questionVideo.getId()));
                } catch (Exception e) {

                }
            } else {
                int random = new Random().nextInt(6);
                linkVideo = JNIUtil.getServer(context, random);
                try {
                    SharedPreferencesUtils.setString("img_link_" + questionVideo.getId(), AESCrypt.encrypt(JNIUtil.apiKey(context), linkVideo));
                } catch (Exception e) {

                }
            }
        }
        return linkVideo.replace("DoneVideo/", "doneimg/") + "TH" + questionVideo.getId() + ".webp";
    }

    public static String getServerVideoVipDownload(Context context, int idVideo) {
        return JNIUtil.getServerVip(context).replace("DoneVideo/", "120cauzip/")  + "120cauzip/TH" + idVideo + ".zip";
    }

    public static String getServerHuongDan(Context context){
        return JNIUtil.getServerVip(context).replace("DoneVideo/", "120cauzip/")  + "120cauzip/guide.zip";
    }

    public static String getServerImg(Context context){
        return JNIUtil.getServerVip(context).replace("DoneVideo/", "doneimg.zip");
    }

    @NonNull
    public static String getETAString(@NonNull final Context context, final long etaInMilliSeconds) {
        if (etaInMilliSeconds < 0) {
            return "";
        }
        int seconds = (int) (etaInMilliSeconds / 1000);
        long hours = seconds / 3600;
        seconds -= hours * 3600;
        long minutes = seconds / 60;
        seconds -= minutes * 60;
        if (hours > 0) {
            return context.getString(R.string.download_eta_hrs, hours, minutes, seconds);
        } else if (minutes > 0) {
            return context.getString(R.string.download_eta_min, minutes, seconds);
        } else {
            return context.getString(R.string.download_eta_sec, seconds);
        }
    }

    @NonNull
    public static String getDownloadSpeedString(@NonNull final Context context, final long downloadedBytesPerSecond) {
        if (downloadedBytesPerSecond < 0) {
            return "";
        }
        double kb = (double) downloadedBytesPerSecond / (double) 1000;
        double mb = kb / (double) 1000;
        final DecimalFormat decimalFormat = new DecimalFormat(".##");
        if (mb >= 1) {
            return context.getString(R.string.download_speed_mb, decimalFormat.format(mb));
        } else if (kb >= 1) {
            return context.getString(R.string.download_speed_kb, decimalFormat.format(kb));
        } else {
            return context.getString(R.string.download_speed_bytes, downloadedBytesPerSecond);
        }
    }
}
