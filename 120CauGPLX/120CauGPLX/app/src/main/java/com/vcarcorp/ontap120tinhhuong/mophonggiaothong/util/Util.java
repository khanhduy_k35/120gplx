package com.vcarcorp.ontap120tinhhuong.mophonggiaothong.util;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.LayerDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;

import com.ads.control.admob.AdCloseListener;
import com.ads.control.admob.AdmobHelp;
import com.ads.control.utils.Utils;
import com.bumptech.glide.Glide;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.BuildConfig;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.R;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.database.GPLXDataManager;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.learnquestion.LearnQuestActivity;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.main.BaseActivity;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.model.QuestionCategory;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.model.QuestionVideo;
import com.ads.control.customview.CustomButton;
import com.ads.control.customview.CustomTextView;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;

import static android.util.TypedValue.COMPLEX_UNIT_DIP;
import static android.util.TypedValue.applyDimension;

import androidx.appcompat.app.AppCompatActivity;

public class Util {
	public static boolean isDisplayAds = true;
	public static final String FINISHGUIDE = "finishguide";

	public static float dp2px(Resources resources, float dp) {
		final float scale = resources.getDisplayMetrics().density;
		return  dp * scale + 0.5f;
	}

	public static float sp2px(Resources resources, float sp){
		final float scale = resources.getDisplayMetrics().scaledDensity;
		return sp * scale;
	}


	public static void setIsPro(Context mContext) {
		SharedPreferences sharedPref = mContext.getSharedPreferences(mContext.getString(R.string.share_preference_name), Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = sharedPref.edit();
		editor.putBoolean("isPro", true);
		editor.commit();
	}

	public static void setNoneIsPro(Context mContext) {
		SharedPreferences sharedPref = mContext.getSharedPreferences(mContext.getString(R.string.share_preference_name), Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = sharedPref.edit();
		editor.putBoolean("isPro", false);
		editor.commit();
	}

	public static String getVoiceCode(){
		return SharedPreferencesUtils.getString("voice_code","hn_female_ngochuyen_full_48k-fhg");
	}

	public static void setVoiceCode(String voiceCode){
		SharedPreferencesUtils.setString("voice_code",voiceCode);
	}


	public static boolean isPro(Context mContext) {
		if(mContext==null)
			return false;
		SharedPreferences sharedPref = mContext.getSharedPreferences(mContext.getString(R.string.share_preference_name), Context.MODE_PRIVATE);
		return sharedPref.getBoolean("isPro", false);
	}

	public static boolean checkConditionalShowAds(Context context){
		return Utils.checkConditionalShowAds(context);
	}

	public static void setIsRated(Context mContext) {
		SharedPreferences sharedPref = mContext.getSharedPreferences(mContext.getString(R.string.share_preference_name), Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = sharedPref.edit();
		editor.putBoolean("isRated", true);
		editor.commit();
	}

	public static boolean isRated(Context mContext) {
		SharedPreferences sharedPref = mContext.getSharedPreferences(mContext.getString(R.string.share_preference_name), Context.MODE_PRIVATE);
		return sharedPref.getBoolean("isRated", false);
	}

	public static int getWidthScreen(Context context) {
		return getDisplayMetrics(context).widthPixels;
	}

	private static DisplayMetrics getDisplayMetrics(Context context) {
		DisplayMetrics displayMetrics = new DisplayMetrics();
		WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
		wm.getDefaultDisplay().getMetrics(displayMetrics);
		return displayMetrics;
	}

	public static final String[] KHAUHIEU = {
			"Dừng đèn đỏ - chứng tỏ văn minh.",
			"Muốn nhanh thì phải từ từ - Đi thong thả cho đỡ vất vả.",
			"Xe không có thắng – chạy thẳng vào hòm.",
			"Miếng cồn khởi nguồn tai nạn.",
			"Đi đúng làn, thấy thật an nhàn.",
			"Ý thức là ở trong ta, chấp hành đội mũ mẹ cha an lòng.",
			"Đua xe đường phố là bố tử thần.",
			"Chạy nhanh thắng gấp, nằm sấp như chơi.",
			"Có văn hóa giao thông là sống vì cộng đồng.",
			"Đi đúng đường, nhường đúng lối, không cần vội, vui tới nhà.",
			"Nhường nhau không phải là hèn - Nhường nhau để khỏi lách, lèn, kẹt xe.",
			"Phía trước tay lái là sự sống. Hãy lái xe bằng cả trái tim.",
			"Ý thức là ở trong ta, chấp hành đội mũ mẹ cha an lòng",
			"Hãy kể cho tôi cách bạn tham gia giao thông, tôi sẽ nói cho bạn biết bạn là người thế nào.",
			"Nhường nhau không phải là hèn - Nhường nhau để khỏi lách, lèn, kẹt xe.",
			"An toàn giao thông – hạnh phúc của mọi người, mọi nhà.",
			"Trẻ em trên 6 tuổi phải đội mũ bảo hiểm đúng quy định khi tham gia giao thông bằng xe mô tô, xe gắn máy, xe đạp điện.",
			"Đội mũ bảo hiểm đúng quy định khi đi mô tô, xe gắn máy, xe đạp điện.",
			"An toàn giao thông là không tai nạn.",
			"Xi-nhan không phải là hâm, Xi-nhan để khỏi bị đâm vỡ đèn",
			"Sử dụng còi xe chính là thể hiện phong cách của chủ xe",
			"Tính mạng con người nằm trong cách chúng ta điều khiển phương tiện giao thông",
			"An toàn giao thông? Nói không với bia rượu",
			"Đội mũ bảo hiểm giảm được 80% tỷ lệ chấn thương sọ não.",
			"Hãy chung tay góp phần xây dựng một đất nước không có tai nạn giao thông",
			"Hãy thể hiện mình là người có văn hóa khi tham gia giao thông.",
			"Đèn đỏ qua rồi lại đến, sinh mạng không có lần hai",
			"Nhanh một phút, chậm cả đời.",
			"Vững bước tới trường, đi đúng phần đường là an toàn nhất",
			"Chậm lại vài giây, hơn gây tai nạn.",
			"Trẻ em như búp trên cành, giao thông đội mũ an lành cho con",
			"Thay đổi văn hóa giao thông - bắt đầu từ chính bạn.",
			"Đi trên đường nên nhường nhịn nhau.",
			"Đi đúng chiều gặp nhiều may mắn."
	};

	public static void copySQLite(String packageName){
		try {
			File sd = Environment.getExternalStorageDirectory();
			Log.e("encrypt","begin copy db");
			if (sd.canWrite()) {
				String currentDBPath = "/data/user/0/" + packageName + "/databases/" + GPLXDataManager.DATABASE_NAME;
				String backupDBPath = GPLXDataManager.DATABASE_NAME;
				File currentDB = new File(currentDBPath);
				File backupDB = new File(sd, backupDBPath);
				Log.e("encrypt","currentDBPath :" + currentDBPath);
				Log.e("encrypt","backupDBPath: " + backupDBPath);
				if (currentDB.exists()) {
					FileChannel src = new FileInputStream(currentDB).getChannel();
					FileChannel dst = new FileOutputStream(backupDB).getChannel();
					dst.transferFrom(src, 0, src.size());
					src.close();
					dst.close();
				}
			}
		} catch (Exception e) {

		}
	}

	public static int convertDpToPx(Context context, float dp) {
		return (int) applyDimension(COMPLEX_UNIT_DIP, dp, context.getResources().getDisplayMetrics());
	}

	public static void showDialogTipForQuestion(AppCompatActivity context, QuestionVideo questionVideo){
		Dialog dialog = new Dialog(context);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		Window window = dialog.getWindow();
		window.setBackgroundDrawableResource(android.R.color.transparent);
		dialog.setCanceledOnTouchOutside(true);
		dialog.setCancelable(true);
		dialog.setContentView(R.layout.layout_tip_question);
		dialog.getWindow().setGravity(Gravity.CENTER);
		if(AppUtils.checkPortraitMode(context)) {
			dialog.getWindow().setLayout(android.widget.LinearLayout.LayoutParams.MATCH_PARENT, android.widget.LinearLayout.LayoutParams.WRAP_CONTENT);
		}else{
			try{
				int width = AppUtils.getWidthScreen(context) < AppUtils.getHeightScreen(context) ? AppUtils.getHeightScreen(context) : AppUtils.getWidthScreen(context);
				width = width * 2/ 3;
				dialog.getWindow().setLayout(width, android.widget.LinearLayout.LayoutParams.WRAP_CONTENT);
			}catch (Exception e){
				dialog.getWindow().setLayout(android.widget.LinearLayout.LayoutParams.WRAP_CONTENT, android.widget.LinearLayout.LayoutParams.WRAP_CONTENT);
			}
		}

		ImageView imgDesAnswerDialog = dialog.findViewById(R.id.imgDesAnswer);
		if(imgDesAnswerDialog!=null) {
			File fileImgDetail = new File(context.getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS).toString() + "/img_app/doneimg/" + "TH" + questionVideo.getId() + ".webp");
			if(fileImgDetail.exists()){
				Uri imageUri = Uri.fromFile(fileImgDetail);
				Glide.with(context)
						.load(imageUri)
						.into(imgDesAnswerDialog);
			}else{
				Glide.with(context)
						.load(Uri.parse(AppUtils.getServerImg(context,questionVideo)))
						.into(imgDesAnswerDialog);
			}
		}

		final CustomTextView ln_done = (CustomTextView) dialog.findViewById(R.id.btn_ok);
		final CustomTextView ln_learn = (CustomTextView) dialog.findViewById(R.id.btn_learn);
		ln_done.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if(dialog!=null) {
					if (dialog.isShowing()) {
						dialog.dismiss();
						dialog.cancel();
					}
				}
			}
		});

		ln_learn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				AdmobHelp.getInstance().showInterstitialAd( new AdCloseListener() {
					@Override
					public void onAdClosed(boolean haveAds) {
						GPLXDataManager dataManager = new GPLXDataManager(context);
						QuestionCategory questionCategory = dataManager.getQuestionCategoryById(questionVideo.getCategoryID());
						Intent i = new Intent(context, LearnQuestActivity.class);
						Bundle bundle = new Bundle();
						bundle.putParcelable("question_category", questionCategory);
						bundle.putParcelable("question",questionVideo);
						i.putExtras(bundle);
						context.startActivity(i);
						if(dialog!=null) {
							if (dialog.isShowing()) {
								dialog.dismiss();
								dialog.cancel();
							}
						}
						if(haveAds) {
							((BaseActivity)context).checkShowSuggestProDisplayAds();
						}
					}
				});
			}
		});
		dialog.show();
	}

	public static void showTipForQuestion(AppCompatActivity context, QuestionVideo questionVideo,boolean isOrigin){
		Dialog dialog = new Dialog(context);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		Window window = dialog.getWindow();
		window.setBackgroundDrawableResource(android.R.color.transparent);
		dialog.setCanceledOnTouchOutside(true);
		dialog.setCancelable(true);
		dialog.setContentView(R.layout.layout_tip_question);
		dialog.getWindow().setGravity(Gravity.CENTER);
		if(AppUtils.checkPortraitMode(context)) {
			dialog.getWindow().setLayout(android.widget.LinearLayout.LayoutParams.MATCH_PARENT, android.widget.LinearLayout.LayoutParams.WRAP_CONTENT);
		}else{
			try{
				int width = AppUtils.getWidthScreen(context) < AppUtils.getHeightScreen(context) ? AppUtils.getHeightScreen(context) : AppUtils.getWidthScreen(context);
				width = width * 2/ 3;
				dialog.getWindow().setLayout(width, android.widget.LinearLayout.LayoutParams.WRAP_CONTENT);
			}catch (Exception e){
				dialog.getWindow().setLayout(android.widget.LinearLayout.LayoutParams.WRAP_CONTENT, android.widget.LinearLayout.LayoutParams.WRAP_CONTENT);
			}
		}

		LinearLayout ln_done = dialog.findViewById(R.id.ln_done);
		LinearLayout lnOKCancel = dialog.findViewById(R.id.lnOKCancel);
		ln_done.setVisibility(View.VISIBLE);
		lnOKCancel.setVisibility(View.INVISIBLE);
		ImageView imgDesAnswerDialog = dialog.findViewById(R.id.imgDesAnswer);
		String linkImage = AppUtils.getServerImg(context,questionVideo);

		if (isOrigin) {
			File fileImgDetail = new File(context.getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS).toString() + "/img_app/doneimgori/" + "THori" + questionVideo.getId() + ".webp");
			if (fileImgDetail.exists()) {
				Uri imageUri = Uri.fromFile(fileImgDetail);
				Glide.with(context)
						.load(imageUri)
						.into(imgDesAnswerDialog);
			} else {
				Glide.with(context)
						.load(Uri.parse(linkImage.replace("doneimg/TH", "doneimgori/THori")))
						.into(imgDesAnswerDialog);
			}
		} else {
			if(imgDesAnswerDialog!=null) {
				File fileImgDetail = new File(context.getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS).toString() + "/img_app/doneimg/" + "TH" + questionVideo.getId() + ".webp");
				if(fileImgDetail.exists()){
					Uri imageUri = Uri.fromFile(fileImgDetail);
					Glide.with(context)
							.load(imageUri)
							.into(imgDesAnswerDialog);
				}else{
					Glide.with(context)
							.load(Uri.parse(linkImage))
							.into(imgDesAnswerDialog);
				}
			}
		}

		final CustomTextView done = (CustomTextView) dialog.findViewById(R.id.btn_done);
		done.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if(dialog!=null) {
					if (dialog.isShowing()) {
						dialog.dismiss();
						dialog.cancel();
					}
				}
			}
		});
		dialog.show();
	}
}
