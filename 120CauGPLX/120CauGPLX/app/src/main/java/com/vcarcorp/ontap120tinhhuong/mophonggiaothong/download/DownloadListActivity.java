package com.vcarcorp.ontap120tinhhuong.mophonggiaothong.download;

import android.content.Context;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.webkit.URLUtil;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.SwitchCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.danikula.videocache.HttpProxyCacheServer;
import com.rey.material.widget.FrameLayout;
import com.rey.material.widget.RelativeLayout;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.App;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.R;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.main.BaseActivity;
import com.ads.control.customview.CustomTextView;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.util.DialogUtil;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.util.MyFileNameGenerator;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.util.UnzipUtility;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.util.Util;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.util.AppUtils;
import com.tonyodev.fetch2.AbstractFetchListener;
import com.tonyodev.fetch2.Download;
import com.tonyodev.fetch2.Error;
import com.tonyodev.fetch2.Fetch;
import com.tonyodev.fetch2.FetchConfiguration;
import com.tonyodev.fetch2.FetchListener;
import com.tonyodev.fetch2.NetworkType;
import com.tonyodev.fetch2.Request;
import com.tonyodev.fetch2.Status;
import com.tonyodev.fetch2core.Downloader;
import com.tonyodev.fetch2okhttp.OkHttpDownloader;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class DownloadListActivity extends BaseActivity implements ActionListener {
    private static final long UNKNOWN_REMAINING_TIME = -1;
    private static final long UNKNOWN_DOWNLOADED_BYTES_PER_SECOND = 0;
    private static final int GROUP_ID = "listGroupVipV3".hashCode();
    static final String FETCH_NAMESPACE = "DownloadListActivityVipV3";
    private FileAdapter fileAdapter;
    private Fetch fetch;
    private RelativeLayout beginUsing;
    private CustomTextView tvDownload;
    private boolean begindown = false;
    private android.widget.RelativeLayout rltRightContent;
    private RecyclerView recyclerView;
    private AppCompatImageView imLock;
    private CustomTextView tvStatus;
    private int countSuccess = 0;
    private Context mContext = null;
    private ArrayList<String> listDownloadCompleted;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = this;
        setContentView(R.layout.activity_download_list);
        listDownloadCompleted = new ArrayList<>();
        setUpViews();
        final FetchConfiguration fetchConfiguration = new FetchConfiguration.Builder(this)
                .setDownloadConcurrentLimit(3)
                .setHttpDownloader(new OkHttpDownloader(Downloader.FileDownloaderType.PARALLEL))
                .setNamespace(FETCH_NAMESPACE)
                .build();
        fetch = Fetch.Impl.getInstance(fetchConfiguration);
        enqueueDownloads();
        fileAdapter = new FileAdapter(this);
        fetch.getDownloadsInGroup(GROUP_ID, downloads -> {
            final ArrayList<Download> list = new ArrayList<>(downloads);
            Download downloadZipAudioGuide = null;
            Download downloadZipImg = null;
            int i = 0;
            for (Download download : list) {
                String file = URLUtil.guessFileName(download.getUrl(), null, null);
                if(file.contains("guide.zip")){
                    downloadZipAudioGuide = list.remove(i);
                    break;
                }
                i++;
            }
            i = 0;
            for (Download download : list) {
                String file = URLUtil.guessFileName(download.getUrl(), null, null);
                if(file.contains("doneimg.zip")){
                    downloadZipImg = list.remove(i);
                    break;
                }
                i++;
            }

            Collections.sort(list, new Comparator<Download>() {
                @Override
                public int compare(Download first, Download second) {
                    try {
                        String file1 = URLUtil.guessFileName(first.getUrl(), null, null);
                        String file2 = URLUtil.guessFileName(second.getUrl(), null, null);
                        int in1 = Integer.parseInt(file1.replace(".zip","").replace("TH",""));
                        int in2 = Integer.parseInt(file2.replace(".zip","").replace("TH",""));
                        if(in1==in2)
                            return 0;
                        else if(in1>in2)
                            return 1;
                        else
                            return -1;
                    }catch (Exception e){
                        return -1;
                    }
                }
            });

            list.add(0,downloadZipAudioGuide);
            list.add(1,downloadZipImg);

            for (Download download : list) {
                if (download.getUrl().endsWith("guide.zip")) {
                    File fileaudio1 = new File(getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS).toString() + "/" + new MyFileNameGenerator(this).generateFileName("hn_female_maiphuong_vdts_48k-fhg_score1.mp3"));
                    File fileaudio2 = new File(getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS).toString() + "/" + new MyFileNameGenerator(this).generateFileName("hn_female_ngochuyen_full_48k-fhg_score1.mp3"));
                    File fileaudio3 = new File(getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS).toString() + "/" + new MyFileNameGenerator(this).generateFileName("hn_male_manhdung_news_48k-fhg_score1.mp3"));
                    File fileaudio4 = new File(getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS).toString() + "/" + new MyFileNameGenerator(this).generateFileName("hn_male_phuthang_news65dt_44k-fhg_score1.mp3"));
                    File fileaudio5 = new File(getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS).toString() + "/" + new MyFileNameGenerator(this).generateFileName("hn_male_thanhlong_talk_48k-fhg_score1.mp3"));
                    File fileaudio6 = new File(getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS).toString() + "/" + new MyFileNameGenerator(this).generateFileName("hue_female_huonggiang_full_48k-fhg_score1.mp3"));
                    File fileaudio7 = new File(getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS).toString() + "/" + new MyFileNameGenerator(this).generateFileName("hue_male_duyphuong_full_48k-fhg_score1.mp3"));
                    File fileaudio8 = new File(getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS).toString() + "/" + new MyFileNameGenerator(this).generateFileName("sg_female_lantrinh_vdts_48k-fhg_score1.mp3"));
                    File fileaudio9 = new File(getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS).toString() + "/" + new MyFileNameGenerator(this).generateFileName("sg_female_thaotrinh_full_48k-fhg_score1.mp3"));
                    File fileaudio10 = new File(getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS).toString() + "/" + new MyFileNameGenerator(this).generateFileName("sg_male_minhhoang_full_48k-fhg_score1.mp3"));
                    File fileaudio11 = new File(getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS).toString() + "/" + new MyFileNameGenerator(this).generateFileName("sg_male_trungkien_vdts_48k-fhg_score1.mp3"));
                    if (fileaudio1.exists() && fileaudio2.exists() && fileaudio3.exists() && fileaudio4.exists() && fileaudio5.exists() && fileaudio6.exists()
                            && fileaudio7.exists() && fileaudio8.exists() && fileaudio9.exists() && fileaudio10.exists() && fileaudio11.exists()) {
                        fileAdapter.addDownload(download, true);
                        listDownloadCompleted.add(String.valueOf(download.getId()));
                    } else {
                        File file = new File(download.getFile());
                        if(file.exists()){
                            file.delete();
                        }
                        fileAdapter.addDownload(download, false);
                    }
                }
                if (download.getUrl().endsWith("doneimg.zip")) {
                    File fileImgDetail = new File(getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS).toString() + "/img_app/doneimg/" + "TH1.webp");
                    if (fileImgDetail.exists()) {
                        fileAdapter.addDownload(download, true);
                        listDownloadCompleted.add(String.valueOf(download.getId()));
                    } else {
                        File file = new File(download.getFile());
                        if(file.exists()){
                            file.delete();
                        }
                        fileAdapter.addDownload(download, false);
                    }
                }
                if (download.getUrl().contains("TH")) {
                    String fileNameTH = URLUtil.guessFileName(download.getUrl(), null, null);
                    File fileWebp = new File(getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS).toString() + "/" + new MyFileNameGenerator(this).generateFileName(fileNameTH.replace(".zip", ".webp")));
                    if (fileWebp.exists()) {
                        fileAdapter.addDownload(download, true);
                        listDownloadCompleted.add(String.valueOf(download.getId()));
                        countSuccess++;
                    } else {
                        File file = new File(download.getFile());
                        if(file.exists()){
                            file.delete();
                        }
                        fileAdapter.addDownload(download, false);
                    }
                }
                if (download.getStatus() == Status.COMPLETED) {

                }
            }
            tvStatus.setText((countSuccess > 120 ? 120 : countSuccess)  + "/120 bài học");
            recyclerView = findViewById(R.id.list);
            recyclerView.setLayoutManager(new LinearLayoutManager(this));

            recyclerView.setAdapter(fileAdapter);
        }).addListener(fetchListener);
        fetch.pauseAll();
        if (savedInstanceState != null) {
            begindown = savedInstanceState.getBoolean("begindown");
            if(begindown) {
                begindown = true;
                fetch.resumeAll();
                if(listDownloadCompleted != null && listDownloadCompleted.size() > 0){
                    for(String downloadID : listDownloadCompleted){
                        fetch.pause(Integer.valueOf(downloadID));
                    }
                }
                tvDownload.setText("Dừng tải tất cả bài học");
            }else{
                begindown = false;
                fetch.pauseAll();
                tvDownload.setText("Tải lần lượt tất cả bài học");
            }
        }
    }

    private void setUpViews() {
        setUpToolBar((FrameLayout)findViewById(R.id.btn_back));
        beginUsing = findViewById(R.id.beginUsing);
        tvStatus = findViewById(R.id.tvStatus);
        rltRightContent = findViewById(R.id.rltRightContent);
        findViewById(R.id.btn_back).setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                onBackPressed();
            }
        });
        tvDownload = findViewById(R.id.tvDownload);
        imLock = findViewById(R.id.imLock);
        if(Util.isPro(this)){
            imLock.setVisibility(View.GONE);
        }else{
            imLock.setVisibility(View.VISIBLE);
        }
        final SwitchCompat networkSwitch = findViewById(R.id.networkSwitch);
        networkSwitch.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                fetch.setGlobalNetworkType(NetworkType.WIFI_ONLY);
            } else {
                fetch.setGlobalNetworkType(NetworkType.ALL);
            }
        });
        beginUsing.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Util.isPro(DownloadListActivity.this)) {
                    if (!begindown) {
                        begindown = true;
                        fetch.resumeAll();
                        tvDownload.setText("Dừng tải tất cả bài học");
                        if(listDownloadCompleted != null && listDownloadCompleted.size() > 0){
                            for(String downloadID : listDownloadCompleted){
                                fetch.pause(Integer.valueOf(downloadID));
                            }
                        }
                    } else {
                        begindown = false;
                        fetch.pauseAll();
                        tvDownload.setText("Tải lần lượt tất cả bài học");
                    }
                }else{
                    new DialogUtil.Builder(DownloadListActivity.this)
                            .title("Tải video học ngoại tuyến")
                            .content("Bạn cần nâng cấp để sử dụng tính năng này và nhiều tính năng cao cấp khác.")
                            .doneText(getString(R.string.text_ok))
                            .onDone(new DialogUtil.SingleButtonCallback() {
                                @Override
                                public void onClick() {
                                    buyPro();
                                }
                            })
                            .show();
                }
            }
        });
        if(!AppUtils.checkPortraitMode(this)){
            if(rltRightContent!=null) {
                android.widget.RelativeLayout.LayoutParams layoutParams = (android.widget.RelativeLayout.LayoutParams) rltRightContent.getLayoutParams();
                final int width = AppUtils.getWidthScreen(DownloadListActivity.this) > AppUtils.getHeightScreen(DownloadListActivity.this) ? AppUtils.getHeightScreen(DownloadListActivity.this) : AppUtils.getWidthScreen(DownloadListActivity.this);
                rltRightContent.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        layoutParams.width = width;
                        rltRightContent.setLayoutParams(layoutParams);
                        rltRightContent.invalidate();
                        rltRightContent.getParent().requestLayout();
                    }
                }, 10);
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if(begindown) {
            fetch.resumeAll();
            if(listDownloadCompleted != null && listDownloadCompleted.size() > 0){
                for(String downloadID : listDownloadCompleted){
                    fetch.pause(Integer.valueOf(downloadID));
                }
            }
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        fetch.pauseAll();
    }

    @Override
    protected void onDestroy() {
        mContext = null;
        super.onDestroy();
        fetch.close();
    }

    private final FetchListener fetchListener = new AbstractFetchListener() {
        @Override
        public void onAdded(@NotNull Download download) {
            fileAdapter.addDownload(download);
        }

        @Override
        public void onQueued(@NotNull Download download, boolean waitingOnNetwork) {
            int pos = fileAdapter.update(download, UNKNOWN_REMAINING_TIME, UNKNOWN_DOWNLOADED_BYTES_PER_SECOND);
            fileAdapter.notifyItemChanged(pos);
        }

        @Override
        public void onCompleted(@NotNull Download download) {
            if(mContext == null){
                return;
            }
            String url = "";
            if (download != null) {
                url = download.getUrl();
            }
            final Status status = download.getStatus();
            if (download.getUrl().contains("TH")) {
                countSuccess++;
            }
            tvStatus.setText((countSuccess > 120 ? 120 : countSuccess)  + "/120 bài học");
            int pos = fileAdapter.update(download, UNKNOWN_REMAINING_TIME, UNKNOWN_DOWNLOADED_BYTES_PER_SECOND);
            fileAdapter.notifyItemChanged(pos);
            if(mContext != null) {
                String filePath = download.getFile();
                HttpProxyCacheServer cacheServer = App.getCacheServer(mContext);
                String fileImg = cacheServer.getCacheFileString(AppUtils.getServerImg(mContext));
                String fileGuide = cacheServer.getCacheFileString(AppUtils.getServerHuongDan(mContext));
                if(filePath.contains(fileImg)){
                    String dest = getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS).toString() + "/img_app";
                    try {
                        UnzipUtility.unzip(filePath, dest);
                    } catch (Exception e) {

                    }
                }else if(filePath.contains(fileGuide)){
                    try {
                        UnzipUtility.unzipCache(mContext,filePath, getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS).toString());
                    } catch (Exception e) {

                    }
                }else{
                    try {
                        UnzipUtility.unzipCache(mContext,filePath, getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS).toString());
                    } catch (Exception e) {

                    }
                }
            }
            listDownloadCompleted.add(String.valueOf(download.getId()));
        }

        @Override
        public void onError(@NotNull Download download, @NotNull Error error, @Nullable Throwable throwable) {
            super.onError(download, error, throwable);
            int pos = fileAdapter.update(download, UNKNOWN_REMAINING_TIME, UNKNOWN_DOWNLOADED_BYTES_PER_SECOND);
            fileAdapter.notifyItemChanged(pos);
        }

        @Override
        public void onProgress(@NotNull Download download, long etaInMilliseconds, long downloadedBytesPerSecond) {
            int pos = fileAdapter.update(download, etaInMilliseconds, downloadedBytesPerSecond);
            fileAdapter.notifyItemChanged(pos);
        }

        @Override
        public void onPaused(@NotNull Download download) {
            int pos =  fileAdapter.update(download, UNKNOWN_REMAINING_TIME, UNKNOWN_DOWNLOADED_BYTES_PER_SECOND);
            fileAdapter.notifyItemChanged(pos);
        }

        @Override
        public void onResumed(@NotNull Download download) {
            int pos = fileAdapter.update(download, UNKNOWN_REMAINING_TIME, UNKNOWN_DOWNLOADED_BYTES_PER_SECOND);
            fileAdapter.notifyItemChanged(pos);
        }

        @Override
        public void onCancelled(@NotNull Download download) {
            int pos =  fileAdapter.update(download, UNKNOWN_REMAINING_TIME, UNKNOWN_DOWNLOADED_BYTES_PER_SECOND);
            fileAdapter.notifyItemChanged(pos);
        }

        @Override
        public void onRemoved(@NotNull Download download) {
            int pos = fileAdapter.update(download, UNKNOWN_REMAINING_TIME, UNKNOWN_DOWNLOADED_BYTES_PER_SECOND);
            fileAdapter.notifyItemChanged(pos);
        }

        @Override
        public void onDeleted(@NotNull Download download) {
            int pos = fileAdapter.update(download, UNKNOWN_REMAINING_TIME, UNKNOWN_DOWNLOADED_BYTES_PER_SECOND);
            fileAdapter.notifyItemChanged(pos);
        }
    };

    private void enqueueDownloads() {
        final List<Request> requests = Data.getFetchRequestWithGroupId(GROUP_ID, this);

        fetch.enqueue(requests, updatedRequests -> {
        });

    }

    @Override
    public void onPauseDownload(int id) {
        fetch.pause(id);
    }

    @Override
    public void onResumeDownload(int id) {
        fetch.resume(id);
    }

    @Override
    public void onRemoveDownload(int id) {
        fetch.remove(id);
    }

    @Override
    public void onRetryDownload(int id) {
        fetch.retry(id);
    }

    @Override
    public void onBackPressed() {
        if(countSuccess < 120){
            new DialogUtil.Builder(DownloadListActivity.this)
                    .title("Bạn chưa tải hết bài học")
                    .content("Bạn mới tải được " + countSuccess + "/120 bài học. Bạn cần tải tất cả bài học để có thể học ngoại tuyến mọi lúc mọi nơi.")
                    .negativeText("Tôi sẽ tải sau")
                    .positiveText("Tiếp tục tải")
                    .onNegative(new DialogUtil.SingleButtonCallback() {
                        @Override
                        public void onClick() {
                            DownloadListActivity.super.onBackPressed();
                        }
                    })
                    .show();
        }else{
            super.onBackPressed();
        }
    }


    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean("begindown",begindown);
    }
}