package com.vcarcorp.ontap120tinhhuong.mophonggiaothong.util;

import android.content.Context;
import android.util.AttributeSet;

import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.R;
import com.rey.material.widget.RelativeLayout;

public class RelativeButton extends RelativeLayout {

    public RelativeButton(Context context) {
        super(context);
    }

    public RelativeButton(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public RelativeButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public void setEnabled(boolean enabled) {
        super.setEnabled(enabled);
        if (AppUtils.checkPortraitMode(getContext())) {
            if (enabled) {
                setBackgroundDrawable(getContext().getResources().getDrawable(R.drawable.background_button_learning));
            } else {
                setBackgroundDrawable(getContext().getResources().getDrawable(R.drawable.background_button_learning_disable));
            }
        }
    }
}
