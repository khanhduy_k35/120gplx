package com.vcarcorp.ontap120tinhhuong.mophonggiaothong.main;

import android.annotation.SuppressLint;
import android.app.Application;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.RelativeLayout;

import com.ads.control.AdsApplication;
import com.ads.control.activity.SplashBaseActivity;
import com.ads.control.admob.AdsOpenManager;
import com.airbnb.lottie.LottieAnimationView;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.App;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.R;
import com.ads.control.customview.CustomTextView;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.util.SharedPreferencesUtils;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.util.Util;

public class SplashActivity extends SplashBaseActivity {
    private LottieAnimationView icon_app;
    private RelativeLayout image_icon;
    private CustomTextView app_name;
    private Animation d;
    private Animation e;
    private Animation f;
    private int actionApp = -1;
    @SuppressLint("SourceLockedOrientationActivity")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getIntent() != null && getIntent().getAction()!=null && getIntent().getAction().equals("write_diary")) {
            try{
                actionApp = getIntent().getExtras().getInt("ACTION_APP", -1);
            }catch (Exception e){

            }
        }
        setContentView(R.layout.activity_splash_view);
        this.icon_app = (LottieAnimationView)findViewById(R.id.icon_app);
        this.image_icon = ((RelativeLayout) findViewById(R.id.image_icon));
        this.app_name = ((CustomTextView) findViewById(R.id.app_name));
        onCreateSplash();
    }

    @Override
    public void animSplash() {
        initAnimation();
    }

    private void initAnimation() {
        this.d = AnimationUtils.loadAnimation(this, R.anim.welcome_fade_in);
        this.d.setDuration(1500);
        this.e = AnimationUtils.loadAnimation(this, R.anim.fade_in_from_bottom);
        this.e.setDuration(1500);
        this.f = AnimationUtils.loadAnimation(this, R.anim.fade_in_from_bottom);
        this.f.setDuration(1000L);
        this.icon_app.startAnimation(this.d);
        this.d.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                icon_app.setVisibility(View.VISIBLE);
                image_icon.startAnimation(e);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        this.e.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                image_icon.setVisibility(View.VISIBLE);
                app_name.startAnimation(f);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        this.f.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                app_name.setVisibility(View.VISIBLE);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (SharedPreferencesUtils.getBoolean(Util.FINISHGUIDE, false)) {
                            Application application = getApplication();

                            // If the application is not an instance of MyApplication, log an error message and
                            // start the MainActivity without showing the app open ad.
                            if (!(application instanceof App)) {
                                startMainActivity();
                                return;
                            }
                            // Show the app open ad.
                            AdsApplication.getInstance().appOpenManager.showAdIfAvailable(SplashActivity.this, new AdsOpenManager.OnShowAdCompleteListener() {
                                @Override
                                public void onFailed() {
                                    startMainActivity();
                                }

                                @Override
                                public void onSuccess() {
                                    startMainActivity();
                                }

                                @Override
                                public void onShow() {

                                }
                            });

                        }else{
                            Intent intent = new Intent(SplashActivity.this, GuideLineActivity.class);
                            SplashActivity.this.startActivity(intent);
                            SplashActivity.this.finish();
                        }
                    }
                },1000);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    public void startMainActivity(){
        Intent intent = new Intent(SplashActivity.this, MainActivity.class);
        if (actionApp != -1){
            intent.putExtra("ACTION_APP", actionApp);
        }
        SplashActivity.this.startActivity(intent);
        SplashActivity.this.finish();
    }
}
