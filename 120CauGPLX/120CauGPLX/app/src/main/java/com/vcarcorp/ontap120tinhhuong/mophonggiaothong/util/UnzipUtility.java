package com.vcarcorp.ontap120tinhhuong.mophonggiaothong.util;

import android.content.Context;
import android.util.Log;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class UnzipUtility {
    /**
     * Size of the buffer to read/write data
     */
    private static final int BUFFER_SIZE = 4096;
    /**
     * Extracts a zip file specified by the zipFilePath to a directory specified by
     * destDirectory (will be created if does not exists)
     * @param zipFilePath
     * @param destDirectory
     * @throws IOException
     */
    public static void unzip(String zipFilePath, String destDirectory) throws IOException {
        File destDir = new File(destDirectory);
        if (!destDir.exists()) {
            destDir.mkdir();
        }
        ZipInputStream zipIn = new ZipInputStream(new FileInputStream(zipFilePath));
        ZipEntry entry = zipIn.getNextEntry();
        // iterates over entries in the zip file
        while (entry != null) {
            String filePath = destDirectory + File.separator + entry.getName();
            if (!entry.isDirectory()) {
                File outputFile = new File(destDirectory, entry.getName());
                try {
                    ensureZipPathSafety(outputFile, destDirectory);
                } catch (Exception e) {
                    e.printStackTrace();
                    return;
                }
                if(outputFile.exists()){
                    outputFile.delete();
                }
                extractFile(zipIn, filePath);
            } else {
                // if the entry is a directory, make the directory
                File dir = new File(filePath);
                dir.mkdirs();
            }
            zipIn.closeEntry();
            entry = zipIn.getNextEntry();
        }
        zipIn.close();
        File zipPath = new File(zipFilePath);
        try {
            zipPath.delete();
        }catch (Exception e){

        }
    }

    public static void unzipCache(Context context, String zipFilePath, String destDirectory) throws IOException {
        File destDir = new File(destDirectory);
        if (!destDir.exists()) {
            destDir.mkdir();
        }
        ZipInputStream zipIn = new ZipInputStream(new FileInputStream(zipFilePath));
        ZipEntry entry = zipIn.getNextEntry();
        while (entry != null) {
            String fileNameCache = new MyFileNameGenerator(context).generateFileName(entry.getName());
            String filePath = destDirectory + File.separator + fileNameCache;
            if (!entry.isDirectory()) {
                File outputFile = new File(destDirectory, fileNameCache);
                try {
                    ensureZipPathSafety(outputFile, destDirectory);
                } catch (Exception e) {
                    e.printStackTrace();
                    return;
                }
                if(outputFile.exists()){
                    outputFile.delete();
                }
                Log.e("khanhduy.le","unzip : " + entry.getName());
                extractFile(zipIn, filePath);
            } else {
                File dir = new File(filePath);
                dir.mkdirs();
            }
            zipIn.closeEntry();
            entry = zipIn.getNextEntry();
        }
        zipIn.close();
        File zipPath = new File(zipFilePath);
        try {
            zipPath.delete();
        }catch (Exception e){

        }
    }
    /**
     * Extracts a zip entry (file entry)
     * @param zipIn
     * @param filePath
     * @throws IOException
     */
    public static void extractFile(ZipInputStream zipIn, String filePath) throws IOException {
        BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(filePath));
        byte[] bytesIn = new byte[BUFFER_SIZE];
        int read = 0;
        while ((read = zipIn.read(bytesIn)) != -1) {
            bos.write(bytesIn, 0, read);
        }
        bos.close();
    }

    public static void ensureZipPathSafety(final File outputFile, final String destDirectory) throws Exception {
        String destDirCanonicalPath = (new File(destDirectory)).getCanonicalPath();
        String outputFilecanonicalPath = outputFile.getCanonicalPath();
        if (!outputFilecanonicalPath.startsWith(destDirCanonicalPath)) {
            Log.e("khanhduy.le","loi o day");
            throw new Exception(String.format("Found Zip Path Traversal Vulnerability with %s", outputFilecanonicalPath));
        }
    }
}
