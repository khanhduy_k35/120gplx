package com.vcarcorp.ontap120tinhhuong.mophonggiaothong.database;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.model.TestCenter;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.model.TestCenterGroup;

import java.util.ArrayList;

public class DataManagerTestCenter extends SQLiteAssetHelper {

    private static final String DATABASE_NAME = "trungtam.db";
    private static final int DATABASE_VERSION = 1;

    public DataManagerTestCenter(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public ArrayList<TestCenterGroup> getListTestCenter(){
        ArrayList<TestCenter> listResult = new ArrayList<TestCenter>();
        SQLiteDatabase localSQLiteDatabase = getWritableDatabase();
        String query = "select * from TrungTamDaoTao where type = 1 order by Tinh asc";
        Cursor cursor = localSQLiteDatabase.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            do {
                TestCenter signsCategory = new TestCenter();
                signsCategory.setProvince(cursor.getString(0));
                signsCategory.setName(cursor.getString(1));
                signsCategory.setAddress(cursor.getString(2));
                signsCategory.setPhoneNumber(cursor.getString(3));
                signsCategory.setLatAddress(cursor.getString(4));
                signsCategory.setLongAddress(cursor.getString(5));
                signsCategory.setImage(cursor.getString(6));
                signsCategory.setType(cursor.getInt(7));
                signsCategory.setNote(cursor.getString(8));
                listResult.add(signsCategory);
            } while (cursor.moveToNext());
        }
        try {
            cursor.close();
        } catch (Exception e) {

        }
        try {
            localSQLiteDatabase.close();
        }catch (Exception e) {

        }

        ArrayList<TestCenterGroup> testCenterGroupArrayList = new ArrayList<TestCenterGroup>();
        String nameTemp = "";
        TestCenterGroup temp = new TestCenterGroup();
        for (TestCenter testCenter: listResult) {
            if (!nameTemp.equals(testCenter.getProvince())) {
                if (!nameTemp.equals("")) {
                    testCenterGroupArrayList.add(temp);
                }
                nameTemp = testCenter.getProvince();
                temp = new TestCenterGroup();
                temp.getTestCenters().add(testCenter);
                temp.setName(testCenter.getProvince());
            } else {
                temp.getTestCenters().add(testCenter);
            }
        }
        testCenterGroupArrayList.add(temp);
        return testCenterGroupArrayList;
    }

    public ArrayList<TestCenterGroup> getListTrainingGround(){
        ArrayList<TestCenter> listResult = new ArrayList<TestCenter>();
        SQLiteDatabase localSQLiteDatabase = getWritableDatabase();
        String query = "select * from TrungTamDaoTao where type = 3 order by Tinh asc";
        Cursor cursor = localSQLiteDatabase.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            do {
                TestCenter signsCategory = new TestCenter();
                signsCategory.setProvince(cursor.getString(0));
                signsCategory.setName(cursor.getString(1));
                signsCategory.setAddress(cursor.getString(2));
                signsCategory.setPhoneNumber(cursor.getString(3));
                signsCategory.setLatAddress(cursor.getString(4));
                signsCategory.setLongAddress(cursor.getString(5));
                signsCategory.setImage(cursor.getString(6));
                signsCategory.setType(cursor.getInt(7));
                signsCategory.setNote(cursor.getString(8));
                listResult.add(signsCategory);
            } while (cursor.moveToNext());
        }
        try {
            cursor.close();
        } catch (Exception e) {

        }
        try {
            localSQLiteDatabase.close();
        }catch (Exception e) {

        }

        ArrayList<TestCenterGroup> testCenterGroupArrayList = new ArrayList<TestCenterGroup>();
        String nameTemp = "";
        TestCenterGroup temp = new TestCenterGroup();
        for (TestCenter testCenter: listResult) {
            if (!nameTemp.equals(testCenter.getProvince())) {
                if (!nameTemp.equals("")) {
                    testCenterGroupArrayList.add(temp);
                }
                nameTemp = testCenter.getProvince();
                temp = new TestCenterGroup();
                temp.getTestCenters().add(testCenter);
                temp.setName(testCenter.getProvince());
            } else {
                temp.getTestCenters().add(testCenter);
            }
        }
        testCenterGroupArrayList.add(temp);
        return testCenterGroupArrayList;
    }

}
