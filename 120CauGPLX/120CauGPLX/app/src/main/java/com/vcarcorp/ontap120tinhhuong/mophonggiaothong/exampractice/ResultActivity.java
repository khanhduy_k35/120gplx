package com.vcarcorp.ontap120tinhhuong.mophonggiaothong.exampractice;

import android.animation.Animator;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;

import com.ads.control.admob.AdCloseListener;
import com.ads.control.admob.AdmobHelp;
import com.airbnb.lottie.LottieAnimationView;
import com.airbnb.lottie.LottieDrawable;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.R;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.database.GPLXDataManager;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.main.BaseActivity;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.model.Test;
import com.ads.control.customview.CustomButton;
import com.ads.control.customview.CustomTextView;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.util.SharedPreferencesUtils;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.util.Util;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.util.AppUtils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class ResultActivity extends BaseActivity {
    private LottieAnimationView animStatus,ltAVfireWorlk;
    private CustomButton btnReview;
    private CustomButton btnRestart;
    private CustomButton btnNext;
    private boolean isSuccess;
    private CustomTextView tvCongratulations,sttTest;
    private Test test;
    private CustomTextView tvNumberQuest,tvPassingScore,tvPercentPass;
    private GPLXDataManager gplxDataManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);
        gplxDataManager = new GPLXDataManager(this);
        initValue();
        initIdView();
        initView();
        initActionView();
        if(!AppUtils.checkPortraitMode(this)){
            RelativeLayout buttonCuttingDiet = findViewById(R.id.rltRight);
            android.widget.RelativeLayout.LayoutParams layoutParams = (android.widget.RelativeLayout.LayoutParams) buttonCuttingDiet.getLayoutParams();
            final int width = AppUtils.getWidthScreen(ResultActivity.this) > AppUtils.getHeightScreen(ResultActivity.this) ? AppUtils.getHeightScreen(ResultActivity.this) : AppUtils.getWidthScreen(ResultActivity.this);
            buttonCuttingDiet.postDelayed(new Runnable() {
                @Override
                public void run() {
                    layoutParams.width = width;
                    buttonCuttingDiet.setLayoutParams(layoutParams);
                    buttonCuttingDiet.invalidate();
                    buttonCuttingDiet.getParent().requestLayout();
                }
            },10);
        }
        if(!Util.isPro(this) && Util.isDisplayAds && Util.checkConditionalShowAds(this)) {
            if (findViewById(R.id.container_ads_native) != null)
                AdmobHelp.getInstance().loadNativeActivity(ResultActivity.this,true);
        }
        else {
            if (findViewById(R.id.container_ads_native) != null)
                findViewById(R.id.container_ads_native).setVisibility(View.GONE);
        }
    }

    private void initValue() {
        test = (Test) getIntent().getExtras().getParcelable("test");
        if(test.isFinish() && test.getTotalScore() >= 35){
            isSuccess = true;
        }else{
            isSuccess = false;
        }
    }

    private void initIdView() {
        animStatus = findViewById(R.id.animStatus);
        btnReview = findViewById(R.id.btnReview);
        btnRestart = findViewById(R.id.btnRestart);
        btnNext = findViewById(R.id.btnNext);
        ltAVfireWorlk = findViewById(R.id.ltAVfireWorlk);
        tvCongratulations = findViewById(R.id.tvCongratulations);
        sttTest = findViewById(R.id.sttTest);
        tvNumberQuest = findViewById(R.id.tvNumberQuest);
        tvPassingScore = findViewById(R.id.tvPassingScore);;
        tvPercentPass = findViewById(R.id.tvPercentPass);;
    }

    private void initView() {
        tvNumberQuest.setText("10" + "");
        tvPassingScore.setText(gplxDataManager.countTotalQuestionCorrect(test)+"");
        tvPercentPass.setText((10 - gplxDataManager.countTotalQuestionCorrect(test)) + "");
        if(isSuccess){
            tvCongratulations.setText(getResources().getString(R.string.test_congratulations));
            tvCongratulations.setTextColor(getColor(R.color.colorPrimaryDark));
            animStatus.setAnimation(R.raw.success_test);
            animStatus.setVisibility(View.VISIBLE);
            animStatus.setSpeed(1.5f);
            animStatus.setRepeatCount(0);
            animStatus.playAnimation();
            sttTest.setText(String.format(getString(R.string.test_result_complete),test.getIdTest() + "",test.getTotalScore()));
            animStatus.addAnimatorListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animation) {

                }

                @Override
                public void onAnimationEnd(Animator animation) {
                    ltAVfireWorlk.setSpeed(1.0f);
                    ltAVfireWorlk.setRepeatCount(LottieDrawable.INFINITE);
                    ltAVfireWorlk.playAnimation();
                }

                @Override
                public void onAnimationCancel(Animator animation) {

                }

                @Override
                public void onAnimationRepeat(Animator animation) {

                }
            });
        }else{
            try{
                Date c = Calendar.getInstance().getTime();
                System.out.println("Current time => " + c);

                SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
                String currentDate = df.format(c);
                String preDate = SharedPreferencesUtils.getString("current_date_pro") == null ? "" : SharedPreferencesUtils.getString("current_date_pro");
                if(!preDate.equals(currentDate)){
                    SharedPreferencesUtils.setString("current_date_pro",currentDate);
                    //if(!Util.isPro(this))
                        //buyPro();
                }
            }catch (Exception e){

            }

            tvCongratulations.setText("Hãy cố gắng thêm 1 chút");
            tvCongratulations.setTextColor(getColor(R.color.colorRedPrimary));
            animStatus.setAnimation(R.raw.failed_test);
            animStatus.setVisibility(View.VISIBLE);
            animStatus.setSpeed(1.0f);
            animStatus.setRepeatCount(LottieDrawable.INFINITE);
            animStatus.playAnimation();
            sttTest.setText(String.format(getString(R.string.test_result_failed),"" + test.getIdTest(),test.getTotalScore()));
        }
    }

    private void initActionView() {
        btnReview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent data = new Intent();
                data.putExtra("action","review");
                Bundle bundle = new Bundle();
                bundle.putParcelable("test", test);
                data.putExtras(bundle);
                setResult(Activity.RESULT_OK, data);
                finish();
                checkShowSuggestProDisplayAds();
            }
        });
        btnRestart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent data = new Intent();
                data.putExtra("action","restart");
                Bundle bundle = new Bundle();
                bundle.putParcelable("test", test);
                data.putExtras(bundle);
                setResult(Activity.RESULT_OK, data);
                finish();
                checkShowSuggestProDisplayAds();
            }
        });
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent data = new Intent();
                data.putExtra("action","next");
                Bundle bundle = new Bundle();
                bundle.putParcelable("test", test);
                data.putExtras(bundle);
                setResult(Activity.RESULT_OK, data);
                finish();
                checkShowSuggestProDisplayAds();
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            gplxDataManager.close();
        }catch (Exception e){

        }
    }
}
