package com.vcarcorp.ontap120tinhhuong.mophonggiaothong.exampractice;

import static com.google.android.exoplayer2.ExoPlayer.STATE_BUFFERING;
import static com.google.android.exoplayer2.ExoPlayer.STATE_ENDED;
import static com.google.android.exoplayer2.ExoPlayer.STATE_READY;

import android.app.Dialog;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.SwitchCompat;
import androidx.fragment.app.FragmentActivity;

import com.ads.control.sound.SoundPoolManager;
import com.airbnb.lottie.LottieAnimationView;
import com.bumptech.glide.Glide;
import com.danikula.videocache.CacheListener;
import com.danikula.videocache.HttpProxyCacheServer;
import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.LoadControl;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.extractor.ExtractorsFactory;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.AdaptiveVideoTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.ui.SimpleExoPlayerView;
import com.google.android.exoplayer2.upstream.BandwidthMeter;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.R;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.util.RelativeButton;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.App;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.database.GPLXDataManager;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.fragment.BaseFragment;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.main.BaseActivity;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.model.Test;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.model.TestQuest;
import com.ads.control.customview.CustomTextView;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.util.DialogUtil;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.util.customview.ImageViewButton;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.util.customview.LockableScrollView;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.util.OnSingleClickListener;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.util.SharedPreferencesUtils;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.util.Util;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.util.AppUtils;

import java.io.File;

import me.everything.android.ui.overscroll.OverScrollDecoratorHelper;

/**
 * Created by duy on 10/23/17.
 */

public class ExamQuestFragment extends BaseFragment {

    private TestQuest testQuest;
    private LockableScrollView scrollView;
    private int question_index;
    private RelativeLayout resultItemBound;
    private RelativeLayout resultItem;
    private com.rey.material.widget.RelativeLayout rltSeekBarThumbBound;
    private RelativeLayout rltSeekBarThumb;
    private RelativeLayout rltSeekBarParrent;
    private RelativeLayout rltSeekBarBuffer;
    private RelativeLayout rltSeekBarCurrent;

    private RelativeButton rltPlay, rltPause, rltNext;
    private com.rey.material.widget.RelativeLayout rltGuide;
    private ImageViewButton imgPlay, imgPause, imgNext;
    private CustomTextView tvGuideStart;
    private com.rey.material.widget.RelativeLayout rltSpace;
    private ImageView imgFlag;
    private CustomTextView tvResultScore;
    private LinearLayout lnResultEx;
    private CustomTextView textDesAnswer;
    private ImageView imgDesAnswer, imgDesAnswerOrigin;
    private LinearLayout rltExplaintion;
    private TextView textDesExplaintion;
    private RelativeLayout rltFullScreen;
    private RelativeLayout rltFulExitScreen;
    private RelativeLayout lnSwitch;
    private SwitchCompat aSwitch;
    private CustomTextView tvScoreSwitch;
    private RelativeLayout container_ads_native;
    private Test test;

    private SimpleExoPlayer player;
    private SimpleExoPlayerView simpleExoPlayerView;
    private RelativeLayout loadingView;
    private RelativeLayout rltError;
    private LottieAnimationView animFailed;
    private SeekDispatcher seekDispatcher;
    private boolean dragging;
    private String proxyVideoUrl;
    long position = 0L;

    private int _xDelta;
    private int withThumbSeekBar;
    private int widthResult;
    private int with16dp;
    private int screenWidth;
    private boolean isPlayingVideo = false;
    private boolean isStopVideo;
    private long currentPosition;
    private boolean isPlayFromCache = false;

    public static ExamQuestFragment init(TestQuest testQuest, Test test, int question_index) {
        ExamQuestFragment truitonFrag = new ExamQuestFragment();
        Bundle mBundle = new Bundle();
        mBundle.putParcelable("test_quest", testQuest);
        mBundle.putParcelable("test", test);
        mBundle.putInt("question_index", question_index);
        truitonFrag.setArguments(mBundle);
        return truitonFrag;
    }

    @Override
    public void stopAudioAnimation() {
        if (getActivity() != null) {
            ((ExamQuestActivity) getActivity()).stopAudioAnimation();
        }
    }

    @Override
    public void startAudioAnimation() {
        if (getActivity() != null) {
            ((ExamQuestActivity) getActivity()).startAudioAnimation();
        }
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        testQuest = (TestQuest) getArguments().getParcelable("test_quest");
        question_index = getArguments().getInt("question_index");
        test = (Test) getArguments().getParcelable("test");
        if (savedInstanceState != null) {
            isPlayingVideo = savedInstanceState.getBoolean("isPlayingVideo");
            currentPosition = savedInstanceState.getLong("currentPosition");
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        isCreated = true;
        withThumbSeekBar = AppUtils.convertDpToPx(getActivity(), 4);
        widthResult = AppUtils.getWidthScreen(getActivity()) - AppUtils.convertDpToPx(getActivity(), 16) * 2 - withThumbSeekBar;
        View view = inflater.inflate(R.layout.fragment_question_test, container, false);
        scrollView = (LockableScrollView) view.findViewById(R.id.scrollView);
        simpleExoPlayerView = (SimpleExoPlayerView) view.findViewById(R.id.videoView);
        seekDispatcher = (SeekDispatcher) DEFAULT_SEEK_DISPATCHER;
        loadingView = (RelativeLayout) view.findViewById(R.id.rltLoading);
        rltError = view.findViewById(R.id.rltError);
        animFailed = view.findViewById(R.id.animFailed);
        tvGuideStart = view.findViewById(R.id.tvGuideStart);
        tvResultScore = view.findViewById(R.id.tvResultScore);
        resultItemBound = (RelativeLayout) view.findViewById(R.id.resultItemBound);
        resultItem = (RelativeLayout) view.findViewById(R.id.resultItem);
        rltSeekBarThumbBound = view.findViewById(R.id.rltSeekBarThumbBound);
        rltSeekBarThumb = view.findViewById(R.id.rltSeekBarThumb);
        rltSeekBarParrent = view.findViewById(R.id.rltSeekBarParrent);
        rltSeekBarBuffer = view.findViewById(R.id.rltSeekBarBuffer);
        rltSeekBarCurrent = view.findViewById(R.id.rltSeekBarCurrent);
        imgFlag = view.findViewById(R.id.imgFlag);
        lnResultEx = view.findViewById(R.id.lnResult);
        textDesAnswer = view.findViewById(R.id.textDesAnswer);
        imgDesAnswer = view.findViewById(R.id.imgDesAnswer);
        imgDesAnswerOrigin = view.findViewById(R.id.imgDesAnswerOrigin);
        rltExplaintion = view.findViewById(R.id.rltExplaintion);
        textDesExplaintion = view.findViewById(R.id.textDesExplaintion);
        rltFullScreen = view.findViewById(R.id.rltFullScreen);
        rltFulExitScreen = view.findViewById(R.id.rltExitFullScreen);
        rltGuide = view.findViewById(R.id.btn_guide);
        lnSwitch = view.findViewById(R.id.lnSwitch);
        aSwitch = view.findViewById(R.id.swShowScore);
        tvScoreSwitch = view.findViewById(R.id.tvScoreSwitch);
        container_ads_native = view.findViewById(R.id.container_ads_native);

        aSwitch.setChecked(Util.isPro(getContext()) ? SharedPreferencesUtils.showScoreStop() : SharedPreferencesUtils.showScoreStopFree());
        aSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (Util.isPro(getContext())) {
                    SharedPreferencesUtils.setShowScoreStop(b);
                    SharedPreferencesUtils.setShowScoreStopFree(b);
                } else {
                    SharedPreferencesUtils.setShowScoreStopFree(b);
                }
            }
        });
        final com.rey.material.widget.RelativeLayout lnSwitchClick = view.findViewById(R.id.lnSwitchClick);
        if (Util.isPro(getActivity())) {
            aSwitch.setEnabled(true);
            lnSwitchClick.setVisibility(View.GONE);
        } else {
            if (test.getIdTest() <= 2) {
                aSwitch.setEnabled(true);
                lnSwitchClick.setVisibility(View.GONE);
            } else {
                aSwitch.setEnabled(false);
                lnSwitchClick.setVisibility(View.VISIBLE);
            }
        }
        lnSwitchClick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!Util.isPro(getActivity())) {
                    ((BaseActivity) getActivity()).showSuggestShowScoreStop();
                    return;
                }
            }
        });
        lnSwitch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Util.isPro(getActivity())) {
                    aSwitch.setChecked(!SharedPreferencesUtils.showScoreStop());
                } else {
                    aSwitch.setChecked(!SharedPreferencesUtils.showScoreStopFree());
                }
            }
        });

        imgPause = view.findViewById(R.id.imgPause);
        imgPlay = view.findViewById(R.id.imgPlay);
        imgNext = view.findViewById(R.id.imgNext);
        rltNext = view.findViewById(R.id.rltNext);
        rltPlay = view.findViewById(R.id.rltPlay);
        rltPause = view.findViewById(R.id.rltPause);
        rltSpace = view.findViewById(R.id.rltSpace);

        //set layout follow scale for result
        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) resultItem.getLayoutParams();
        layoutParams.setMarginStart(testQuest.getQuestionVideo().getBegin_pos() * widthResult / testQuest.getQuestionVideo().getTotal_length());
        layoutParams.width = (testQuest.getQuestionVideo().getEnd_pos() - testQuest.getQuestionVideo().getBegin_pos()) * widthResult / testQuest.getQuestionVideo().getTotal_length();
        resultItem.postDelayed(new Runnable() {
            @Override
            public void run() {
                resultItem.setLayoutParams(layoutParams);
                resultItem.invalidate();
            }
        }, 100);
        //config seekbar
        screenWidth = AppUtils.getWidthScreen(getActivity());
        with16dp = AppUtils.convertDpToPx(getActivity(), 16);
        RelativeLayout.LayoutParams rltSeekBarThumbLpr = (RelativeLayout.LayoutParams) rltSeekBarThumb.getLayoutParams();
        rltSeekBarThumbLpr.width = withThumbSeekBar;
        rltSeekBarThumb.postDelayed(new Runnable() {
            @Override
            public void run() {
                rltSeekBarThumb.setLayoutParams(rltSeekBarThumbLpr);
                rltSeekBarThumb.invalidate();
            }
        }, 100);

        checkResult();

        rltPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!isPlayingVideo) {
                    isStopVideo = false;
                    isPlayingVideo = true;
                    imgPlay.setEnabled(false);
                    rltPlay.setEnabled(false);
                    imgPause.setEnabled(true);
                    rltPause.setEnabled(true);
                    if (question_index == 9) {
                        imgNext.setEnabled(false);
                        rltNext.setEnabled(false);
                    } else {
                        imgNext.setEnabled(true);
                        rltNext.setEnabled(true);
                    }
                    tvGuideStart.setVisibility(View.GONE);
                    if (!Util.isPro(getActivity())) {
                        playVideoMyServer();
                    } else {
                        playVideo();
                    }
                }
            }
        });

        rltPause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isPlayingVideo) {
                    imgPlay.setEnabled(true);
                    rltPlay.setEnabled(true);
                    imgPause.setEnabled(false);
                    rltPause.setEnabled(false);
                    if (question_index == 9) {
                        imgNext.setEnabled(false);
                        rltNext.setEnabled(false);
                    } else {
                        imgNext.setEnabled(true);
                        rltNext.setEnabled(true);
                    }
                    pauseVideo();
                    isPlayingVideo = false;
                }
            }
        });

        rltNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((ExamQuestActivity) getActivity()).nextVideo();
            }
        });

        rltSpace.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                long position = player == null ? 0 : player.getCurrentPosition();
                if (isPlayingVideo && position > 0 && testQuest.getAnswer() == 0) {
                    int currrentPos = progressBarValue(position);
                    RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) imgFlag.getLayoutParams();
                    layoutParams.leftMargin = AppUtils.convertDpToPx(getActivity(), 48) + currrentPos - (AppUtils.convertDpToPx(getActivity(), 32) * 10 / 192);
                    imgFlag.setLayoutParams(layoutParams);
                    imgFlag.setVisibility(View.VISIBLE);
                    resultItemBound.invalidate();

                    testQuest.setAnswer(currrentPos * testQuest.getQuestionVideo().getTotal_length() / widthResult);
                    scoreVideo();
                    testQuest.setLearned(true);
                    ((ExamQuestActivity) getActivity()).updateQuestion(testQuest, question_index);
                    enableSpace(false);

                    if (testQuest.getScore() == 0) {
                        testQuest.getQuestionVideo().setWrong(testQuest.getQuestionVideo().getWrong() + 1);
                    } else {
                        testQuest.getQuestionVideo().setWrong(0);
                    }
                    GPLXDataManager dataManager = new GPLXDataManager(getContext());
                    dataManager.updateWrong(testQuest.getQuestionVideo());
                    SoundPoolManager.getSoundPoolManager().playNote(2);
                }
            }
        });

        rltSeekBarThumbBound.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent event) {
                final int X = (int) event.getRawX();
                RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) view.getLayoutParams();
                RelativeLayout.LayoutParams layoutParamsCurrent = (RelativeLayout.LayoutParams) rltSeekBarCurrent.getLayoutParams();
                int detalX = X - _xDelta;
                if (detalX < 0)
                    detalX = 0;
                if (detalX > screenWidth - withThumbSeekBar - 2 * with16dp)
                    detalX = screenWidth - withThumbSeekBar - 2 * with16dp;
                long position = positionValue(detalX);
                switch (event.getAction() & MotionEvent.ACTION_MASK) {
                    case MotionEvent.ACTION_DOWN:
                        dragging = true;
                        RelativeLayout.LayoutParams lParams = (RelativeLayout.LayoutParams) view.getLayoutParams();
                        _xDelta = X - lParams.leftMargin;
                        break;
                    case MotionEvent.ACTION_UP:
                        ((ExamQuestActivity) getActivity()).disableViewPagerEvent(true);
                        dragging = false;
                        layoutParams.leftMargin = detalX;
                        layoutParamsCurrent.width = detalX;
                        rltSeekBarCurrent.setLayoutParams(layoutParamsCurrent);
                        view.setLayoutParams(layoutParams);
                        if (player != null && !dragging) {
                            seekTo(position);
                        }
                        if (scrollView != null)
                            scrollView.setScrollingEnabled(true);
                        break;
                    case MotionEvent.ACTION_CANCEL:
                        ((ExamQuestActivity) getActivity()).disableViewPagerEvent(true);
                        dragging = false;
                        layoutParams.leftMargin = detalX;
                        layoutParamsCurrent.width = detalX;
                        rltSeekBarCurrent.setLayoutParams(layoutParamsCurrent);
                        view.setLayoutParams(layoutParams);
                        if (player != null && !dragging) {
                            seekTo(position);
                        }
                        if (scrollView != null)
                            scrollView.setScrollingEnabled(true);
                        break;
                    case MotionEvent.ACTION_MOVE:
                        if (scrollView != null)
                            scrollView.setScrollingEnabled(false);
                        ((ExamQuestActivity) getActivity()).disableViewPagerEvent(false);
                        layoutParams.leftMargin = detalX;
                        layoutParamsCurrent.width = detalX;
                        rltSeekBarCurrent.setLayoutParams(layoutParamsCurrent);
                        view.setLayoutParams(layoutParams);
                        if (player != null && !dragging) {
                            seekTo(position);
                        }
                        break;
                    default:
                        return false;
                }
                rltSeekBarParrent.invalidate();
                return true;
            }
        });

        if (rltFullScreen != null) {
            rltFullScreen.setOnClickListener(new OnSingleClickListener() {
                @Override
                public void onSingleClick(View v) {
                    if (getActivity() != null) {
                        ((ExamQuestActivity) getActivity()).requestOritention(true);
                    }
                }
            });
        }

        if (rltFulExitScreen != null) {
            rltFulExitScreen.setOnClickListener(new OnSingleClickListener() {
                @Override
                public void onSingleClick(View v) {
                    if (getActivity() != null) {
                        ((ExamQuestActivity) getActivity()).requestOritention(false);
                    }
                }
            });
        }

        if (scrollView != null)
            OverScrollDecoratorHelper.setUpOverScroll(scrollView);

        if (isPlayingVideo) {
            if (currentPosition > 0) {
                position = currentPosition;
            }
            isStopVideo = false;
            isPlayingVideo = true;
            imgPlay.setEnabled(false);
            rltPlay.setEnabled(false);
            imgPause.setEnabled(true);
            rltPause.setEnabled(true);
            if (question_index == 9) {
                imgNext.setEnabled(false);
                rltNext.setEnabled(false);
            } else {
                imgNext.setEnabled(true);
                rltNext.setEnabled(true);
            }
            tvGuideStart.setVisibility(View.GONE);
            if (!Util.isPro(getActivity())) {
                playVideoMyServer();
            } else {
                playVideo();
            }
            resumeVideo();
        }
        if (imgDesAnswer != null) {
            imgDesAnswer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Util.showTipForQuestion((ExamQuestActivity) getActivity(), testQuest.getQuestionVideo(), false);
                }
            });
        }

        if (imgDesAnswerOrigin != null) {
            imgDesAnswerOrigin.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Util.showTipForQuestion((ExamQuestActivity) getActivity(), testQuest.getQuestionVideo(), true);
                }
            });
        }

        if (rltGuide != null) {
            rltGuide.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showDialogGuide();
                }
            });
        }
        loadNativeAds();
        return view;
    }

    public void showDialogGuide() {
        Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Window window = dialog.getWindow();
        window.setBackgroundDrawableResource(android.R.color.transparent);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.layout_result_question_explantion);
        dialog.getWindow().setGravity(Gravity.BOTTOM);
        if (AppUtils.checkPortraitMode((FragmentActivity) getActivity())) {
            dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        } else {
            try {
                int width = AppUtils.getWidthScreen((FragmentActivity) getActivity()) < AppUtils.getHeightScreen((FragmentActivity) getActivity()) ? AppUtils.getHeightScreen((FragmentActivity) getActivity()) : AppUtils.getWidthScreen((FragmentActivity) getActivity());
                width = width * 2 / 3;
                dialog.getWindow().setLayout(width, LinearLayout.LayoutParams.WRAP_CONTENT);
            } catch (Exception e) {
                dialog.getWindow().setLayout(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            }
        }

        LinearLayout lnResultExDialog = dialog.findViewById(R.id.lnResult);
        CustomTextView textDesAnswerDialog = dialog.findViewById(R.id.textDesAnswer);
        ImageView imgDesAnswerDialog = dialog.findViewById(R.id.imgDesAnswer);
        ImageView imgDesAnswerDialogOrigin = dialog.findViewById(R.id.imgDesAnswerOrigin);
        LinearLayout rltExplaintionDialog = dialog.findViewById(R.id.rltExplaintion);
        TextView textDesExplaintionDialog = dialog.findViewById(R.id.textDesExplaintion);
        TextView tvResultScoreDialog = dialog.findViewById(R.id.tvResultScore);

        if (testQuest.getQuestionVideo().getImageDescription() != null && !testQuest.getQuestionVideo().getImageDescription().isEmpty()) {
            if (tvResultScoreDialog != null) {
                tvResultScoreDialog.setText("Kết quả của bạn: " + scoreVideo() + "/5 điểm");
                tvResultScoreDialog.setVisibility(View.VISIBLE);
            }
            if (lnResultExDialog != null)
                lnResultExDialog.setVisibility(View.VISIBLE);
            if (textDesAnswerDialog != null)
                textDesAnswerDialog.setText(testQuest.getQuestionVideo().getImageDescription());
            String linkImage = AppUtils.getServerImg(getActivity(),testQuest.getQuestionVideo());
            if (imgDesAnswerDialog != null) {
                File fileImgDetail = new File(getActivity().getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS).toString() + "/img_app/doneimg/" + "TH" + testQuest.getQuestionVideo().getId() + ".webp");
                if (fileImgDetail.exists()) {
                    Uri imageUri = Uri.fromFile(fileImgDetail);
                    Glide.with(getActivity())
                            .load(imageUri)
                            .into(imgDesAnswerDialog);
                } else {
                    Glide.with(getActivity())
                            .load(Uri.parse(linkImage))
                            .into(imgDesAnswerDialog);
                }
            }

            if (imgDesAnswerDialogOrigin != null) {
                File fileImgDetail = new File(getActivity().getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS).toString() + "/img_app/doneimgori/" + "THori" + testQuest.getQuestionVideo().getId() + ".webp");
                if (fileImgDetail.exists()) {
                    Uri imageUri = Uri.fromFile(fileImgDetail);
                    Glide.with(getActivity())
                            .load(imageUri)
                            .into(imgDesAnswerDialogOrigin);
                } else {
                    Glide.with(getActivity())
                            .load(Uri.parse(linkImage.replace("doneimg/TH", "doneimgori/THori")))
                            .into(imgDesAnswerDialogOrigin);
                }
            }

            if (testQuest.getQuestionVideo().getExplantion() != null && !testQuest.getQuestionVideo().getExplantion().isEmpty()) {
                if (rltExplaintionDialog != null)
                    rltExplaintionDialog.setVisibility(View.VISIBLE);
                if (textDesExplaintionDialog != null)
                    textDesExplaintionDialog.setText(testQuest.getQuestionVideo().getExplantion());
            } else {
                if (rltExplaintionDialog != null)
                    rltExplaintionDialog.setVisibility(View.GONE);
            }
        }

        final CustomTextView ln_done = (CustomTextView) dialog.findViewById(R.id.btn_done);
        ln_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dialog != null) {
                    if (dialog.isShowing()) {
                        dialog.dismiss();
                        dialog.cancel();
                    }
                }
            }
        });
        dialog.show();
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        isCreated = false;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        ExamQuestActivity examQuestAcitivty = (ExamQuestActivity) getActivity();
        if (examQuestAcitivty != null) {
            examQuestAcitivty.destroyItem(question_index);
        }
        destroyVideo();
        destroyNativeAds(container_ads_native);
    }

    public int scoreVideo() {
        int score = 0;
        if (testQuest.getAnswer() > 0) {
            int distance = (testQuest.getQuestionVideo().getEnd_pos() - testQuest.getQuestionVideo().getBegin_pos()) / 5;
            if (testQuest.getAnswer() >= testQuest.getQuestionVideo().getBegin_pos() && testQuest.getAnswer() < (testQuest.getQuestionVideo().getBegin_pos() + distance)) {
                score = 5;
            } else if (testQuest.getAnswer() >= (testQuest.getQuestionVideo().getBegin_pos() + distance) && testQuest.getAnswer() < (testQuest.getQuestionVideo().getBegin_pos() + distance * 2)) {
                score = 4;
            } else if (testQuest.getAnswer() >= (testQuest.getQuestionVideo().getBegin_pos() + distance * 2) && testQuest.getAnswer() < (testQuest.getQuestionVideo().getBegin_pos() + distance * 3)) {
                score = 3;
            } else if (testQuest.getAnswer() >= (testQuest.getQuestionVideo().getBegin_pos() + distance * 3) && testQuest.getAnswer() < (testQuest.getQuestionVideo().getBegin_pos() + distance * 4)) {
                score = 2;
            } else if (testQuest.getAnswer() >= (testQuest.getQuestionVideo().getBegin_pos() + distance * 4) && testQuest.getAnswer() < (testQuest.getQuestionVideo().getBegin_pos() + distance * 5)) {
                score = 1;
            }
            testQuest.setScore(score);
            return score;
        }
        return 0;
    }

    public void playVideo() {
        showLoadError(false);
        if (player == null) {
            String linkVideo = AppUtils.getServerVideoVip(getActivity(), testQuest.getQuestionVideo().getId());
            HttpProxyCacheServer cacheServer = App.getCacheServer(getActivity());
            proxyVideoUrl = cacheServer.getProxyUrl(linkVideo, true);
            if (cacheServer.isCached(linkVideo)) {
                Log.e("khanhduy.le", "Playing Offline");
                isPlayFromCache = true;
                loadingView.setVisibility(View.GONE);
            } else {
                isPlayFromCache = false;
                loadingView.setVisibility(View.VISIBLE);
                cacheServer.registerCacheListener(new CacheListener() {
                    @Override
                    public void onCacheAvailable(File cacheFile, String url, int percentsAvailable) {
                        if (percentsAvailable == 100) {
                            Log.e("khanhduy.le", "Playing Offline");
                        }
                    }
                }, linkVideo);
            }
            if (getActivity() != null) {
                if (!isStopVideo) {
                    showLoadError(false);
                    createPlayer();
                    simpleExoPlayerView.setPlayer(player);
                    player.seekTo(position);
                    preparePlayer(true);
                    initPlayerListner();
                    if (position == 0) {
                        if (((ExamQuestActivity) getActivity()).getTest().isFinish()) {
                            if (getActivity() != null && ((ExamQuestActivity) getActivity()).isEnableAudio())
                                playAudioMota(testQuest.getQuestionVideo());
                        } else {
                            if (Util.isPro(getActivity())) {
                                if (SharedPreferencesUtils.showDetail()) {
                                    if (getActivity() != null && ((ExamQuestActivity) getActivity()).isEnableAudio())
                                        playAudioTinhhuong(testQuest.getQuestionVideo());
                                } else {
                                    if (getActivity() != null && ((ExamQuestActivity) getActivity()).isEnableAudio())
                                        playAudioTinhHuongThi(testQuest.getQuestionVideo(), question_index + 1);
                                }
                            } else {
                                if (SharedPreferencesUtils.showDetailFree() && test.getIdTest() <= 2) {
                                    if (getActivity() != null && ((ExamQuestActivity) getActivity()).isEnableAudio())
                                        playAudioTinhHuongThi(testQuest.getQuestionVideo(), question_index + 1);
                                } else {
                                    if (getActivity() != null && ((ExamQuestActivity) getActivity()).isEnableAudio())
                                        playAudioTinhhuong(testQuest.getQuestionVideo());
                                }
                            }
                        }
                    }
                } else {
                    stopVideo();
                }
            }
        } else {
            if (loadingView != null)
                loadingView.setVisibility(View.GONE);
            showLoadError(false);
            player.setPlayWhenReady(true);
            if (player!=null && player.getCurrentPosition() == 0) {
                if (((ExamQuestActivity) getActivity()).getTest().isFinish()) {
                    if (getActivity() != null && ((ExamQuestActivity) getActivity()).isEnableAudio())
                        playAudioMota(testQuest.getQuestionVideo());
                } else {
                    if (Util.isPro(getActivity())) {
                        if (SharedPreferencesUtils.showDetail()) {
                            if (getActivity() != null && ((ExamQuestActivity) getActivity()).isEnableAudio())
                                playAudioTinhhuong(testQuest.getQuestionVideo());
                        } else {
                            if (getActivity() != null && ((ExamQuestActivity) getActivity()).isEnableAudio())
                                playAudioTinhHuongThi(testQuest.getQuestionVideo(), question_index + 1);
                        }
                    } else {
                        if (SharedPreferencesUtils.showDetailFree() && test.getIdTest() <= 2) {
                            if (getActivity() != null && ((ExamQuestActivity) getActivity()).isEnableAudio())
                                playAudioTinhhuong(testQuest.getQuestionVideo());
                        } else {
                            if (getActivity() != null && ((ExamQuestActivity) getActivity()).isEnableAudio())
                                playAudioTinhHuongThi(testQuest.getQuestionVideo(), question_index + 1);
                        }
                    }
                }
            }
        }
    }

    public void playVideoMyServer() {
        showLoadError(false);
        if (player == null) {
            String linkVideo = AppUtils.getServerVideo(getActivity(), testQuest.getQuestionVideo().getId());
            HttpProxyCacheServer cacheServer = App.getCacheServer(getActivity());
            proxyVideoUrl = cacheServer.getProxyUrl(linkVideo, true);
            if (cacheServer.isCached(linkVideo)) {
                isPlayFromCache = true;
                loadingView.setVisibility(View.GONE);
            } else {
                isPlayFromCache = false;
                loadingView.setVisibility(View.VISIBLE);
                cacheServer.registerCacheListener(new CacheListener() {
                    @Override
                    public void onCacheAvailable(File cacheFile, String url, int percentsAvailable) {
                        if (percentsAvailable == 100) {
                            Log.e("khanhduy.le", "Playing Offline");
                        }
                    }
                }, linkVideo);
            }
            if (getActivity() != null) {
                if (!isStopVideo) {
                    showLoadError(false);
                    createPlayer();
                    simpleExoPlayerView.setPlayer(player);
                    player.seekTo(position);
                    preparePlayer(true);
                    initPlayerListner();
                    if (position == 0) {
                        if (((ExamQuestActivity) getActivity()).getTest().isFinish()) {
                            if (getActivity() != null && ((ExamQuestActivity) getActivity()).isEnableAudio())
                                playAudioMota(testQuest.getQuestionVideo());
                        } else {
                            if (Util.isPro(getActivity())) {
                                if (SharedPreferencesUtils.showDetail()) {
                                    if (getActivity() != null && ((ExamQuestActivity) getActivity()).isEnableAudio())
                                        playAudioTinhhuong(testQuest.getQuestionVideo());
                                } else {
                                    if (getActivity() != null && ((ExamQuestActivity) getActivity()).isEnableAudio())
                                        playAudioTinhHuongThi(testQuest.getQuestionVideo(), question_index + 1);
                                }
                            } else {
                                if (SharedPreferencesUtils.showDetailFree() && test.getIdTest() <= 2) {
                                    if (getActivity() != null && ((ExamQuestActivity) getActivity()).isEnableAudio())
                                        playAudioTinhhuong(testQuest.getQuestionVideo());
                                } else {
                                    if (getActivity() != null && ((ExamQuestActivity) getActivity()).isEnableAudio())
                                        playAudioTinhHuongThi(testQuest.getQuestionVideo(), question_index + 1);
                                }
                            }
                        }
                    }
                } else {
                    stopVideo();
                }
            }
        } else {
            if (loadingView != null)
                loadingView.setVisibility(View.GONE);
            showLoadError(false);
            player.setPlayWhenReady(true);
            if (player!=null && player.getCurrentPosition() == 0) {
                if (((ExamQuestActivity) getActivity()).getTest().isFinish()) {
                    if (getActivity() != null && ((ExamQuestActivity) getActivity()).isEnableAudio())
                        playAudioMota(testQuest.getQuestionVideo());
                } else {
                    if (Util.isPro(getActivity())) {
                        if (SharedPreferencesUtils.showDetail()) {
                            if (getActivity() != null && ((ExamQuestActivity) getActivity()).isEnableAudio())
                                playAudioTinhhuong(testQuest.getQuestionVideo());
                        } else {
                            if (getActivity() != null && ((ExamQuestActivity) getActivity()).isEnableAudio())
                                playAudioTinhHuongThi(testQuest.getQuestionVideo(), question_index + 1);
                        }
                    } else {
                        if (SharedPreferencesUtils.showDetailFree() && test.getIdTest() <= 2) {
                            if (getActivity() != null && ((ExamQuestActivity) getActivity()).isEnableAudio())
                                playAudioTinhhuong(testQuest.getQuestionVideo());
                        } else {
                            if (getActivity() != null && ((ExamQuestActivity) getActivity()).isEnableAudio())
                                playAudioTinhHuongThi(testQuest.getQuestionVideo(), question_index + 1);
                        }
                    }
                }
            }
        }
    }

    public void showLoadError(boolean isShow) {
        if (isShow) {
            if (rltError != null)
                rltError.setVisibility(View.VISIBLE);
            if (animFailed != null)
                animFailed.playAnimation();
        } else {
            if (rltError != null)
                rltError.setVisibility(View.GONE);
            if (animFailed != null)
                animFailed.pauseAnimation();
        }
    }

    public void stopVideo() {
        showLoadError(false);
        isStopVideo = true;
        destroyAudio();
        if (getActivity() != null) {
            loadingView.setVisibility(View.GONE);
        }
        if (player != null) {
            seekTo(0);
            player.setPlayWhenReady(false);
            try {
                player.release();
                player = null;
            } catch (Exception e) {

            }
        }
        isPlayingVideo = false;
        if (!isCreated)
            return;
        checkResult();
    }

    public void checkResult() {
        if (getActivity() != null && !((ExamQuestActivity) getActivity()).getTest().isFinish()) {
            if (testQuest.getAnswer() > 0) {
                tvResultScore.setText("Kết quả của bạn: " + testQuest.getScore() + "/5 điểm");
                tvResultScore.setVisibility(View.GONE);
                lnSwitch.setVisibility(View.GONE);
                resultItem.setVisibility(View.INVISIBLE);
                if (rltGuide != null) {
                    rltGuide.setVisibility(View.GONE);
                }
                tvGuideStart.setText("Vui lòng bấm nút \"Phát video\" ở bên dưới để xem lại bài học");
                tvGuideStart.setVisibility(View.VISIBLE);
                RelativeLayout.LayoutParams layoutParamsFlag = (RelativeLayout.LayoutParams) imgFlag.getLayoutParams();
                layoutParamsFlag.leftMargin = AppUtils.convertDpToPx(getActivity(), 48) + testQuest.getAnswer() * widthResult / testQuest.getQuestionVideo().getTotal_length() - (AppUtils.convertDpToPx(getActivity(), 32) * 10 / 192);
                imgFlag.setLayoutParams(layoutParamsFlag);
                imgFlag.setVisibility(View.VISIBLE);
                resultItemBound.invalidate();

                enableSpace(false);
                imgPlay.setEnabled(true);
                rltPlay.setEnabled(true);
                imgPause.setEnabled(false);
                rltPause.setEnabled(false);

                if (testQuest.getQuestionVideo().getImageDescription() != null && !testQuest.getQuestionVideo().getImageDescription().isEmpty()) {
                    if (lnResultEx != null)
                        lnResultEx.setVisibility(View.GONE);
                    if (textDesAnswer != null)
                        textDesAnswer.setText(testQuest.getQuestionVideo().getImageDescription());
                    String linkImage = AppUtils.getServerImg(getActivity(),testQuest.getQuestionVideo());
                    if (imgDesAnswer != null) {
                        File fileImgDetail = new File(getActivity().getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS).toString() + "/img_app/doneimg/" + "TH" + testQuest.getQuestionVideo().getId() + ".webp");
                        if (fileImgDetail.exists()) {
                            Uri imageUri = Uri.fromFile(fileImgDetail);
                            Glide.with(getActivity())
                                    .load(imageUri)
                                    .into(imgDesAnswer);
                        } else {
                            Glide.with(getActivity())
                                    .load(linkImage)
                                    .into(imgDesAnswer);
                        }
                    }
                    if (imgDesAnswerOrigin != null) {
                        File fileImgDetail = new File(getActivity().getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS).toString() + "/img_app/doneimgori/" + "THori" + testQuest.getQuestionVideo().getId() + ".webp");
                        if (fileImgDetail.exists()) {
                            Uri imageUri = Uri.fromFile(fileImgDetail);
                            Glide.with(getActivity())
                                    .load(imageUri)
                                    .into(imgDesAnswerOrigin);
                        } else {
                            Glide.with(getActivity())
                                    .load(Uri.parse(linkImage.replace("doneimg/TH", "doneimgori/THori")))
                                    .into(imgDesAnswerOrigin);
                        }
                    }

                    if (testQuest.getQuestionVideo().getExplantion() != null && !testQuest.getQuestionVideo().getExplantion().isEmpty()) {
                        if (rltExplaintion != null)
                            rltExplaintion.setVisibility(View.VISIBLE);
                        if (textDesExplaintion != null) {
                            textDesExplaintion.setText(testQuest.getQuestionVideo().getExplantion());
                        }
                    } else {
                        if (rltExplaintion != null)
                            rltExplaintion.setVisibility(View.GONE);
                    }
                }
            } else {
                tvGuideStart.setText("Vui lòng bấm nút \"Phát video\" ở bên dưới bắt đầu bài thi");
                tvGuideStart.setVisibility(View.VISIBLE);
                imgFlag.setVisibility(View.INVISIBLE);
                enableSpace(false);
                imgPlay.setEnabled(true);
                rltPlay.setEnabled(true);
                imgPause.setEnabled(false);
                rltPause.setEnabled(false);
                if (question_index == 9) {
                    imgNext.setEnabled(false);
                    rltNext.setEnabled(false);
                } else {
                    imgNext.setEnabled(true);
                    rltNext.setEnabled(true);
                }
            }
        } else {
            if (getActivity() != null) {
                tvResultScore.setText("Kết quả của bạn: " + testQuest.getScore() + "/5 điểm");
                tvResultScore.setVisibility(View.VISIBLE);
                if (container_ads_native != null) {
                    container_ads_native.setVisibility(View.GONE);
                    destroyNativeAds(container_ads_native);
                }
                lnSwitch.setVisibility(View.VISIBLE);
                if (rltGuide != null) {
                    rltGuide.setVisibility(View.VISIBLE);
                }
                resultItem.setVisibility(View.VISIBLE);
                imgPlay.setEnabled(true);
                rltPlay.setEnabled(true);
                tvGuideStart.setText("Vui lòng bấm nút \"Phát video\" ở bên dưới để xem lại bài học");
                tvGuideStart.setVisibility(View.VISIBLE);
                RelativeLayout.LayoutParams layoutParamsFlag = (RelativeLayout.LayoutParams) imgFlag.getLayoutParams();
                layoutParamsFlag.leftMargin = AppUtils.convertDpToPx(getActivity(), 48) + testQuest.getAnswer() * widthResult / testQuest.getQuestionVideo().getTotal_length() - (AppUtils.convertDpToPx(getActivity(), 32) * 10 / 192);
                imgFlag.setLayoutParams(layoutParamsFlag);
                imgFlag.setVisibility(View.VISIBLE);
                resultItemBound.invalidate();

                enableSpace(false);
                imgPause.setEnabled(false);
                rltPause.setEnabled(false);

                if (testQuest.getQuestionVideo().getImageDescription() != null && !testQuest.getQuestionVideo().getImageDescription().isEmpty()) {
                    if (lnResultEx != null)
                        lnResultEx.setVisibility(View.VISIBLE);
                    if (textDesAnswer != null)
                        textDesAnswer.setText(testQuest.getQuestionVideo().getImageDescription());
                    String linkImage = AppUtils.getServerImg(getActivity(),testQuest.getQuestionVideo());
                    if (imgDesAnswer != null) {
                        File fileImgDetail = new File(getActivity().getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS).toString() + "/img_app/doneimg/" + "TH" + testQuest.getQuestionVideo().getId() + ".webp");
                        if (fileImgDetail.exists()) {
                            Uri imageUri = Uri.fromFile(fileImgDetail);
                            Glide.with(getActivity())
                                    .load(imageUri)
                                    .into(imgDesAnswer);
                        } else {
                            Glide.with(getActivity())
                                    .load(linkImage)
                                    .into(imgDesAnswer);
                        }
                    }

                    if (imgDesAnswerOrigin != null) {
                        File fileImgDetail = new File(getActivity().getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS).toString() + "/img_app/doneimgori/" + "THori" + testQuest.getQuestionVideo().getId() + ".webp");
                        if (fileImgDetail.exists()) {
                            Uri imageUri = Uri.fromFile(fileImgDetail);
                            Glide.with(getActivity())
                                    .load(imageUri)
                                    .into(imgDesAnswerOrigin);
                        } else {
                            Glide.with(getActivity())
                                    .load(Uri.parse(linkImage.replace("doneimg/TH", "doneimgori/THori")))
                                    .into(imgDesAnswerOrigin);
                        }
                    }

                    if (testQuest.getQuestionVideo().getExplantion() != null && !testQuest.getQuestionVideo().getExplantion().isEmpty()) {
                        if (rltExplaintion != null)
                            rltExplaintion.setVisibility(View.VISIBLE);
                        if (textDesExplaintion != null) {
                            textDesExplaintion.setText(testQuest.getQuestionVideo().getExplantion());
                        }
                    } else {
                        if (rltExplaintion != null)
                            rltExplaintion.setVisibility(View.GONE);
                    }
                }
            }
        }
    }

    public void destroyVideo() {
        destroyAudio();
        if (player != null) {
            try {
                player.setPlayWhenReady(false);
                player.release();
            } catch (Exception e) {

            }
        }
    }

    public void resumeVideo() {
        try {
            if (player != null) {
                player.seekTo(position);
            }
            if (player != null && isPlayingVideo) {
                player.setPlayWhenReady(true);
            }

        } catch (Exception e) {

        }
    }

    public void pauseVideo() {
        try {
            if (player != null && isPlayingVideo) {
                player.setPlayWhenReady(false);
                position = player == null ? 0 : player.getCurrentPosition();
            }
        } catch (Exception e) {

        }
    }

    private void createPlayer() {
        // Create a default TrackSelector
        simpleExoPlayerView.setUseController(false);
        BandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
        TrackSelection.Factory videoTrackSelectionFactory =
                new AdaptiveVideoTrackSelection.Factory(bandwidthMeter);
        TrackSelector trackSelector =
                new DefaultTrackSelector(videoTrackSelectionFactory);
        // Create a default LoadControl
        LoadControl loadControl = new DefaultLoadControl();
        // Create the player
        player = ExoPlayerFactory.newSimpleInstance(getActivity(), trackSelector, loadControl);
    }

    private void preparePlayer(boolean play) {
        // Measures bandwidth during playback. Can be null if not required.
        DefaultBandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
        // Produces DataSource instances through which media data is loaded.
        DataSource.Factory dataSourceFactory = new DefaultDataSourceFactory(getActivity(), com.google.android.exoplayer2.util.Util.getUserAgent(getActivity(), "OfflinePlayer"), bandwidthMeter);
        // Produces Extractor instances for parsing the media data.
        ExtractorsFactory extractorsFactory = new DefaultExtractorsFactory();
        // This is the MediaSource representing the media to be played.
        MediaSource videoSource = new ExtractorMediaSource(Uri.parse(proxyVideoUrl),
                dataSourceFactory, extractorsFactory, null, null);
        // Prepare the player with the source and play when ready
        player.setPlayWhenReady(true);
        player.prepare(videoSource);
    }

    private void initPlayerListner() {
        player.addListener(new ExoPlayer.EventListener() {
            @Override
            public void onTimelineChanged(Timeline timeline, Object manifest) {
                updateProgress();
            }

            @Override
            public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {
            }

            @Override
            public void onLoadingChanged(boolean isLoading) {
            }

            @Override
            public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
                if (playWhenReady && playbackState == STATE_READY) {
                    if (loadingView != null)
                        loadingView.setVisibility(View.GONE);
                }
                if (playWhenReady && playbackState == STATE_BUFFERING) {
                    if (loadingView != null && !isPlayFromCache)
                        loadingView.setVisibility(View.VISIBLE);
                }
                if (playWhenReady && playbackState == STATE_ENDED) {
                    if (getActivity() != null) {
                        ((ExamQuestActivity) getActivity()).nextVideo();
                    }
                }
                updateProgress();
            }

            @Override
            public void onPlayerError(ExoPlaybackException error) {
                if (loadingView != null) {
                    if (Util.isPro(loadingView.getContext())) {
                        if (loadingView != null)
                            loadingView.setVisibility(View.GONE);
                        showLoadError(true);
                        if (loadingView != null && loadingView.getContext() != null && getActivity() != null) {
                            Bundle params = new Bundle();
                            params.putString("url_video", testQuest.getQuestionVideo().getId() + "");
                            ((ExamQuestActivity) getActivity()).logFirebase(params);
                        }
                    } else {
                        if (loadingView != null)
                            loadingView.setVisibility(View.GONE);
                        showLoadError(true);
                        if (loadingView != null && loadingView.getContext() != null) {
                            new DialogUtil.Builder(loadingView.getContext())
                                    .title("Hệ thống quá tải")
                                    .content("Phiên bản miễn phí sẽ đôi khi gặp sự cố khi tải video.Bạn có muốn nâng cấp lên lên bản cao cấp để không gặp sự cố này và sử dụng nhiều tính năng khác không?")
                                    .doneText(getString(R.string.text_ok))
                                    .onDone(new DialogUtil.SingleButtonCallback() {
                                        @Override
                                        public void onClick() {
                                            if (getActivity() != null)
                                                ((BaseActivity) getActivity()).buyPro();
                                        }
                                    })
                                    .show();
                        }
                        if (loadingView != null && loadingView.getContext() != null && getActivity() != null) {
                            Bundle params = new Bundle();
                            params.putString("url_video", AppUtils.getServerVideo(getActivity(), testQuest.getQuestionVideo().getId()));
                            ((ExamQuestActivity) getActivity()).logFirebase(params);
                        }
                    }
                    if (imgPlay != null)
                        imgPlay.setEnabled(true);
                    if(rltPlay!=null)
                        rltPlay.setEnabled(true);
                    if (imgPause != null)
                        imgPause.setEnabled(false);
                    if(rltPause !=null)
                        rltPause.setEnabled(false);
                    if (imgNext != null) {
                        if (question_index == 9) {
                            imgNext.setEnabled(false);
                            rltNext.setEnabled(false);
                        } else {
                            imgNext.setEnabled(true);
                            rltNext.setEnabled(true);
                        }
                    }
                    destroyVideo();
                    isPlayingVideo = false;
                }
            }

            @Override
            public void onPositionDiscontinuity() {
                updateProgress();
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        resumeVideo();
    }

    @Override
    public void onPause() {
        super.onPause();
        pauseVideo();
    }

    public interface SeekDispatcher {

        /**
         * @param player      The player to seek.
         * @param windowIndex The index of the window.
         * @param positionMs  The seek position in the specified window, or {@link C#TIME_UNSET} to seek
         *                    to the window's default position.
         * @return True if the seek was dispatched. False otherwise.
         */
        boolean dispatchSeek(ExoPlayer player, int windowIndex, long positionMs);

    }

    public static final SeekDispatcher DEFAULT_SEEK_DISPATCHER = new SeekDispatcher() {
        @Override
        public boolean dispatchSeek(ExoPlayer player, int windowIndex, long positionMs) {
            player.seekTo(windowIndex, positionMs);
            return true;
        }
    };

    private void seekTo(long positionMs) {
        seekTo(player.getCurrentWindowIndex(), positionMs);
        player.setPlayWhenReady(false);
        isPlayingVideo = false;
        imgPlay.setEnabled(true);
        rltPlay.setEnabled(true);
        if (question_index == 9) {
            imgNext.setEnabled(false);
            rltNext.setEnabled(false);
        } else {
            imgNext.setEnabled(true);
            rltNext.setEnabled(true);
        }
        imgPause.setEnabled(false);
        rltPause.setEnabled(false);
    }

    private void seekTo(int windowIndex, long positionMs) {
        boolean dispatched = seekDispatcher.dispatchSeek(player, windowIndex, positionMs);
        if (!dispatched) {
            // The seek wasn't dispatched. If the progress bar was dragged by the user to perform the
            // seek then it'll now be in the wrong position. Trigger a progress update to snap it back.
            updateProgress();
        }
    }

    private int progressBarValue(long position) {
        long duration = player == null ? C.TIME_UNSET : player.getDuration();
        return duration == C.TIME_UNSET || duration == 0 ? 0
                : (int) ((position * (screenWidth - with16dp * 2 - withThumbSeekBar)) / duration);
    }

    private int progressBarOffsetValue(long position) {
        long duration = player == null ? C.TIME_UNSET : player.getDuration();
        return duration == C.TIME_UNSET || duration == 0 ? 0
                : (int) ((position * (screenWidth - with16dp * 2)) / duration);
    }

    private long positionValue(int progress) {
        long duration = player == null ? C.TIME_UNSET : player.getDuration();
        long durationTemp = ((duration * progress) / (screenWidth - with16dp * 2 - withThumbSeekBar));
        return duration == C.TIME_UNSET ? 0 : (durationTemp >= duration ? (duration - 1) : durationTemp);
    }

    private void updateProgress() {
        long position = player == null ? 0 : player.getCurrentPosition();
        if (tvResultScore.getVisibility() == View.VISIBLE) {
            scoreVideoWithTime(position);
        }
        if (isPlayingVideo && position > 0) {
            if (testQuest.getAnswer() > 0) {
                enableSpace(false);
            } else {
                enableSpace(true);
            }
        } else {
            enableSpace(false);
        }
        if (rltSeekBarThumbBound != null) {
            if (!dragging) {
                RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) rltSeekBarThumbBound.getLayoutParams();
                layoutParams.leftMargin = progressBarValue(position);
                rltSeekBarThumbBound.setLayoutParams(layoutParams);
                RelativeLayout.LayoutParams layoutParamsCurrent = (RelativeLayout.LayoutParams) rltSeekBarCurrent.getLayoutParams();
                layoutParamsCurrent.width = progressBarValue(position);
                rltSeekBarCurrent.setLayoutParams(layoutParamsCurrent);
            }
            long bufferedPosition = player == null ? 0 : player.getBufferedPosition();
            RelativeLayout.LayoutParams layoutParamsCurrent = (RelativeLayout.LayoutParams) rltSeekBarBuffer.getLayoutParams();
            layoutParamsCurrent.width = progressBarOffsetValue(bufferedPosition);
            rltSeekBarBuffer.setLayoutParams(layoutParamsCurrent);
            rltSeekBarParrent.invalidate();
        }
        rltSeekBarThumbBound.removeCallbacks(updateProgressAction);
        // Schedule an update if necessary.
        int playbackState = player == null ? ExoPlayer.STATE_IDLE : player.getPlaybackState();
        if (playbackState != ExoPlayer.STATE_IDLE && playbackState != ExoPlayer.STATE_ENDED) {
            long delayMs;
            if (player.getPlayWhenReady() && playbackState == ExoPlayer.STATE_READY) {
                delayMs = 40 - (position % 40);
                if (delayMs < 20) {
                    delayMs += 40;
                }
            } else {
                delayMs = 40;
            }
            rltSeekBarThumbBound.postDelayed(updateProgressAction, delayMs);
        }
    }

    private final Runnable updateProgressAction = new Runnable() {
        @Override
        public void run() {
            updateProgress();
        }
    };

    public void enableSpace(boolean enable) {
        if (enable) {
            if (rltSpace != null) {
                rltSpace.setBackgroundResource(R.drawable.background_button_space);
                rltSpace.invalidate();
            }
        } else {
            if (rltSpace != null) {
                rltSpace.setBackgroundResource(R.drawable.background_button_space_disable);
                rltSpace.invalidate();
            }
        }
    }

    public void enableNextBtn(boolean event) {
        if (imgNext != null)
            imgNext.setEnabled(event);
        if(rltNext!=null)
            rltNext.setEnabled(event);
    }

    public TestQuest getTestQuest() {
        return testQuest;
    }

    public void setTestQuest(TestQuest testQuest) {
        this.testQuest = testQuest;
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean("isPlayingVideo", isPlayingVideo);
        long position = player == null ? 0 : player.getCurrentPosition();
        outState.putLong("currentPosition", position);
        pauseAudio();
    }

    public void changeCheckedShowScore() {
        if (aSwitch != null) {
            if (Util.isPro(getContext())) {
                aSwitch.setChecked(SharedPreferencesUtils.showScoreStop());
            } else {
                aSwitch.setChecked(SharedPreferencesUtils.showScoreStopFree());
            }
        }
    }

    public void scoreVideoWithTime(long time) {
        int score = 0;
        time = time / 10;
        if (time > 0) {
            int distance = (testQuest.getQuestionVideo().getEnd_pos() - testQuest.getQuestionVideo().getBegin_pos()) / 5;
            if (time >= testQuest.getQuestionVideo().getBegin_pos() && time < (testQuest.getQuestionVideo().getBegin_pos() + distance)) {
                time = testQuest.getQuestionVideo().getBegin_pos();
                score = 5;
            } else if (time >= (testQuest.getQuestionVideo().getBegin_pos() + distance) && time < (testQuest.getQuestionVideo().getBegin_pos() + distance * 2)) {
                time = testQuest.getQuestionVideo().getBegin_pos() + distance;
                score = 4;
            } else if (time >= (testQuest.getQuestionVideo().getBegin_pos() + distance * 2) && time < (testQuest.getQuestionVideo().getBegin_pos() + distance * 3)) {
                time = testQuest.getQuestionVideo().getBegin_pos() + distance * 2;
                score = 3;
            } else if (time >= (testQuest.getQuestionVideo().getBegin_pos() + distance * 3) && time < (testQuest.getQuestionVideo().getBegin_pos() + distance * 4)) {
                time = testQuest.getQuestionVideo().getBegin_pos() + distance * 3;
                score = 2;
            } else if (time >= (testQuest.getQuestionVideo().getBegin_pos() + distance * 4) && time < (testQuest.getQuestionVideo().getBegin_pos() + distance * 5)) {
                time = testQuest.getQuestionVideo().getBegin_pos() + distance * 4;
                score = 1;
            }
        }
        if (score > 0) {
            if ((SharedPreferencesUtils.showScoreStop() || (SharedPreferencesUtils.showScoreStopFree() && test.getIdTest() <= 2)) && tvResultScore.getVisibility() == View.VISIBLE) {
                if (isPlayingVideo && !tvScoreSwitch.getText().equals(score + " điểm")) {
                    tvScoreSwitch.setText(score + " điểm");
                    imgPlay.setEnabled(true);
                    rltPlay.setEnabled(true);
                    imgPause.setEnabled(false);
                    rltPause.setEnabled(false);
                    if (question_index == 9) {
                        imgNext.setEnabled(false);
                        rltNext.setEnabled(false);
                    } else {
                        if (question_index == 9) {
                            imgNext.setEnabled(false);
                            rltNext.setEnabled(false);
                        } else {
                            imgNext.setEnabled(true);
                            rltNext.setEnabled(true);
                        }
                    }
                    pauseVideo();
                    isPlayingVideo = false;
                    seekTo(time * 10 + 10);
                }
            }
        } else {
            tvScoreSwitch.setText("0 điểm");
        }
    }

    public void loadNativeAds() {
        if (rltGuide == null) {
            if (tvResultScore.getVisibility() == View.VISIBLE) {
                if (container_ads_native != null) {
                    container_ads_native.setVisibility(View.GONE);
                    destroyNativeAds(container_ads_native);
                }
            } else {
                if (container_ads_native != null) {
                    loadNativeAds(container_ads_native);
                }
            }
        }
    }
}
