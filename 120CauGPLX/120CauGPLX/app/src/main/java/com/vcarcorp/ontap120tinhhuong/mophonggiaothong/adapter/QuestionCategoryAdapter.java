package com.vcarcorp.ontap120tinhhuong.mophonggiaothong.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Typeface;
import android.text.Spannable;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.akexorcist.roundcornerprogressbar.RoundCornerProgressBar;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.R;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.learnquestion.LearnQuestCategoryActivity;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.model.QuestionCategory;
import com.ads.control.customview.CustomTextView;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.util.OnSingleClickListener;

import java.util.ArrayList;
import java.util.List;

public class QuestionCategoryAdapter extends RecyclerView.Adapter<QuestionCategoryAdapter.ViewHolder> {
    //recycle
    private ItemClickListener mClickListener;
    private ArrayList<QuestionCategory> questionCategories;
    private Context mContext;

    public QuestionCategoryAdapter(final ArrayList<QuestionCategory> data, Context mContext) {
        this.questionCategories = data;
        this.mContext = mContext;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        this.mContext = parent.getContext();
        return new QuestionCategoryAdapter.ViewHolder(LayoutInflater.from(mContext)
                .inflate(R.layout.theory_question_category_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position, @NonNull List<Object> payloads) {
        super.onBindViewHolder(holder, position, payloads);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, @SuppressLint("RecyclerView") int position) {
        final QuestionCategory item = questionCategories.get(position);

        holder.textViewTitle.setText(item.getQuestName());
        String desc = item.getQuestDes();
        holder.textViewSumQuest.setText(desc);

        holder.roundCornerProgressBar.setMax(item.getNumberQuest()*5);
        holder.roundCornerProgressBar.setProgress(item.getTotalScore());
        holder.roundCornerProgressBar.setSecondaryProgress(0);

        String currentQuest = "Tổng điểm " + item.getTotalScore() + " / " + item.getNumberQuest()*5;
        int beginIndex = currentQuest.indexOf(String.valueOf(item.getTotalScore()));
        int endIndex = currentQuest.indexOf(String.valueOf(item.getTotalScore())) + String.valueOf(item.getTotalScore()).length();
        Spannable spanText = Spannable.Factory.getInstance().newSpannable(currentQuest);
        spanText.setSpan(new ForegroundColorSpan(mContext.getColor(R.color.colorPrimary)), beginIndex, endIndex , Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        spanText.setSpan(new StyleSpan(Typeface.BOLD), beginIndex, endIndex, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        holder.textViewCurrent.setText(spanText, TextView.BufferType.SPANNABLE);
        holder.rltView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((LearnQuestCategoryActivity)mContext).goToLearn(item,position);
            }
        });
        if(position == 0){
            holder.imgIcon.setImageResource(R.drawable.category_street);
        }else if(position == 1){
            holder.imgIcon.setImageResource(R.drawable.category_countryside);
        }else if(position == 2){
            holder.imgIcon.setImageResource(R.drawable.category_highway);
        }else if(position == 3){
            holder.imgIcon.setImageResource(R.drawable.category_moutain);
        }else if(position == 4){
            holder.imgIcon.setImageResource(R.drawable.category_route);
        }else if(position == 5){
            holder.imgIcon.setImageResource(R.drawable.category_accident);
        }
    }

    @Override
    public int getItemCount() {
        return questionCategories.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private com.rey.material.widget.RelativeLayout rltView;
        private CustomTextView textViewTitle;
        private CustomTextView textViewSumQuest;
        private CustomTextView textViewCurrent;
        private ImageView imgIcon;
        private RoundCornerProgressBar roundCornerProgressBar;

        public ViewHolder(View view) {
            super(view);
            rltView = (com.rey.material.widget.RelativeLayout) view.findViewById(R.id.rltView);
            imgIcon = (ImageView) view.findViewById(R.id.iconCategory);
            textViewSumQuest = (CustomTextView) view.findViewById(R.id.textViewSumQuest);
            textViewTitle = (CustomTextView) view.findViewById(R.id.textViewTitle);
            textViewCurrent = (CustomTextView)view.findViewById(R.id.textViewCurrent);
            roundCornerProgressBar = (RoundCornerProgressBar)view.findViewById(R.id.roundCornerProgressBar);
            rltView =  (com.rey.material.widget.RelativeLayout)view.findViewById(R.id.rltView);
            rltView.setOnClickListener(new OnSingleClickListener() {
                @Override
                public void onSingleClick(View v) {
                    if (mClickListener != null) mClickListener.onItemClick(v, getAdapterPosition());
                }
            });
        }
    }
    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }

    public ArrayList<QuestionCategory> getQuestionCategories() {
        return questionCategories;
    }

    public void setQuestionCategories(ArrayList<QuestionCategory> questionCategories) {
        this.questionCategories = questionCategories;
    }
}
