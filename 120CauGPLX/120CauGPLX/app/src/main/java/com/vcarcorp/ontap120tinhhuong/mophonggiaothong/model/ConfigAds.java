package com.vcarcorp.ontap120tinhhuong.mophonggiaothong.model;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.Keep;

import com.google.firebase.database.IgnoreExtraProperties;

@IgnoreExtraProperties
@Keep
public class ConfigAds implements Parcelable {
    private boolean enable;
    private String des1;
    private String des2;
    private String icon;
    private String img1;
    private String img2;
    private String img3;
    private String name;
    private String packages;
    private int index;

    public boolean isEnable() {
        return enable;
    }

    public void setEnable(boolean enable) {
        this.enable = enable;
    }

    public String getDes1() {
        return des1;
    }

    public void setDes1(String des1) {
        this.des1 = des1;
    }

    public String getDes2() {
        return des2;
    }

    public void setDes2(String des2) {
        this.des2 = des2;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getImg1() {
        return img1;
    }

    public void setImg1(String img1) {
        this.img1 = img1;
    }

    public String getImg2() {
        return img2;
    }

    public void setImg2(String img2) {
        this.img2 = img2;
    }

    public String getImg3() {
        return img3;
    }

    public void setImg3(String img3) {
        this.img3 = img3;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPackages() {
        return packages;
    }

    public void setPackages(String packages) {
        this.packages = packages;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeByte(this.enable ? (byte) 1 : (byte) 0);
        dest.writeString(this.des1);
        dest.writeString(this.des2);
        dest.writeString(this.icon);
        dest.writeString(this.img1);
        dest.writeString(this.img2);
        dest.writeString(this.img3);
        dest.writeString(this.name);
        dest.writeString(this.packages);
        dest.writeInt(this.index);
    }

    public void readFromParcel(Parcel source) {
        this.enable = source.readByte() != 0;
        this.des1 = source.readString();
        this.des2 = source.readString();
        this.icon = source.readString();
        this.img1 = source.readString();
        this.img2 = source.readString();
        this.img3 = source.readString();
        this.name = source.readString();
        this.packages = source.readString();
        this.index = source.readInt();
    }

    public ConfigAds() {
    }

    protected ConfigAds(Parcel in) {
        this.enable = in.readByte() != 0;
        this.des1 = in.readString();
        this.des2 = in.readString();
        this.icon = in.readString();
        this.img1 = in.readString();
        this.img2 = in.readString();
        this.img3 = in.readString();
        this.name = in.readString();
        this.packages = in.readString();
        this.index = in.readInt();
    }

    public static final Creator<ConfigAds> CREATOR = new Creator<ConfigAds>() {
        @Override
        public ConfigAds createFromParcel(Parcel source) {
            return new ConfigAds(source);
        }

        @Override
        public ConfigAds[] newArray(int size) {
            return new ConfigAds[size];
        }
    };
}
