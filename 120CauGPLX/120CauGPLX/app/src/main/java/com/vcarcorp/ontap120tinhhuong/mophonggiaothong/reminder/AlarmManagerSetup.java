package com.vcarcorp.ontap120tinhhuong.mophonggiaothong.reminder;

import static android.content.Context.ALARM_SERVICE;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.preference.PreferenceManager;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.App;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.util.SharedPreferencesUtils;

import java.util.Calendar;
import java.util.concurrent.TimeUnit;

public class AlarmManagerSetup {
    public static final int REQUEST_CODE_REMINDER_RECEIVER = 12345;
    private static SharedPreferences preferences;

    public static void scheduleAlarmReceiverDiaryReminder(Context context) {
        Intent intent = new Intent(context, ReminderNotifierReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, REQUEST_CODE_REMINDER_RECEIVER, intent, PendingIntent.FLAG_IMMUTABLE);
        Calendar calendar = calculateCalendarNotifier();
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(ALARM_SERVICE);
        setNextTime(calendar.getTimeInMillis());
        alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), TimeUnit.MINUTES.toMillis(SharedPreferencesUtils.getReminderInterval()), pendingIntent);
        if (Build.VERSION.SDK_INT >= 23) {
            alarmManager.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), pendingIntent);
        } else {
            alarmManager.setExact(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), pendingIntent);
        }
        ComponentName receiver = new ComponentName(context, ReminderNotifierReceiver.class);
        context.getPackageManager().setComponentEnabledSetting(
                receiver,
                PackageManager.COMPONENT_ENABLED_STATE_ENABLED,
                PackageManager.DONT_KILL_APP
        );
    }

    private static Calendar calculateCalendarNotifier() {
        Calendar calendar = Calendar.getInstance();
        String[] time = SharedPreferencesUtils.getWakeUp().split(":");
        calendar.set(Calendar.HOUR_OF_DAY, Integer.parseInt(time[0]));
        calendar.set(Calendar.MINUTE, Integer.parseInt(time[1]));
        calendar.set(Calendar.SECOND, 0);
        while (System.currentTimeMillis() >= calendar.getTimeInMillis()) {
            calendar.setTimeInMillis(calendar.getTimeInMillis() + (60000 * SharedPreferencesUtils.getReminderInterval()));
        }
        return calendar;
    }

    public static void setNextTime(long nextTime) {
        preferences = PreferenceManager.getDefaultSharedPreferences(App.getInstance());
        preferences.edit().putLong("_nextTime", nextTime).apply();
    }

}