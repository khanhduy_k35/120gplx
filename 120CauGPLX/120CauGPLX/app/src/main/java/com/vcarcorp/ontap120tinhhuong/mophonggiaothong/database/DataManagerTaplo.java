package com.vcarcorp.ontap120tinhhuong.mophonggiaothong.database;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.model.Taplo;

import java.util.ArrayList;

/**
 * Created by duy on 10/23/17.
 */

public class DataManagerTaplo extends SQLiteAssetHelper {

    private static final String DATABASE_NAME = "taplo_light.db";
    private static final int DATABASE_VERSION = 1;

    public DataManagerTaplo(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public ArrayList<Taplo> getListTaplos(){
        ArrayList<Taplo> listResult = new ArrayList<Taplo>();
        SQLiteDatabase localSQLiteDatabase = getWritableDatabase();
        String query = "select * from light_info;";
        Cursor cursor = localSQLiteDatabase.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            do {
                Taplo signsCategory = new Taplo(cursor.getInt(0),cursor.getString(1),cursor.getString(2));
                listResult.add(signsCategory);
            } while (cursor.moveToNext());
        }
        try {
            cursor.close();
        } catch (Exception e) {

        }
        try {
            localSQLiteDatabase.close();
        }catch (Exception e) {

        }
        return listResult;

    }
}