package com.vcarcorp.ontap120tinhhuong.mophonggiaothong.model;

import android.os.Parcel;
import android.os.Parcelable;
/**
 * Created by duy on 10/24/17.
 */

public class QuestionCategory implements Parcelable {
    private int questionCategoryID;
    private String questDes;
    private String questName;
    private int numberQuest;
    private int totalScore;
    private int numberPass;
    private int currentIndex;
    private String nameImage;

    public int getQuestionCategoryID() {
        return questionCategoryID;
    }

    public void setQuestionCategoryID(int questionCategoryID) {
        this.questionCategoryID = questionCategoryID;
    }

    public String getQuestDes() {
        return questDes;
    }

    public void setQuestDes(String questDes) {
        this.questDes = questDes;
    }

    public String getQuestName() {
        return questName;
    }

    public void setQuestName(String questName) {
        this.questName = questName;
    }

    public int getNumberQuest() {
        return numberQuest;
    }

    public void setNumberQuest(int numberQuest) {
        this.numberQuest = numberQuest;
    }

    public int getTotalScore() {
        return totalScore;
    }

    public void setTotalScore(int totalScore) {
        this.totalScore = totalScore;
    }

    public int getNumberPass() {
        return numberPass;
    }

    public void setNumberPass(int numberPass) {
        this.numberPass = numberPass;
    }

    public int getCurrentIndex() {
        return currentIndex;
    }

    public void setCurrentIndex(int currentIndex) {
        this.currentIndex = currentIndex;
    }

    public String getNameImage() {
        return nameImage;
    }

    public void setNameImage(String nameImage) {
        this.nameImage = nameImage;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.questionCategoryID);
        dest.writeString(this.questDes);
        dest.writeString(this.questName);
        dest.writeInt(this.numberQuest);
        dest.writeInt(this.totalScore);
        dest.writeInt(this.numberPass);
        dest.writeInt(this.currentIndex);
        dest.writeString(this.nameImage);
    }

    public void readFromParcel(Parcel source) {
        this.questionCategoryID = source.readInt();
        this.questDes = source.readString();
        this.questName = source.readString();
        this.numberQuest = source.readInt();
        this.totalScore = source.readInt();
        this.numberPass = source.readInt();
        this.currentIndex = source.readInt();
        this.nameImage = source.readString();
    }

    public QuestionCategory() {
    }

    protected QuestionCategory(Parcel in) {
        this.questionCategoryID = in.readInt();
        this.questDes = in.readString();
        this.questName = in.readString();
        this.numberQuest = in.readInt();
        this.totalScore = in.readInt();
        this.numberPass = in.readInt();
        this.currentIndex = in.readInt();
        this.nameImage = in.readString();
    }

    public static final Creator<QuestionCategory> CREATOR = new Creator<QuestionCategory>() {
        @Override
        public QuestionCategory createFromParcel(Parcel source) {
            return new QuestionCategory(source);
        }

        @Override
        public QuestionCategory[] newArray(int size) {
            return new QuestionCategory[size];
        }
    };
}
