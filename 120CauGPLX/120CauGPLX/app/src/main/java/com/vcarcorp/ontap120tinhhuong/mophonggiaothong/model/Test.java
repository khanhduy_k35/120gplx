package com.vcarcorp.ontap120tinhhuong.mophonggiaothong.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by duy on 11/1/17.
 */

public class Test implements Parcelable {
    private int idTest;
    private int currentQuest;
    private int totalScore;
    private boolean isFinish;
    private String testName;
    private int countQuestion;

    public int getIdTest() {
        return idTest;
    }

    public void setIdTest(int idTest) {
        this.idTest = idTest;
    }

    public int getCurrentQuest() {
        return currentQuest;
    }

    public void setCurrentQuest(int currentQuest) {
        this.currentQuest = currentQuest;
    }

    public int getTotalScore() {
        return totalScore;
    }

    public void setTotalScore(int totalScore) {
        this.totalScore = totalScore;
    }

    public boolean isFinish() {
        return isFinish;
    }

    public void setFinish(boolean finish) {
        isFinish = finish;
    }

    public String getTestName() {
        return "Bài thi thử " + getIdTest();
    }

    public void setTestName(String testName) {
        this.testName = testName;
    }

    public int getCountQuestion() {
        return countQuestion;
    }

    public void setCountQuestion(int countQuestion) {
        this.countQuestion = countQuestion;
    }

    public Test() {
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.idTest);
        dest.writeInt(this.currentQuest);
        dest.writeInt(this.totalScore);
        dest.writeByte(this.isFinish ? (byte) 1 : (byte) 0);
        dest.writeString(this.testName);
        dest.writeInt(this.countQuestion);
    }

    public void readFromParcel(Parcel source) {
        this.idTest = source.readInt();
        this.currentQuest = source.readInt();
        this.totalScore = source.readInt();
        this.isFinish = source.readByte() != 0;
        this.testName = source.readString();
        this.countQuestion = source.readInt();
    }

    protected Test(Parcel in) {
        this.idTest = in.readInt();
        this.currentQuest = in.readInt();
        this.totalScore = in.readInt();
        this.isFinish = in.readByte() != 0;
        this.testName = in.readString();
        this.countQuestion = in.readInt();
    }

    public static final Creator<Test> CREATOR = new Creator<Test>() {
        @Override
        public Test createFromParcel(Parcel source) {
            return new Test(source);
        }

        @Override
        public Test[] newArray(int size) {
            return new Test[size];
        }
    };
}