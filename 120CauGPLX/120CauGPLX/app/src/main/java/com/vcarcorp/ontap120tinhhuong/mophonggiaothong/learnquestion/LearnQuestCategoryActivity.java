package com.vcarcorp.ontap120tinhhuong.mophonggiaothong.learnquestion;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ads.control.admob.AdCloseListener;
import com.ads.control.admob.AdmobHelp;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.main.GuideLearnActivity;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.util.DialogUtil;
import com.rey.material.widget.FrameLayout;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.R;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.adapter.QuestionCategoryAdapter;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.database.GPLXDataManager;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.main.BaseActivity;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.model.QuestionCategory;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.util.Util;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.util.AppUtils;

import java.util.ArrayList;

/**
 * Created by duy on 10/23/17.
 */

public class LearnQuestCategoryActivity extends BaseActivity {
    private ArrayList<QuestionCategory> questionCategories;
    private GPLXDataManager dataManager;
    private RecyclerView recyclerView;
    private QuestionCategoryAdapter questionCategoryAdapter;
    private int posClick;

    @SuppressLint("SourceLockedOrientationActivity")
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.learning_category_screen);
        setUpToolBar();
        Window window = this.getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | 0);
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(Color.TRANSPARENT);
        }
        dataManager =  new GPLXDataManager(this);
        recyclerView = (RecyclerView) findViewById(R.id.list);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        questionCategories = dataManager.getQuestionCategory();
        questionCategoryAdapter = new QuestionCategoryAdapter(questionCategories,this);


        recyclerView.setAdapter(questionCategoryAdapter);
    }

    private QuestionCategory questionCategoryClick;

    public void goToLearn(QuestionCategory questionCategory,int pos){
        AdmobHelp.getInstance().showInterstitialAd( new AdCloseListener() {
            @Override
            public void onAdClosed(boolean haveAds) {
                questionCategoryClick = questionCategory;
                posClick = pos;
                Intent i = new Intent(LearnQuestCategoryActivity.this, LearnQuestActivity.class);
                Bundle bundle = new Bundle();
                bundle.putParcelable("question_category", questionCategory);
                i.putExtras(bundle);
                startActivity(i);
                if(haveAds) {
                    checkShowSuggestProDisplayAds();
                }
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        if(questionCategoryClick!=null) {
            dataManager.getQuestionCategoryById(questionCategoryClick);
            questionCategoryAdapter.notifyItemChanged(this.posClick);
            questionCategoryClick = null;
        }
    }

    public void setUpToolBar(){
        setUpToolBar((FrameLayout)findViewById(R.id.btn_back));
        setUpToolBar((FrameLayout)findViewById(R.id.btn_guide));
        findViewById(R.id.btn_back).setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                onBackPressed();
            }
        });
        setUpToolBar((FrameLayout)findViewById(R.id.btn_reset));
        findViewById(R.id.btn_reset).setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                showDialogReset();
            }
        });
        if(findViewById(R.id.btn_guide)!=null) {
            findViewById(R.id.btn_guide).setOnClickListener(new OnSingleClickListener() {
                @Override
                public void onSingleClick(View v) {
                    AdmobHelp.getInstance().showInterstitialAd( new AdCloseListener() {
                        @Override
                        public void onAdClosed(boolean haveAds) {
                            Intent i = new Intent(LearnQuestCategoryActivity.this, GuideLearnActivity.class);
                            startActivity(i);
                            if(haveAds) {
                                checkShowSuggestProDisplayAds();
                            }
                        }
                    });
                }
            });
        }
        if(!AppUtils.checkPortraitMode(this)){
            android.widget.RelativeLayout rltList = findViewById(R.id.rltList);
            android.widget.RelativeLayout.LayoutParams layoutParams = (android.widget.RelativeLayout.LayoutParams) rltList.getLayoutParams();
            final int width = AppUtils.getWidthScreen(LearnQuestCategoryActivity.this) > AppUtils.getHeightScreen(LearnQuestCategoryActivity.this) ? AppUtils.getHeightScreen(LearnQuestCategoryActivity.this) : AppUtils.getWidthScreen(LearnQuestCategoryActivity.this);
            rltList.postDelayed(new Runnable() {
                @Override
                public void run() {
                    layoutParams.width = width;
                    rltList.setLayoutParams(layoutParams);
                    rltList.invalidate();
                    rltList.getParent().requestLayout();
                }
            },10);
        }
    }

    public void showDialogReset(){
        new DialogUtil.Builder(LearnQuestCategoryActivity.this)
                .title("Reset tất cả bài học")
                .content("Bạn có muốn xóa tất cả các câu đã học. Nếu bạn chọn \"Đồng ý\" tất cả các câu đã học sẽ bị xóa và bạn sẽ phải học lại từ đầu.")
                .positiveText("Huỷ bỏ")
                .negativeText("Đồng ý")
                .onPositive(new DialogUtil.SingleButtonCallback() {
                    @Override
                    public void onClick() {

                    }
                })
                .onNegative(new DialogUtil.SingleButtonCallback() {
                    @Override
                    public void onClick() {
                        if(!Util.isPro(LearnQuestCategoryActivity.this)){
                            new DialogUtil.Builder(LearnQuestCategoryActivity.this)
                                    .title("Reset tất cả bài học")
                                    .content("Bạn cần nâng cấp để sử dụng tính năng này và nhiều tính năng cao cấp khác.")
                                    .doneText(getString(R.string.text_ok))
                                    .onDone(new DialogUtil.SingleButtonCallback() {
                                        @Override
                                        public void onClick() {
                                            buyPro();
                                        }
                                    })
                                    .show();
                        }else{
                            dataManager.updateAllQuestion();
                            questionCategories = dataManager.getQuestionCategory();
                            questionCategoryAdapter.setQuestionCategories(questionCategories);
                            for(int i = 0; i < questionCategories.size() ; i++){
                                questionCategoryAdapter.notifyItemChanged(i);
                            }
                        }
                    }
                })
                .show();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            dataManager.close();
        }catch (Exception e){

        }

    }
}
