package com.vcarcorp.ontap120tinhhuong.mophonggiaothong.main;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.text.Spannable;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;

import com.ads.control.admob.AdCloseListener;
import com.ads.control.admob.AdmobHelp;
import com.ads.control.rate.Rate;
import com.airbnb.lottie.LottieAnimationView;
import com.bumptech.glide.Glide;
import com.pricecar.activity.CarMainActivity;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.moredriver.PenaltyActivity;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.moredriver.testcenter.TestCenterActivity;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.moredriver.trainingground.TrainingGroundActivity;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.taplo.TaploCategory;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.util.customview.CustomSquareProgress;
import com.rey.material.widget.FrameLayout;
import com.rey.material.widget.LinearLayout;
import com.rey.material.widget.RelativeLayout;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.R;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.database.GPLXDataManager;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.download.DownloadListActivity;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.exampractice.ExamQuestActivity;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.exampractice.ExamTestCategory;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.exampractice.ResultActivity;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.reminder.AlarmManagerSetup;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.learnquestion.LearnQuestActivity;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.learnquestion.LearnQuestCategoryActivity;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.marked.MakerQuestActivity;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.model.QuestionCategory;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.model.QuestionVideo;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.model.Test;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.tips.TipsActivity;
import com.ads.control.customview.CustomTextView;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.util.DialogUtil;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.util.SharedPreferencesUtils;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.util.Util;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.util.AppUtils;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.wrong.WrongQuestAcitivty;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Random;

public class MainActivity extends BaseActivity {
    private LinearLayout iconHome1, iconHome2, iconHome3, iconHome4, iconHome5, iconHome6;
    private boolean isShowPro = false;
    private DrawerLayout drawer;
    private LinearLayout lnEmail, lnOther, lnShare, lnGuide, lnReview, lnClass, lnSetting, lnSettingVoice;
    private CustomTextView tv_index, tv_tip, tv_tip_name;
    private ImageView imgDesAnswer;
    private GPLXDataManager dataManager;
    private QuestionVideo questionVideo;
    private LinearLayout rltTip;
    private CustomTextView tv_sub_1;//,tv_sub_3, tv_sub_4;
    private RelativeLayout rltClickDownload;
    private FrameLayout btn_platium;
    private CustomSquareProgress progressLearn;
    private CustomTextView tvCheer, tvPercentPass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(SharedPreferencesUtils.showReminder()){
            AlarmManagerSetup.scheduleAlarmReceiverDiaryReminder(this);
        }
        setContentView(R.layout.activity_main_view);
        Window window = this.getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | 0);
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(Color.TRANSPARENT);
        }

        dataManager = new GPLXDataManager(this);
        tvCheer = (CustomTextView) findViewById(R.id.tv_cheer);
        tvPercentPass = (CustomTextView) findViewById(R.id.tv_percent_pass);
        progressLearn = (CustomSquareProgress) findViewById(R.id.progress_learn);
        progressLearn.setShouldShowBackground(true);
        initView();
        if (SharedPreferencesUtils.getBoolean("newapp_redirect")) {
            if (!isShowPro) {
                showDialogDownloadNewApp();
            }
        } else {
            try {
                Date c = Calendar.getInstance().getTime();
                System.out.println("Current time => " + c);

                SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
                String currentDate = df.format(c);
                String preDate = SharedPreferencesUtils.getString("current_date") == null ? "" : SharedPreferencesUtils.getString("current_date");
                if (!preDate.equals(currentDate) && !preDate.isEmpty()) {
                    SharedPreferencesUtils.setString("current_date", currentDate);
                    if (SharedPreferencesUtils.getBoolean("other_redirect")) {
                        if (!isShowPro) {
                            showDialogDownloadMoreApp(false);
                        }
                    } else {
                        if(!Util.isPro(this))
                            buyProDialog();
                    }
                }
                if(preDate!=null && preDate.isEmpty()){
                    SharedPreferencesUtils.setString("current_date", currentDate);
                }
            } catch (Exception e) {

            }
        }

        if (!SharedPreferencesUtils.getBoolean("show_settings", false)) {
            if(AppUtils.checkPortraitMode(this)) {
                showDiglogSuggestSettings();
                SharedPreferencesUtils.setBoolean("show_settings", true);
            }
        }

        if(!AppUtils.checkPortraitMode(this)){
            android.widget.RelativeLayout buttonCuttingDiet = findViewById(R.id.buttonCuttingDiet);
            android.widget.RelativeLayout.LayoutParams layoutParams = (android.widget.RelativeLayout.LayoutParams) buttonCuttingDiet.getLayoutParams();
            final int width = AppUtils.getWidthScreen(MainActivity.this) > AppUtils.getHeightScreen(MainActivity.this) ? AppUtils.getHeightScreen(MainActivity.this) : AppUtils.getWidthScreen(MainActivity.this);
            buttonCuttingDiet.postDelayed(new Runnable() {
                @Override
                public void run() {
                    layoutParams.width = width;
                    buttonCuttingDiet.setLayoutParams(layoutParams);
                    buttonCuttingDiet.invalidate();
                    buttonCuttingDiet.getParent().requestLayout();
                }
            },10);
            CardView cardView = findViewById(R.id.cardView);
            android.widget.RelativeLayout.LayoutParams layoutParams1 = (android.widget.RelativeLayout.LayoutParams) cardView.getLayoutParams();
            cardView.postDelayed(new Runnable() {
                @Override
                public void run() {
                    layoutParams1.width = width;
                    cardView.setLayoutParams(layoutParams1);
                    cardView.invalidate();
                    cardView.getParent().requestLayout();
                }
            },10);
        }

        LottieAnimationView animView = findViewById(R.id.animView);
        if (SharedPreferencesUtils.isNightMode()) {
            if (animView != null)
                animView.setAnimation(R.raw.info_icon);
        } else {
            if (animView != null)
                animView.setAnimation(R.raw.info_icon_black);
        }
        if(Util.isPro(this)){
            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams)( (FrameLayout) findViewById(R.id.btn_guide)).getLayoutParams();
            params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
            ( (FrameLayout) findViewById(R.id.btn_guide)).setLayoutParams(params);
        }
        if(SharedPreferencesUtils.getUpdateQuestionID()!=null && !SharedPreferencesUtils.getUpdateQuestionID().isEmpty()){
            setUpToolBar((FrameLayout) findViewById(R.id.btn_guide));
            if (findViewById(R.id.btn_guide) != null) {
                findViewById(R.id.btn_guide).setOnClickListener(new OnSingleClickListener() {
                    @Override
                    public void onSingleClick(View v) {
                       showDialogUpdateQuestion();
                    }
                });
            }
            ((FrameLayout) findViewById(R.id.btn_guide)).setVisibility(View.VISIBLE);
        }else{
            ((FrameLayout) findViewById(R.id.btn_guide)).setVisibility(View.GONE);
        }

        if (findViewById(R.id.container_ads_native) != null)
            findViewById(R.id.container_ads_native).setVisibility(View.VISIBLE);
        if (!Util.isPro(this) && Util.isDisplayAds) {
            if (findViewById(R.id.container_ads_native) != null)
                AdmobHelp.getInstance().loadNativeActivity(this,true);
        } else {
            if (findViewById(R.id.container_ads_native) != null)
                findViewById(R.id.container_ads_native).setVisibility(View.GONE);
        }

        if (getIntent() != null && getIntent().getExtras() != null) {
            int actionApp = getIntent().getExtras().getInt("ACTION_APP", -1);
            Log.e("quang.phungvan", "" + actionApp);
            if (actionApp == 0) {
                // Lam de thi dang lam do
                if (SharedPreferencesUtils.getInt("CURRENT_TEST") != -1) {
                    Test test = dataManager.getTestWithID(SharedPreferencesUtils.getInt("CURRENT_TEST"));
                    if (test.isFinish()) {
                        Intent intent = new Intent(MainActivity.this, ResultActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putParcelable("test", test);
                        intent.putExtras(bundle);
                        startActivity(intent);
                    } else {
                        Intent i = new Intent(MainActivity.this, ExamQuestActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putParcelable("test", test);
                        i.putExtras(bundle);
                        startActivity(i);
                    }
                } else {
                    Intent intent = new Intent(MainActivity.this, ExamTestCategory.class);
                    startActivity(intent);
                }
            } else if (actionApp == 1) {
                // Vao man cac cau sai
                int countMarked = dataManager.countTotalQuestionWrong();
                if (countMarked > 0) {
                    AdmobHelp.getInstance().showInterstitialAd( new AdCloseListener() {
                        @Override
                        public void onAdClosed(boolean haveAds) {
                            Intent intent = new Intent(MainActivity.this, WrongQuestAcitivty.class);
                            startActivity(intent);
                            if(haveAds) {
                                checkShowSuggestProDisplayAds();
                            }
                        }
                    });
                } else {
                    new DialogUtil.Builder(MainActivity.this)
                            .title(getString(R.string.learn_by_wrong_dialog_title))
                            .content(getString(R.string.learn_by_wrong_dialog_content))
                            .doneText(getResources().getString(R.string.text_ok))
                            .onDone(new DialogUtil.SingleButtonCallback() {
                                @Override
                                public void onClick() {

                                }
                            })
                            .show();
                }
            } else if (actionApp == 2) {
                // vao danh sach chu de
                Intent intent = new Intent(MainActivity.this, LearnQuestCategoryActivity.class);
                startActivity(intent);
            } else if (actionApp == 3) {
                // vao danh sach test
                Intent intent = new Intent(MainActivity.this, ExamTestCategory.class);
                startActivity(intent);
            }
        }
        findViewById(R.id.btn_iap).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buyPro();
            }
        });
        if(Util.isPro(this)){
            ((View) findViewById(R.id.btn_iap).getParent()).setVisibility(View.GONE);
        }else{
            ((View) findViewById(R.id.btn_iap).getParent()).setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if(SharedPreferencesUtils.getUpdateQuestionID()!=null && !SharedPreferencesUtils.getUpdateQuestionID().isEmpty()){
            setUpToolBar((FrameLayout) findViewById(R.id.btn_guide));
            if (findViewById(R.id.btn_guide) != null) {
                findViewById(R.id.btn_guide).setOnClickListener(new OnSingleClickListener() {
                    @Override
                    public void onSingleClick(View v) {
                        showDialogUpdateQuestion();
                    }
                });
            }
            ((FrameLayout) findViewById(R.id.btn_guide)).setVisibility(View.VISIBLE);
        }else{
            ((FrameLayout) findViewById(R.id.btn_guide)).setVisibility(View.GONE);
        }
        TextView tv_title = findViewById(R.id.tv_title);
        int index = (new Random()).nextInt(Util.KHAUHIEU.length);
        if (index < Util.KHAUHIEU.length) {
            tv_title.setText(Util.KHAUHIEU[index]);
        } else {
            tv_title.setText("Muốn nhanh thì phải từ từ - Đi thong thả cho đỡ vất vả.");
        }
        tv_sub_1 = (CustomTextView) findViewById(R.id.tv_sub_1);
        tv_index = (CustomTextView) findViewById(R.id.tv_index);
        tv_tip = (CustomTextView) findViewById(R.id.tv_tip);
        tv_tip_name = findViewById(R.id.tv_tip_name);
        rltTip = findViewById(R.id.rltTip);

        int totalTest = dataManager.countTotalTest();
        tv_sub_1.setText("Thi thử\n" + totalTest + " bộ đề");
        questionVideo = dataManager.getRandomQuestionVideo(MainActivity.this);
        if(questionVideo!=null){
            tv_index.setText(questionVideo.getName());
            tv_tip.setText(questionVideo.getImageDescription());
            tv_tip_name.setText(questionVideo.getDescription());
            File fileImgDetail = new File(getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS).toString() + "/img_app/doneimg/" + "TH" + questionVideo.getId() + ".webp");
            if(fileImgDetail.exists()){
                Uri imageUri = Uri.fromFile(fileImgDetail);
                Glide.with(this)
                        .load(imageUri)
                        .into(imgDesAnswer);
            }else{
                Glide.with(this)
                        .load(Uri.parse(AppUtils.getServerImg(this,questionVideo)))
                        .into(imgDesAnswer);
            }
            rltTip.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    QuestionCategory questionCategory = dataManager.getQuestionCategoryById(questionVideo.getCategoryID());
                    Intent i = new Intent(MainActivity.this, LearnQuestActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putParcelable("question_category", questionCategory);
                    bundle.putParcelable("question",questionVideo);
                    i.putExtras(bundle);
                    startActivity(i);
                }
            });
        }
        Handler handler = new Handler();
        new Thread(new Runnable() {
            public void run() {
                handler.post(new Runnable() {
                    public void run() {
                        int percentLearn = dataManager.getTotalQuestCorrect() * 40 / 120;
                        int percentTest = dataManager.getTotalDefaultTestPass() * 30 / 12;
                        int countTestMore = dataManager.getTotalDefaultTestRandom();
                        if (countTestMore > 6) {
                            countTestMore = 30;
                        } else {
                            countTestMore = countTestMore * 30 / 6;
                        }

                        int finalPercentMore = percentLearn + percentTest + countTestMore;
                        progressLearn.setProgress(finalPercentMore);
                        tvPercentPass.setText((finalPercentMore) + "%");
                        if ((finalPercentMore) < 40) {
                            tvPercentPass.setTextColor(getColor(R.color.colorRedPrimary));
                            tvCheer.setTextColor(getColor(R.color.colorRedPrimary));
                            progressLearn.setPaintColor("#EF5350");
                            tvCheer.setText("BẠN CẦN NỖ LỰC HƠN NỮA!");
                        } else if ((finalPercentMore) < 70) {
                            tvPercentPass.setTextColor(getColor(R.color.color_orange_1));
                            tvCheer.setTextColor(getColor(R.color.color_orange_1));
                            progressLearn.setPaintColor("#f57c00");
                            tvCheer.setText("CỐ LÊN BẠN ƠI!");
                        } else if ((finalPercentMore) < 85) {
                            tvPercentPass.setTextColor(getColor(R.color.colorPrimary));
                            tvCheer.setTextColor(getColor(R.color.colorPrimary));
                            progressLearn.setPaintColor("#00C6C1");
                            tvCheer.setText("HÃY CỐ GẮNG THÊM CHÚT NỮA!");
                        } else {
                            tvPercentPass.setTextColor(getColor(R.color.color_text_main));
                            tvCheer.setTextColor(getColor(R.color.color_text_main));
                            progressLearn.setPaintColor("#298ae8");
                            tvCheer.setText("BẠN ĐÃ ÔN TẬP RẤT TỐT!");
                        }
                    }
                });
            }
        }).start();

        if(isPackageInstalled("com.gplx.oto.b2.gplx600",getPackageManager())){
            findViewById(R.id.btn_ads_app).setVisibility(View.GONE);
            findViewById(R.id.btn_law).setVisibility(View.VISIBLE);
        }else{
            findViewById(R.id.btn_ads_app).setVisibility(View.VISIBLE);
            findViewById(R.id.btn_law).setVisibility(View.GONE);
        }
        findViewById(R.id.btn_ads_app).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String appPackageName = "com.gplx.oto.b2.gplx600";
                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                } catch (android.content.ActivityNotFoundException anfe) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                }
            }
        });
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    public void initView() {
        btn_platium = (FrameLayout) findViewById(R.id.btn_platium);
        rltClickDownload = (RelativeLayout) findViewById(R.id.rltClickDownload);
        tv_index = (CustomTextView) findViewById(R.id.tv_index);
        tv_tip = (CustomTextView) findViewById(R.id.tv_tip);
        tv_tip_name = findViewById(R.id.tv_tip_name);
        rltTip = findViewById(R.id.rltTip);
        imgDesAnswer = (ImageView) findViewById(R.id.imgDesAnswer);
        lnEmail = (LinearLayout) findViewById(R.id.lnEmail);
        lnShare = (LinearLayout) findViewById(R.id.lbShare);
        lnOther = (LinearLayout) findViewById(R.id.lnOther);
        lnGuide = (LinearLayout) findViewById(R.id.lnGuide);
        lnClass = (LinearLayout) findViewById(R.id.lnClass);
        lnReview = (LinearLayout) findViewById(R.id.lnReview);
        lnSettingVoice = (LinearLayout) findViewById(R.id.lnSettingsVoice);
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        iconHome1 = (LinearLayout) findViewById(R.id.btn_test);
        iconHome2 = (LinearLayout) findViewById(R.id.btn_learn);
        iconHome3 = (LinearLayout) findViewById(R.id.btn_sign);
        iconHome4 = (LinearLayout) findViewById(R.id.btn_wrong_question);
        iconHome5 = (LinearLayout) findViewById(R.id.btn_tip);
        iconHome6 = (LinearLayout) findViewById(R.id.btn_law);
        lnSetting = (LinearLayout) findViewById(R.id.lnSettings);
        tv_sub_1 = (CustomTextView) findViewById(R.id.tv_sub_1);
//        tv_sub_3 = (CustomTextView) findViewById(R.id.tv_sub_3);
//        tv_sub_4 = (CustomTextView) findViewById(R.id.tv_sub_4);
        setUpToolBar(btn_platium);
        ((LinearLayout) findViewById(R.id.lnIap)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this,IapActivity.class));
            }
        });
        if(Util.isPro(this)){
            btn_platium.setVisibility(View.GONE);
        }else{
            btn_platium.setVisibility(View.VISIBLE);
        }
        btn_platium.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                buyPro();
            }
        });

        if(rltClickDownload!=null){
            rltClickDownload.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    new DialogUtil.Builder(MainActivity.this)
                            .title("Tải trước bài học và hình ảnh chất lượng cao")
                            .content("Tải trước toàn bộ hình ảnh và video cho phép bạn sử dụng đầy đủ chức năng của ứng dụng và học ngay cả khi không có kết nối mạng.")
                            .positiveText(getString(R.string.text_ok))
                            .negativeText(getString(R.string.text_cancel))
                            .onPositive(new DialogUtil.SingleButtonCallback() {
                                @Override
                                public void onClick() {
                                    AdmobHelp.getInstance().showInterstitialAd( new AdCloseListener() {
                                        @Override
                                        public void onAdClosed(boolean haveAds) {
                                            Intent intent = new Intent(MainActivity.this, DownloadListActivity.class);
                                            startActivity(intent);
                                            if(haveAds) {
                                                checkShowSuggestProDisplayAds();
                                            }
                                        }
                                    });
                                }
                            })
                            .show();
                }
            });
        }

        iconHome1.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View view) {
                AdmobHelp.getInstance().showInterstitialAd( new AdCloseListener() {
                    @Override
                    public void onAdClosed(boolean haveAds) {
                        Intent intent = new Intent(MainActivity.this, ExamTestCategory.class);
                        startActivity(intent);
                        if(haveAds) {
                            checkShowSuggestProDisplayAds();
                        }
                    }
                });
            }
        });
        iconHome2.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View view) {
                AdmobHelp.getInstance().showInterstitialAd( new AdCloseListener() {
                    @Override
                    public void onAdClosed(boolean haveAds) {
                        Intent intent = new Intent(MainActivity.this, LearnQuestCategoryActivity.class);
                        startActivity(intent);
                        if(haveAds) {
                            checkShowSuggestProDisplayAds();
                        }
                    }
                });
            }
        });
        iconHome4.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View view) {
                int countMarked = dataManager.countTotalQuestionWrong();
                if (countMarked > 0) {
                    AdmobHelp.getInstance().showInterstitialAd( new AdCloseListener() {
                        @Override
                        public void onAdClosed(boolean haveAds) {
                            Intent intent = new Intent(MainActivity.this, WrongQuestAcitivty.class);
                            startActivity(intent);
                            if(haveAds) {
                                checkShowSuggestProDisplayAds();
                            }
                        }
                    });
                } else {
                    new DialogUtil.Builder(MainActivity.this)
                            .title(getString(R.string.learn_by_wrong_dialog_title))
                            .content(getString(R.string.learn_by_wrong_dialog_content))
                            .doneText(getResources().getString(R.string.text_ok))
                            .onDone(new DialogUtil.SingleButtonCallback() {
                                @Override
                                public void onClick() {

                                }
                            })
                            .show();
                }
            }
        });
        iconHome3.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View view) {
                int countMarked = dataManager.countTotalQuestionMarked();
                if (countMarked > 0) {
                    AdmobHelp.getInstance().showInterstitialAd( new AdCloseListener() {
                        @Override
                        public void onAdClosed(boolean haveAds) {
                            Intent intent = new Intent(MainActivity.this, MakerQuestActivity.class);
                            startActivity(intent);
                            if(haveAds) {
                                checkShowSuggestProDisplayAds();
                            }
                        }
                    });
                } else {
                    new DialogUtil.Builder(MainActivity.this)
                            .title(getString(R.string.question_marked))
                            .content(getString(R.string.no_question_marked))
                            .doneText(getResources().getString(R.string.text_ok))
                            .onDone(new DialogUtil.SingleButtonCallback() {
                                @Override
                                public void onClick() {

                                }
                            })
                            .show();
                }
            }
        });
        iconHome5.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View view) {
                AdmobHelp.getInstance().showInterstitialAd( new AdCloseListener() {
                    @Override
                    public void onAdClosed(boolean haveAds) {
                        Intent intent = new Intent(MainActivity.this, TipsActivity.class);
                        startActivity(intent);
                        if(haveAds) {
                            checkShowSuggestProDisplayAds();
                        }
                    }
                });
            }
        });
        iconHome6.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View view) {
                Intent intent = new Intent(MainActivity.this, SelectSoundActivity.class);
                startActivity(intent);
            }
        });

        lnSetting.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                if (drawer.isDrawerOpen(GravityCompat.START)) {
                    drawer.closeDrawer(GravityCompat.START);
                } else if (drawer.isDrawerOpen(GravityCompat.END)) {
                    drawer.closeDrawer(GravityCompat.END);
                }
                startActivity(new Intent(MainActivity.this, SettingsActivity.class));
            }
        });

        FrameLayout btn_menu = findViewById(R.id.btn_menu);
        setUpToolBar(btn_menu);
        btn_menu.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                if (drawer != null) {
                    drawer.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            drawer.openDrawer(GravityCompat.START);
                        }
                    }, 10);
                }
            }
        });
        TextView tv_title = findViewById(R.id.tv_title);
        int index = (new Random()).nextInt(Util.KHAUHIEU.length);
        if (index < Util.KHAUHIEU.length) {
            tv_title.setText(Util.KHAUHIEU[index]);
        } else {
            tv_title.setText("Muốn nhanh thì phải từ từ - Đi thong thả cho đỡ vất vả.");
        }
        lnReview.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                if (drawer.isDrawerOpen(GravityCompat.START)) {
                    drawer.closeDrawer(GravityCompat.START);
                } else if (drawer.isDrawerOpen(GravityCompat.END)) {
                    drawer.closeDrawer(GravityCompat.END);
                }
                openRating();
            }
        });
        lnGuide.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                if (drawer.isDrawerOpen(GravityCompat.START)) {
                    drawer.closeDrawer(GravityCompat.START);
                } else if (drawer.isDrawerOpen(GravityCompat.END)) {
                    drawer.closeDrawer(GravityCompat.END);
                }
                Intent intent = new Intent(MainActivity.this, GuideLineScreen.class);
                intent.putExtra("back", "true");
                MainActivity.this.startActivity(intent);
            }
        });

        lnClass.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                if (drawer.isDrawerOpen(GravityCompat.START)) {
                    drawer.closeDrawer(GravityCompat.START);
                } else if (drawer.isDrawerOpen(GravityCompat.END)) {
                    drawer.closeDrawer(GravityCompat.END);
                }
            }
        });

        lnSettingVoice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (drawer.isDrawerOpen(GravityCompat.START)) {
                    drawer.closeDrawer(GravityCompat.START);
                } else if (drawer.isDrawerOpen(GravityCompat.END)) {
                    drawer.closeDrawer(GravityCompat.END);
                }
                Intent intent = new Intent(MainActivity.this, SelectSoundActivity.class);
                startActivity(intent);
            }
        });

        lnOther.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                if (drawer.isDrawerOpen(GravityCompat.START)) {
                    drawer.closeDrawer(GravityCompat.START);
                } else if (drawer.isDrawerOpen(GravityCompat.END)) {
                    drawer.closeDrawer(GravityCompat.END);
                }
                try {
                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    intent.setData(Uri.parse("market://developer?id=" + getString(R.string.store)));
                    startActivity(intent);
                }catch (Exception e){
                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    intent.setData(Uri.parse("https://play.google.com/store/apps/developer?id=vcarcorp"));
                    startActivity(intent);
                }
            }
        });
        lnShare.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                if (drawer.isDrawerOpen(GravityCompat.START)) {
                    drawer.closeDrawer(GravityCompat.START);
                } else if (drawer.isDrawerOpen(GravityCompat.END)) {
                    drawer.closeDrawer(GravityCompat.END);
                }
                final String appPackageName = getPackageName();
                try {
                    Intent i = new Intent(Intent.ACTION_SEND);
                    i.setType("text/plain");
                    i.putExtra(Intent.EXTRA_SUBJECT, "Học bằng lái xe");
                    String sAux = "\nHọc lý thuyết, thi thử lý thuyết dễ dàng với ứng dụng \"Học bằng lái xe\"\n\n";
                    sAux = sAux + "https://play.google.com/store/apps/details?id=" + appPackageName;
                    i.putExtra(Intent.EXTRA_TEXT, sAux);
                    startActivity(Intent.createChooser(i, "Chia sẻ ứng dụng"));
                } catch(Exception e) {
                }
            }
        });
        lnEmail.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                if (drawer.isDrawerOpen(GravityCompat.START)) {
                    drawer.closeDrawer(GravityCompat.START);
                } else if (drawer.isDrawerOpen(GravityCompat.END)) {
                    drawer.closeDrawer(GravityCompat.END);
                }
                Intent mailer = new Intent(Intent.ACTION_SEND);
                mailer.setType("text/plain");
                mailer.putExtra(Intent.EXTRA_EMAIL, new String[]{getString(R.string.email_feedback)});
                mailer.putExtra(Intent.EXTRA_SUBJECT, "Hỗ trợ câu hỏi lý thuyết");
                mailer.putExtra(Intent.EXTRA_TEXT, "Bạn giải thích giúp mình câu số..");
                startActivity(Intent.createChooser(mailer, "Hỗ trợ qua email..."));
            }
        });

        imgDesAnswer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Util.showDialogTipForQuestion(MainActivity.this, questionVideo);
            }
        });
        findViewById(R.id.btn_info).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                View customView = getLayoutInflater().inflate(R.layout.info_pass_layout, null);
                TextView tv1 = customView.findViewById(R.id.tv1);
                Spannable spanText = Spannable.Factory.getInstance().newSpannable("- Học qua tất cả các tình huống (tối thiểu mỗi tình huống đạt 3 điểm) trong phần Ôn tập theo chủ đề (40%)");
                spanText.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.colorRedPrimary)), 100, 105, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                spanText.setSpan(new StyleSpan(Typeface.BOLD), 100, 105, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                tv1.setText(spanText, TextView.BufferType.SPANNABLE);

                TextView tv2 = customView.findViewById(R.id.tv2);
                Spannable spanText2 = Spannable.Factory.getInstance().newSpannable("- Vượt qua 12 đề thi mô phỏng mẫu trong phần Thi thử theo bộ đề (30%)");
                spanText2.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.colorRedPrimary)), 64, 69, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                spanText2.setSpan(new StyleSpan(Typeface.BOLD), 64, 69, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                tv2.setText(spanText2, TextView.BufferType.SPANNABLE);

                TextView tv3 = customView.findViewById(R.id.tv3);
                Spannable spanText3 = Spannable.Factory.getInstance().newSpannable("- Vượt qua 6 đề thi tạo ngẫu nhiên trong phần Thi thử theo bộ đề (30%)");
                spanText3.setSpan(new ForegroundColorSpan(getColor(R.color.colorRedPrimary)), 64, 69, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                spanText3.setSpan(new StyleSpan(Typeface.BOLD), 64, 69, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                tv3.setText(spanText3, TextView.BufferType.SPANNABLE);

                TextView tvTitle = customView.findViewById(R.id.tv_cheer);
                TextView tvPercent = customView.findViewById(R.id.tv_percent_pass);

                CustomSquareProgress progress = customView.findViewById(R.id.progress_learn);
                progress.setCornerRadius(96);

                int percentLearn = dataManager.getTotalQuestCorrect() * 40 / 120;
                int percentTest = dataManager.getTotalDefaultTestPass() * 30 / 12;
                int countTestMore = dataManager.getTotalDefaultTestRandom();
                if (countTestMore > 6) {
                    countTestMore = 30;
                } else {
                    countTestMore = countTestMore * 30 / 6;
                }

                int finalPercentMore = percentLearn + percentTest + countTestMore;
                tvPercent.setText((finalPercentMore) + "%");
                if ((finalPercentMore) < 50) {
                    tvPercent.setTextColor(getColor(R.color.colorRedPrimary));
                    tvTitle.setTextColor(getColor(R.color.colorRedPrimary));
                    progressLearn.setPaintColor("#EF5350");
                    tvTitle.setText("BẠN CẦN NỖ LỰC HƠN NỮA!");
                } else if ((finalPercentMore) < 80) {
                    tvPercent.setTextColor(getColor(R.color.color_orange_1));
                    tvTitle.setTextColor(getColor(R.color.color_orange_1));
                    progressLearn.setPaintColor("#f57c00");
                    tvTitle.setText("CỐ LÊN BẠN ƠI!");
                } else if ((finalPercentMore) < 95) {
                    tvPercent.setTextColor(getColor(R.color.colorPrimary));
                    tvTitle.setTextColor(getColor(R.color.colorPrimary));
                    progressLearn.setPaintColor("#00C6C1");
                    tvTitle.setText("HÃY CỐ GẮNG THÊM CHÚT NỮA!");
                } else {
                    tvPercent.setTextColor(getColor(R.color.color_text_main));
                    tvTitle.setTextColor(getColor(R.color.color_text_main));
                    progressLearn.setPaintColor("#298ae8");
                    tvTitle.setText("BẠN ĐÃ ÔN TẬP RẤT TỐT!");
                }

                new DialogUtil.Builder(MainActivity.this)
                        .setCustomView(customView)
                        .doneText("Đã hiểu")
                        .onDone(new DialogUtil.SingleButtonCallback() {
                            @Override
                            public void onClick() {
                            }
                        }).show();
                runOnUiThread(new Runnable() {
                    public void run() {
                        new Handler().post(new Runnable() {
                            public void run() {
                                progress.setProgress(finalPercentMore);
                            }
                        });

                    }
                });
            }
        });
        findViewById(R.id.btn_home_penalty).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AdmobHelp.getInstance().showInterstitialAd( new AdCloseListener() {
                    @Override
                    public void onAdClosed(boolean haveAds) {
                        startActivity(new Intent(MainActivity.this, PenaltyActivity.class));
                    }
                });
            }
        });
        findViewById(R.id.btn_home_test_center).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AdmobHelp.getInstance().showInterstitialAd( new AdCloseListener() {
                    @Override
                    public void onAdClosed(boolean haveAds) {
                        startActivity(new Intent(MainActivity.this, TestCenterActivity.class));
                    }
                });
            }
        });
        findViewById(R.id.btn_home_training_ground).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AdmobHelp.getInstance().showInterstitialAd( new AdCloseListener() {
                    @Override
                    public void onAdClosed(boolean haveAds) {
                        startActivity(new Intent(MainActivity.this, TrainingGroundActivity.class));
                    }
                });
            }
        });
        findViewById(R.id.btn_taplo).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AdmobHelp.getInstance().showInterstitialAd(new AdCloseListener() {
                    @Override
                    public void onAdClosed(boolean haveAds) {
                        startActivity(new Intent(MainActivity.this, TaploCategory.class));
                    }
                });
            }
        });
        findViewById(R.id.rltPriceCar).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AdmobHelp.getInstance().showInterstitialAd( new AdCloseListener() {
                    @Override
                    public void onAdClosed(boolean haveAds) {
                        startActivity(new Intent(MainActivity.this, CarMainActivity.class));
                    }
                });
            }
        });
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        // TODO Auto-generated method stub
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else if (drawer.isDrawerOpen(GravityCompat.END)) {
            drawer.closeDrawer(GravityCompat.END);
        } else {
            int count = getSupportFragmentManager().getBackStackEntryCount();
            if (count > 0) {
                super.onBackPressed();
            } else {
                if (SharedPreferencesUtils.getBoolean("other_redirect")) {
                    if (!isPackageInstalled(SharedPreferencesUtils.getString("other_package"), getPackageManager())) {
                        if (SharedPreferencesUtils.getInt("showValue", 0) % 2 == 0) {
                            int value = SharedPreferencesUtils.getInt("showValue", 0);
                            value++;
                            SharedPreferencesUtils.setInt("showValue", value);
                            Rate.Show(this, 1);
                        } else {
                            int value = SharedPreferencesUtils.getInt("showValue", 0);
                            value++;
                            SharedPreferencesUtils.setInt("showValue", value);
                            showDialogDownloadMoreApp(true);
                        }

                    } else {
                        Rate.Show(this, 1);
                    }
                } else {
                    Rate.Show(this, 1);
                }

            }
        }
    }

    public void openRating() {
        final String appPackageName = getPackageName();
        try {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
        } catch (android.content.ActivityNotFoundException anfe) {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            dataManager.close();
        } catch (Exception e) {

        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        List<Fragment> lstFragment = getSupportFragmentManager().getFragments();
        if (lstFragment != null && lstFragment.size() > 0) {
            for (int i = 0; i < lstFragment.size(); i++) {
            }
        }
    }


    public void showDialogDownloadNewApp() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Window window = dialog.getWindow();
        window.setBackgroundDrawableResource(android.R.color.transparent);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.customdialog_moreapp);
        final TextView btn_quit = (TextView) dialog.findViewById(R.id.btn_custom_dialog_exit_quit);
        final TextView btn_ok = (TextView) dialog.findViewById(R.id.btn_custom_dialog_exit_ok);
        final TextView tv_app_name = (TextView) dialog.findViewById(R.id.tv_app_name);
        final ImageView iconApp = (ImageView) dialog.findViewById(R.id.icon_app);
        final com.rey.material.widget.TextView btn_install = (com.rey.material.widget.TextView) dialog.findViewById(R.id.btn_install);
        final CustomTextView tv_description_1 = (CustomTextView) dialog.findViewById(R.id.tv_description_1);
        final CustomTextView tv_description_2 = (CustomTextView) dialog.findViewById(R.id.tv_description_2);
        final ImageView img_ads_1 = (ImageView) dialog.findViewById(R.id.img_ads_1);
        final ImageView img_ads_2 = (ImageView) dialog.findViewById(R.id.img_ads_2);
        final ImageView img_ads_3 = (ImageView) dialog.findViewById(R.id.img_ads_3);

        tv_app_name.setText(SharedPreferencesUtils.getString("newapp_name"));
        tv_description_1.setText(SharedPreferencesUtils.getString("newapp_des1"));
        tv_description_2.setText(SharedPreferencesUtils.getString("newapp_des2"));
        btn_quit.setVisibility(View.INVISIBLE);
        iconApp.setVisibility(View.GONE);
        Glide.with(this).load(SharedPreferencesUtils.getString("newapp_img1")).into(img_ads_1);
        Glide.with(this).load(SharedPreferencesUtils.getString("newapp_img2")).into(img_ads_2);
        Glide.with(this).load(SharedPreferencesUtils.getString("newapp_img3")).into(img_ads_3);

        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String appPackageName = SharedPreferencesUtils.getString("newapp_package");
                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                } catch (android.content.ActivityNotFoundException anfe) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                }
            }
        });
        btn_install.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String appPackageName = SharedPreferencesUtils.getString("newapp_package");
                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                } catch (android.content.ActivityNotFoundException anfe) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                }
            }
        });
        dialog.show();
    }

    public void showDialogDownloadMoreApp(final boolean isExit) {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Window window = dialog.getWindow();
        window.setBackgroundDrawableResource(android.R.color.transparent);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.customdialog_moreapp);
        final TextView btn_quit = (TextView) dialog.findViewById(R.id.btn_custom_dialog_exit_quit);
        final TextView btn_ok = (TextView) dialog.findViewById(R.id.btn_custom_dialog_exit_ok);
        final CustomTextView tv_app_name = (CustomTextView) dialog.findViewById(R.id.tv_app_name);
        final ImageView iconApp = (ImageView) dialog.findViewById(R.id.icon_app);
        final com.rey.material.widget.TextView btn_install = (com.rey.material.widget.TextView) dialog.findViewById(R.id.btn_install);
        final CustomTextView tv_description_1 = (CustomTextView) dialog.findViewById(R.id.tv_description_1);
        final CustomTextView tv_description_2 = (CustomTextView) dialog.findViewById(R.id.tv_description_2);
        final ImageView img_ads_1 = (ImageView) dialog.findViewById(R.id.img_ads_1);
        final ImageView img_ads_2 = (ImageView) dialog.findViewById(R.id.img_ads_2);
        final ImageView img_ads_3 = (ImageView) dialog.findViewById(R.id.img_ads_3);

        tv_app_name.setText(SharedPreferencesUtils.getString("other_name"));
        tv_description_1.setText(SharedPreferencesUtils.getString("other_des1"));
        tv_description_2.setText(SharedPreferencesUtils.getString("other_des2"));
        btn_quit.setVisibility(View.VISIBLE);
        iconApp.setVisibility(View.VISIBLE);
        Glide.with(this).load(SharedPreferencesUtils.getString("other_img1")).into(img_ads_1);
        Glide.with(this).load(SharedPreferencesUtils.getString("other_img2")).into(img_ads_2);
        Glide.with(this).load(SharedPreferencesUtils.getString("other_img3")).into(img_ads_3);
        Glide.with(this).load(SharedPreferencesUtils.getString("other_icon")).into(iconApp);

        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
                dialog.dismiss();
                final String appPackageName = SharedPreferencesUtils.getString("other_package");
                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                } catch (android.content.ActivityNotFoundException anfe) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                }
                finish();
            }
        });
        btn_install.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.cancel();
                dialog.dismiss();
                final String appPackageName = SharedPreferencesUtils.getString("other_package");
                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                } catch (android.content.ActivityNotFoundException anfe) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                }
                finish();
            }
        });
        if (isExit) {
            btn_quit.setText("Thoát");
        } else {
            btn_quit.setText("Cài đặt sau");
        }
        btn_quit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
                dialog.dismiss();
                if (isExit) {
                    finish();
                }
            }
        });

        dialog.show();
    }

    public void showDiglogSuggestSettings() {
        View customView = getLayoutInflater().inflate(R.layout.view_suggest_settings, null);
        new DialogUtil.Builder(MainActivity.this)
                .title("Thay đổi kích thước chữ và chế độ nền tối")
                .content("Ấn biểu tượng menu bên trái trên cùng màn hinh trang chủ -> vào Cài đặt ứng dụng để thay đổi kích thước phông chữ và chế độ hiển thị. Chọn chế độ nền tối để bảo vệ mắt của bạn đỡ mỏi khi ôn thi trong 1 thời gian dài.")
                .negativeText(getString(R.string.text_cancel))
                .positiveText(getString(R.string.text_ok))
                .onNegative(new DialogUtil.SingleButtonCallback() {
                    @Override
                    public void onClick() {

                    }
                })
                .onPositive(new DialogUtil.SingleButtonCallback() {
                    @Override
                    public void onClick() {
                        AdmobHelp.getInstance().showInterstitialAd(new AdCloseListener() {
                            @Override
                            public void onAdClosed(boolean haveAds) {
                                Intent intent = new Intent(MainActivity.this, SettingsActivity.class);
                                startActivity(intent);
                                if(haveAds) {
                                    checkShowSuggestProDisplayAds();
                                }
                            }
                        });
                    }
                })
                .setCustomView(customView)
                .show();
    }

}

