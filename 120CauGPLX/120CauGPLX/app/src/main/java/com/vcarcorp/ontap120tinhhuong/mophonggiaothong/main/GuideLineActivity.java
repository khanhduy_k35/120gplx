package com.vcarcorp.ontap120tinhhuong.mophonggiaothong.main;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.ads.control.admob.AdCloseListener;
import com.ads.control.admob.AdmobHelp;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.DexterError;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequestErrorListener;
import com.karumi.dexter.listener.single.PermissionListener;
import com.tbuonomo.viewpagerdotsindicator.DotsIndicator;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.R;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.util.AnimationListener;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.util.HTextView;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.util.SharedPreferencesUtils;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.util.TyperTextView;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.util.Util;
import com.ads.control.customview.CustomTextView;

public class GuideLineActivity extends BaseActivity {
    private CustomTextView btnNext,timeCounter;
    private TyperTextView tvGuide;
    private DotsIndicator indicator;
    private ViewPager viewpager;
    private boolean isBack = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_guide_line);
        Bundle extras = getIntent().getExtras();
        if (extras == null) {
        } else {
            isBack = true;
        }
        btnNext = findViewById(R.id.btnNext);
        timeCounter = findViewById(R.id.timeCounter);
        indicator = findViewById(R.id.indicator);
        viewpager = findViewById(R.id.viewpager);
        tvGuide = findViewById(R.id.tv_guide);

        Adapter adapter = new Adapter(GuideLineActivity.this);

        viewpager.setAdapter(adapter);
        indicator.attachTo(viewpager);
        viewpager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (position == 3) {
                    btnNext.setVisibility(View.VISIBLE);
                } else {
                    btnNext.setVisibility(View.INVISIBLE);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isBack == true) {
                    onBackPressed();
                } else {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU && checkSelfPermission(Manifest.permission.POST_NOTIFICATIONS) != PackageManager.PERMISSION_GRANTED) {
                        Dexter.withContext(GuideLineActivity.this)
                                .withPermission(Manifest.permission.POST_NOTIFICATIONS)
                                .withListener(new PermissionListener() {
                                    @Override
                                    public void onPermissionGranted(PermissionGrantedResponse response) {
                                        gotoMain();
                                    }

                                    @Override
                                    public void onPermissionDenied(PermissionDeniedResponse response) {
                                        gotoMain();
                                    }

                                    @Override
                                    public void onPermissionRationaleShouldBeShown(com.karumi.dexter.listener.PermissionRequest permissionRequest, PermissionToken permissionToken) {
                                        permissionToken.continuePermissionRequest();
                                    }
                                })
                                .withErrorListener(new PermissionRequestErrorListener() {
                                    @Override
                                    public void onError(DexterError dexterError) {
                                        gotoMain();
                                    }
                                }).check();
                    }else{
                        gotoMain();
                    }
                }
            }
        });

        tvGuide.setCharIncrease(2);
        tvGuide.setTyperSpeed(75);
        tvGuide.animateText(tvGuide.getText());
        tvGuide.setAnimationListener(new AnimationListener() {
            @Override
            public void onAnimationEnd(HTextView hTextView) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        viewpager.setVisibility(View.VISIBLE);
                        indicator.setVisibility(View.VISIBLE);
                        tvGuide.setVisibility(View.GONE);
                        timeCounter.setText("Hướng dẫn ôn thi!");
                    }
                },1000);
             }
        });
    }

    class Adapter extends PagerAdapter {

        private Context mContext;

        public Adapter(Context context) {
            mContext = context;
        }

        @Override
        public int getCount() {
            return 4;
        }

        @Override
        public Object instantiateItem(ViewGroup collection, int position) {
            View view = (View) LayoutInflater.from(mContext).inflate(R.layout.fragment_guide_line, collection, false);
            collection.addView(view);

            CustomTextView tvTitle = view.findViewById(R.id.tv_title);
            ImageView imvIntro = view.findViewById(R.id.imv_intro);
            CustomTextView tvDetail = view.findViewById(R.id.tv_detail);

            if (position == 0) {
                tvTitle.setText("Bước 1:");
                imvIntro.setImageResource(R.drawable.ic_intro1);
                tvDetail.setText("Bước 1. Bạn nên vào mục Ôn tập câu hỏi (6 chương-120 câu hỏi) để học từng câu hỏi, học đi học lại đến khi nào bạn nắm chắc mẹo để vượt qua tất cả các câu hỏi với điểm tối đa 5 điểm. Mỗi câu hỏi đều có mẹo thi và giải thích chi tiết giúp bạn học và nhớ 1 cách dễ dàng.");
            } else if (position == 1) {
                tvTitle.setText("Bước 2:");
                imvIntro.setImageResource(R.drawable.ic_intro2);
                tvDetail.setText("Bước 2. Bạn hãy vào phần đề thi mô phỏng làm lần lượt các đề thi đến khi nào bạn hoàn thành tất cả các đề thi. Trong quá trình làm đề thi chúng tôi sẽ lưu lại các câu hỏi bạn hay sai (để bạn ghi nhớ) và bạn có thể đánh dấu các câu hỏi bạn muốn nhớ để truy cập 1 cách dễ dàng.");
            } else if (position == 2) {
                tvTitle.setText("Bước 3:");
                imvIntro.setImageResource(R.drawable.ic_intro3);
                tvDetail.setText("Bước 3. Hãy tạo 1 đề thi ngẫu nhiên và làm đề thi đó, hãy làm đi làm lại bước này đến khi nào đề thi ngẫu nhiên nào bạn cũng vượt qua thì đảm bảo bạn sẽ thi đỗ 100% khi thi thực tế.");
            } else {
                tvTitle.setText("Bước 4:");
                imvIntro.setImageResource(R.drawable.ic_intro4);
                tvDetail.setText("Bước 4. Nếu bạn đã hoàn thành 3 bước trên, thì bạn chỉ cần giữ tự tin và bình tĩnh khi thi, chắc chắn bạn sẽ đạt kết quả cao.Chúc bạn thi đỗ với số điểm tối đa, bình an và hạnh phúc sau tay lái.");
            }
            return view;
        }

        @Override
        public void destroyItem(ViewGroup collection, int position, Object view) {
            collection.removeView((View) view);
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return "";
        }

    }

    public void gotoMain(){
        AdmobHelp.getInstance().showInterstitialAd(new AdCloseListener() {
            @Override
            public void onAdClosed(boolean haveAds) {
                if(SharedPreferencesUtils.isShowProFisrt()) {
                    SharedPreferencesUtils.setBoolean(Util.FINISHGUIDE, true);
                    Intent intent = new Intent(GuideLineActivity.this, UnlockProActivity.class);
                    intent.putExtra("first_start", true);
                    GuideLineActivity.this.startActivity(intent);
                    GuideLineActivity.this.finish();
                }else{
                    SharedPreferencesUtils.setBoolean(Util.FINISHGUIDE, true);
                    Intent intent = new Intent(GuideLineActivity.this, MainActivity.class);
                    intent.putExtra("first_start", true);
                    GuideLineActivity.this.startActivity(intent);
                    GuideLineActivity.this.finish();
                }
            }
        });
    }
}