package com.vcarcorp.ontap120tinhhuong.mophonggiaothong.moredriver.trainingground;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.ads.control.admob.AdmobHelp;
import com.ads.control.customview.DialogUtil;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.R;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.database.DataManagerTestCenter;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.main.BaseActivity;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.model.TestCenter;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.model.TestCenterGroup;

import java.util.ArrayList;

public class TrainingGroundActivity extends BaseActivity {

    private DataManagerTestCenter dataManagerTestCenter;
    private ArrayList<TestCenterGroup> testCenterGroups = new ArrayList<>();
    private RecyclerView recyclerView;
    private TrainingGroundGroupAdapter trainingGroundGroupAdapter;
    private EditText edtSearch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_training_ground);
        AdmobHelp.getInstance().loadBanner(this);
        dataManagerTestCenter = new DataManagerTestCenter(TrainingGroundActivity.this);
        testCenterGroups = dataManagerTestCenter.getListTrainingGround();
        recyclerView = (RecyclerView) findViewById(R.id.recycle_view);
        edtSearch = (EditText) findViewById(R.id.edt_search);

        recyclerView.setLayoutManager(new LinearLayoutManager(TrainingGroundActivity.this, LinearLayoutManager.VERTICAL, false));
        trainingGroundGroupAdapter = new TrainingGroundGroupAdapter(testCenterGroups, TrainingGroundActivity.this);
        recyclerView.setAdapter(trainingGroundGroupAdapter);
        trainingGroundGroupAdapter.setClickListener(new TrainingGroundGroupAdapter.ItemClickListener() {
            @Override
            public void onItemClick(TestCenter testCenter) {
                new DialogUtil.Builder(TrainingGroundActivity.this)
                        .title("Nhận xét sân tập")
                        .content(testCenter.getNote())
                        .doneText("Đồng ý")
                        .show();
            }
        });
        findViewById(R.id.btn_back).setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                onBackPressed();
            }
        });

        findViewById(R.id.btn_platinum).setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                buyPro();
            }
        });
        edtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                trainingGroundGroupAdapter.filter(charSequence.toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

    }
}