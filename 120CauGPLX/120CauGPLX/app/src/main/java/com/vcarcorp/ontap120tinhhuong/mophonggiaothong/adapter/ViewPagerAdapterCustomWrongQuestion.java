package com.vcarcorp.ontap120tinhhuong.mophonggiaothong.adapter;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.viewpager2.adapter.FragmentStateAdapter;

import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.model.QuestionVideo;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.wrong.WrongQuestionFragment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ViewPagerAdapterCustomWrongQuestion extends FragmentStateAdapter {
    private ArrayList<QuestionVideo> questionVideos;
    private Map<Integer, Fragment> mPageReferenceMap = new HashMap<Integer, Fragment>();

    public ViewPagerAdapterCustomWrongQuestion(FragmentActivity fragmentActivity, ArrayList<QuestionVideo> questionVideos){
        super(fragmentActivity);
        this.questionVideos = questionVideos;
    }

    @NonNull
    @Override
    public Fragment createFragment(int position) {
        WrongQuestionFragment learnQuestionFragment =  WrongQuestionFragment.init(questionVideos.get(position),position);
        mPageReferenceMap.put(position,learnQuestionFragment);
        return learnQuestionFragment;
    }

    @Override
    public int getItemCount() {
        return questionVideos.size();
    }

    public void detroyAllVideo(){
        try {
            for (Map.Entry<Integer, Fragment> entry : mPageReferenceMap.entrySet()) {
                WrongQuestionFragment wrongQuestionFragment = (WrongQuestionFragment) entry.getValue();
                wrongQuestionFragment.destroyVideo();
            }
        }catch (Exception e){

        }
    }

    public void changeCheckedShowScore(){
        try {
            for (Map.Entry<Integer, Fragment> entry : mPageReferenceMap.entrySet()) {
                WrongQuestionFragment wrongQuestionFragment = (WrongQuestionFragment) entry.getValue();
                if(wrongQuestionFragment!=null)
                    wrongQuestionFragment.changeCheckedShowScore();
            }
        }catch (Exception e){

        }
    }

    public WrongQuestionFragment getFragment(int key) {
        return (WrongQuestionFragment) mPageReferenceMap.get(key);
    }

    public void destroyItem(int position){
        try{
            WrongQuestionFragment learnQuestionFragment = getFragment(position);
            learnQuestionFragment.destroyVideo();
            mPageReferenceMap.remove(position);
        }catch (Exception e){

        }
    }

}
