package com.vcarcorp.ontap120tinhhuong.mophonggiaothong.util.customview;

import android.content.Context;
import android.graphics.Color;
import android.util.AttributeSet;

import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.util.AppUtils;
import com.rey.material.widget.ImageView;

public class ImageViewButton extends ImageView {

    public ImageViewButton(Context context) {
        super(context);
    }

    public ImageViewButton(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ImageViewButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public void setEnabled(boolean enabled) {
        super.setEnabled(enabled);
        if (AppUtils.checkPortraitMode(getContext())) {
            setColorFilter(Color.parseColor("#ffffff"));
        } else {
            if (enabled) {
                setColorFilter(Color.parseColor("#ffffff"));
            } else {
                setColorFilter(Color.parseColor("#a9a9aa"));
            }
        }
    }
}
