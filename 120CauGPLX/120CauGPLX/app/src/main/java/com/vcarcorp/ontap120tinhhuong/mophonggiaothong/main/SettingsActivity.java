package com.vcarcorp.ontap120tinhhuong.mophonggiaothong.main;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TimePicker;

import androidx.appcompat.widget.SwitchCompat;

import com.ads.control.admob.AdCloseListener;
import com.ads.control.admob.AdmobHelp;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.R;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.reminder.AlarmManagerSetup;
import com.ads.control.customview.CustomTextView;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.util.DialogUtil;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.util.SharedPreferencesUtils;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.util.Util;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.util.AppUtils;
import com.rey.material.widget.FrameLayout;
import com.rey.material.widget.RelativeLayout;
import com.xw.repo.BubbleSeekBar;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class SettingsActivity extends BaseActivity {

    private FrameLayout btn_back;
    private CustomTextView tvLight,tvDark;
    private RelativeLayout rltDark,rltLight;
    private ImageView imgDark,imgLight;
    private int currentFontSize = -1;
    private BubbleSeekBar sbChangeFont;
    private RelativeLayout rltShowScore,rltShowScoreSpace,rltShowDetail,rltReminder,rltShowSuggest;
    private SwitchCompat swShowScore,swShowScoreSpace,swShowDetail,swReminder,swSuggest;
    private CustomTextView tvTime;
    private RelativeLayout rltClickPro;
    private CustomTextView tvFeatureOpen;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings_activity);
        Window window = this.getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | 0);
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(Color.TRANSPARENT);
        }
        initValue();
        initIdView();
        initView();
        initActionView();
        if(!Util.isPro(this) && Util.isDisplayAds && Util.checkConditionalShowAds(this)) {
            if (findViewById(R.id.container_ads_native) != null)
                AdmobHelp.getInstance().loadNativeActivity(this,true);
        }
        else {
            if (findViewById(R.id.container_ads_native) != null)
                findViewById(R.id.container_ads_native).setVisibility(View.GONE);
        }
        RelativeLayout rltAudio = findViewById(R.id.rltAudio);
        SwitchCompat swAudio = findViewById(R.id.swAudio);
        swAudio.setChecked(SharedPreferencesUtils.getBoolean("audio_setting",true));
        swAudio.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                SharedPreferencesUtils.setBoolean("audio_setting",b);
            }
        });
        rltAudio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                swAudio.setChecked(!SharedPreferencesUtils.getBoolean("audio_setting",true));
            }
        });
    }

    private void initValue() {
        currentFontSize = SharedPreferencesUtils.getInt("font_setting_size");
    }

    private void initIdView() {
        btn_back = findViewById(R.id.btn_back);
        tvLight = findViewById(R.id.tvLight);
        tvDark = findViewById(R.id.tvDark);
        rltDark = findViewById(R.id.rltDark);
        rltLight = findViewById(R.id.rltLight);
        imgDark = findViewById(R.id.imgDark);
        imgLight = findViewById(R.id.imgLight);
        sbChangeFont = findViewById(R.id.sbChangeFont);
        rltShowScore = findViewById(R.id.rltShowScore);
        rltShowScoreSpace = findViewById(R.id.rltShowScoreSpace);
        rltShowDetail = findViewById(R.id.rltShowDetail);
        rltReminder = findViewById(R.id.rltReminder);
        rltShowSuggest = findViewById(R.id.rltShowSuggest);
        swShowScore = findViewById(R.id.swShowScore);
        swShowScoreSpace = findViewById(R.id.swShowScoreSpace);
        swShowDetail = findViewById(R.id.swShowDetail);
        swReminder = findViewById(R.id.swReminder);
        swSuggest = findViewById(R.id.swShowSuggest);
        tvTime = findViewById(R.id.tvTime);
        rltClickPro = findViewById(R.id.rltClickPro);
        tvFeatureOpen = findViewById(R.id.tvFeatureOpen);
    }

    private void initView() {
        setUpToolBar(btn_back);
        if(SharedPreferencesUtils.isNightMode()){
            imgDark.setImageResource(R.drawable.ic_settting_check);
            imgDark.setColorFilter(AppUtils.getColorFromAttr(SettingsActivity.this,R.attr.color_control_selected));
            tvDark.setTextColor(AppUtils.getColorFromAttr(SettingsActivity.this,R.attr.color_control_selected));

            imgLight.setImageResource(R.drawable.ic_uncheck);
            imgLight.setColorFilter(AppUtils.getColorFromAttr(SettingsActivity.this,R.attr.color_control_normal));
            tvLight.setTextColor(AppUtils.getColorFromAttr(SettingsActivity.this,R.attr.color_text_item_setting));
        }else{
            imgLight.setImageResource(R.drawable.ic_settting_check);
            imgLight.setColorFilter(AppUtils.getColorFromAttr(SettingsActivity.this,R.attr.color_control_selected));
            tvLight.setTextColor(AppUtils.getColorFromAttr(SettingsActivity.this,R.attr.color_control_selected));

            imgDark.setImageResource(R.drawable.ic_uncheck);
            imgDark.setColorFilter(AppUtils.getColorFromAttr(SettingsActivity.this,R.attr.color_control_normal));
            tvDark.setTextColor(AppUtils.getColorFromAttr(SettingsActivity.this,R.attr.color_text_item_setting));
        }
        sbChangeFont.setProgress(SharedPreferencesUtils.getInt("font_setting_size"));
        swShowScore.setChecked(SharedPreferencesUtils.showScoreStop());
        swShowScoreSpace.setChecked(SharedPreferencesUtils.showScoreSpace());
        swShowDetail.setChecked(SharedPreferencesUtils.showDetail());
        swReminder.setChecked(SharedPreferencesUtils.showReminder());
        swSuggest.setChecked(SharedPreferencesUtils.showSuggest());

        long time = SharedPreferencesUtils.getTimeNotification();
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(time);
        tvTime.setText(new SimpleDateFormat("HH:mm", Locale.getDefault()).format(calendar.getTime()));
        if(SharedPreferencesUtils.showReminder()){
            tvTime.setTextColor(Color.parseColor("#00C6C1"));
        }else{
            tvTime.setTextColor(Color.parseColor("#8000C6C1"));
        }
        if(Util.isPro(this)){
            rltClickPro.setVisibility(View.GONE);
            tvFeatureOpen.setText("Các tính năng cao cấp");
        }else{
            rltClickPro.setVisibility(View.VISIBLE);
            tvFeatureOpen.setText("Chỉ dành cho phiên bản trả phí");
        }
    }

    private void changeAllFontAllEdittexts(ViewGroup viewGroup) {
        int count = viewGroup.getChildCount();
        for (int i = 0; i < count; i++) {
            View view = viewGroup.getChildAt(i);
            if (view instanceof ViewGroup)
                changeAllFontAllEdittexts((ViewGroup) view);
            else if (view instanceof CustomTextView) {
                CustomTextView customTextView = (CustomTextView) view;
                customTextView.changeFont();
            }
        }
    }

    private void initActionView() {
        sbChangeFont.setOnProgressChangedListener(new BubbleSeekBar.OnProgressChangedListener() {
            @Override
            public void onProgressChanged(BubbleSeekBar bubbleSeekBar, int progress, float progressFloat, boolean fromUser) {
                SharedPreferencesUtils.setInt("font_setting_size",progress);
                changeAllFontAllEdittexts((ViewGroup) findViewById(R.id.rltMain_content));
            }

            @Override
            public void getProgressOnActionUp(BubbleSeekBar bubbleSeekBar, int progress, float progressFloat) {

            }

            @Override
            public void getProgressOnFinally(BubbleSeekBar bubbleSeekBar, int progress, float progressFloat, boolean fromUser) {
                SharedPreferencesUtils.setInt("font_setting_size",progress);
                changeAllFontAllEdittexts((ViewGroup) findViewById(R.id.rltMain_content));
            }
        });
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        rltDark.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                new DialogUtil.Builder(SettingsActivity.this)
                        .title("Thay đổi chế độ ban đêm")
                        .content("Bạn có muốn thay đổi thành chế độ nền tối để sử dụng không?")
                        .negativeText(getString(R.string.text_cancel))
                        .positiveText(getString(R.string.text_ok))
                        .onNegative(new DialogUtil.SingleButtonCallback() {
                            @Override
                            public void onClick() {

                            }
                        })
                        .onPositive(new DialogUtil.SingleButtonCallback() {
                            @Override
                            public void onClick() {
                                AdmobHelp.getInstance().showInterstitialAd( new AdCloseListener() {
                                    @Override
                                    public void onAdClosed(boolean haveAds) {
                                        SharedPreferencesUtils.setNightMode(true);
                                        if(SharedPreferencesUtils.isNightMode()){
                                            imgDark.setImageResource(R.drawable.ic_settting_check);
                                            imgDark.setColorFilter(AppUtils.getColorFromAttr(SettingsActivity.this,R.attr.color_control_selected));
                                            tvDark.setTextColor(AppUtils.getColorFromAttr(SettingsActivity.this,R.attr.color_control_selected));

                                            imgLight.setImageResource(R.drawable.ic_uncheck);
                                            imgLight.setColorFilter(AppUtils.getColorFromAttr(SettingsActivity.this,R.attr.color_control_normal));
                                            tvLight.setTextColor(AppUtils.getColorFromAttr(SettingsActivity.this,R.attr.color_text_item_setting));
                                        }else{
                                            imgLight.setImageResource(R.drawable.ic_settting_check);
                                            imgLight.setColorFilter(AppUtils.getColorFromAttr(SettingsActivity.this,R.attr.color_control_selected));
                                            tvLight.setTextColor(AppUtils.getColorFromAttr(SettingsActivity.this,R.attr.color_control_selected));

                                            imgDark.setImageResource(R.drawable.ic_uncheck);
                                            imgDark.setColorFilter(AppUtils.getColorFromAttr(SettingsActivity.this,R.attr.color_control_normal));
                                            tvDark.setTextColor(AppUtils.getColorFromAttr(SettingsActivity.this,R.attr.color_text_item_setting));
                                        }
                                        Intent intent = new Intent(SettingsActivity.this, MainActivity.class);
                                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        startActivity(intent);
                                        if(haveAds) {
                                            checkShowSuggestProDisplayAds();
                                        }
                                    }
                                });
                            }
                        })
                        .show();
            }
        });
        rltLight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DialogUtil.Builder(SettingsActivity.this)
                        .title("Thay đổi chế độ ban ngày")
                        .content("Bạn có muốn thay đổi thành chế độ ban ngày để sử dụng không?")
                        .negativeText(getString(R.string.text_cancel))
                        .positiveText(getString(R.string.text_ok))
                        .onNegative(new DialogUtil.SingleButtonCallback() {
                            @Override
                            public void onClick() {

                            }
                        })
                        .onPositive(new DialogUtil.SingleButtonCallback() {
                            @Override
                            public void onClick() {
                                AdmobHelp.getInstance().showInterstitialAd(new AdCloseListener() {
                                    @Override
                                    public void onAdClosed(boolean haveAds) {
                                        SharedPreferencesUtils.setNightMode(false);
                                        if(SharedPreferencesUtils.isNightMode()){
                                            imgDark.setImageResource(R.drawable.ic_settting_check);
                                            imgDark.setColorFilter(AppUtils.getColorFromAttr(SettingsActivity.this,R.attr.color_control_selected));
                                            tvDark.setTextColor(AppUtils.getColorFromAttr(SettingsActivity.this,R.attr.color_control_selected));

                                            imgLight.setImageResource(R.drawable.ic_uncheck);
                                            imgLight.setColorFilter(AppUtils.getColorFromAttr(SettingsActivity.this,R.attr.color_control_normal));
                                            tvLight.setTextColor(AppUtils.getColorFromAttr(SettingsActivity.this,R.attr.color_text_item_setting));
                                        }else{
                                            imgLight.setImageResource(R.drawable.ic_settting_check);
                                            imgLight.setColorFilter(AppUtils.getColorFromAttr(SettingsActivity.this,R.attr.color_control_selected));
                                            tvLight.setTextColor(AppUtils.getColorFromAttr(SettingsActivity.this,R.attr.color_control_selected));

                                            imgDark.setImageResource(R.drawable.ic_uncheck);
                                            imgDark.setColorFilter(AppUtils.getColorFromAttr(SettingsActivity.this,R.attr.color_control_normal));
                                            tvDark.setTextColor(AppUtils.getColorFromAttr(SettingsActivity.this,R.attr.color_text_item_setting));
                                        }
                                        Intent intent = new Intent(SettingsActivity.this, MainActivity.class);
                                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        startActivity(intent);
                                        if(haveAds) {
                                            checkShowSuggestProDisplayAds();
                                        }
                                    }
                                });
                            }
                        })
                        .show();
            }
        });
        swShowScore.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                SharedPreferencesUtils.setShowScoreStop(b);
            }
        });
        swShowScoreSpace.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                SharedPreferencesUtils.setShowScoreSpace(b);
            }
        });
        swShowDetail.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                SharedPreferencesUtils.setShowDetail(b);
            }
        });
        swReminder.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                SharedPreferencesUtils.setShowReminder(b);
                if(b){
                    tvTime.setTextColor(Color.parseColor("#4caf50"));
                }else{
                    tvTime.setTextColor(Color.parseColor("#804caf50"));
                }
            }
        });

        swSuggest.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                SharedPreferencesUtils.setShowSuggest(b);
            }
        });

        rltShowSuggest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                swSuggest.setChecked(!SharedPreferencesUtils.showSuggest());
            }
        });

        rltShowScore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                swShowScore.setChecked(!SharedPreferencesUtils.showScoreStop());
            }
        });
        rltShowScoreSpace.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                swShowScoreSpace.setChecked(!SharedPreferencesUtils.showScoreSpace());
            }
        });
        rltShowDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                swShowDetail.setChecked(!SharedPreferencesUtils.showDetail());
            }
        });
        rltReminder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                swReminder.setChecked(!SharedPreferencesUtils.showReminder());
            }
        });
        tvTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(SharedPreferencesUtils.showReminder()) {
                    long time = SharedPreferencesUtils.getTimeNotification();
                    Calendar calendar = Calendar.getInstance();
                    calendar.setTimeInMillis(time);
                    TimePickerDialog mTimePicker = new TimePickerDialog(SettingsActivity.this, new TimePickerDialog.OnTimeSetListener() {
                        @Override
                        public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                            calendar.set(Calendar.HOUR_OF_DAY, selectedHour);
                            calendar.set(Calendar.MINUTE, selectedMinute);
                            SharedPreferencesUtils.setTimeNotification(calendar.getTimeInMillis());
                            updateTimeNotification();
                            long time = SharedPreferencesUtils.getTimeNotification();
                            Calendar calendar = Calendar.getInstance();
                            calendar.setTimeInMillis(time);
                            tvTime.setText(new SimpleDateFormat("HH:mm", Locale.getDefault()).format(calendar.getTime()));
                        }
                    }, calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE), true);//Yes 24 hour time
                    mTimePicker.setTitle("Cài đặt thời gian nhắc nhở");
                    mTimePicker.setOnShowListener(new DialogInterface.OnShowListener() {
                        @Override
                        public void onShow(DialogInterface dialogInterface) {
                            int positiveColor = AppUtils.getColorFromAttr(view.getContext(),R.attr.color_text_item_setting);
                            int negativeColor = AppUtils.getColorFromAttr(view.getContext(),R.attr.color_text_item_setting);
                            mTimePicker.getButton(DatePickerDialog.BUTTON_POSITIVE).setTextColor(positiveColor);
                            mTimePicker.getButton(DatePickerDialog.BUTTON_NEGATIVE).setTextColor(negativeColor);
                        }
                    });
                    mTimePicker.show();
                }
            }
        });
        rltClickPro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                buyPro();
            }
        });
    }

    private void updateTimeNotification() {
        if(SharedPreferencesUtils.showReminder()){
            AlarmManagerSetup.scheduleAlarmReceiverDiaryReminder(SettingsActivity.this);
        }
    }

    @Override
    public void onBackPressed() {
        if(currentFontSize != SharedPreferencesUtils.getInt("font_setting_size")){
            Intent intent = new Intent(SettingsActivity.this, MainActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
        }else {
            super.onBackPressed();
        }
    }
}
