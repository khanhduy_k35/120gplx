package com.vcarcorp.ontap120tinhhuong.mophonggiaothong.main;

import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import com.rey.material.widget.LinearLayout;
import com.rey.material.widget.TextView;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.R;

public class IapActivity extends BaseActivity {
    private LinearLayout pro_1, pro_3, pro_5, pro_10, sub_1, sub_3, sub_5, sub_10;

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.diary_activity_iap);
        pro_1 = findViewById(R.id.pro_1);
        pro_3 = findViewById(R.id.pro_3);
        pro_5 = findViewById(R.id.pro_5);
        pro_10 = findViewById(R.id.pro_10);

        sub_1 = findViewById(R.id.sub_1);
        sub_3 = findViewById(R.id.sub_3);
        sub_5 = findViewById(R.id.sub_5);
        sub_10 = findViewById(R.id.sub_10);


        pro_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buyPro(getString(R.string.pro_1), false);
            }
        });

        pro_3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buyPro(getString(R.string.pro_3), false);
            }
        });

        pro_5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buyPro(getString(R.string.pro_5), false);
            }
        });

        pro_10.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buyPro(getString(R.string.pro_10), false);
            }
        });


        sub_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buyPro(getString(R.string.sub_1), true);
            }
        });

        sub_3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buyPro(getString(R.string.sub_3), true);
            }
        });

        sub_5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buyPro(getString(R.string.sub_5), true);
            }
        });

        sub_10.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buyPro(getString(R.string.sub_10), true);
            }
        });
    }


    public void onDestroy() {

        super.onDestroy();
        Runtime.getRuntime().gc();
    }
}
