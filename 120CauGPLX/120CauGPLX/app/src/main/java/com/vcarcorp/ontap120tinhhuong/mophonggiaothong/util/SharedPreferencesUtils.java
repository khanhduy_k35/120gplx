package com.vcarcorp.ontap120tinhhuong.mophonggiaothong.util;
import android.content.Context;
import android.content.SharedPreferences;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class SharedPreferencesUtils {
    private static SharedPreferences mPref = null;
    private static String mName = "";
    public enum Cmd {
        INIT("null");

        private String mDefault;

        Cmd(String def) {
            this.mDefault = def;
        }

        public String getDefault() {
            return mDefault;
        }
    }


    public static void init(Context context) {
        mName = context.getString(com.ads.control.R.string.share_preference_name);
        mPref = context.getSharedPreferences(mName, Context.MODE_PRIVATE);
    }

    public static void set(Cmd cmd, String data) {
        if (mPref != null) {
            SharedPreferences.Editor edit = mPref.edit();
            edit.putString(cmd.toString(), data);
            edit.commit();
        }
    }

    public static String getCmd(Cmd cmd) {
        if (mPref == null || !mPref.contains(cmd.name())) {
            SharedPreferences.Editor edit = mPref.edit();
            edit.putString(cmd.toString(), cmd.getDefault());
            edit.commit();
        }

        return mPref.getString(cmd.toString(), "null");
    }

    public static void setBoolean(String _key, boolean _value) {
        if (_key == null) {
            return;
        }

        if (mPref != null) {
            SharedPreferences.Editor edit = mPref.edit();
            edit.putBoolean(_key, _value);
            edit.commit();
        }
    }

    public static boolean getBoolean(String _key) {
        if (mPref == null || !mPref.contains(_key)) {
            SharedPreferences.Editor edit = mPref.edit();
            edit.putBoolean(_key, false);
            edit.commit();
        }

        return mPref.getBoolean(_key, false);
    }

    public static boolean getBoolean(String _key, boolean _value) {
        if (mPref == null || !mPref.contains(_key)) {
            SharedPreferences.Editor edit = mPref.edit();
            edit.putBoolean(_key, _value);
            edit.commit();
        }

        return mPref.getBoolean(_key, _value);
    }

    public static void setString(String _key, String _value) {
        if (_key == null) {
            return;
        }

        if (mPref != null) {
            SharedPreferences.Editor edit = mPref.edit();
            edit.putString(_key, _value);
            edit.commit();
        }
    }

    public static String getString(String _key) {
        if (mPref == null || !mPref.contains(_key)) {
            SharedPreferences.Editor edit = mPref.edit();
            edit.putString(_key, null);
            edit.commit();
        }
        return mPref.getString(_key, null);

    }

    public static String getString(String _key, String _value) {
        if (mPref == null || !mPref.contains(_key)) {
            SharedPreferences.Editor edit = mPref.edit();
            edit.putString(_key, _value);
            edit.commit();
        }
        return mPref.getString(_key, _value);

    }

    public static void setInt(String _key, int _value) {
        if (_key == null) {
            return;
        }

        if (mPref != null) {
            SharedPreferences.Editor edit = mPref.edit();
            edit.putInt(_key, _value);
            edit.commit();
        }
    }

    public static long getLong(String _key, long _value) {
        if (mPref == null || !mPref.contains(_key)) {
            SharedPreferences.Editor edit = mPref.edit();
            edit.putLong(_key, _value);
            edit.commit();
        }
        return mPref.getLong(_key, _value);

    }

    public static void setLong(String _key, long _value) {
        if (_key == null) {
            return;
        }

        if (mPref != null) {
            SharedPreferences.Editor edit = mPref.edit();
            edit.putLong(_key, _value);
            edit.commit();
        }
    }

    public static int getInt(String _key, int _value) {
        if (mPref == null || !mPref.contains(_key)) {
            SharedPreferences.Editor edit = mPref.edit();
            edit.putInt(_key, _value);
            edit.commit();
        }
        return mPref.getInt(_key, _value);

    }


    public static int getInt(String _key) {
        return mPref.getInt(_key, -1);
    }

    public static void setFloat(String _key, float _value) {
        if (_key == null) {
            return;
        }

        if (mPref != null) {
            SharedPreferences.Editor edit = mPref.edit();
            edit.putFloat(_key, _value);
            edit.commit();
        }
    }

    public static float getFloat(String _key, float _value) {
        if (mPref == null || !mPref.contains(_key)) {
            SharedPreferences.Editor edit = mPref.edit();
            edit.putFloat(_key, _value);
            edit.commit();
        }
        return mPref.getFloat(_key, _value);
    }

    public static boolean isNightMode(){
        return getBoolean("APP_THEME_DARK",false);
    }

    public static void setNightMode(boolean value){
        setBoolean("APP_THEME_DARK",value);
    }

    public static String getWakeUp() {
        long time = getTimeNotification();
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(time);
        return new SimpleDateFormat("HH:mm", Locale.getDefault()).format(calendar.getTime());
    }

    public static long getTimeNotification(){
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY,21);
        calendar.set(Calendar.MINUTE,0);
        calendar.set(Calendar.SECOND,0);
        return SharedPreferencesUtils.getLong("time_notification",calendar.getTimeInMillis());
    }

    public static void setTimeNotification(long time){
        SharedPreferencesUtils.setLong("time_notification",time);
    }

    public static int getReminderInterval() {
        return getInt("REMINDER_INTERVAL", 720);
    }

    public static boolean showScoreStop(){
        return SharedPreferencesUtils.getBoolean("show_score",false);
    }

    public static void setShowScoreStop(boolean b){
        SharedPreferencesUtils.setBoolean("show_score",b);
    }

    public static boolean showDetail(){
        return SharedPreferencesUtils.getBoolean("show_detail",false);
    }

    public static void setShowDetail(boolean b){
        SharedPreferencesUtils.setBoolean("show_detail",b);
    }

    public static boolean showReminder(){
        return SharedPreferencesUtils.getBoolean("show_reminder",true);
    }

    public static void setShowReminder(boolean b){
        SharedPreferencesUtils.setBoolean("show_reminder",b);
    }

    public static boolean showScoreSpace(){
        return SharedPreferencesUtils.getBoolean("show_score_space",true);
    }

    public static void setShowScoreSpace(boolean b){
         SharedPreferencesUtils.setBoolean("show_score_space",b);
    }

    public static boolean showScoreSpaceFree(){
        return SharedPreferencesUtils.getBoolean("show_score_space",true);
    }

    public static void setShowScoreSpaceFree(boolean b){
        SharedPreferencesUtils.setBoolean("show_score_space",b);
    }

    public static boolean showDetailFree(){
        return SharedPreferencesUtils.getBoolean("show_detail_free",false);
    }

    public static void setShowDetailFree(boolean b){
        SharedPreferencesUtils.setBoolean("show_detail_free",b);
    }

    public static boolean showScoreStopFree(){
        return SharedPreferencesUtils.getBoolean("show_score_free",false);
    }

    public static void setShowScoreStopFree(boolean b){
        SharedPreferencesUtils.setBoolean("show_score_free",b);
    }

    public static boolean showVoidEnable(){
        return SharedPreferencesUtils.getBoolean("sound_enable",true);
    }

    public static void setShowVoidEnable(boolean b){
        SharedPreferencesUtils.setBoolean("sound_enable",b);
    }

    public static boolean showVoidEnableFree(){
        return SharedPreferencesUtils.getBoolean("sound_enable_free",true);
    }

    public static void setShowVoidEnableFree(boolean b){
        SharedPreferencesUtils.setBoolean("sound_enable_free",b);
    }

    public static boolean isShowProFisrt() {
        return SharedPreferencesUtils.getBoolean("isShowProFisrt",false);
    }

    public static void setShowProFisrt(boolean showProFisrt) {
        SharedPreferencesUtils.setBoolean("isShowProFisrt",showProFisrt);
    }

    public static int getNumberTestFree() {
        return SharedPreferencesUtils.getInt("numberTestFree",0);
    }

    public static void setNumberTestFree(int numberTestFree) {
        SharedPreferencesUtils.setInt("numberTestFree",numberTestFree);
    }

    public static int getNumberQuestFree() {
        return SharedPreferencesUtils.getInt("numberQuestFree",0);
    }

    public static void setNumberQuestFree(int numberQuestFree) {
        SharedPreferencesUtils.setInt("numberQuestFree",numberQuestFree);
    }

    public static boolean showAds3g4G(){
        return SharedPreferencesUtils.getBoolean("show_ads_3g4g",false);
    }

    public static void setShowAds3g4G(boolean value){
        SharedPreferencesUtils.setBoolean("show_ads_3g4g",value);
    }

    public static boolean showAds3g4GVietetel(){
        return SharedPreferencesUtils.getBoolean("show_ads_3g4g_vt",false);
    }

    public static void setShowAds3g4GVietetel(boolean value){
        SharedPreferencesUtils.setBoolean("show_ads_3g4g_vt",value);
    }

    public static boolean showAds3g4GVina(){
        return SharedPreferencesUtils.getBoolean("show_ads_3g4g_vn",false);
    }

    public static void setShowAds3g4GVina(boolean value){
        SharedPreferencesUtils.setBoolean("show_ads_3g4g_vn",value);
    }

    public static String getUpdateQuestionID(){
        return SharedPreferencesUtils.getString("update_question","");
    }

    public static void setUpdateQuestionID(String value){
        SharedPreferencesUtils.setString("update_question",value);
    }

    public static String getMainVer(){
        return SharedPreferencesUtils.getString("main_ver","");
    }

    public static void setMainver(String value){
        SharedPreferencesUtils.setString("main_ver",value);
    }


    public static boolean showSuggest(){
        return SharedPreferencesUtils.getBoolean("show_suggest",false);
    }

    public static void setShowSuggest(boolean b){
        SharedPreferencesUtils.setBoolean("show_suggest",b);
    }

    public static boolean openLink(){
        return SharedPreferencesUtils.getBoolean("openLink",false);
    }

    public static void setOpenLink(boolean b){
        SharedPreferencesUtils.setBoolean("openLink",b);
    }

    public static boolean onlyLink(){
        return SharedPreferencesUtils.getBoolean("onlyLink",false);
    }

    public static void setOnlyLink(boolean b){
        SharedPreferencesUtils.setBoolean("onlyLink",b);
    }


    public static String getPhoneVT(){
        return SharedPreferencesUtils.getString("phone_vt","");
    }

    public static void setPhoneVT(String value){
        SharedPreferencesUtils.setString("phone_vt",value);
    }

}
