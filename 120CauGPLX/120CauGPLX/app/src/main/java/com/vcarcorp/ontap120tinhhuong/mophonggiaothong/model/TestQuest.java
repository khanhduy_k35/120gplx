package com.vcarcorp.ontap120tinhhuong.mophonggiaothong.model;

import android.os.Parcel;
import android.os.Parcelable;

public class TestQuest implements Parcelable {
    private int id;
    private int testID;
    private int questionID;
    private int answer;
    private int score;
    private boolean isLearned;
    private QuestionVideo questionVideo;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getTestID() {
        return testID;
    }

    public void setTestID(int testID) {
        this.testID = testID;
    }

    public int getQuestionID() {
        return questionID;
    }

    public void setQuestionID(int questionID) {
        this.questionID = questionID;
    }

    public int getAnswer() {
        return answer;
    }

    public void setAnswer(int answer) {
        this.answer = answer;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public boolean isLearned() {
        return isLearned;
    }

    public void setLearned(boolean learned) {
        isLearned = learned;
    }

    public QuestionVideo getQuestionVideo() {
        return questionVideo;
    }

    public void setQuestionVideo(QuestionVideo questionVideo) {
        this.questionVideo = questionVideo;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeInt(this.testID);
        dest.writeInt(this.questionID);
        dest.writeInt(this.answer);
        dest.writeInt(this.score);
        dest.writeByte(this.isLearned ? (byte) 1 : (byte) 0);
        dest.writeParcelable(this.questionVideo, flags);
    }

    public void readFromParcel(Parcel source) {
        this.id = source.readInt();
        this.testID = source.readInt();
        this.questionID = source.readInt();
        this.answer = source.readInt();
        this.score = source.readInt();
        this.isLearned = source.readByte() != 0;
        this.questionVideo = source.readParcelable(QuestionVideo.class.getClassLoader());
    }

    public TestQuest() {
    }

    protected TestQuest(Parcel in) {
        this.id = in.readInt();
        this.testID = in.readInt();
        this.questionID = in.readInt();
        this.answer = in.readInt();
        this.score = in.readInt();
        this.isLearned = in.readByte() != 0;
        this.questionVideo = in.readParcelable(QuestionVideo.class.getClassLoader());
    }

    public static final Parcelable.Creator<TestQuest> CREATOR = new Parcelable.Creator<TestQuest>() {
        @Override
        public TestQuest createFromParcel(Parcel source) {
            return new TestQuest(source);
        }

        @Override
        public TestQuest[] newArray(int size) {
            return new TestQuest[size];
        }
    };
}
