package com.vcarcorp.ontap120tinhhuong.mophonggiaothong.adapter;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.viewpager2.adapter.FragmentStateAdapter;

import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.exampractice.ExamQuestFragment;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.model.Test;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.model.TestQuest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ViewPagerAdapterCustomTestQuestion extends FragmentStateAdapter {
    private ArrayList<TestQuest> testQuests;
    private Test test;
    private Map<Integer, Fragment> mPageReferenceMap = new HashMap<Integer, Fragment>();

    public ViewPagerAdapterCustomTestQuestion(FragmentActivity fragmentActivity, ArrayList<TestQuest> testQuests, Test test){
        super(fragmentActivity);
        this.test = test;
        this.testQuests = testQuests;
    }

    @NonNull
    @Override
    public Fragment createFragment(int position) {
        ExamQuestFragment examQuestFragment =  ExamQuestFragment.init(testQuests.get(position),test,position);
        mPageReferenceMap.put(position,examQuestFragment);
        return examQuestFragment;
    }

    @Override
    public int getItemCount() {
        return testQuests.size();
    }

    public void detroyAllVideo(){
        try {
            for (Map.Entry<Integer, Fragment> entry : mPageReferenceMap.entrySet()) {
                ExamQuestFragment examQuestFragment = (ExamQuestFragment) entry.getValue();
                if(examQuestFragment!=null)
                    examQuestFragment.destroyVideo();
            }
        }catch (Exception e){

        }
    }

    public void changeCheckedShowScore(){
        try {
            for (Map.Entry<Integer, Fragment> entry : mPageReferenceMap.entrySet()) {
                ExamQuestFragment examQuestFragment = (ExamQuestFragment) entry.getValue();
                if(examQuestFragment!=null)
                    examQuestFragment.changeCheckedShowScore();
            }
        }catch (Exception e){

        }
    }

    public void checkResult(){
        try {
            for (Map.Entry<Integer, Fragment> entry : mPageReferenceMap.entrySet()) {
                ExamQuestFragment examQuestFragment = (ExamQuestFragment) entry.getValue();
                if(examQuestFragment!=null)
                    examQuestFragment.checkResult();
            }
        }catch (Exception e){

        }
    }

    public void resetData(ArrayList<TestQuest> testQuests){
        try {
            for (Map.Entry<Integer, Fragment> entry : mPageReferenceMap.entrySet()) {
                ExamQuestFragment examQuestFragment = (ExamQuestFragment) entry.getValue();
                int key = entry.getKey();
                if(examQuestFragment!=null) {
                    examQuestFragment.checkResult();
                    examQuestFragment.setTestQuest(testQuests.get(key));
                }
            }
        }catch (Exception e){

        }
    }


    public ExamQuestFragment getFragment(int key) {
        return (ExamQuestFragment) mPageReferenceMap.get(key);
    }

    public void destroyItem(int position){
        try{
            ExamQuestFragment examQuestFragment = getFragment(position);
            examQuestFragment.destroyVideo();
            mPageReferenceMap.remove(position);
        }catch (Exception e){

        }
    }

}
