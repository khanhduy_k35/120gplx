package com.vcarcorp.ontap120tinhhuong.mophonggiaothong.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by duy on 10/24/17.
 */

public class QuestionVideoCategory implements Parcelable {
    private int id;
    private String name;
    private String desc;
    private int count;
    private int numberQuestion;

    private ArrayList<QuestionVideo> listQuestion = new ArrayList<>();

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public ArrayList<QuestionVideo> getListQuestion() {
        return listQuestion;
    }

    public void setListQuestion(ArrayList<QuestionVideo> listQuestion) {
        this.listQuestion = listQuestion;
    }

    public QuestionVideoCategory(int id, String name, String desc, int count) {
        this.id = id;
        this.name = name;
        this.desc = desc;
        this.count = count;
    }

    public int getNumberQuestion() {
        return numberQuestion;
    }

    public void setNumberQuestion(int numberQuestion) {
        this.numberQuestion = numberQuestion;
    }

    public QuestionVideoCategory() {
    }

    public ArrayList<QuestionVideo> getRamdomQuestion() {
        ArrayList<QuestionVideo> questionVideos = new ArrayList<>();
        Random random = new Random();
        while (questionVideos.size() < numberQuestion) {
            int choice = random.nextInt(listQuestion.size());
            boolean check = false;
            for (QuestionVideo questionVideo : questionVideos) {
                if (questionVideo.getId() == listQuestion.get(choice).getId()) {
                    check = true;
                }
            }
            if (check == false) {
                QuestionVideo video = (QuestionVideo) listQuestion.get(choice).clone();
                questionVideos.add(video);
            }
        }
        ArrayList<QuestionVideo> temp = new ArrayList<>();

        for (QuestionVideo video : listQuestion) {
            boolean checkAdd = false;
            for (QuestionVideo questionVideo : questionVideos) {
                if (video.getId() == questionVideo.getId()) {
                    checkAdd = true;
                }
            }
            if (checkAdd == false) {
                temp.add(video);
            }
        }
        listQuestion = temp;
        return questionVideos;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.name);
        dest.writeString(this.desc);
        dest.writeInt(this.count);
    }

    protected QuestionVideoCategory(Parcel in) {
        this.id = in.readInt();
        this.name = in.readString();
        this.desc = in.readString();
        this.count = in.readInt();
    }

    public static final Creator<QuestionVideoCategory> CREATOR = new Creator<QuestionVideoCategory>() {
        @Override
        public QuestionVideoCategory createFromParcel(Parcel source) {
            return new QuestionVideoCategory(source);
        }

        @Override
        public QuestionVideoCategory[] newArray(int size) {
            return new QuestionVideoCategory[size];
        }
    };
}
