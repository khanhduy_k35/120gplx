package com.vcarcorp.ontap120tinhhuong.mophonggiaothong.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import androidx.recyclerview.widget.RecyclerView;

import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.R;
import com.vcarcorp.ontap120tinhhuong.mophonggiaothong.model.QuestionVideo;
import com.ads.control.customview.CustomTextView;

import java.util.List;

public class QuestionResultWrongAdapterRecycler extends RecyclerView.Adapter<QuestionResultWrongAdapterRecycler.ViewHolder> {

    private List<QuestionVideo> data;
    private Context context;
    private ItemClickListener mClickListener;
    private int indexSelection;

    public QuestionResultWrongAdapterRecycler(final List<QuestionVideo> data, Context mContext) {
        this.data = data;
        this.context = mContext;
    }

    @Override
    public ViewHolder onCreateViewHolder(final ViewGroup parent,  @SuppressLint("RecyclerView") int viewType) {
        this.context = parent.getContext();
        return new ViewHolder(LayoutInflater.from(context)
                .inflate(R.layout.item_question_result, parent, false));
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder,@SuppressLint("RecyclerView") int position) {
        final QuestionVideo item = data.get(position);
        holder.tvtTitle.setText(item.getDescription());
        holder.numberQuestion.setText(String.valueOf(item.getId()));
        holder.resultQuestion.setVisibility(View.GONE);
        if (item.getTimeSelectedWrong() > 0) {
            holder.rltAnswer.setVisibility(View.VISIBLE);
            if (scoreVideo(item) > 0) {
                holder.rltAnswer.setBackgroundResource(R.drawable.b2_grid_item_question_bg_success);
                holder.tvAnswer.setText(scoreVideo(item) + "");
            } else {
                holder.rltAnswer.setBackgroundResource(R.drawable.b2_grid_item_question_bg_error);
                holder.tvAnswer.setText("0");
            }
        } else {
            holder.rltAnswer.setVisibility(View.GONE);
            holder.resultQuestion.setVisibility(View.GONE);
        }
        if (this.indexSelection == position) {
            holder.rtlView.setBackgroundResource(R.drawable.right_item_selected_stroke);
            holder.numberQuestion.setBackgroundResource(R.drawable.b2_grid_item_question_bg_selected);
        } else {
            holder.numberQuestion.setBackgroundResource(R.drawable.b2_grid_item_question_bg);
            holder.rtlView.setBackgroundResource(R.drawable.right_item_selected_none);
        }
        holder.rtlView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mClickListener != null) mClickListener.onItemClick(view, position);
            }
        });
    }

    public void setIndexSelection(int indexSelection) {
        this.indexSelection = indexSelection;
    }

    public int getIndexSelection() {
        return indexSelection;
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private CustomTextView numberQuestion;
        private ImageView resultQuestion;
        private com.rey.material.widget.RelativeLayout rtlView;
        private RelativeLayout rltAnswer;
        private CustomTextView tvAnswer;
        private CustomTextView tvtTitle;

        public ViewHolder(View v) {
            super(v);
            ViewGroup.LayoutParams params = v.getLayoutParams();
            numberQuestion = (CustomTextView) v.findViewById(R.id.tvNumberQuest);
            resultQuestion = (ImageView) v.findViewById(R.id.imvNumberQuest);
            rtlView = (com.rey.material.widget.RelativeLayout) v.findViewById(R.id.rltView);
            rltAnswer = v.findViewById(R.id.rltAnswer);
            tvAnswer = v.findViewById(R.id.tvAnswer);
            tvtTitle = v.findViewById(R.id.tvtTitle);
        }
    }

    private int scoreVideo(QuestionVideo questionVideo){
        int score = 0;
        if(questionVideo.getTimeSelectedWrong() > 0){
            int distance = (questionVideo.getEnd_pos() - questionVideo.getBegin_pos())/5;
            if(questionVideo.getTimeSelectedWrong() >= questionVideo.getBegin_pos() && questionVideo.getTimeSelectedWrong() < (questionVideo.getBegin_pos() + distance)){
                score = 5;
            }else if(questionVideo.getTimeSelectedWrong() >= (questionVideo.getBegin_pos() + distance)  && questionVideo.getTimeSelectedWrong() < (questionVideo.getBegin_pos() + distance *2)){
                score = 4;
            }else if(questionVideo.getTimeSelectedWrong() >= (questionVideo.getBegin_pos() + distance*2)  && questionVideo.getTimeSelectedWrong() < (questionVideo.getBegin_pos() + distance *3)){
                score = 3;
            }else if(questionVideo.getTimeSelectedWrong() >= (questionVideo.getBegin_pos() + distance*3)  && questionVideo.getTimeSelectedWrong() < (questionVideo.getBegin_pos() + distance *4)){
                score = 2;
            }else if(questionVideo.getTimeSelectedWrong() >= (questionVideo.getBegin_pos() + distance*4)  && questionVideo.getTimeSelectedWrong() < (questionVideo.getBegin_pos() + distance *5)){
                score = 1;
            }
            questionVideo.setScore(score);
            return score;
        }
        return 0;
    }


    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }
}