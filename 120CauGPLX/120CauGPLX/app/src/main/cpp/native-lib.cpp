#include <jni.h>
#include <string>
#include <syslog.h>
#include "native-lib.h"
extern "C" JNIEXPORT jstring
JNICALL
Java_com_vcarcorp_ontap120tinhhuong_mophonggiaothong_main_JNIUtil_apiKey(JNIEnv *env, jclass object,jobject p_context) {
    char *version_name = "1.0.3.0";
    int version_code = 1030;
    std::string cdl_key_return = "120GPLX1@#$%";
    std::string cdl_key_return_empty = "";
    jobject context = getApplicationContext(env,p_context);
    jobject package_info = getPackageInfo(env,context);
    if(package_info != NULL){
        jclass cls = env->GetObjectClass(package_info);
        jfieldID fidInt = env->GetFieldID(cls,"versionCode","I");
        jint versionCode = env->GetIntField(package_info, fidInt);
        jfieldID fidString = env->GetFieldID(cls,"versionName","Ljava/lang/String;");
        jstring versionName = (jstring)env->GetObjectField(package_info, fidString);

        jstring jStringCompare = env->NewStringUTF(version_name);
        jclass cls_string = env->GetObjectClass(versionName);
        jmethodID mID = env->GetMethodID(cls_string, "equals", "(Ljava/lang/Object;)Z");
        jboolean equals = env->CallBooleanMethod(versionName, mID, jStringCompare);

        if(versionCode == version_code && equals){
            return env->NewStringUTF(cdl_key_return.c_str());
        }
    }
    return env->NewStringUTF(cdl_key_return_empty.c_str());
}

extern "C" JNIEXPORT jstring
JNICALL
Java_com_vcarcorp_ontap120tinhhuong_mophonggiaothong_main_JNIUtil_getServer(JNIEnv *env, jclass object,jobject p_context,jint argv) {
    char *version_name = "1.0.3.0";
    int version_code = 1030;
    std::string server_1 = "http://103.226.250.77/120caugplx/DoneVideo/"; //con
    std::string server_2 = "http://103.226.251.89/120caugplx/DoneVideo/"; //con
    std::string server_3 = "http://103.226.250.114/120caugplx/DoneVideo/"; //con
    std::string server_5 = "http://103.226.251.33/120caugplx/DoneVideo/"; //con
    std::string server_empty = "";

    jobject context = getApplicationContext(env,p_context);
    jobject package_info = getPackageInfo(env,context);
    if(package_info != NULL){
        jclass cls = env->GetObjectClass(package_info);
        jfieldID fidInt = env->GetFieldID(cls,"versionCode","I");
        jint versionCode = env->GetIntField(package_info, fidInt);
        jfieldID fidString = env->GetFieldID(cls,"versionName","Ljava/lang/String;");
        jstring versionName = (jstring)env->GetObjectField(package_info, fidString);

        jstring jStringCompare = env->NewStringUTF(version_name);
        jclass cls_string = env->GetObjectClass(versionName);
        jmethodID mID = env->GetMethodID(cls_string, "equals", "(Ljava/lang/Object;)Z");
        jboolean equals = env->CallBooleanMethod(versionName, mID, jStringCompare);

        if(versionCode == version_code && equals){
            if(argv == 0){
                return env->NewStringUTF(server_1.c_str());
            }else if(argv == 1){
                return env->NewStringUTF(server_2.c_str());
            }else  if(argv == 2){
                return env->NewStringUTF(server_3.c_str());
            }else  if(argv == 3){
                return env->NewStringUTF(server_1.c_str());
            }else  if(argv == 4){
                return env->NewStringUTF(server_5.c_str());
            }else  if(argv == 5){
                return env->NewStringUTF(server_2.c_str());
            }else {
                return env->NewStringUTF(server_empty.c_str());
            }
        }
    }
    return env->NewStringUTF(server_empty.c_str());
}

extern "C" JNIEXPORT jstring
JNICALL
Java_com_vcarcorp_ontap120tinhhuong_mophonggiaothong_main_JNIUtil_getServerVip(JNIEnv *env, jclass object,jobject p_context) {
    char *version_name = "1.0.2.1";
    int version_code = 1021;
    std::string server_vip = "http://103.226.248.172/120caugplx/DoneVideo/";
    std::string server_empty = "";

    jobject context = getApplicationContext(env,p_context);
    jobject package_info = getPackageInfo(env,context);
    if(package_info != NULL){
        jclass cls = env->GetObjectClass(package_info);
        jfieldID fidInt = env->GetFieldID(cls,"versionCode","I");
        jint versionCode = env->GetIntField(package_info, fidInt);
        jfieldID fidString = env->GetFieldID(cls,"versionName","Ljava/lang/String;");
        jstring versionName = (jstring)env->GetObjectField(package_info, fidString);

        jstring jStringCompare = env->NewStringUTF(version_name);
        jclass cls_string = env->GetObjectClass(versionName);
        jmethodID mID = env->GetMethodID(cls_string, "equals", "(Ljava/lang/Object;)Z");
        jboolean equals = env->CallBooleanMethod(versionName, mID, jStringCompare);

        if(versionCode == version_code && equals){
            return env->NewStringUTF(server_vip.c_str());
        }else{
            return env->NewStringUTF(server_empty.c_str());
        }
    }
    return env->NewStringUTF(server_empty.c_str());
}

jobject getPackageInfo(JNIEnv *jniEnv,jobject context) {
    jclass context_clz = jniEnv->GetObjectClass(context);
    jmethodID get_package_manager_method_id = jniEnv->GetMethodID(context_clz,
                                                                  "getPackageManager",
                                                                  "()Landroid/content/pm/PackageManager;");
    jobject package_manager = jniEnv->CallObjectMethod(context, get_package_manager_method_id);
    jclass package_manager_clz = jniEnv->GetObjectClass(package_manager);
    jmethodID get_package_info_method_id = jniEnv->GetMethodID(package_manager_clz,
                                                               "getPackageInfo",
                                                               "(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;");
    jobject package_info = jniEnv->CallObjectMethod(package_manager, get_package_info_method_id,
                                                    getPackageName(jniEnv,context), 64);
    jniEnv->DeleteLocalRef(context_clz);
    jniEnv->DeleteLocalRef(package_manager);
    jniEnv->DeleteLocalRef(package_manager_clz);
    return package_info;
}

jstring getPackageName(JNIEnv *jniEnv,jobject context) {
    jclass context_clz = jniEnv->GetObjectClass(context);
    jmethodID get_package_name_method_id = jniEnv->GetMethodID(context_clz,
                                                               "getPackageName",
                                                               "()Ljava/lang/String;");
    jstring packageName = (jstring) jniEnv->CallObjectMethod(context, get_package_name_method_id);
    jniEnv->DeleteLocalRef(context_clz);
    return packageName;
}

jobject getApplicationContext(JNIEnv *jniEnv,jobject context) {
    jobject application = NULL;
    jclass application_clz = jniEnv->FindClass("android/app/ActivityThread");
    if (application_clz != NULL) {
        jmethodID current_application_method_id = jniEnv->GetStaticMethodID(application_clz,
                                                                            "currentApplication",
                                                                            "()Landroid/app/Application;");
        if (current_application_method_id != NULL) {
            application = jniEnv->CallStaticObjectMethod(application_clz,
                                                         current_application_method_id);
        }
        jniEnv->DeleteLocalRef(application_clz);
    }
    if (application == NULL) {
        application = context;
    }
    return application;
}