#include <jni.h>
#include <string>
#include <syslog.h>

jobject getApplicationContext(JNIEnv *jniEnv,jobject context);
jstring getPackageName(JNIEnv *jniEnv,jobject context);
jobject getPackageInfo(JNIEnv *jniEnv,jobject context);