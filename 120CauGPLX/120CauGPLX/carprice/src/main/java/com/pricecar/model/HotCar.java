package com.pricecar.model;

import com.google.gson.annotations.SerializedName;

public class HotCar {
    @SerializedName("id")
    private String id;

    @SerializedName("title")
    private String title;

    public HotCar() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
