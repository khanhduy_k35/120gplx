package com.pricecar.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class VersionApp implements Parcelable {

    @SerializedName("version")
    public String version;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.version);
    }

    public void readFromParcel(Parcel source) {
        this.version = source.readString();
    }

    public VersionApp() {
    }

    protected VersionApp(Parcel in) {
        this.version = in.readString();
    }

    public static final Parcelable.Creator<VersionApp> CREATOR = new Parcelable.Creator<VersionApp>() {
        @Override
        public VersionApp createFromParcel(Parcel source) {
            return new VersionApp(source);
        }

        @Override
        public VersionApp[] newArray(int size) {
            return new VersionApp[size];
        }
    };

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }
}
