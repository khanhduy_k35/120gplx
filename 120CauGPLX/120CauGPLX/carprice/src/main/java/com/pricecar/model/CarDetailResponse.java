package com.pricecar.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CarDetailResponse extends BaseResponse {
    @SerializedName("cars_detail")
    @Expose
    private Car car;

    public Car getCar() {
        return car;
    }

    public void setCar(Car car) {
        this.car = car;
    }
}
