package com.pricecar.fragment;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Handler;
import android.os.Looper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.pricecar.R;
import com.pricecar.activity.CarDetailActivity;
import com.pricecar.activity.CarSpecificationsActivity;
import com.pricecar.adapter.CarDetailVersionAdapter;
import com.pricecar.adapter.CarSpecificationsAdapter;
import com.pricecar.model.Car;
import com.pricecar.model.TechInfo;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;

public class CarSpecificationsFragment extends Fragment {

    private RecyclerView recycleView;
    private CarSpecificationsAdapter carSpecificationsAdapter;
    private ArrayList<Car> cars = new ArrayList<>();
    private Car car = new Car();
    private ArrayList<TechInfo> techInfo = new ArrayList<>();
    private RecyclerView recycleVersion;
    private CarDetailVersionAdapter carDetailVersionAdapter;

    public CarSpecificationsFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_car_specifications, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        recycleView = view.findViewById(R.id.recycle_view);
        recycleVersion = view.findViewById(R.id.recycle_version);
        recycleView.setNestedScrollingEnabled(false);
        carSpecificationsAdapter = new CarSpecificationsAdapter(techInfo, getContext());
        carSpecificationsAdapter.setClickListener(new CarSpecificationsAdapter.ItemClickListener() {
            @Override
            public void onItemClick() {
                ((CarDetailActivity) getActivity()).reloadSizeViewPager();
            }
        });
        recycleView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recycleView.setAdapter(carSpecificationsAdapter);

        carDetailVersionAdapter = new CarDetailVersionAdapter(cars, car, getContext());
        recycleVersion.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        recycleVersion.setAdapter(carDetailVersionAdapter);

        carDetailVersionAdapter.setClickListener(new CarDetailVersionAdapter.ItemClickListener() {
            @Override
            public void onItemClick(Car car) {
                CarSpecificationsFragment.this.car = car;
                carDetailVersionAdapter.notifyData(CarSpecificationsFragment.this.car);
                final Handler handler = new Handler(Looper.getMainLooper());
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                progressCar();
                            }
                        });
                    }
                }, 100);
            }
        });

        if (car.getTechBase() != "" && carSpecificationsAdapter != null) {
            progressCar();
        }

        view.findViewById(R.id.tv_show_more).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(), CarSpecificationsActivity.class);
                intent.putExtra("Car", car);
                startActivity(intent);
            }
        });

    }

    private void progressCar(){
        techInfo = new ArrayList<>();
        TechInfo base = new TechInfo();
        base.setTitle("Thông số kỹ thuật");
        ArrayList<String> info = new ArrayList<>();
        try {
            JSONObject obj = new JSONObject(car.getTechBase());
            Iterator<String> keys = obj.keys();
            while(keys.hasNext()) {
                String temp = obj.getString(keys.next());
                if (temp.split("::").length > 1) {
                    info.add(temp);
                }
            }
        } catch (Throwable t) {
        }
        base.setDetail(info);
        techInfo.add(base);
        carSpecificationsAdapter.notifyData(techInfo);
        ((CarDetailActivity) getActivity()).reloadSizeViewPager();
    }

    public void setCar(ArrayList<Car> cars, Car car) {
        this.cars = cars;
        this.car = car;
        if (this != null) {
            if (car.getTechBase() != "" && carSpecificationsAdapter != null) {
                progressCar();
            }
        }
    }
}