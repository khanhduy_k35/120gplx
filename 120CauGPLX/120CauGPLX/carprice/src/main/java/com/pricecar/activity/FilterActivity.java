package com.pricecar.activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;

import com.ads.control.admob.AdCloseListener;
import com.ads.control.admob.AdmobHelp;
import com.ads.control.utils.SharedPreferencesUtils;
import com.ads.control.utils.Utils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.pricecar.R;
import com.pricecar.model.Car;

import java.lang.reflect.Type;
import java.util.ArrayList;

import co.lujun.androidtagview.TagContainerLayout;
import co.lujun.androidtagview.TagView;

public class FilterActivity extends BaseCarActivity {

    private TagContainerLayout tagCarBrand, tagVehicleType, tagVehicleSegment, tagCarSource, tagNumberSeat, tagFuel;
    private ArrayList<Car> cars = new ArrayList<>();
    private ArrayList<String> carBrand = new ArrayList<>();
    private ArrayList<String> vehicleType = new ArrayList<>();
    private ArrayList<String> vehicleSegment = new ArrayList<>();
    private ArrayList<String> carSource = new ArrayList<>();
    private ArrayList<String> numberSeat = new ArrayList<>();
    private ArrayList<String> fuel = new ArrayList<>();

    private ArrayList<int[]> carBrandColor = new ArrayList<>();
    private ArrayList<int[]> vehicleTypeColor = new ArrayList<>();
    private ArrayList<int[]> vehicleSegmentColor = new ArrayList<>();
    private ArrayList<int[]> carSourceColor = new ArrayList<>();
    private ArrayList<int[]> numberSeatColor = new ArrayList<>();
    private ArrayList<int[]> fuelColor = new ArrayList<>();

    private ArrayList<String> carBrandAdd = new ArrayList<>();
    private ArrayList<String> vehicleTypeAdd = new ArrayList<>();
    private ArrayList<String> vehicleSegmentAdd = new ArrayList<>();
    private ArrayList<String> carSourceAdd = new ArrayList<>();
    private ArrayList<String> numberSeatAdd = new ArrayList<>();
    private ArrayList<String> fuelAdd = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter);
        AdmobHelp.getInstance().loadBanner(this);
        Gson gson = new Gson();
        Type listCar = new TypeToken<ArrayList<Car>>() {}.getType();
        cars = gson.fromJson(SharedPreferencesUtils.getString("CarNormal", ""), listCar);

        for (Car car : cars) {
            if (carBrand.lastIndexOf(car.getCar_brand()) == -1) {
                if (car.getCar_brand() != null && !car.getCar_brand().equals("")) {
                    carBrand.add(car.getCar_brand());
                    int[] color = {Utils.getColorFromAttr(FilterActivity.this, R.attr.background_row), Color.TRANSPARENT, Utils.getColorFromAttr(FilterActivity.this, R.attr.color_text_item_setting), Color.parseColor("#00C6C1")};
                    carBrandColor.add(color);
                }
            }

            if (vehicleType.lastIndexOf(car.getVehicle_type()) == -1) {
                if (car.getVehicle_type() != null && !car.getVehicle_type().equals("")) {
                    vehicleType.add(car.getVehicle_type());
                    int[] color = {Utils.getColorFromAttr(FilterActivity.this, R.attr.background_row), Color.TRANSPARENT, Utils.getColorFromAttr(FilterActivity.this, R.attr.color_text_item_setting), Color.parseColor("#00C6C1")};
                    vehicleTypeColor.add(color);
                }
            }

            if (vehicleSegment.lastIndexOf(car.getVehicle_segment()) == -1) {
                if (car.getVehicle_segment() != null && !car.getVehicle_segment().equals("")) {
                    vehicleSegment.add(car.getVehicle_segment());
                    int[] color = {Utils.getColorFromAttr(FilterActivity.this, R.attr.background_row), Color.TRANSPARENT, Utils.getColorFromAttr(FilterActivity.this, R.attr.color_text_item_setting), Color.parseColor("#00C6C1")};
                    vehicleSegmentColor.add(color);
                }
            }

            if (carSource.lastIndexOf(car.getCar_source()) == -1) {
                if (car.getCar_source() != null && !car.getCar_source().equals("")) {
                    carSource.add(car.getCar_source());
                    int[] color = {Utils.getColorFromAttr(FilterActivity.this, R.attr.background_row), Color.TRANSPARENT, Utils.getColorFromAttr(FilterActivity.this, R.attr.color_text_item_setting), Color.parseColor("#00C6C1")};
                    carSourceColor.add(color);
                }
            }

            if (numberSeat.lastIndexOf(car.getNumberSeat()) == -1) {
                if (car.getNumberSeat() != null && !car.getNumberSeat().equals("")) {
                    numberSeat.add(car.getNumberSeat());
                    int[] color = {Utils.getColorFromAttr(FilterActivity.this, R.attr.background_row), Color.TRANSPARENT, Utils.getColorFromAttr(FilterActivity.this, R.attr.color_text_item_setting), Color.parseColor("#00C6C1")};
                    numberSeatColor.add(color);
                }
            }

            if (fuel.lastIndexOf(car.getFlue()) == -1) {
                if (car.getFlue() != null && !car.getFlue().equals("")) {
                    fuel.add(car.getFlue());
                    int[] color = {Utils.getColorFromAttr(FilterActivity.this, R.attr.background_row), Color.TRANSPARENT, Utils.getColorFromAttr(FilterActivity.this, R.attr.color_text_item_setting), Color.parseColor("#00C6C1")};
                    fuelColor.add(color);
                }
            }
        }

        tagCarBrand = findViewById(R.id.tag_car_brand);
        tagVehicleType = findViewById(R.id.tag_vehicle_type);
        tagVehicleSegment = findViewById(R.id.tag_vehicle_segment);
        tagCarSource = findViewById(R.id.tag_car_source);
        tagNumberSeat = findViewById(R.id.tag_number_seat);
        tagFuel = findViewById(R.id.tag_fuel);

        tagCarBrand.setTags(carBrand, carBrandColor);
        tagVehicleType.setTags(vehicleType, vehicleTypeColor);
        tagVehicleSegment.setTags(vehicleSegment, vehicleSegmentColor);
        tagCarSource.setTags(carSource, carSourceColor);
        tagNumberSeat.setTags(numberSeat, numberSeatColor);
        tagFuel.setTags(fuel, fuelColor);

        tagCarBrand.setOnTagClickListener(new TagView.OnTagClickListener() {

            @Override
            public void onTagClick(int position, String text) {
                if (carBrandAdd.lastIndexOf(text) == -1) {
                    carBrandAdd.add(text);
                    int[] color = {Color.parseColor("#00C6C1"), Color.TRANSPARENT, Color.WHITE, Color.parseColor("#00C6C1")};
                    carBrandColor.set(position, color);
                } else {
                    carBrandAdd.remove(text);
                    int[] color = {Utils.getColorFromAttr(FilterActivity.this, R.attr.background_row), Color.TRANSPARENT, Utils.getColorFromAttr(FilterActivity.this, R.attr.color_text_item_setting), Color.parseColor("#00C6C1")};
                    carBrandColor.set(position, color);
                }
                tagCarBrand.setTags(carBrand, carBrandColor);
            }

            @Override
            public void onTagLongClick(final int position, String text) {
            }

            @Override
            public void onSelectedTagDrag(int position, String text){
            }

            @Override
            public void onTagCrossClick(int position) {
            }
        });

        tagVehicleType.setOnTagClickListener(new TagView.OnTagClickListener() {

            @Override
            public void onTagClick(int position, String text) {
                if (vehicleTypeAdd.lastIndexOf(text) == -1) {
                    vehicleTypeAdd.add(text);
                    int[] color = {Color.parseColor("#00C6C1"), Color.TRANSPARENT, Color.WHITE, Color.parseColor("#00C6C1")};
                    vehicleTypeColor.set(position, color);
                } else {
                    vehicleTypeAdd.remove(text);
                    int[] color = {Utils.getColorFromAttr(FilterActivity.this, R.attr.background_row), Color.TRANSPARENT, Utils.getColorFromAttr(FilterActivity.this, R.attr.color_text_item_setting), Color.parseColor("#00C6C1")};
                    vehicleTypeColor.set(position, color);
                }
                tagVehicleType.setTags(vehicleType, vehicleTypeColor);
            }

            @Override
            public void onTagLongClick(final int position, String text) {
            }

            @Override
            public void onSelectedTagDrag(int position, String text){
            }

            @Override
            public void onTagCrossClick(int position) {
            }
        });

        tagVehicleSegment.setOnTagClickListener(new TagView.OnTagClickListener() {

            @Override
            public void onTagClick(int position, String text) {
                if (vehicleSegmentAdd.lastIndexOf(text) == -1) {
                    vehicleSegmentAdd.add(text);
                    int[] color = {Color.parseColor("#00C6C1"), Color.TRANSPARENT, Color.WHITE, Color.parseColor("#00C6C1")};
                    vehicleSegmentColor.set(position, color);
                } else {
                    vehicleSegmentAdd.remove(text);
                    int[] color = {Utils.getColorFromAttr(FilterActivity.this, R.attr.background_row), Color.TRANSPARENT, Utils.getColorFromAttr(FilterActivity.this, R.attr.color_text_item_setting), Color.parseColor("#00C6C1")};
                    vehicleSegmentColor.set(position, color);
                }
                tagVehicleSegment.setTags(vehicleSegment, vehicleSegmentColor);
            }

            @Override
            public void onTagLongClick(final int position, String text) {
            }

            @Override
            public void onSelectedTagDrag(int position, String text){
            }

            @Override
            public void onTagCrossClick(int position) {
            }
        });

        tagCarSource.setOnTagClickListener(new TagView.OnTagClickListener() {

            @Override
            public void onTagClick(int position, String text) {
                if (carSourceAdd.lastIndexOf(text) == -1) {
                    carSourceAdd.add(text);
                    int[] color = {Color.parseColor("#00C6C1"), Color.TRANSPARENT, Color.WHITE, Color.parseColor("#00C6C1")};
                    carSourceColor.set(position, color);
                } else {
                    carSourceAdd.remove(text);
                    int[] color = {Utils.getColorFromAttr(FilterActivity.this, R.attr.background_row), Color.TRANSPARENT, Utils.getColorFromAttr(FilterActivity.this, R.attr.color_text_item_setting), Color.parseColor("#00C6C1")};
                    carSourceColor.set(position, color);
                }
                tagCarSource.setTags(carSource, carSourceColor);
            }

            @Override
            public void onTagLongClick(final int position, String text) {
            }

            @Override
            public void onSelectedTagDrag(int position, String text){
            }

            @Override
            public void onTagCrossClick(int position) {
            }
        });

        tagNumberSeat.setOnTagClickListener(new TagView.OnTagClickListener() {

            @Override
            public void onTagClick(int position, String text) {
                if (numberSeatAdd.lastIndexOf(text) == -1) {
                    numberSeatAdd.add(text);
                    int[] color = {Color.parseColor("#00C6C1"), Color.TRANSPARENT, Color.WHITE, Color.parseColor("#00C6C1")};
                    numberSeatColor.set(position, color);
                } else {
                    numberSeatAdd.remove(text);
                    int[] color = {Utils.getColorFromAttr(FilterActivity.this, R.attr.background_row), Color.TRANSPARENT, Utils.getColorFromAttr(FilterActivity.this, R.attr.color_text_item_setting), Color.parseColor("#00C6C1")};
                    numberSeatColor.set(position, color);
                }
                tagNumberSeat.setTags(numberSeat, numberSeatColor);
            }

            @Override
            public void onTagLongClick(final int position, String text) {
            }

            @Override
            public void onSelectedTagDrag(int position, String text){
            }

            @Override
            public void onTagCrossClick(int position) {
            }
        });

        tagFuel.setOnTagClickListener(new TagView.OnTagClickListener() {

            @Override
            public void onTagClick(int position, String text) {
                if (fuelAdd.lastIndexOf(text) == -1) {
                    fuelAdd.add(text);
                    int[] color = {Color.parseColor("#00C6C1"), Color.TRANSPARENT, Color.WHITE, Color.parseColor("#00C6C1")};
                    fuelColor.set(position, color);
                } else {
                    fuelAdd.remove(text);
                    int[] color = {Utils.getColorFromAttr(FilterActivity.this, R.attr.background_row), Color.TRANSPARENT, Utils.getColorFromAttr(FilterActivity.this, R.attr.color_text_item_setting), Color.parseColor("#00C6C1")};
                    fuelColor.set(position, color);
                }
                tagFuel.setTags(fuel, fuelColor);
            }

            @Override
            public void onTagLongClick(final int position, String text) {
            }

            @Override
            public void onSelectedTagDrag(int position, String text){
            }

            @Override
            public void onTagCrossClick(int position) {
            }
        });


        findViewById(R.id.btnNext).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AdmobHelp.getInstance().showInterstitialAd(new AdCloseListener() {
                    @Override
                    public void onAdClosed(boolean hasAds) {
                        Intent intent = new Intent(FilterActivity.this, CarListActivity.class);
                        intent.putExtra("isFilter", true);
                        intent.putExtra("carBrandAdd", carBrandAdd);
                        intent.putExtra("vehicleTypeAdd", vehicleTypeAdd);
                        intent.putExtra("vehicleSegmentAdd", vehicleSegmentAdd);
                        intent.putExtra("carSourceAdd", carSourceAdd);
                        intent.putExtra("numberSeatAdd", numberSeatAdd);
                        intent.putExtra("fuelAdd", fuelAdd);
                        startActivity(intent);
                    }
                });
            }
        });

        findViewById(R.id.btn_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }
}