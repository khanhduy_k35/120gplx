package com.pricecar.fragment;

import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;

import com.ads.control.customview.CustomTextView;
import com.ads.control.utils.SharedPreferencesUtils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.pricecar.R;
import com.pricecar.adapter.OptionDialogAdapter;
import com.pricecar.adapter.OptionLocationAdapter;
import com.pricecar.model.Car;
import com.pricecar.model.Province;
import com.rey.material.widget.RelativeLayout;

import java.lang.reflect.Type;
import java.util.ArrayList;

public class CarPriceFragment extends Fragment {

    private CustomTextView tvVersion, tvLocation, tvPrice, tvDetail;
    private RelativeLayout rlVersion, rlLocation;
    private Car car = new Car();
    private ArrayList<Car> cars = new ArrayList<>();
    private int idLocation = 1;

    public CarPriceFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_car_price, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        idLocation = SharedPreferencesUtils.getInt("ProvinceSelect");
        if (idLocation == -1) {
            idLocation = 1;
        }

        tvVersion = view.findViewById(R.id.tv_version);
        tvLocation = view.findViewById(R.id.tv_location);
        tvPrice = view.findViewById(R.id.tv_price);
        tvDetail = view.findViewById(R.id.tv_detail);
        rlVersion = view.findViewById(R.id.rl_version);
        rlLocation = view.findViewById(R.id.rl_location);

        if (car.getPrice_int() > 0) {
            Gson gson = new Gson();
            Type listBrand = new TypeToken<ArrayList<Province>>() {}.getType();
            ArrayList<Province> provinces = gson.fromJson(SharedPreferencesUtils.getString("Provinces", ""), listBrand);
            for (Province province: provinces) {
                if (province.getId() == idLocation) {
                    tvLocation.setText(province.getName());
                }
            }
            tvVersion.setText(car.getVersion() + " - " + car.getListed_price());
            tvPrice.setText(convertToDisplay(calculatorPrice(car, idLocation)));
        }

        tvDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final int[] idProvince = {idLocation};
                final Car[] carSelect = {car};
                Dialog dialog = new Dialog(getContext());
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                Window window = dialog.getWindow();
                window.setBackgroundDrawableResource(android.R.color.transparent);
                dialog.setCanceledOnTouchOutside(false);
                dialog.setCancelable(true);
                dialog.setContentView(R.layout.dialog_price);
                dialog.getWindow().setGravity(Gravity.CENTER);
                dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                RelativeLayout rlVersion = dialog.findViewById(R.id.rl_version);
                RelativeLayout rlLocation = dialog.findViewById(R.id.rl_location);
                CustomTextView tvVersion = dialog.findViewById(R.id.tv_version);
                CustomTextView tvLocation = dialog.findViewById(R.id.tv_location);
                CustomTextView tvPrice = dialog.findViewById(R.id.tv_price);
                CustomTextView tvRegistrationTitle = dialog.findViewById(R.id.tv_registration_title);
                CustomTextView tvRegistrationFee = dialog.findViewById(R.id.tv_registration_fee);
                CustomTextView tvInsurance = dialog.findViewById(R.id.tv_insurance);
                CustomTextView tvLicensePlate = dialog.findViewById(R.id.tv_license_plate);
                CustomTextView tvTotalPrice = dialog.findViewById(R.id.tv_total_price);
                tvVersion.setText(carSelect[0].getVersion());
                tvPrice.setText(convertToDisplay((long) carSelect[0].getPrice_int() * 1000000l));
                tvLocation.setText(CarPriceFragment.this.tvLocation.getText());
                if (carSelect[0].getFlue().equals("Điện")) {
                    tvRegistrationTitle.setText("Phí trước bạ (12%):");
                } else {
                    if (carSelect[0].getVehicle_type().equals("Bán tải")) {
                        if (idProvince[0] == 0) {
                            tvRegistrationTitle.setText("Phí trước bạ (7.2%):");
                            tvRegistrationFee.setText(convertToDisplay((long) carSelect[0].getPrice_int() * 1000000l * 72 / 1000));
                        } else {
                            tvRegistrationTitle.setText("Phí trước bạ (6%):");
                            tvRegistrationFee.setText(convertToDisplay((long) carSelect[0].getPrice_int() * 1000000l * 6 / 100));
                        }
                    } else {
                        if (idProvince[0] == 1) {
                            tvRegistrationTitle.setText("Phí trước bạ (12%):");
                            tvRegistrationFee.setText(convertToDisplay((long) carSelect[0].getPrice_int() * 1000000l * 12 / 100));
                        } else if (idProvince[0] == 27) {
                            tvRegistrationTitle.setText("Phí trước bạ (11%):");
                            tvRegistrationFee.setText(convertToDisplay((long) carSelect[0].getPrice_int() * 1000000l * 11 / 100));
                        } else {
                            tvRegistrationTitle.setText("Phí trước bạ (10%):");
                            tvRegistrationFee.setText(convertToDisplay((long) carSelect[0].getPrice_int() * 1000000l * 10 / 100));
                        }
                    }
                }
                if (idProvince[0] == 1 || idProvince[0] == 2) {
                    tvLicensePlate.setText(convertToDisplay(20000000));
                } else {
                    tvLicensePlate.setText(convertToDisplay(200000));
                }

                if (carSelect[0].getNumberSeat().contains("6") || carSelect[0].getNumberSeat().contains("7") || carSelect[0].getNumberSeat().contains("8") || carSelect[0].getNumberSeat().contains("9")) {
                    tvInsurance.setText(convertToDisplay(794000));
                } else {
                    tvInsurance.setText(convertToDisplay(430700));
                }
                tvTotalPrice.setText(convertToDisplay(calculatorPrice(carSelect[0], idProvince[0])));

                rlVersion.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        PopupWindow popup = new PopupWindow(getContext());
                        View dialog = getLayoutInflater().inflate(R.layout.dialog_version, null);
                        popup.setContentView(dialog);
                        popup.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
                        popup.setWidth(WindowManager.LayoutParams.WRAP_CONTENT);
                        popup.setOutsideTouchable(true);
                        popup.setFocusable(true);
                        popup.setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                        popup.showAsDropDown(rlVersion,0 , 0, Gravity.NO_GRAVITY);
                        RecyclerView recyclerView = dialog.findViewById(R.id.recycle_view);
                        OptionDialogAdapter adapter = new OptionDialogAdapter(cars, carSelect[0], getContext());
                        adapter.setClickListener(new OptionDialogAdapter.ItemClickListener() {
                            @Override
                            public void onItemClick(Car car) {
                                carSelect[0] = car;
                                tvVersion.setText(car.getVersion());
                                tvPrice.setText(convertToDisplay((long) car.getPrice_int() * 1000000l));
                                if (car.getFlue().equals("Điện")) {
                                    tvRegistrationTitle.setText("Phí trước bạ (12%):");
                                } else {
                                    if (car.getVehicle_type().equals("Bán tải")) {
                                        if (idProvince[0] == 1) {
                                            tvRegistrationTitle.setText("Phí trước bạ (7.2%):");
                                            tvRegistrationFee.setText(convertToDisplay((long) car.getPrice_int() * 1000000l * 72 / 1000));
                                        } else {
                                            tvRegistrationTitle.setText("Phí trước bạ (6%):");
                                            tvRegistrationFee.setText(convertToDisplay((long) car.getPrice_int() * 1000000l * 6 / 100));
                                        }
                                    } else {
                                        if (idProvince[0] == 1) {
                                            tvRegistrationTitle.setText("Phí trước bạ (12%):");
                                            tvRegistrationFee.setText(convertToDisplay((long) car.getPrice_int() * 1000000l * 12 / 100));
                                        } else if (idProvince[0] == 27) {
                                            tvRegistrationTitle.setText("Phí trước bạ (11%):");
                                            tvRegistrationFee.setText(convertToDisplay((long) car.getPrice_int() * 1000000l * 11 / 100));
                                        } else {
                                            tvRegistrationTitle.setText("Phí trước bạ (10%):");
                                            tvRegistrationFee.setText(convertToDisplay((long) car.getPrice_int() * 1000000l * 10 / 100));
                                        }

                                    }
                                }
                                if (car.getNumberSeat().contains("6") || car.getNumberSeat().contains("7") || car.getNumberSeat().contains("8") || car.getNumberSeat().contains("9")) {
                                    tvInsurance.setText(convertToDisplay(794000));
                                } else {
                                    tvInsurance.setText(convertToDisplay(430700));
                                }

                                tvTotalPrice.setText(convertToDisplay(calculatorPrice(car, idProvince[0])));
                                popup.dismiss();
                            }
                        });
                        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
                        recyclerView.setAdapter(adapter);
                    }
                });

                rlLocation.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Gson gson = new Gson();
                        Type listBrand = new TypeToken<ArrayList<Province>>() {}.getType();
                        ArrayList<Province> provinces = gson.fromJson(SharedPreferencesUtils.getString("Provinces", ""), listBrand);

                        PopupWindow popup = new PopupWindow(getContext());
                        View dialog = getLayoutInflater().inflate(R.layout.dialog_version, null);
                        popup.setContentView(dialog);
                        popup.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
                        popup.setWidth(WindowManager.LayoutParams.WRAP_CONTENT);
                        popup.setOutsideTouchable(true);
                        popup.setFocusable(true);
                        popup.setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                        popup.showAsDropDown(rlLocation,0 , 0, Gravity.NO_GRAVITY);
                        RecyclerView recyclerView = dialog.findViewById(R.id.recycle_view);
                        OptionLocationAdapter adapter = new OptionLocationAdapter(provinces, idProvince[0], getContext());
                        adapter.setClickListener(new OptionLocationAdapter.ItemClickListener() {
                            @Override
                            public void onItemClick(Province province) {
                                idProvince[0] = province.getId();
                                tvLocation.setText(province.getName());
                                tvVersion.setText(carSelect[0].getVersion());
                                if (car.getFlue().equals("Điện")) {
                                    tvRegistrationTitle.setText("Phí trước bạ (12%):");
                                } else {
                                    if (car.getVehicle_type().equals("Bán tải")) {
                                        if (idProvince[0] == 1) {
                                            tvRegistrationTitle.setText("Phí trước bạ (7.2%):");
                                            tvRegistrationFee.setText(convertToDisplay((long) carSelect[0].getPrice_int() * 1000000l * 72 / 1000));
                                        } else {
                                            tvRegistrationTitle.setText("Phí trước bạ (6%):");
                                            tvRegistrationFee.setText(convertToDisplay((long) carSelect[0].getPrice_int() * 1000000l * 6 / 100));
                                        }
                                    } else {
                                        if (idProvince[0] == 1) {
                                            tvRegistrationTitle.setText("Phí trước bạ (12%):");
                                            tvRegistrationFee.setText(convertToDisplay((long) carSelect[0].getPrice_int() * 1000000l * 12 / 100));
                                        } else if (idProvince[0] == 27) {
                                            tvRegistrationTitle.setText("Phí trước bạ (11%):");
                                            tvRegistrationFee.setText(convertToDisplay((long) carSelect[0].getPrice_int() * 1000000l * 11 / 100));
                                        } else {
                                            tvRegistrationTitle.setText("Phí trước bạ (10%):");
                                            tvRegistrationFee.setText(convertToDisplay((long) carSelect[0].getPrice_int() * 1000000l * 10 / 100));
                                        }

                                    }
                                }
                                if (idProvince[0] == 1 || idProvince[0] == 2) {
                                    tvLicensePlate.setText(convertToDisplay(20000000));
                                } else {
                                    tvLicensePlate.setText(convertToDisplay(1000000));
                                }
                                tvTotalPrice.setText(convertToDisplay(calculatorPrice(carSelect[0], idProvince[0])));
                                popup.dismiss();
                            }
                        });
                        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
                        recyclerView.setAdapter(adapter);
                    }
                });

                ImageView btnClose = (ImageView) dialog.findViewById(R.id.btn_close);
                btnClose.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.cancel();
                        dialog.dismiss();
                    }
                });
                dialog.show();
            }
        });

        rlVersion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupWindow popup = new PopupWindow(getContext());
                View dialog = getLayoutInflater().inflate(R.layout.dialog_version, null);
                popup.setContentView(dialog);
                popup.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
                popup.setWidth(WindowManager.LayoutParams.WRAP_CONTENT);
                popup.setOutsideTouchable(true);
                popup.setFocusable(true);
                popup.setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                popup.showAsDropDown(rlVersion,0 , 0, Gravity.NO_GRAVITY);
                RecyclerView recyclerView = dialog.findViewById(R.id.recycle_view);
                OptionDialogAdapter adapter = new OptionDialogAdapter(cars, car, getContext());
                adapter.setClickListener(new OptionDialogAdapter.ItemClickListener() {
                    @Override
                    public void onItemClick(Car car) {
                        CarPriceFragment.this.car = car;
                        if (this != null) {
                            if (tvVersion != null) {
                                tvVersion.setText(car.getVersion() + " - " + car.getListed_price());
                                tvPrice.setText(convertToDisplay(calculatorPrice(car, idLocation)));
                                popup.dismiss();
                            }
                        }
                    }
                });
                recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
                recyclerView.setAdapter(adapter);
            }
        });

        rlLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Gson gson = new Gson();
                Type listBrand = new TypeToken<ArrayList<Province>>() {}.getType();
                ArrayList<Province> provinces = gson.fromJson(SharedPreferencesUtils.getString("Provinces", ""), listBrand);

                PopupWindow popup = new PopupWindow(getContext());
                View dialog = getLayoutInflater().inflate(R.layout.dialog_version, null);
                popup.setContentView(dialog);
                popup.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
                popup.setWidth(WindowManager.LayoutParams.WRAP_CONTENT);
                popup.setOutsideTouchable(true);
                popup.setFocusable(true);
                popup.setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                popup.showAsDropDown(rlLocation,0 , 0, Gravity.NO_GRAVITY);
                RecyclerView recyclerView = dialog.findViewById(R.id.recycle_view);
                OptionLocationAdapter adapter = new OptionLocationAdapter(provinces, idLocation, getContext());
                adapter.setClickListener(new OptionLocationAdapter.ItemClickListener() {
                    @Override
                    public void onItemClick(Province province) {
                        idLocation = province.getId();
                        SharedPreferencesUtils.setInt("ProvinceSelect", province.getId());
                        if (this != null) {
                            if (tvVersion != null) {
                                tvLocation.setText(province.getName());
                                tvPrice.setText(convertToDisplay(calculatorPrice(car, idLocation)));
                                popup.dismiss();
                            }
                        }
                    }
                });
                recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
                recyclerView.setAdapter(adapter);
            }
        });
    }

    public void setCar(ArrayList<Car> cars, Car car) {
        this.cars = cars;
        this.car = car;
        if (this != null) {
            if (tvVersion != null) {
                tvVersion.setText(car.getVersion() + " - " + car.getListed_price());
                tvLocation.setText("Hà Nội");
                tvPrice.setText(convertToDisplay(calculatorPrice(car, idLocation)));
            }
        }
    }

    public String convertToDisplay(long price) {
        String prString = String.valueOf((long) price * 1l);
        StringBuilder builder = new StringBuilder();
        int count = 0;
        while (count < prString.length()) {
            builder.append(prString.charAt(count));
            count++;
            if ((prString.length() - count) % 3 == 0 && count < prString.length()) {
                builder.append(".");
            }
        }
        return builder.toString() + " đ";
    }

    private long calculatorPrice(Car car, int idLocation) {
        long priceCurrent = (long) car.getPrice_int() * 1000000l;
        long priceFinal = 0;
        long phitruocba = 0;
        if (car.getFlue().equals("Điện")) {
            phitruocba = 0;
        } else {
            if (car.getVehicle_type().equals("Bán tải")) {
                if (idLocation == 1) {
                    phitruocba = priceCurrent * 72 / 1000;
                } else {
                    phitruocba = priceCurrent * 6 / 100;
                }
            } else {
                if (idLocation == 1) {
                    phitruocba = priceCurrent * 12 / 100;
                } else if (idLocation == 27) {
                    phitruocba = priceCurrent * 11 / 100;
                } else {
                    phitruocba = priceCurrent * 10 / 100;
                }
            }
        }

        priceFinal = priceFinal + priceCurrent;
        priceFinal = priceFinal + phitruocba;
        priceFinal += 1560000;
        if (car.getNumberSeat().contains("6") || car.getNumberSeat().contains("7") || car.getNumberSeat().contains("8") || car.getNumberSeat().contains("9")) {
            priceFinal += 794000;
        } else {
            priceFinal += 430700;
        }
        priceFinal += 340000;
        if (idLocation == 1 || idLocation == 2) {
            priceFinal += 20000000;
        } else {
            priceFinal += 1000000;
        }
        return priceFinal;
    }

}