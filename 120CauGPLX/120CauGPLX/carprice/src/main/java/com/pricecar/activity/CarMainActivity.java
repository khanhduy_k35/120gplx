package com.pricecar.activity;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ads.control.admob.AdCloseListener;
import com.ads.control.admob.AdmobHelp;
import com.ads.control.customview.CustomTextView;
import com.ads.control.utils.SharedPreferencesUtils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.pricecar.adapter.CarMainAdapter;
import com.pricecar.adapter.CarBrandAdapter;
import com.pricecar.R;
import com.pricecar.model.AllInfoAppResponse;
import com.pricecar.model.Car;
import com.pricecar.model.CarBrand;
import com.pricecar.model.HotCar;
import com.pricecar.model.Province;
import com.pricecar.model.VehicleSegment;
import com.pricecar.model.VersionResponse;
import com.pricecar.service.AppClient;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;

import retrofit2.Call;
import retrofit2.Callback;

public class CarMainActivity extends BaseCarActivity {

    private LinearLayout viewReviewCar, viewBrandCar, viewNewCar, viewHotCar;
    private RecyclerView recycleReviewCarBrand, recycleCarBrand, recycleNewCar, recycleHotCar;
    private CustomTextView tvTitleHot;
    private ArrayList<CarBrand> carBrands = new ArrayList<>();
    private CarBrandAdapter carBrandAdapter;
    private CarMainAdapter reviewCarMainAdapter;
    private CarMainAdapter newCarMainAdapter;
    private CarMainAdapter hotCarMainAdapter;
    private HotCar hotCar = new HotCar();
    private ArrayList<Car> newCars = new ArrayList<>();
    private ArrayList<Car> hotCars = new ArrayList<>();
    private ArrayList<Car> previewCar = new ArrayList<>();
    private ArrayList<Car> carNormal = new ArrayList<Car>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.activity_car_main);
        initValue();
        initIdView();
        initView();
        initActionView();
        loadVersion();
        AdmobHelp.getInstance().loadNativeActivity(this,true);
    }

    private void initValue() {
    }

    private void initIdView() {
        viewBrandCar = (LinearLayout) findViewById(R.id.view_brand_car);
        viewNewCar = (LinearLayout) findViewById(R.id.view_new_car);
        viewReviewCar = (LinearLayout) findViewById(R.id.view_review_car);
        viewHotCar = (LinearLayout) findViewById(R.id.view_hot_car);
        recycleReviewCarBrand = (RecyclerView) findViewById(R.id.recycle_review_car_brand);
        recycleCarBrand = (RecyclerView) findViewById(R.id.recycle_car_brand);
        recycleNewCar = (RecyclerView) findViewById(R.id.recycle_new_car);
        recycleHotCar = (RecyclerView) findViewById(R.id.recycle_hot_car);
        tvTitleHot = (CustomTextView) findViewById(R.id.tv_title_hot);
    }

    private void initView() {
        carBrandAdapter = new CarBrandAdapter(carBrands, this);
        recycleCarBrand.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        recycleCarBrand.setAdapter(carBrandAdapter);

        reviewCarMainAdapter = new CarMainAdapter(previewCar, this, false);
        recycleReviewCarBrand.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        recycleReviewCarBrand.setAdapter(reviewCarMainAdapter);

        newCarMainAdapter = new CarMainAdapter(newCars, this, false);
        recycleNewCar.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        recycleNewCar.setAdapter(newCarMainAdapter);

        hotCarMainAdapter = new CarMainAdapter(hotCars, this, true);
        recycleHotCar.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        recycleHotCar.setAdapter(hotCarMainAdapter);
    }

    private void initActionView() {

        carBrandAdapter.setClickListener(new CarBrandAdapter.ItemClickListener() {
            @Override
            public void onItemClick(CarBrand carBrand) {
                AdmobHelp.getInstance().showInterstitialAd(new AdCloseListener() {
                    @Override
                    public void onAdClosed(boolean hasAds) {
                        Intent intent = new Intent(CarMainActivity.this, CarProducerActivity.class);
                        intent.putExtra("CarBrand", carBrand);
                        startActivity(intent);
                    }
                });
            }
        });

        reviewCarMainAdapter.setClickListener(new CarMainAdapter.ItemClickListener() {
            @Override
            public void onItemClick(Car car) {
                AdmobHelp.getInstance().showInterstitialAd(new AdCloseListener() {
                    @Override
                    public void onAdClosed(boolean hasAds) {
                        ArrayList<Car> cars = new ArrayList<>();
                        cars.add(car);
                        for (Car temp: carNormal) {
                            if (temp.getCar_name().equals(car.getCar_name()) && (car.getId() != temp.getId())) {
                                cars.add(temp);
                            }
                        }
                        Intent intent = new Intent(CarMainActivity.this, CarDetailActivity.class);
                        intent.putExtra("Car", cars);
                        startActivity(intent);
                    }
                });
            }
        });

        newCarMainAdapter.setClickListener(new CarMainAdapter.ItemClickListener() {
            @Override
            public void onItemClick(Car car) {
                AdmobHelp.getInstance().showInterstitialAd(new AdCloseListener() {
                    @Override
                    public void onAdClosed(boolean hasAds) {
                        ArrayList<Car> cars = new ArrayList<>();
                        cars.add(car);
                        for (Car temp: carNormal) {
                            if (temp.getCar_name().equals(car.getCar_name()) && (car.getId() != temp.getId())) {
                                cars.add(temp);
                            }
                        }
                        Intent intent = new Intent(CarMainActivity.this, CarDetailActivity.class);
                        intent.putExtra("Car", cars);
                        startActivity(intent);
                    }
                });
            }
        });

        hotCarMainAdapter.setClickListener(new CarMainAdapter.ItemClickListener() {
            @Override
            public void onItemClick(Car car) {
                AdmobHelp.getInstance().showInterstitialAd( new AdCloseListener() {
                    @Override
                    public void onAdClosed(boolean hasAds) {
                        ArrayList<Car> cars = new ArrayList<>();
                        cars.add(car);
                        for (Car temp: carNormal) {
                            if (temp.getCar_name().equals(car.getCar_name()) && (car.getId() != temp.getId())) {
                                cars.add(temp);
                            }
                        }
                        Intent intent = new Intent(CarMainActivity.this, CarDetailActivity.class);
                        intent.putExtra("Car", cars);
                        startActivity(intent);
                    }
                });
            }
        });

        findViewById(R.id.btn_all_review_car).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AdmobHelp.getInstance().showInterstitialAd(new AdCloseListener() {
                    @Override
                    public void onAdClosed(boolean hasAds) {
                        Intent intent = new Intent(CarMainActivity.this, CarListActivity.class);
                        intent.putExtra("Title", "Danh sách xe đã xem");
                        intent.putExtra("Car", previewCar);
                        startActivity(intent);
                    }
                });
            }
        });

        findViewById(R.id.btn_all_brand).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AdmobHelp.getInstance().showInterstitialAd( new AdCloseListener() {
                    @Override
                    public void onAdClosed(boolean hasAds) {
                        Intent intent = new Intent(CarMainActivity.this, CarListActivity.class);
                        intent.putExtra("Title", "Tất cả các xe");
                        startActivity(intent);
                    }
                });
            }
        });

        findViewById(R.id.btn_more_new_car).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AdmobHelp.getInstance().showInterstitialAd( new AdCloseListener() {
                    @Override
                    public void onAdClosed(boolean hasAds) {
                        Intent intent = new Intent(CarMainActivity.this, CarListActivity.class);
                        intent.putExtra("Title", "Danh sách xe mới ra mắt");
                        intent.putExtra("Car", newCars);
                        startActivity(intent);
                    }
                });
            }
        });

        findViewById(R.id.btn_more_top_car).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AdmobHelp.getInstance().showInterstitialAd( new AdCloseListener() {
                    @Override
                    public void onAdClosed(boolean hasAds) {
                        Intent intent = new Intent(CarMainActivity.this, CarListActivity.class);
                        intent.putExtra("Title", tvTitleHot.getText());
                        intent.putExtra("Car", hotCars);
                        startActivity(intent);
                    }
                });
            }
        });

        findViewById(R.id.btn_search).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AdmobHelp.getInstance().showInterstitialAd( new AdCloseListener() {
                    @Override
                    public void onAdClosed(boolean hasAds) {
                        Intent intent = new Intent(CarMainActivity.this, SearchActivity.class);
                        startActivity(intent);
                    }
                });
            }
        });

        findViewById(R.id.btn_filter).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AdmobHelp.getInstance().showInterstitialAd( new AdCloseListener() {
                    @Override
                    public void onAdClosed(boolean hasAds) {
                        Intent intent = new Intent(CarMainActivity.this, FilterActivity.class);
                        startActivity(intent);
                    }
                });
            }
        });

        findViewById(R.id.btn_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        reloadReviewCar();
    }

    private void reloadReviewCar(){
        String ids = SharedPreferencesUtils.getString("PreviewCar", "");
        ArrayList<String> idsPreview = new ArrayList<String>(Arrays.asList(ids.split(",")));
        if (ids.equals("")) {
            viewReviewCar.setVisibility(View.GONE);
        } else {
            previewCar = new ArrayList<>();
            for (String id : idsPreview) {
                for (Car car : carNormal) {
                    if ((car.getId() + "").equals(id)) {
                        previewCar.add(car);
                        break;
                    }
                }
            }

            if (previewCar.size() > 0) {
                if (viewReviewCar != null) {
                    viewReviewCar.setVisibility(View.VISIBLE);
                }
                reviewCarMainAdapter.notifyData(previewCar);
            } else {
                if (viewReviewCar != null) {
                    viewReviewCar.setVisibility(View.GONE);
                }
            }
        }
    }

    private void loadVersion() {
        showLoading();
        AppClient.getAPIService().getVersionApp().enqueue(new Callback<VersionResponse>() {
            @Override
            public void onResponse(Call<VersionResponse> call, retrofit2.Response<VersionResponse> response) {
                VersionResponse resource = response.body();
                String version = resource.getVersionApp().getVersion();
                if (!SharedPreferencesUtils.getString("version", "").equals(version)) {
                    SharedPreferencesUtils.setString("version", version);
                    loadData(version);
                } else {
                    hiddenLoading();
                    //findViewById(R.id.native_ads).setVisibility(View.VISIBLE);
                    Gson gson = new Gson();
                    Type listBrand = new TypeToken<ArrayList<CarBrand>>() {}.getType();
                    carBrands = gson.fromJson(SharedPreferencesUtils.getString("CarBrand", ""), listBrand);
                    if (carBrandAdapter != null) {
                        carBrandAdapter.notifyData(carBrands);
                    }
                    if (viewBrandCar != null) {
                        if (carBrands.size() > 0) {
                            viewBrandCar.setVisibility(View.VISIBLE);
                        } else {
                            viewBrandCar.setVisibility(View.GONE);
                        }
                    }

                    Type listCar = new TypeToken<ArrayList<Car>>() {
                    }.getType();
                    carNormal = gson.fromJson(SharedPreferencesUtils.getString("CarNormal", ""), listCar);
                    if (carNormal.size() > 0) {
                        findViewById(R.id.btn_filter).setVisibility(View.VISIBLE);
                        findViewById(R.id.btn_search).setVisibility(View.VISIBLE);
                        reloadReviewCar();
                    }

                    String cars_new = SharedPreferencesUtils.getString("NewCar", "");
                    Type listCarNew = new TypeToken<HotCar>() {}.getType();
                    HotCar temp = gson.fromJson(cars_new, listCarNew);
                    String ids = temp.getId();
                    if (ids != null && !ids.equals("")) {
                        ArrayList<String> idsNewCar = new ArrayList<String>(Arrays.asList(ids.split(",")));
                        newCars = new ArrayList<>();
                        for (String id : idsNewCar) {
                            for (Car car : carNormal) {
                                if ((car.getId() + "").equals(id)) {
                                    newCars.add(car);
                                    break;
                                }
                            }
                        }

                        if (newCars.size() > 1) {
                            if (viewNewCar != null) {
                                viewNewCar.setVisibility(View.VISIBLE);
                            }
                            newCarMainAdapter.notifyData(newCars);
                        } else {
                            if (viewNewCar != null) {
                                viewNewCar.setVisibility(View.GONE);
                            }
                        }
                    }

                    String cars_sales = SharedPreferencesUtils.getString("HotCar", "");
                    Type listCarSale = new TypeToken<HotCar>() {}.getType();
                    HotCar tempSale = gson.fromJson(cars_sales, listCarSale);
                    tvTitleHot.setText(tempSale.getTitle());
                    String idsSale = tempSale.getId();
                    if (idsSale != null && !idsSale.equals("")) {
                        ArrayList<String> idsHotCar = new ArrayList<String>(Arrays.asList(idsSale.split(",")));
                        hotCars = new ArrayList<>();
                        for (String id : idsHotCar) {
                            for (Car car : carNormal) {
                                if ((car.getId() + "").equals(id)) {
                                    hotCars.add(car);
                                    break;
                                }
                            }
                        }

                        if (hotCars.size() > 1) {
                            if (viewHotCar != null) {
                                viewHotCar.setVisibility(View.VISIBLE);
                            }
                            hotCarMainAdapter.notifyData(hotCars);
                        } else {
                            if (viewHotCar != null) {
                                viewHotCar.setVisibility(View.GONE);
                            }
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<VersionResponse> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    public static void sortBrandPopular(ArrayList<CarBrand> carBrands){
        ArrayList<CarBrand> carBrands1 = new ArrayList<>(14);
        for (int i = 0; i < 14 ; i ++){
            carBrands1.add(new CarBrand());
        }
        for(int i = 0; i < carBrands.size() ; i ++){
            Iterator<CarBrand> itr = carBrands.iterator();
            while (itr.hasNext()){
                CarBrand p = itr.next();
                if (p.getName().toLowerCase().equals("toyota")) {
                    carBrands1.set(0,p);
                    itr.remove();
                }else if (p.getName().toLowerCase().equals("kia")) {
                    carBrands1.set(1,p);
                    itr.remove();
                }else if (p.getName().toLowerCase().equals("hyundai")) {
                    carBrands1.set(2,p);
                    itr.remove();
                }else if (p.getName().toLowerCase().equals("mazda")) {
                    carBrands1.set(3,p);
                    itr.remove();
                }else if (p.getName().toLowerCase().equals("ford")) {
                    carBrands1.set(4,p);
                    itr.remove();
                }else if (p.getName().toLowerCase().equals("honda")) {
                    carBrands1.set(5,p);
                    itr.remove();
                }else if (p.getName().toLowerCase().equals("nissan")) {
                    carBrands1.set(6,p);
                    itr.remove();
                } else if (p.getName().toLowerCase().equals("mitsubishi")) {
                    carBrands1.set(7,p);
                    itr.remove();
                }else if (p.getName().toLowerCase().equals("vinfast")) {
                    carBrands1.set(8,p);
                    itr.remove();
                }else if (p.getName().toLowerCase().equals("mercedes")) {
                    carBrands1.set(9,p);
                    itr.remove();
                }else if (p.getName().toLowerCase().equals("audi")) {
                    carBrands1.set(10,p);
                    itr.remove();
                }else if (p.getName().toLowerCase().equals("suzuki")) {
                    carBrands1.set(11,p);
                    itr.remove();
                }else if (p.getName().toLowerCase().equals("peugeot")) {
                    carBrands1.set(12,p);
                    itr.remove();
                }else if (p.getName().toLowerCase().equals("volkswagen")) {
                    carBrands1.set(13,p);
                    itr.remove();
                }
            }
        }
        carBrands.addAll(0,carBrands1);
    }

    private void loadData(String version) {
        AppClient.getAPIService().getAllInfoApp(version).enqueue(new Callback<AllInfoAppResponse>() {
            @Override
            public void onResponse(Call<AllInfoAppResponse> call, retrofit2.Response<AllInfoAppResponse> response) {
                hiddenLoading();
                //findViewById(R.id.native_ads).setVisibility(View.VISIBLE);
                carBrands = response.body().getCarBrands();
                if(carBrands!=null && carBrands.size() > 0){
                    sortBrandPopular(carBrands);
                }
                carNormal = response.body().getCar_normal();
                HotCar tempNewCar = response.body().getCars_new();
                HotCar tempSalesCar = response.body().getCars_sale();
                ArrayList<Province> provinces = response.body().getProvince();
                ArrayList<VehicleSegment> segments = response.body().getSegment();
                Gson gson = new Gson();
                SharedPreferencesUtils.setString("CarBrand", gson.toJson(carBrands));
                SharedPreferencesUtils.setString("CarNormal", gson.toJson(carNormal));
                SharedPreferencesUtils.setString("NewCar", gson.toJson(tempNewCar));
                SharedPreferencesUtils.setString("HotCar", gson.toJson(tempSalesCar));
                SharedPreferencesUtils.setString("Provinces", gson.toJson(provinces));
                SharedPreferencesUtils.setString("Segment", gson.toJson(segments));

                if (carBrandAdapter != null) {
                    carBrandAdapter.notifyData(carBrands);
                }
                if (viewBrandCar != null) {
                    if (carBrands.size() > 0) {
                        viewBrandCar.setVisibility(View.VISIBLE);
                    } else {
                        viewBrandCar.setVisibility(View.GONE);
                    }
                }

                if (carNormal.size() > 0) {
                    findViewById(R.id.btn_filter).setVisibility(View.VISIBLE);
                    findViewById(R.id.btn_search).setVisibility(View.VISIBLE);
                    reloadReviewCar();
                }

                String ids = tempNewCar.getId();
                if (ids != null && !ids.equals("")) {
                    ArrayList<String> idsNewCar = new ArrayList<String>(Arrays.asList(ids.split(",")));
                    newCars = new ArrayList<>();
                    for (String id : idsNewCar) {
                        for (Car car : carNormal) {
                            if ((car.getId() + "").equals(id)) {
                                newCars.add(car);
                                break;
                            }
                        }
                    }

                    if (newCars.size() > 1) {
                        if (viewNewCar != null) {
                            viewNewCar.setVisibility(View.VISIBLE);
                        }
                        newCarMainAdapter.notifyData(newCars);
                    } else {
                        if (viewNewCar != null) {
                            viewNewCar.setVisibility(View.GONE);
                        }
                    }

                    String idsSale = tempSalesCar.getId();
                    tvTitleHot.setText(tempSalesCar.getTitle());
                    if (idsSale != null && !idsSale.equals("")) {
                        ArrayList<String> idsHotCar = new ArrayList<String>(Arrays.asList(idsSale.split(",")));
                        hotCars = new ArrayList<>();
                        for (String id : idsHotCar) {
                            for (Car car : carNormal) {
                                if ((car.getId() + "").equals(id)) {
                                    hotCars.add(car);
                                    break;
                                }
                            }
                        }

                        if (hotCars.size() > 1) {
                            if (viewHotCar != null) {
                                viewHotCar.setVisibility(View.VISIBLE);
                            }
                            hotCarMainAdapter.notifyData(hotCars);
                        } else {
                            if (viewHotCar != null) {
                                viewHotCar.setVisibility(View.GONE);
                            }
                        }
                    }
                }

            }

            @Override
            public void onFailure(Call<AllInfoAppResponse> call, Throwable t) {
                hiddenLoading();
                t.printStackTrace();
            }
        });
    }
}