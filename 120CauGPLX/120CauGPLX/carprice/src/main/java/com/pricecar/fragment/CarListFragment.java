package com.pricecar.fragment;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ads.control.admob.AdCloseListener;
import com.ads.control.admob.AdmobHelp;
import com.ads.control.utils.SharedPreferencesUtils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.pricecar.activity.CarCompareActivity;
import com.pricecar.activity.CarDetailActivity;
import com.pricecar.adapter.CarAdapter;
import com.pricecar.R;
import com.pricecar.model.Car;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class CarListFragment extends Fragment {

    private RecyclerView recycle;
    private CarAdapter carAdapter;
    private int typeSort = 0;
    private ArrayList<Car> cars = new ArrayList<>();

    public static CarListFragment newInstance(ArrayList<Car> cars, int typeSort) {
        CarListFragment carListFragment = new CarListFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList("Car", cars);
        bundle.putInt("TypeSort", typeSort);
        carListFragment.setArguments(bundle);
        return carListFragment;
    }

    public CarListFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        cars = getArguments().getParcelableArrayList("Car");
        typeSort = getArguments().getInt("TypeSort");
        if (typeSort == 0) {
            Collections.sort(cars, new Comparator() {
                @Override
                public int compare(Object o1, Object o2) {
                    Car p1 = (Car) o1;
                    Car p2 = (Car) o2;
                    return Integer.compare(p1.getPrice_int(), p2.getPrice_int());
                }
            });
        } else if (typeSort == 1) {
            Collections.sort(cars, new Comparator() {
                @Override
                public int compare(Object o1, Object o2) {
                    Car p1 = (Car) o1;
                    Car p2 = (Car) o2;
                    return Integer.compare(p2.getPrice_int(), p1.getPrice_int());
                }
            });
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_car_list, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        recycle = view.findViewById(R.id.recycle);
        carAdapter = new CarAdapter(cars, getContext());
        recycle.setLayoutManager(new GridLayoutManager(getContext(), 2));
        recycle.setAdapter(carAdapter);
        carAdapter.setClickListener(new CarAdapter.ItemClickListener() {
            @Override
            public void onItemClick(Car car) {
                AdmobHelp.getInstance().showInterstitialAd(new AdCloseListener() {
                    @Override
                    public void onAdClosed(boolean hasAds) {
                        ArrayList<Car> carsNew = new ArrayList<>();
                        carsNew.add(car);

                        Gson gson = new Gson();
                        Type listCar = new TypeToken<ArrayList<Car>>() {
                        }.getType();
                        ArrayList<Car> carsNormal = gson.fromJson(SharedPreferencesUtils.getString("CarNormal", ""), listCar);

                        for (Car temp : carsNormal) {
                            if (temp.getCar_name().equals(car.getCar_name()) && (car.getId() != temp.getId())) {
                                carsNew.add(temp);
                            }
                        }
                        Intent intent = new Intent(getContext(), CarDetailActivity.class);
                        intent.putExtra("Car", carsNew);
                        startActivity(intent);
                    }
                });
            }

            @Override
            public void onCompare(Car car) {
                AdmobHelp.getInstance().showInterstitialAd( new AdCloseListener() {
                    @Override
                    public void onAdClosed(boolean hasAds) {
                        ArrayList<Car> carsNew = new ArrayList<>();
                        carsNew.add(car);

                        Gson gson = new Gson();
                        Type listCar = new TypeToken<ArrayList<Car>>() {
                        }.getType();
                        ArrayList<Car> carsNormal = gson.fromJson(SharedPreferencesUtils.getString("CarNormal", ""), listCar);

                        for (Car temp : carsNormal) {
                            if (temp.getCar_name().equals(car.getCar_name()) && (car.getId() != temp.getId())) {
                                carsNew.add(temp);
                            }
                        }
                        Intent intent = new Intent(getContext(), CarCompareActivity.class);
                        intent.putExtra("Car", carsNew);
                        startActivity(intent);
                    }
                });
            }
        });
    }
}