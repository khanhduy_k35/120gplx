package com.pricecar.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ads.control.customview.CustomTextView;
import com.bumptech.glide.Glide;
import com.pricecar.R;
import com.pricecar.model.Car;
import com.rey.material.widget.RelativeLayout;

import java.util.ArrayList;

public class CarAdapter extends RecyclerView.Adapter<CarAdapter.ViewHolder> {
    private Context context;
    private ArrayList<Car> cars;
    private ItemClickListener mClickListener;

    public CarAdapter(ArrayList<Car> cars, Context context) {
        this.cars = cars;
        this.context = context;
    }

    @NonNull
    @Override
    public CarAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        return new CarAdapter.ViewHolder(inflater.inflate(R.layout.item_car, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull CarAdapter.ViewHolder holder, int position) {
        Car car = cars.get(position);
        holder.tvName.setText(car.getCar_brand() + " " + car.getCar_name());
        if (car.getRelease_time() == null || car.getRelease_time().equals("")) {
            holder.viewTime.setVisibility(View.INVISIBLE);
        } else {
            holder.viewTime.setVisibility(View.VISIBLE);
            holder.tvTime.setText(car.getRelease_time());
        }
        if (car.getPrice_range() == null || car.getPrice_range().equals("")) {
            if (car.getListed_price() == null || car.getListed_price().equals("")) {
                holder.viewPrice.setVisibility(View.GONE);
                holder.tvPrice.setText("Đang cập nhật");
            } else {
                holder.viewPrice.setVisibility(View.VISIBLE);
                holder.tvPrice.setText(car.getListed_price());
            }
        } else {
            holder.tvPrice.setText(car.getPrice_range());
        }

        if (car.getNegotiate() == null || car.getNegotiate().equals("")) {
            holder.viewPromotion.setVisibility(View.GONE);
        } else {
            holder.viewPromotion.setVisibility(View.VISIBLE);
        }
        Glide.with(context)
                .load(car.getImg_thumb())
                .placeholder(R.drawable.ic_car_temp)
                .into(holder.imgCar);

        holder.btnClick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mClickListener != null) {
                    mClickListener.onItemClick(car);
                }
            }
        });

        holder.btnCompare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mClickListener != null) {
                    mClickListener.onCompare(car);
                }
            }
        });

    }

    public void notifyData(ArrayList<Car> carBrands) {
        this.cars = carBrands;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return cars.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private CustomTextView tvName,tvTime, tvPrice;
        private RelativeLayout btnClick, viewPromotion;
        private ImageView imgCar, btnCompare;
        private LinearLayout viewPrice, viewTime;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            btnClick = itemView.findViewById(R.id.btn_click);
            tvName = itemView.findViewById(R.id.tv_name);
            tvTime = itemView.findViewById(R.id.tv_time);
            tvPrice = itemView.findViewById(R.id.tv_price);
            imgCar = itemView.findViewById(R.id.img_car);
            btnCompare = itemView.findViewById(R.id.btn_compare);
            viewPromotion = itemView.findViewById(R.id.view_promotion);
            viewPrice = itemView.findViewById(R.id.view_price);
            viewTime = itemView.findViewById(R.id.view_time);
        }
    }

    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    public interface ItemClickListener {
        void onItemClick(Car car);
        void onCompare(Car car);
    }

}
