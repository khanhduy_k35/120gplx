package com.pricecar.model;

import java.util.ArrayList;

public class VehicleType {
    private String type;
    private ArrayList<Car> cars = new ArrayList<>();

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public ArrayList<Car> getCars() {
        return cars;
    }

    public void setCars(ArrayList<Car> cars) {
        this.cars = cars;
    }
}
