package com.pricecar.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class VersionResponse extends BaseResponse {
    @SerializedName("app_info")
    @Expose
    private VersionApp versionApp;

    public VersionApp getVersionApp() {
        return versionApp;
    }

    public void setVersionApp(VersionApp versionApp) {
        this.versionApp = versionApp;
    }
}
