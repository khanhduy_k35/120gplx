package com.pricecar.activity;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.ads.control.admob.AdCloseListener;
import com.ads.control.admob.AdmobHelp;
import com.ads.control.utils.SharedPreferencesUtils;
import com.ads.control.utils.Utils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.pricecar.adapter.SearchAdapter;
import com.pricecar.R;
import com.pricecar.model.Car;

import java.lang.reflect.Type;
import java.util.ArrayList;

public class SearchActivity extends BaseCarActivity {

    private RecyclerView recyclerView;
    private ArrayList<Car> cars = new ArrayList<>();
    private SearchAdapter searchAdapter;
    private EditText edtSearch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        AdmobHelp.getInstance().loadBanner(this);
        edtSearch = findViewById(R.id.edt_search);
        recyclerView = findViewById(R.id.recycle);

        Gson gson = new Gson();
        Type listCar = new TypeToken<ArrayList<Car>>() {}.getType();
        cars = gson.fromJson(SharedPreferencesUtils.getString("CarNormal", ""), listCar);

        searchAdapter = new SearchAdapter(cars, this);
        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        recyclerView.setAdapter(searchAdapter);
        searchAdapter.setClickListener(new SearchAdapter.ItemClickListener() {
            @Override
            public void onItemClick(Car car) {
                AdmobHelp.getInstance().showInterstitialAd( new AdCloseListener() {
                    @Override
                    public void onAdClosed(boolean hasAds) {
                        Utils.hideKeyboard(SearchActivity.this);
                        ArrayList<Car> carsNew = new ArrayList<>();
                        carsNew.add(car);
                        for (Car temp: cars) {
                            if (temp.getCar_name().equals(car.getCar_name()) && (car.getId() != temp.getId())) {
                                carsNew.add(temp);
                            }
                        }
                        Intent intent = new Intent(SearchActivity.this, CarDetailActivity.class);
                        intent.putExtra("Car", carsNew);
                        startActivity(intent);
                    }
                });
            }
        });

        edtSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                if ((keyEvent.getAction() == KeyEvent.ACTION_DOWN) &&
                        (i == KeyEvent.KEYCODE_ENTER)) {
                    Toast.makeText(SearchActivity.this, edtSearch.getText(), Toast.LENGTH_SHORT).show();
                    return true;
                }
                return false;
            }
        });

        edtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int start, int before, int count) {
                searchAdapter.filter(charSequence.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        findViewById(R.id.btn_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (edtSearch.getText() == null || edtSearch.getText().toString().equals("")) {
                    onBackPressed();
                } else {
                    edtSearch.setText("");
                    searchAdapter.filter("");
                }
            }
        });

    }
}