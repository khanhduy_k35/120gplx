package com.pricecar.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class AllInfoAppResponse {

    @SerializedName("car_brands")
    @Expose
    private ArrayList<CarBrand> carBrands;

    @SerializedName("cars_normal")
    @Expose
    private ArrayList<Car> car_normal;

    @SerializedName("cars_new")
    @Expose
    private HotCar cars_new;

    @SerializedName("cars_sales")
    @Expose
    private HotCar cars_sale;

    @SerializedName("provinces")
    @Expose
    private ArrayList<Province> province;

    @SerializedName("segment")
    @Expose
    private ArrayList<VehicleSegment> segment;

    public ArrayList<CarBrand> getCarBrands() {
        return carBrands;
    }

    public void setCarBrands(ArrayList<CarBrand> carBrands) {
        this.carBrands = carBrands;
    }

    public ArrayList<Car> getCar_normal() {
        return car_normal;
    }

    public void setCar_normal(ArrayList<Car> car_normal) {
        this.car_normal = car_normal;
    }

    public HotCar getCars_new() {
        return cars_new;
    }

    public void setCars_new(HotCar cars_new) {
        this.cars_new = cars_new;
    }

    public HotCar getCars_sale() {
        return cars_sale;
    }

    public void setCars_sale(HotCar cars_sale) {
        this.cars_sale = cars_sale;
    }

    public ArrayList<Province> getProvince() {
        return province;
    }

    public void setProvince(ArrayList<Province> province) {
        this.province = province;
    }

    public ArrayList<VehicleSegment> getSegment() {
        return segment;
    }

    public void setSegment(ArrayList<VehicleSegment> segment) {
        this.segment = segment;
    }
}
