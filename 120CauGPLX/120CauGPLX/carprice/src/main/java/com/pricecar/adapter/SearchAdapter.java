package com.pricecar.adapter;

import android.content.Context;
import android.text.Spannable;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ads.control.customview.CustomTextView;
import com.pricecar.R;
import com.pricecar.model.Car;
import com.rey.material.widget.RelativeLayout;

import java.util.ArrayList;
import java.util.Locale;

public class SearchAdapter extends RecyclerView.Adapter<SearchAdapter.ViewHolder> {

    private Context context;
    private ArrayList<Car> preDataList;
    private ArrayList<Car> cars;
    private ItemClickListener mClickListener;
    private String searchString = "";

    public SearchAdapter(ArrayList<Car> cars, Context context) {
        this.preDataList = cars;
        this.context = context;
        this.cars = new ArrayList<>();
        this.cars.addAll(preDataList);
    }

    @NonNull
    @Override
    public SearchAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        return new SearchAdapter.ViewHolder(inflater.inflate(R.layout.item_search, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull SearchAdapter.ViewHolder holder, int position) {
        Car car = cars.get(position);
        String search = (car.getCar_brand() + " " + car.getCar_name()).toLowerCase(Locale.getDefault());
        if (!searchString.equals("") && search.contains(searchString)) {
            int startPos = search.indexOf(searchString);
            int endPos = startPos + searchString.length();
            if (endPos > search.length()) {
                endPos = search.length();
            }
            Spannable spanText = Spannable.Factory.getInstance().newSpannable((car.getCar_brand() + " " + car.getCar_name()));
            spanText.setSpan(new ForegroundColorSpan(context.getResources().getColor(R.color.colorRedPrimary)), startPos, endPos, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

            holder.tvName.setText(spanText, TextView.BufferType.SPANNABLE);
        } else {
            holder.tvName.setText((car.getCar_brand() + " " + car.getCar_name()));
        }

        holder.btnClick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mClickListener != null) {
                    mClickListener.onItemClick(car);
                }
            }
        });

    }

    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        searchString = charText;
        cars.clear();
        ArrayList<Car> list = new ArrayList<Car>();
        if (charText.length() != 0) {
            for (Car car : preDataList) {
                if ((car.getCar_brand() + " " + car.getCar_name()).toLowerCase(Locale.getDefault()).contains(charText)) {
                    list.add(car);
                }
            }
            cars.addAll(list);
        } else {
            cars.addAll(preDataList);
        }
        notifyDataSetChanged();
    }

    public void notifyData(ArrayList<Car> cars) {
        this.preDataList = cars;
        this.cars = new ArrayList<>();
        this.cars.addAll(preDataList);
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return cars.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private CustomTextView tvName;
        private RelativeLayout btnClick;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            btnClick = itemView.findViewById(R.id.btn_click);
            tvName = itemView.findViewById(R.id.tv_name);
        }
    }

    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    public interface ItemClickListener {
        void onItemClick(Car car);
    }
}
