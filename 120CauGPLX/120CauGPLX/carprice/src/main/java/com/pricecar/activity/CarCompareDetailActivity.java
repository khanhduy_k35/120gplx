package com.pricecar.activity;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.ads.control.admob.AdmobHelp;
import com.ads.control.customview.CustomTextView;
import com.bumptech.glide.Glide;
import com.pricecar.R;
import com.pricecar.adapter.CarCompareSpecificationsAdapter;
import com.pricecar.model.Car;
import com.pricecar.model.CarDetailResponse;
import com.pricecar.model.TechInfo;
import com.pricecar.service.AppClient;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;

import retrofit2.Call;
import retrofit2.Callback;

public class CarCompareDetailActivity extends BaseCarActivity {

    private ImageView imgCar, imgCar2;
    private CustomTextView tvName, tvName2;
    private RecyclerView recycleView;

    private ArrayList<TechInfo> techInfo = new ArrayList<>();
    private CarCompareSpecificationsAdapter carCompareSpecificationsAdapter;

    private Car carsSelect1 = new Car();
    private Car carsSelect2 = new Car();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_car_compare_detail);
        AdmobHelp.getInstance().loadBanner(this);
        carsSelect1 = getIntent().getExtras().getParcelable("CarsSelect1");
        carsSelect2 = getIntent().getExtras().getParcelable("CarsSelect2");

        imgCar = findViewById(R.id.img_car);
        imgCar2 = findViewById(R.id.img_car2);
        tvName = findViewById(R.id.tv_name);
        tvName2 = findViewById(R.id.tv_name2);
        recycleView = findViewById(R.id.recycle_view);
        Glide.with(this)
                .load(carsSelect1.getImg_thumb())
                .placeholder(R.drawable.ic_car_temp)
                .into(imgCar);
        tvName.setText(carsSelect1.getCar_name());

        Glide.with(this)
                .load(carsSelect2.getImg_thumb())
                .placeholder(R.drawable.ic_car_temp)
                .into(imgCar2);
        tvName2.setText(carsSelect2.getCar_name());

        carCompareSpecificationsAdapter = new CarCompareSpecificationsAdapter(techInfo, this);
        recycleView.setLayoutManager(new LinearLayoutManager(this));
        recycleView.setAdapter(carCompareSpecificationsAdapter);

        findViewById(R.id.btn_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        loadCarDetail();
    }

    private void loadCarDetail() {
        showLoading();
        AppClient.getAPIService().getCarDetailByID(carsSelect2.getId() + "").enqueue(new Callback<CarDetailResponse>() {
            @Override
            public void onResponse(Call<CarDetailResponse> call, retrofit2.Response<CarDetailResponse> response) {
                CarDetailResponse temp = response.body();
                if (temp.getCar() != null) {
                    carsSelect2.updateDetail(temp.getCar());
                    progressCar();
                } else {
                    hiddenLoading();
                }
            }

            @Override
            public void onFailure(Call<CarDetailResponse> call, Throwable t) {
                hiddenLoading();
            }
        });
    }

    private void progressCar(){
        techInfo = new ArrayList<>();
        TechInfo base = new TechInfo();
        base.setTitle("Thông số kỹ thuật");
        base.setDetail(processInfoCar(carsSelect1.getTechBase(), carsSelect2.getTechBase()));
        techInfo.add(base);

        ArrayList<TechInfo> techInfoCar1  = new ArrayList<>();
        ArrayList<TechInfo> techInfoCar2  = new ArrayList<>();

        try {
            JSONObject obj = new JSONObject(carsSelect1.getTech_info());
            Iterator<String> keys = obj.keys();
            while(keys.hasNext()) {
                TechInfo techInfoSub = new TechInfo();
                JSONObject tech = new JSONObject(obj.getString(keys.next()));
                String title = tech.getString("title");
                techInfoSub.setTitle(title);
                String detail = tech.getString("detail");
                techInfoSub.setDetailString(detail);
                techInfoCar1.add(techInfoSub);
            }

            obj = new JSONObject(carsSelect2.getTech_info());
            keys = obj.keys();
            while(keys.hasNext()) {
                TechInfo techInfoSub = new TechInfo();
                JSONObject tech = new JSONObject(obj.getString(keys.next()));
                String title = tech.getString("title");
                techInfoSub.setTitle(title);
                String detail = tech.getString("detail");
                techInfoSub.setDetailString(detail);
                techInfoCar2.add(techInfoSub);
            }

        } catch (Throwable t) {
            t.printStackTrace();
        }

        ArrayList<String> titleInfo = new ArrayList<>();
        for(TechInfo techInfo: techInfoCar1) {
            if (titleInfo.indexOf(techInfo.getTitle()) == -1) {
                titleInfo.add(techInfo.getTitle());
            }
        }
        for(TechInfo techInfo: techInfoCar2) {
            if (titleInfo.indexOf(techInfo.getTitle()) == -1) {
                titleInfo.add(techInfo.getTitle());
            }
        }
        for(String detail: titleInfo) {
            TechInfo techInfoNew = new TechInfo();
            techInfoNew.setTitle(detail);
            String infoCar1 = "";
            String infoCar2 = "";
            for(TechInfo techInfo: techInfoCar1) {
                if (detail == techInfo.getTitle()) {
                    infoCar1 = techInfo.getDetailString();
                    break;
                }
            }

            for(TechInfo techInfo: techInfoCar1) {
                if (detail == techInfo.getTitle()) {
                    infoCar2 = techInfo.getDetailString();
                    break;
                }
            }
            techInfoNew.setDetail(processInfoCar(infoCar1, infoCar2));
            techInfo.add(techInfoNew);
        }
        hiddenLoading();
        carCompareSpecificationsAdapter.notifyData(techInfo);
    }

    private ArrayList<String> processInfoCar(String infoCar1, String infoCar2) {
        ArrayList<String> info = new ArrayList<>();
        try {
            JSONObject obj = new JSONObject(infoCar1);
            Iterator<String> keys = obj.keys();
            while(keys.hasNext()) {
                String temp = obj.getString(keys.next());
                if (temp.split("::").length > 1) {
                    info.add(temp);
                }
            }
        } catch (Throwable t) {
        }

        ArrayList<String> info2 = new ArrayList<>();
        try {
            JSONObject obj = new JSONObject(infoCar2);
            Iterator<String> keys = obj.keys();
            while(keys.hasNext()) {
                String temp = obj.getString(keys.next());
                if (temp.split("::").length > 1) {
                    info2.add(temp);
                }
            }
        } catch (Throwable t) {
        }

        ArrayList<String> titleInfo = new ArrayList<>();
        for(String detail: info) {
            if (titleInfo.indexOf(detail.split("::")[0]) == -1) {
                titleInfo.add(detail.split("::")[0]);
            }
        }
        for(String detail: info2) {
            if (titleInfo.indexOf(detail.split("::")[0]) == -1) {
                titleInfo.add(detail.split("::")[0]);
            }
        }

        ArrayList<String> infoBase = new ArrayList<>();
        for(String detail: titleInfo) {
            String value1 = "Không có";
            String value2 = "Không có";
            for(String value: info) {
                if (value.contains(detail)) {
                    value1 = value.split("::")[1];
                    break;
                }
            }

            for(String value: info2) {
                if (value.contains(detail)) {
                    value2 = value.split("::")[1];
                    break;
                }
            }
            String newString = detail+"::"+value1+"::"+value2;
            infoBase.add(newString);
        }
        return infoBase;
    }
}