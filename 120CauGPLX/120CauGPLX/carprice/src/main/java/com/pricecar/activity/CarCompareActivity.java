package com.pricecar.activity;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;

import com.ads.control.admob.AdCloseListener;
import com.ads.control.admob.AdmobHelp;
import com.ads.control.customview.CustomTextView;
import com.ads.control.customview.DialogUtil;
import com.ads.control.utils.SharedPreferencesUtils;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.pricecar.R;
import com.pricecar.adapter.OptionCarAdapter;
import com.pricecar.adapter.OptionCarBandAdapter;
import com.pricecar.adapter.OptionDialogAdapter;
import com.pricecar.model.Car;
import com.pricecar.model.CarBrand;
import com.rey.material.widget.RelativeLayout;

import java.lang.reflect.Type;
import java.util.ArrayList;

public class CarCompareActivity extends BaseCarActivity {

    private RelativeLayout btnAddCar;
    private ImageView imgCar, imgCar2;
    private CustomTextView tvName, tvName2, tvVersion, tvVersion2, tvPrice, tvPrice2;

    private ConstraintLayout viewImage2;
    private RelativeLayout rlVersion2;
    private LinearLayout viewPrice2;

    private ArrayList<Car> cars1 = new ArrayList<>();
    private ArrayList<Car> cars2 = new ArrayList<>();

    private Car carsSelect1 = new Car();
    private Car carsSelect2 = new Car();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_car_compare);
        AdmobHelp.getInstance().loadNativeActivity(this,true);
        cars1 = getIntent().getExtras().getParcelableArrayList("Car");
        carsSelect1 = cars1.get(0);

        btnAddCar = findViewById(R.id.btn_add_car);
        imgCar = findViewById(R.id.img_car);
        imgCar2 = findViewById(R.id.img_car2);
        tvName = findViewById(R.id.tv_name);
        tvVersion = findViewById(R.id.tv_version);
        tvPrice = findViewById(R.id.tv_price2);
        tvName2 = findViewById(R.id.tv_name2);
        tvVersion2 = findViewById(R.id.tv_version2);
        tvPrice2 = findViewById(R.id.tv_price2);

        viewImage2 = findViewById(R.id.view_image2);
        rlVersion2 = findViewById(R.id.rl_version2);
        viewPrice2 = findViewById(R.id.view_price2);

        setViewForCar1();
        setViewForCar2();

        findViewById(R.id.rl_version).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupWindow popup = new PopupWindow(CarCompareActivity.this);
                View dialog = getLayoutInflater().inflate(R.layout.dialog_version, null);
                popup.setContentView(dialog);
                popup.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
                popup.setWidth(WindowManager.LayoutParams.WRAP_CONTENT);
                popup.setOutsideTouchable(true);
                popup.setFocusable(true);
                popup.setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                popup.showAsDropDown(findViewById(R.id.rl_version), 0, 0, Gravity.NO_GRAVITY);
                RecyclerView recyclerView = dialog.findViewById(R.id.recycle_view);
                OptionDialogAdapter adapter = new OptionDialogAdapter(cars1, carsSelect1, CarCompareActivity.this);
                adapter.setClickListener(new OptionDialogAdapter.ItemClickListener() {
                    @Override
                    public void onItemClick(Car car) {
                        carsSelect1 = car;
                        setViewForCar1();
                        popup.dismiss();
                    }
                });
                recyclerView.setLayoutManager(new LinearLayoutManager(CarCompareActivity.this, LinearLayoutManager.VERTICAL, false));
                recyclerView.setAdapter(adapter);
            }
        });

        findViewById(R.id.rl_version2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupWindow popup = new PopupWindow(CarCompareActivity.this);
                View dialog = getLayoutInflater().inflate(R.layout.dialog_version, null);
                popup.setContentView(dialog);
                popup.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
                popup.setWidth(WindowManager.LayoutParams.WRAP_CONTENT);
                popup.setOutsideTouchable(true);
                popup.setFocusable(true);
                popup.setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                popup.showAsDropDown(findViewById(R.id.rl_version2), 0, 0, Gravity.NO_GRAVITY);
                RecyclerView recyclerView = dialog.findViewById(R.id.recycle_view);
                OptionDialogAdapter adapter = new OptionDialogAdapter(cars2, carsSelect2, CarCompareActivity.this);
                adapter.setClickListener(new OptionDialogAdapter.ItemClickListener() {
                    @Override
                    public void onItemClick(Car car) {
                        carsSelect2 = car;
                        setViewForCar2();
                        popup.dismiss();
                    }
                });
                recyclerView.setLayoutManager(new LinearLayoutManager(CarCompareActivity.this, LinearLayoutManager.VERTICAL, false));
                recyclerView.setAdapter(adapter);
            }
        });

        findViewById(R.id.btn_delete_compare).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cars2 = new ArrayList<>();
                carsSelect2 = new Car();
                setViewForCar2();
            }
        });

        findViewById(R.id.btnNext).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (carsSelect2.getId() == 0) {
                    new DialogUtil.Builder(CarCompareActivity.this)
                            .cancelable(false)
                            .canceledOnTouchOutside(false)
                            .title("Vui lòng chọn xe")
                            .content("Bạn vui lòng chọn xe cần so sánh để chúng tôi có thể đưa ra các so sánh chi tiết nhất dành cho bạn")
                            .doneText(getString(R.string.text_ok))
                            .show();
                } else {
                    AdmobHelp.getInstance().showInterstitialAd(new AdCloseListener() {
                        @Override
                        public void onAdClosed(boolean hasAds) {
                            Intent intent = new Intent(CarCompareActivity.this, CarCompareDetailActivity.class);
                            intent.putExtra("CarsSelect1", carsSelect1);
                            intent.putExtra("CarsSelect2", carsSelect2);
                            startActivity(intent);
                        }
                    });
                }
            }
        });

        btnAddCar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Dialog dialog = new Dialog(CarCompareActivity.this);
                dialog.setContentView(getLayoutInflater().inflate(R.layout.dialog_select_car, null));
                dialog.setCanceledOnTouchOutside(true);
                dialog.setCancelable(true);
                dialog.getWindow().setGravity(Gravity.CENTER);
                dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                Window window = dialog.getWindow();
                window.setBackgroundDrawableResource(android.R.color.transparent);
                dialog.show();

                RelativeLayout rlCarBand = dialog.findViewById(R.id.rl_car_band);
                RelativeLayout rlCar = dialog.findViewById(R.id.rl_car);
                RelativeLayout rlVersion = dialog.findViewById(R.id.rl_version);
                CustomTextView tvCarBrand = dialog.findViewById(R.id.tv_car_brand);
                CustomTextView tvCar = dialog.findViewById(R.id.tv_car);
                CustomTextView tvVersion = dialog.findViewById(R.id.tv_version);
                CustomTextView btnNext = dialog.findViewById(R.id.btnNext);

                final CarBrand[] carBrandSelect = {new CarBrand()};
                final Car[] carSelect = {new Car()};
                final Car[] carVersionSelect = {new Car()};

                Gson gson = new Gson();
                Type listBrand = new TypeToken<ArrayList<CarBrand>>() {
                }.getType();
                ArrayList<CarBrand> carBrands = gson.fromJson(SharedPreferencesUtils.getString("CarBrand", ""), listBrand);

                rlCarBand.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        PopupWindow popup = new PopupWindow(CarCompareActivity.this);
                        View dialog = getLayoutInflater().inflate(R.layout.dialog_version, null);
                        popup.setContentView(dialog);
                        popup.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
                        popup.setWidth(WindowManager.LayoutParams.WRAP_CONTENT);
                        popup.setOutsideTouchable(true);
                        popup.setFocusable(true);
                        popup.setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                        popup.showAsDropDown(rlCarBand, 0, 0, Gravity.NO_GRAVITY);
                        RecyclerView recyclerView = dialog.findViewById(R.id.recycle_view);
                        OptionCarBandAdapter adapter = new OptionCarBandAdapter(carBrands, carBrandSelect[0], CarCompareActivity.this);
                        adapter.setClickListener(new OptionCarBandAdapter.ItemClickListener() {
                            @Override
                            public void onItemClick(CarBrand carBrand) {
                                carBrandSelect[0] = carBrand;
                                carSelect[0] = new Car();
                                tvCarBrand.setText(carBrand.getName());
                                tvCar.setText("Chọn dòng xe");
                                tvVersion.setText("Chọn phiên bản xe");
                                popup.dismiss();
                            }
                        });
                        recyclerView.setLayoutManager(new LinearLayoutManager(CarCompareActivity.this, LinearLayoutManager.VERTICAL, false));
                        recyclerView.setAdapter(adapter);
                    }
                });

                rlCar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (carBrandSelect[0].getId() == 0) {
                            new DialogUtil.Builder(CarCompareActivity.this)
                                    .cancelable(false)
                                    .canceledOnTouchOutside(false)
                                    .title("Vui lòng chọn hãng xe")
                                    .content("Bạn vui lòng chọn hãng xe cần so sánh để chúng tôi có thể đưa ra các so sánh chi tiết nhất dành cho bạn")
                                    .doneText(getString(R.string.text_ok))
                                    .show();
                        } else {
                            ArrayList<Car> cars = processGroupCarWithVersion(carBrandSelect[0]);

                            PopupWindow popup = new PopupWindow(CarCompareActivity.this);
                            View dialog = getLayoutInflater().inflate(R.layout.dialog_version, null);
                            popup.setContentView(dialog);
                            popup.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
                            popup.setWidth(WindowManager.LayoutParams.WRAP_CONTENT);
                            popup.setOutsideTouchable(true);
                            popup.setFocusable(true);
                            popup.setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                            popup.showAsDropDown(rlCar, 0, 0, Gravity.NO_GRAVITY);
                            RecyclerView recyclerView = dialog.findViewById(R.id.recycle_view);
                            OptionCarAdapter adapter = new OptionCarAdapter(cars, carSelect[0], CarCompareActivity.this);
                            adapter.setClickListener(new OptionCarAdapter.ItemClickListener() {
                                @Override
                                public void onItemClick(Car car) {
                                    carSelect[0] = car;
                                    tvCar.setText(car.getCar_name());
                                    tvVersion.setText("Chọn phiên bản xe");
                                    popup.dismiss();
                                }
                            });
                            recyclerView.setLayoutManager(new LinearLayoutManager(CarCompareActivity.this, LinearLayoutManager.VERTICAL, false));
                            recyclerView.setAdapter(adapter);
                        }
                    }
                });

                rlVersion.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (carBrandSelect[0].getId() == 0) {
                            new DialogUtil.Builder(CarCompareActivity.this)
                                    .cancelable(false)
                                    .canceledOnTouchOutside(false)
                                    .title("Vui lòng chọn hãng xe")
                                    .content("Bạn vui lòng chọn hãng xe cần so sánh để chúng tôi có thể đưa ra các so sánh chi tiết nhất dành cho bạn")
                                    .doneText(getString(R.string.text_ok))
                                    .show();
                        } else if (carSelect[0].getId() == 0) {
                            new DialogUtil.Builder(CarCompareActivity.this)
                                    .cancelable(false)
                                    .canceledOnTouchOutside(false)
                                    .title("Vui lòng chọn dòng xe")
                                    .content("Bạn vui lòng chọn dòng xe cần so sánh để chúng tôi có thể đưa ra các so sánh chi tiết nhất dành cho bạn")
                                    .doneText(getString(R.string.text_ok))
                                    .show();
                        } else {
                            ArrayList<Car> cars = processCarWithVersion(carSelect[0]);
                            PopupWindow popup = new PopupWindow(CarCompareActivity.this);
                            View dialog = getLayoutInflater().inflate(R.layout.dialog_version, null);
                            popup.setContentView(dialog);
                            popup.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
                            popup.setWidth(WindowManager.LayoutParams.WRAP_CONTENT);
                            popup.setOutsideTouchable(true);
                            popup.setFocusable(true);
                            popup.setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                            popup.showAsDropDown(rlVersion, 0, 0, Gravity.NO_GRAVITY);
                            RecyclerView recyclerView = dialog.findViewById(R.id.recycle_view);
                            OptionDialogAdapter adapter = new OptionDialogAdapter(cars, carVersionSelect[0], CarCompareActivity.this);
                            adapter.setClickListener(new OptionDialogAdapter.ItemClickListener() {
                                @Override
                                public void onItemClick(Car car) {
                                    carVersionSelect[0] = car;
                                    tvVersion.setText("Phiên bản: " + car.getVersion());
                                    popup.dismiss();
                                }
                            });
                            recyclerView.setLayoutManager(new LinearLayoutManager(CarCompareActivity.this, LinearLayoutManager.VERTICAL, false));
                            recyclerView.setAdapter(adapter);
                        }
                    }
                });

                btnNext.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (carBrandSelect[0].getId() == 0) {
                            new DialogUtil.Builder(CarCompareActivity.this)
                                    .cancelable(false)
                                    .canceledOnTouchOutside(false)
                                    .title("Vui lòng chọn hãng xe")
                                    .content("Bạn vui lòng chọn hãng xe cần so sánh để chúng tôi có thể đưa ra các so sánh chi tiết nhất dành cho bạn")
                                    .doneText(getString(R.string.text_ok))
                                    .show();
                        } else if (carSelect[0].getId() == 0) {
                            new DialogUtil.Builder(CarCompareActivity.this)
                                    .cancelable(false)
                                    .canceledOnTouchOutside(false)
                                    .title("Vui lòng chọn dòng xe")
                                    .content("Bạn vui lòng chọn dòng xe cần so sánh để chúng tôi có thể đưa ra các so sánh chi tiết nhất dành cho bạn")
                                    .doneText(getString(R.string.text_ok))
                                    .show();
                        } else if (carVersionSelect[0].getId() == 0) {
                            new DialogUtil.Builder(CarCompareActivity.this)
                                    .cancelable(false)
                                    .canceledOnTouchOutside(false)
                                    .title("Vui lòng chọn phiên bản xe")
                                    .content("Bạn vui lòng chọn phiên bản xe cần so sánh để chúng tôi có thể đưa ra các so sánh chi tiết nhất dành cho bạn")
                                    .doneText(getString(R.string.text_ok))
                                    .show();
                        } else {
                            carsSelect2 = carVersionSelect[0];
                            cars2 = processCarWithVersion(carVersionSelect[0]);
                            dialog.dismiss();
                            setViewForCar2();
                        }
                    }
                });

            }
        });

        findViewById(R.id.btn_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

    }

    private void setViewForCar1() {
        if (tvName != null) {
            Glide.with(this)
                    .load(carsSelect1.getImg_thumb())
                    .placeholder(R.drawable.ic_car_temp)
                    .into(imgCar);
            tvName.setText(carsSelect1.getCar_name());
            tvVersion.setText(carsSelect1.getVersion());
            tvPrice.setText(carsSelect1.getListed_price());
        }
    }

    private void setViewForCar2() {
        if (tvName2 != null) {
            if (carsSelect2.getId() == 0) {
                viewImage2.setVisibility(View.INVISIBLE);
                rlVersion2.setVisibility(View.INVISIBLE);
                viewPrice2.setVisibility(View.INVISIBLE);
                btnAddCar.setVisibility(View.VISIBLE);
            } else {
                viewImage2.setVisibility(View.VISIBLE);
                rlVersion2.setVisibility(View.VISIBLE);
                viewPrice2.setVisibility(View.VISIBLE);
                btnAddCar.setVisibility(View.GONE);
                Glide.with(this)
                        .load(carsSelect2.getImg_thumb())
                        .placeholder(R.drawable.ic_car_temp)
                        .into(imgCar2);
                tvName2.setText(carsSelect2.getCar_name());
                tvVersion2.setText(carsSelect2.getVersion());
                tvPrice2.setText(carsSelect2.getListed_price());
            }
        }
    }

    private ArrayList<Car> processGroupCarWithVersion(CarBrand carBrand) {
        Gson gson = new Gson();
        Type listCar = new TypeToken<ArrayList<Car>>() {
        }.getType();
        ArrayList<Car> cars = gson.fromJson(SharedPreferencesUtils.getString("CarNormal", ""), listCar);
        ArrayList<Car> newList = new ArrayList<>();
        ArrayList<String> nameCar = new ArrayList<>();
        for (Car car : cars) {
            if (nameCar.lastIndexOf(car.getCar_name()) == -1) {
                nameCar.add(car.getCar_name());
            }
        }

        for (String name : nameCar) {
            ArrayList<Car> temp = new ArrayList<>();
            for (Car car : cars) {
                if (car.getCar_name().equals(name) && car.getCar_brand().equals(carBrand.getName())) {
                    temp.add(car);
                }
            }
            if (temp.size() == 1) {
                newList.add(temp.get(0));
            } else if (temp.size() > 0) {
                Car carNew = temp.get(0);
                int maxPrice = carNew.getPrice_int();
                int minPrice = carNew.getPrice_int();

                String maxPriceString = carNew.getListed_price();
                String minPriceString = carNew.getListed_price();

                for (Car car : temp) {
                    if (maxPrice < car.getPrice_int()) {
                        maxPrice = car.getPrice_int();
                        maxPriceString = car.getListed_price();
                    }

                    if (minPrice > car.getPrice_int() && car.getPrice_int() > 0) {
                        minPrice = car.getPrice_int();
                        minPriceString = car.getListed_price();
                    }
                }

                if (maxPrice != minPrice) {
                    carNew.setPrice_range(minPriceString + " - " + maxPriceString);
                }
                newList.add(carNew);
            }

        }
        return newList;
    }

    private ArrayList<Car> processCarWithVersion(Car car) {
        ArrayList<Car> carsNew = new ArrayList<>();
        carsNew.add(car);
        Gson gson = new Gson();
        Type listCar = new TypeToken<ArrayList<Car>>() {}.getType();
        ArrayList<Car> cars = gson.fromJson(SharedPreferencesUtils.getString("CarNormal", ""), listCar);
        for (Car temp : cars) {
            if (temp.getCar_name().equals(car.getCar_name()) && (car.getId() != temp.getId())) {
                carsNew.add(temp);
            }
        }
        return carsNew;
    }

}