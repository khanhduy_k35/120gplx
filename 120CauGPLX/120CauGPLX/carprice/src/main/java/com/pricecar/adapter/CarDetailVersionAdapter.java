package com.pricecar.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ads.control.customview.CustomTextView;
import com.ads.control.utils.SharedPreferencesUtils;
import com.ads.control.utils.Utils;
import com.pricecar.R;
import com.pricecar.model.Car;
import com.rey.material.widget.RelativeLayout;

import java.util.ArrayList;

public class CarDetailVersionAdapter extends RecyclerView.Adapter<CarDetailVersionAdapter.ViewHolder> {

    private ArrayList<Car> cars;
    private Car carSelect;
    private Context context;
    private ItemClickListener mClickListener;

    public CarDetailVersionAdapter(ArrayList<Car> cars, Car carSelect, Context context) {
        this.cars = cars;
        this.context = context;
        this.carSelect = carSelect;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        return new ViewHolder(inflater.inflate(R.layout.item_car_version, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Car car = cars.get(position);
        holder.tvVersion.setText(car.getVersion());
        if (car.getId() == carSelect.getId()) {
            holder.tvVersion.setBackgroundResource(R.drawable.background_gradient_18);
            holder.tvVersion.setTextColor(Color.WHITE);
        } else {
            if (SharedPreferencesUtils.isNightMode()) {
                holder.tvVersion.setBackgroundResource(R.drawable.background_line_18);
            } else {
                holder.tvVersion.setBackgroundResource(R.drawable.background_gray_18);
            }
            holder.tvVersion.setTextColor(Utils.getColorFromAttr(context, R.attr.color_text_item_setting));
        }
        holder.btnClick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mClickListener != null) {
                    mClickListener.onItemClick(car);
                }
            }
        });
    }

    public void notifyData(Car carSelect) {
        this.carSelect = carSelect;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return cars.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private CustomTextView tvVersion;
        private RelativeLayout btnClick;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            btnClick = itemView.findViewById(R.id.btn_click);
            tvVersion = itemView.findViewById(R.id.tv_version);
        }
    }

    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    public interface ItemClickListener {
        void onItemClick(Car car);
    }
}
