package com.pricecar.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class Car implements Parcelable {
    @SerializedName("id")
    private int id;

    @SerializedName("price_int")
    private int price_int;

    @SerializedName("car_brand")
    private String car_brand = ""; //hãng xe

    @SerializedName("car_name")
    private String car_name = ""; //tên dòng xe

    @SerializedName("version")
    private String version = ""; // phiên bản

    @SerializedName("vehicle_segment")
    private String vehicle_segment = ""; //phân khúc xe

    @SerializedName("engine")
    private String engine = ""; //động cơ

    @SerializedName("listed_price")
    private String listed_price = ""; //giá niêm yết

    @SerializedName("negotiate")
    private String negotiate = ""; //giá đàm phán

    @SerializedName("link_detail")
    private String link_detail = ""; //link chi tiết

    @SerializedName("release_time")
    private String release_time = ""; //thời gian ra mắt

    @SerializedName("img_thumb")
    private String img_thumb = ""; //ảnh thumb

    @SerializedName("sales")
    private String sales = ""; //doanh số bán

    @SerializedName("vehicle_type")
    private String vehicle_type = ""; //loại xe

    @SerializedName("link_detail_info")
    private String link_detail_info = ""; //link chi tiết về thông số kỹ thuật

    @SerializedName("car_source")
    private String car_source = ""; //nguồn gốc xe

    @SerializedName("numberSeat")
    private String numberSeat = "" ; //số ghế

    @SerializedName("flue")
    private String flue = ""; // nhiên liệu

    @SerializedName("tech_info")
    private String tech_info = ""; //công nghệ

    @SerializedName("same_segment")
    private String same_segment = ""; //xe cùng phân khúc

    @SerializedName("same_brand")
    private String same_brand = ""; //xe cùng hãng

    @SerializedName("price_range")
    private String price_range = ""; //xe cùng hãng

    @SerializedName("thumbList")
    private String thumbList = ""; //xe cùng hãng

    @SerializedName("techBase")
    private String techBase = ""; //xe cùng hãng

    @SerializedName("introShort")
    private String introShort = ""; //xe cùng hãng

    @SerializedName("bigImgList")
    private String bigImgList = ""; //xe cùng hãng

    @SerializedName("introFull")
    private String introFull = ""; //xe cùng hãng

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeInt(this.price_int);
        dest.writeString(this.car_brand);
        dest.writeString(this.car_name);
        dest.writeString(this.version);
        dest.writeString(this.vehicle_segment);
        dest.writeString(this.engine);
        dest.writeString(this.listed_price);
        dest.writeString(this.negotiate);
        dest.writeString(this.link_detail);
        dest.writeString(this.release_time);
        dest.writeString(this.img_thumb);
        dest.writeString(this.sales);
        dest.writeString(this.vehicle_type);
        dest.writeString(this.link_detail_info);
        dest.writeString(this.car_source);
        dest.writeString(this.numberSeat);
        dest.writeString(this.flue);
        dest.writeString(this.tech_info);
        dest.writeString(this.same_segment);
        dest.writeString(this.same_brand);
        dest.writeString(this.price_range);
        dest.writeString(this.thumbList);
        dest.writeString(this.techBase);
        dest.writeString(this.introShort);
        dest.writeString(this.bigImgList);
        dest.writeString(this.introFull);
    }

    protected Car(Parcel in) {
        this.id = in.readInt();
        this.price_int = in.readInt();
        this.car_brand = in.readString();
        this.car_name = in.readString();
        this.version = in.readString();
        this.vehicle_segment = in.readString();
        this.engine = in.readString();
        this.listed_price = in.readString();
        this.negotiate = in.readString();
        this.link_detail = in.readString();
        this.release_time = in.readString();
        this.img_thumb = in.readString();
        this.sales = in.readString();
        this.vehicle_type = in.readString();
        this.link_detail_info = in.readString();
        this.car_source = in.readString();
        this.numberSeat = in.readString();
        this.flue = in.readString();
        this.tech_info = in.readString();
        this.same_segment = in.readString();
        this.same_brand = in.readString();
        this.price_range = in.readString();
        this.thumbList = in.readString();
        this.techBase = in.readString();
        this.introShort = in.readString();
        this.bigImgList = in.readString();
        this.introFull = in.readString();
    }

    public static final Parcelable.Creator<Car> CREATOR = new Parcelable.Creator<Car>() {
        @Override
        public Car createFromParcel(Parcel source) {
            return new Car(source);
        }

        @Override
        public Car[] newArray(int size) {
            return new Car[size];
        }
    };

    public Car() {
    }

    public void updateDetail(Car car) {
        this.id = car.getId();
        this.price_int = car.getPrice_int();
        this.car_brand = car.getCar_brand();
        this.car_name = car.getCar_name();
        this.version = car.getVersion();
        this.vehicle_segment = car.getVehicle_segment();
        this.engine = car.getEngine();
        this.listed_price = car.getListed_price();
        this.negotiate = car.getNegotiate();
        this.link_detail = car.getLink_detail();
        this.release_time = car.getRelease_time();
        this.img_thumb = car.getImg_thumb();
        this.sales = car.getSales();
        this.vehicle_type = car.getVehicle_type();
        this.link_detail_info = car.getLink_detail_info();
        this.car_source = car.getCar_source();
        this.numberSeat = car.getNumberSeat();
        this.flue = car.getFlue();
        this.tech_info = car.getTech_info();
        this.same_segment = car.getSame_segment();
        this.same_brand = car.getSame_brand();
        this.price_range = car.getPrice_range();
        this.thumbList = car.getThumbList();
        this.techBase = car.getTechBase();
        this.introShort = car.getIntroShort();
        this.bigImgList = car.getBigImgList();
        this.introFull = car.getIntroFull();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCar_brand() {
        return car_brand;
    }

    public void setCar_brand(String car_brand) {
        this.car_brand = car_brand;
    }

    public String getCar_name() {
        return car_name;
    }

    public void setCar_name(String car_name) {
        this.car_name = car_name;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getVehicle_segment() {
        return vehicle_segment;
    }

    public void setVehicle_segment(String vehicle_segment) {
        this.vehicle_segment = vehicle_segment;
    }

    public String getEngine() {
        return engine;
    }

    public void setEngine(String engine) {
        this.engine = engine;
    }

    public String getListed_price() {
        return listed_price;
    }

    public void setListed_price(String listed_price) {
        this.listed_price = listed_price;
    }

    public String getNegotiate() {
        return negotiate;
    }

    public void setNegotiate(String negotiate) {
        this.negotiate = negotiate;
    }

    public String getLink_detail() {
        return link_detail;
    }

    public void setLink_detail(String link_detail) {
        this.link_detail = link_detail;
    }

    public String getRelease_time() {
        return release_time;
    }

    public void setRelease_time(String release_time) {
        this.release_time = release_time;
    }

    public String getImg_thumb() {
        return img_thumb;
    }

    public void setImg_thumb(String img_thumb) {
        this.img_thumb = img_thumb;
    }

    public String getSales() {
        return sales;
    }

    public void setSales(String sales) {
        this.sales = sales;
    }

    public String getVehicle_type() {
        return vehicle_type;
    }

    public void setVehicle_type(String vehicle_type) {
        this.vehicle_type = vehicle_type;
    }

    public String getLink_detail_info() {
        return link_detail_info;
    }

    public void setLink_detail_info(String link_detail_info) {
        this.link_detail_info = link_detail_info;
    }

    public String getCar_source() {
        return car_source;
    }

    public void setCar_source(String car_source) {
        this.car_source = car_source;
    }

    public String getNumberSeat() {
        return numberSeat;
    }

    public void setNumberSeat(String numberSeat) {
        this.numberSeat = numberSeat;
    }

    public String getFlue() {
        return flue;
    }

    public void setFlue(String flue) {
        this.flue = flue;
    }

    public String getTech_info() {
        return tech_info;
    }

    public void setTech_info(String tech_info) {
        this.tech_info = tech_info;
    }

    public String getSame_segment() {
        return same_segment;
    }

    public void setSame_segment(String same_segment) {
        this.same_segment = same_segment;
    }

    public String getSame_brand() {
        return same_brand;
    }

    public void setSame_brand(String same_brand) {
        this.same_brand = same_brand;
    }

    public String getPrice_range() {
        return price_range;
    }

    public void setPrice_range(String price_range) {
        this.price_range = price_range;
    }

    public int getPrice_int() {
        return price_int;
    }

    public void setPrice_int(int price_int) {
        this.price_int = price_int;
    }

    public String getThumbList() {
        return thumbList;
    }

    public void setThumbList(String thumbList) {
        this.thumbList = thumbList;
    }

    public String getTechBase() {
        return techBase;
    }

    public void setTechBase(String techBase) {
        this.techBase = techBase;
    }

    public String getIntroShort() {
        return introShort;
    }

    public void setIntroShort(String introShort) {
        this.introShort = introShort;
    }

    public String getBigImgList() {
        return bigImgList;
    }

    public void setBigImgList(String bigImgList) {
        this.bigImgList = bigImgList;
    }

    public String getIntroFull() {
        return introFull;
    }

    public void setIntroFull(String introFull) {
        this.introFull = introFull;
    }
}
