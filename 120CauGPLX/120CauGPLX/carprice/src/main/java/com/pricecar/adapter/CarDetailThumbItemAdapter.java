package com.pricecar.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.pricecar.R;
import com.rey.material.widget.RelativeLayout;

import java.util.ArrayList;


public class CarDetailThumbItemAdapter extends RecyclerView.Adapter<CarDetailThumbItemAdapter.ViewHolder> {

    private Context context;
    private ArrayList<String> listThumbImage = new ArrayList<>();
    private ItemClickListener itemClickListener;
    private int indexSelect = 0;

    public CarDetailThumbItemAdapter(ArrayList<String> listThumbImage, int indexSelect, Context context) {
        this.listThumbImage = listThumbImage;
        this.context = context;
        this.indexSelect = indexSelect;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        return new ViewHolder(inflater.inflate(R.layout.item_car_thumb, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, @SuppressLint("RecyclerView") int position) {
        String image = listThumbImage.get(position);
        Glide.with(context)
                .load(image)
                .placeholder(R.drawable.ic_car_temp)
                .into(holder.imageView);

        holder.btnClick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (itemClickListener != null) {
                    itemClickListener.onItemClick(position);
                }
            }
        });

        if (position == indexSelect) {
            holder.btnClick.setBackgroundResource(R.drawable.background_thumb_detail_line);
        } else {
            holder.btnClick.setBackgroundResource(R.drawable.background_thumb_detail);
        }
    }

    public void notifyData(int indexSelect) {
        this.indexSelect = indexSelect;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return listThumbImage.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private ImageView imageView;
        private RelativeLayout btnClick;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            btnClick = itemView.findViewById(R.id.btn_click);
            imageView = itemView.findViewById(R.id.img_car);
        }
    }

    public void setClickListener(ItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    public interface ItemClickListener {
        void onItemClick(int indexSelect);
    }
}
