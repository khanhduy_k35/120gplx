package com.pricecar.activity;

import android.app.Dialog;
import android.os.Bundle;
import android.view.Gravity;
import android.view.Window;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;

import com.ads.control.activity.BaseAdsActivity;
import com.pricecar.R;

public class BaseCarActivity extends BaseAdsActivity {

    private Dialog dialogLoading;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        dialogLoading = new Dialog(this);
        dialogLoading.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogLoading.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Window window = dialogLoading.getWindow();
        window.setBackgroundDrawableResource(android.R.color.transparent);
        dialogLoading.setCanceledOnTouchOutside(false);
        dialogLoading.setCancelable(true);
        dialogLoading.setContentView(R.layout.diakog_loading);
        dialogLoading.getWindow().setGravity(Gravity.CENTER);
        dialogLoading.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
    }

    public void showLoading() {
        if (dialogLoading != null) {
            dialogLoading.show();
        }
    }

    public void hiddenLoading() {
        if (dialogLoading != null && dialogLoading.isShowing()) {
            dialogLoading.dismiss();
        }
    }

}
