package com.pricecar.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;
import androidx.viewpager2.adapter.FragmentStateAdapter;
import androidx.viewpager2.widget.ViewPager2;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.ads.control.admob.AdCloseListener;
import com.ads.control.admob.AdmobHelp;
import com.ads.control.customview.CustomTextView;
import com.ads.control.utils.SharedPreferencesUtils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.pricecar.R;
import com.pricecar.adapter.CarDetailThumbAdapter;
import com.pricecar.adapter.CarMainAdapter;
import com.pricecar.customview.ultraviewpager.UltraViewPager;
import com.pricecar.customview.ultraviewpager.transformer.UltraDepthScaleTransformer;
import com.pricecar.fragment.CarInfoFragment;
import com.pricecar.fragment.CarPriceFragment;
import com.pricecar.fragment.CarSpecificationsFragment;
import com.pricecar.model.Car;
import com.pricecar.model.CarBrand;
import com.pricecar.model.CarDetailResponse;
import com.pricecar.model.VehicleSegment;
import com.pricecar.service.AppClient;
import com.tbuonomo.viewpagerdotsindicator.DotsIndicator;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;

import retrofit2.Call;
import retrofit2.Callback;

public class CarDetailActivity extends BaseCarActivity {

    private CustomTextView tvIndexThumb, tvPrice, tvTitle;
    private DotsIndicator indicator;
    private ViewPager2 viewpager;
    private AppCompatImageView btnPromotion;
    private LinearLayout viewSameSegment, viewSameBrand;
    private RecyclerView recycleSameSegment, recycleSameBrand;
    private UltraViewPager listImageThumb;

    private ArrayList<Car> cars = new ArrayList<>();
    private Car carSelect = new Car();
    private CarInfoFragment carInfoFragment;
    private CarSpecificationsFragment carSpecificationsFragment;
    private CarPriceFragment carPriceFragment;

    private ArrayList<Car> carNormal = new ArrayList<Car>();
    private ArrayList<Car> carSameSegment = new ArrayList<Car>();
    private ArrayList<Car> carSameBrand = new ArrayList<Car>();

    private CarMainAdapter sameSegmentAdapter, sameBrand;

    private SectionsPagerAdapter adapter;
    private CarDetailThumbAdapter carDetailThumbAdapter;
    private ArrayList<String> listThumb = new ArrayList<>();
    private int countCarDetail = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_car_detail);
        AdmobHelp.getInstance().loadBanner(this);
        cars = getIntent().getExtras().getParcelableArrayList("Car");
        carInfoFragment = new CarInfoFragment();
        carSpecificationsFragment = new CarSpecificationsFragment();
        carPriceFragment = new CarPriceFragment();
        Gson gson = new Gson();
        Type listCar = new TypeToken<ArrayList<Car>>() {
        }.getType();
        carNormal = gson.fromJson(SharedPreferencesUtils.getString("CarNormal", ""), listCar);

        tvIndexThumb = findViewById(R.id.tv_index_thumb);
        btnPromotion = findViewById(R.id.btn_promotion);
        tvPrice = findViewById(R.id.tv_price);
        tvTitle = findViewById(R.id.tv_title);
        indicator = findViewById(R.id.indicator);
        viewpager = findViewById(R.id.viewpager);
        viewSameSegment = findViewById(R.id.view_same_segment);
        viewSameBrand = findViewById(R.id.view_same_brand);
        recycleSameSegment = findViewById(R.id.recycle_same_segment);
        recycleSameBrand = findViewById(R.id.recycle_same_brand);
        listImageThumb = findViewById(R.id.list_image_thumb);

        adapter = new SectionsPagerAdapter(this);
        viewpager.setAdapter(adapter);
        viewpager.setOffscreenPageLimit(3);
        indicator.attachTo(viewpager);
        sameSegmentAdapter = new CarMainAdapter(carSameSegment, CarDetailActivity.this, false);
        sameSegmentAdapter.notifyData(true);
        recycleSameSegment.setLayoutManager(new LinearLayoutManager(CarDetailActivity.this, LinearLayoutManager.HORIZONTAL, false));
        recycleSameSegment.setAdapter(sameSegmentAdapter);

        sameBrand = new CarMainAdapter(carSameBrand, CarDetailActivity.this, false);
        sameBrand.notifyData(true);
        recycleSameBrand.setLayoutManager(new LinearLayoutManager(CarDetailActivity.this, LinearLayoutManager.HORIZONTAL, false));
        recycleSameBrand.setAdapter(sameBrand);

        carSelect = cars.get(0);
        String ids = SharedPreferencesUtils.getString("PreviewCar", "");
        ArrayList<String> idsHotCar = new ArrayList<String>(Arrays.asList(ids.split(",")));
        if (idsHotCar.indexOf(carSelect.getId() + "") != -1) {
            idsHotCar.remove(carSelect.getId() + "");
        }
        idsHotCar.add(0, carSelect.getId() + "");
        SharedPreferencesUtils.setString("PreviewCar", String.join(",", idsHotCar));
        carDetailThumbAdapter = new CarDetailThumbAdapter(listThumb, this);
        tvTitle.setText(cars.get(0).getCar_brand() + " " + cars.get(0).getCar_name());

        carInfoFragment.setCar(carSelect);
        carSpecificationsFragment.setCar(cars, carSelect);
        carPriceFragment.setCar(cars, carSelect);
        setCar(carSelect);
        if (carSelect.getNegotiate() == null || carSelect.getNegotiate().equals("")) {
            btnPromotion.setVisibility(View.GONE);
        } else {
            btnPromotion.setVisibility(View.VISIBLE);
        }

        if (cars.get(0).getPrice_range() == null || cars.get(0).getPrice_range().equals("")) {
            if (cars.get(0).getListed_price() == null || cars.get(0).getListed_price().equals("")) {
                tvPrice.setText("Đang cập nhật");
            } else {
                tvPrice.setText(cars.get(0).getListed_price());
            }
        } else {
            tvPrice.setText(cars.get(0).getPrice_range());
        }

        findViewById(R.id.btn_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        findViewById(R.id.btn_next_same_segment).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AdmobHelp.getInstance().showInterstitialAd(new AdCloseListener() {
                    @Override
                    public void onAdClosed(boolean hasAds) {
                        Gson gson = new Gson();
                        Type listBrand = new TypeToken<ArrayList<VehicleSegment>>() {}.getType();
                        ArrayList<VehicleSegment> vehicleSegments = gson.fromJson(SharedPreferencesUtils.getString("Segment", ""), listBrand);
                        VehicleSegment vehicleSegment = new VehicleSegment();
                        for (VehicleSegment temp : vehicleSegments) {
                            if (carSelect.getVehicle_segment().equals(temp.getName())) {
                                vehicleSegment = temp;
                            }
                        }
                        Intent intent = new Intent(CarDetailActivity.this, CarSameActivity.class);
                        intent.putExtra("Title", carSelect.getVehicle_segment());
                        intent.putExtra("Car", carSameSegment);
                        intent.putExtra("Detail", vehicleSegment.getIntro());
                        startActivity(intent);
                    }
                });
            }
        });

        findViewById(R.id.btn_next_same_brand).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AdmobHelp.getInstance().showInterstitialAd( new AdCloseListener() {
                    @Override
                    public void onAdClosed(boolean hasAds) {
                        Gson gson = new Gson();
                        Type listBrand = new TypeToken<ArrayList<CarBrand>>() {
                        }.getType();
                        ArrayList<CarBrand> carBrands = gson.fromJson(SharedPreferencesUtils.getString("CarBrand", ""), listBrand);
                        CarBrand carSame = new CarBrand();
                        for (CarBrand temp : carBrands) {
                            if (carSelect.getCar_brand().equals(temp.getName())) {
                                carSame = temp;
                            }
                        }
                        Intent intent = new Intent(CarDetailActivity.this, CarProducerActivity.class);
                        intent.putExtra("CarBrand", carSame);
                        startActivity(intent);
                    }
                });
            }
        });

        findViewById(R.id.btn_compare).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AdmobHelp.getInstance().showInterstitialAd(new AdCloseListener() {
                    @Override
                    public void onAdClosed(boolean hasAds) {
                        Intent intent = new Intent(CarDetailActivity.this, CarCompareActivity.class);
                        intent.putExtra("Car", cars);
                        startActivity(intent);
                    }
                });
            }
        });

        findViewById(R.id.btn_promotion).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Dialog dialog = new Dialog(CarDetailActivity.this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                Window window = dialog.getWindow();
                window.setBackgroundDrawableResource(android.R.color.transparent);
                dialog.setCanceledOnTouchOutside(false);
                dialog.setCancelable(true);
                dialog.setContentView(R.layout.dialog_promorion);
                dialog.getWindow().setGravity(Gravity.CENTER);
                dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                CustomTextView textView = dialog.findViewById(R.id.tv_promotion);
                textView.setText(carSelect.getNegotiate());
                ImageView btnClose = (ImageView) dialog.findViewById(R.id.btn_close);
                btnClose.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.cancel();
                        dialog.dismiss();
                    }
                });
                dialog.show();
            }
        });

        listImageThumb.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                int size = carSelect.getThumbList().split("\\|\\|").length;
                tvIndexThumb.setText((position % size + 1) + "/" + carSelect.getThumbList().split("\\|\\|").length);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        sameSegmentAdapter.setClickListener(new CarMainAdapter.ItemClickListener() {
            @Override
            public void onItemClick(Car car) {
                AdmobHelp.getInstance().showInterstitialAd(new AdCloseListener() {
                    @Override
                    public void onAdClosed(boolean hasAds) {
                        ArrayList<Car> carsNew = new ArrayList<>();
                        carsNew.add(car);
                        for (Car temp : carNormal) {
                            if (temp.getCar_name().equals(car.getCar_name()) && (car.getId() != temp.getId())) {
                                carsNew.add(temp);
                            }
                        }
                        Intent intent = new Intent(CarDetailActivity.this, CarDetailActivity.class);
                        intent.putExtra("Car", carsNew);
                        startActivity(intent);
                    }
                });
            }
        });
        sameBrand.setClickListener(new CarMainAdapter.ItemClickListener() {
            @Override
            public void onItemClick(Car car) {
                AdmobHelp.getInstance().showInterstitialAd( new AdCloseListener() {
                    @Override
                    public void onAdClosed(boolean hasAds) {
                        ArrayList<Car> carsNew = new ArrayList<>();
                        carsNew.add(car);
                        for (Car temp : carNormal) {
                            if (temp.getCar_name().equals(car.getCar_name()) && (car.getId() != temp.getId())) {
                                carsNew.add(temp);
                            }
                        }
                        Intent intent = new Intent(CarDetailActivity.this, CarDetailActivity.class);
                        intent.putExtra("Car", carsNew);
                        startActivity(intent);
                    }
                });
            }
        });
        viewpager.registerOnPageChangeCallback(new ViewPager2.OnPageChangeCallback() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                super.onPageScrolled(position, positionOffset, positionOffsetPixels);
            }

            @Override
            public void onPageSelected(int position) {
                super.onPageSelected(position);
                if (position == 0 || position == 2) {
                    View view = adapter.getViewAtPosition(position);
                    if (view != null) {
                        view.post(new Runnable() {
                            @Override
                            public void run() {
                                int wMeasureSpec = View.MeasureSpec.makeMeasureSpec(view.getWidth(), View.MeasureSpec.EXACTLY);
                                int hMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
                                view.measure(wMeasureSpec, hMeasureSpec);
                                if (viewpager.getLayoutParams().height != view.getMeasuredHeight()) {
                                    ViewGroup.LayoutParams layoutParams = viewpager.getLayoutParams();
                                    layoutParams.height = view.getMeasuredHeight();
                                    viewpager.setLayoutParams(layoutParams);

                                }
                            }
                        });
                    }
                } else {
                    final Handler handler = new Handler(Looper.getMainLooper());
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    View view = adapter.getViewAtPosition(position);
                                    if (view != null) {
                                        view.post(new Runnable() {
                                            @Override
                                            public void run() {
                                                int wMeasureSpec = View.MeasureSpec.makeMeasureSpec(view.getWidth(), View.MeasureSpec.EXACTLY);
                                                int hMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
                                                view.measure(wMeasureSpec, hMeasureSpec);
                                                if (viewpager.getLayoutParams().height != view.getMeasuredHeight()) {
                                                    ViewGroup.LayoutParams layoutParams = viewpager.getLayoutParams();
                                                    layoutParams.height = view.getMeasuredHeight();
                                                    viewpager.setLayoutParams(layoutParams);

                                                }
                                            }
                                        });
                                    }
                                }
                            });
                        }
                    }, 200);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {
                super.onPageScrollStateChanged(state);
            }
        });

        loadCarDetail();
    }

    @Override
    public void onResume() {
        super.onResume();
        initPager();
    }

    private void initPager() {
        if (carDetailThumbAdapter != null) {
            listImageThumb.setScrollMode(UltraViewPager.ScrollMode.HORIZONTAL);
            listImageThumb.setMultiScreen(1.0f);
            listImageThumb.setItemRatio(1.0f);
            listImageThumb.setRatio(1.5f);
            listImageThumb.setAutoMeasureHeight(true);
            listImageThumb.setPageTransformer(false, new UltraDepthScaleTransformer());
            listImageThumb.setInfiniteLoop(true);
            listImageThumb.setAutoScroll(3000);
            listThumb = new ArrayList<String>(Arrays.asList(carSelect.getThumbList().split("\\|\\|")));
            carDetailThumbAdapter = new CarDetailThumbAdapter(listThumb, this);
            carDetailThumbAdapter.setCarDetailThumbClick(new CarDetailThumbAdapter.CarDetailThumbClick() {
                @Override
                public void selectItem(String url) {
                    AdmobHelp.getInstance().showInterstitialAd(new AdCloseListener() {
                        @Override
                        public void onAdClosed(boolean hasAds) {
                            Intent intent = new Intent(CarDetailActivity.this, CarDetailThumbActivity.class);
                            intent.putExtra("Car", carSelect);
                            intent.putExtra("CurrentThumb", url);
                            startActivity(intent);
                        }
                    });
                }
            });
            listImageThumb.setAdapter(carDetailThumbAdapter);
        }
    }

    private void initView() {
        hiddenLoading();
        carSelect = cars.get(0);
        tvIndexThumb.setText("1/" + cars.get(0).getThumbList().split("\\|\\|").length);
        carInfoFragment.setCar(carSelect);
        carSpecificationsFragment.setCar(cars, carSelect);
        carPriceFragment.setCar(cars, carSelect);
        listThumb = new ArrayList<String>(Arrays.asList(carSelect.getThumbList().split("\\|\\|")));
        initPager();
    }

    private void loadCarDetail() {
        showLoading();
        for (int i = 0; i < cars.size(); i++) {
            int finalI = i;
            AppClient.getAPIService().getCarDetailByID(cars.get(i).getId() + "").enqueue(new Callback<CarDetailResponse>() {
                @Override
                public void onResponse(Call<CarDetailResponse> call, retrofit2.Response<CarDetailResponse> response) {
                    countCarDetail = countCarDetail + 1;
                    CarDetailResponse temp = response.body();
                    if (temp.getCar() != null) {
                        cars.get(finalI).updateDetail(temp.getCar());
                        if (countCarDetail == cars.size()) {
                            if (CarDetailActivity.this != null) {
                                initView();
                            }
                        }
                    }
                }

                @Override
                public void onFailure(Call<CarDetailResponse> call, Throwable t) {
                    hiddenLoading();
                }
            });
        }
    }

    public void reloadSizeViewPager() {
        final Handler handler = new Handler(Looper.getMainLooper());
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        View view = adapter.getViewAtPosition(viewpager.getCurrentItem());
                        view.post(new Runnable() {
                            @Override
                            public void run() {
                                int wMeasureSpec = View.MeasureSpec.makeMeasureSpec(view.getWidth(), View.MeasureSpec.EXACTLY);
                                int hMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
                                view.measure(wMeasureSpec, hMeasureSpec);
                                if (viewpager.getLayoutParams().height != view.getMeasuredHeight()) {
                                    ViewGroup.LayoutParams layoutParams = viewpager.getLayoutParams();
                                    layoutParams.height = view.getMeasuredHeight();
                                    viewpager.setLayoutParams(layoutParams);

                                }
                            }
                        });
                    }
                });
            }
        }, 300);
    }

    public void reloadSizeViewPager100() {
        final Handler handler = new Handler(Looper.getMainLooper());
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        View view = adapter.getViewAtPosition(viewpager.getCurrentItem());
                        view.post(new Runnable() {
                            @Override
                            public void run() {
                                int wMeasureSpec = View.MeasureSpec.makeMeasureSpec(view.getWidth(), View.MeasureSpec.EXACTLY);
                                int hMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
                                view.measure(wMeasureSpec, hMeasureSpec);
                                if (viewpager.getLayoutParams().height != view.getMeasuredHeight()) {
                                    ViewGroup.LayoutParams layoutParams = viewpager.getLayoutParams();
                                    layoutParams.height = view.getMeasuredHeight();
                                    viewpager.setLayoutParams(layoutParams);

                                }
                            }
                        });
                    }
                });
            }
        }, 200);
    }

    private void setCar(Car car) {
        ArrayList<String> idsSameSegment = new ArrayList<String>(Arrays.asList(car.getSame_segment().split(",")));
        carSameSegment = new ArrayList<>();
        for (String id : idsSameSegment) {
            for (Car temp : carNormal) {
                if ((temp.getId() + "").equals(id)) {
                    carSameSegment.add(temp);
                    break;
                }
            }
        }

        if (carSameSegment.size() > 1) {
            if (viewSameSegment != null) {
                viewSameSegment.setVisibility(View.VISIBLE);
                sameSegmentAdapter.notifyData(carSameSegment);
            }
        } else {
            if (viewSameSegment != null) {
                viewSameSegment.setVisibility(View.GONE);
            }
        }

        ArrayList<String> idsSameBrand = new ArrayList<String>(Arrays.asList(car.getSame_brand().split(",")));
        carSameBrand = new ArrayList<>();
        for (String id : idsSameBrand) {
            for (Car temp : carNormal) {
                if ((temp.getId() + "").equals(id)) {
                    carSameBrand.add(temp);
                    break;
                }
            }
        }

        if (carSameBrand.size() > 1) {
            if (viewSameBrand != null) {
                viewSameBrand.setVisibility(View.VISIBLE);
                sameBrand.notifyData(carSameBrand);
            } else {
                viewSameBrand.setVisibility(View.GONE);
            }
        }
    }

    public class SectionsPagerAdapter extends FragmentStateAdapter {

        public SectionsPagerAdapter(FragmentActivity fragmentActivity) {
            super(fragmentActivity);
        }

        @Override
        public int getItemCount() {
            return 3;
        }

        @NonNull
        @Override
        public Fragment createFragment(int position) {
            if (position == 0) {
                return carInfoFragment;
            } else if (position == 1) {
                return carSpecificationsFragment;
            } else {
                return carPriceFragment;
            }
        }

        public View getViewAtPosition(int position) {
            if (position == 0) {
                return carInfoFragment.getView();
            } else if (position == 1) {
                return carSpecificationsFragment.getView();
            } else {
                return carPriceFragment.getView();
            }
        }
    }
}