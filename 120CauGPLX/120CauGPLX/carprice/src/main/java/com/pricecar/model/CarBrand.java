package com.pricecar.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class CarBrand implements Parcelable {
    @SerializedName("id")
    private int id;

    @SerializedName("logo")
    private String logo;

    @SerializedName("name")
    private String name;

    @SerializedName("intro")
    private String intro;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.logo);
        dest.writeString(this.name);
        dest.writeString(this.intro);
    }

    protected CarBrand(Parcel in) {
        this.id = in.readInt();
        this.logo = in.readString();
        this.name = in.readString();
        this.intro = in.readString();
    }

    public CarBrand(JSONObject jsonObject) {
        try {
            id = jsonObject.getInt("id");
            logo = jsonObject.getString("logo");
            name = jsonObject.getString("name");
            intro = jsonObject.getString("intro");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public static final Parcelable.Creator<CarBrand> CREATOR = new Parcelable.Creator<CarBrand>() {
        @Override
        public CarBrand createFromParcel(Parcel source) {
            return new CarBrand(source);
        }

        @Override
        public CarBrand[] newArray(int size) {
            return new CarBrand[size];
        }
    };

    public CarBrand() {
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getIntro() {
        return intro;
    }

    public void setIntro(String intro) {
        this.intro = intro;
    }
}
