package com.pricecar.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ads.control.customview.CustomTextView;
import com.bumptech.glide.Glide;
import com.pricecar.R;
import com.pricecar.model.Car;
import com.rey.material.widget.RelativeLayout;

import java.util.ArrayList;

public class CarInfoAdapter extends RecyclerView.Adapter<CarInfoAdapter.ViewHolder> {
    private Context context;
    private ArrayList<Car> cars;
    private CarInfoAdapter.ItemClickListener mClickListener;

    public CarInfoAdapter(ArrayList<Car> cars, Context context) {
        this.cars = cars;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        return new ViewHolder(inflater.inflate(R.layout.item_info, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Car car = cars.get(position);
        holder.tvName.setText(car.getCar_brand() + " " + car.getCar_name());
        Glide.with(context)
                .load(car.getImg_thumb())
                .placeholder(R.drawable.ic_car_temp)
                .into(holder.imgCar);

        if (car.getNegotiate() == null || car.getNegotiate().equals("")) {
            holder.viewPromotion.setVisibility(View.GONE);
        } else {
            holder.viewPromotion.setVisibility(View.VISIBLE);
        }

        holder.btnClick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mClickListener != null) {
                    mClickListener.onItemClick(car);
                }
            }
        });

    }

    public void notifyData(ArrayList<Car> carBrands) {
        this.cars = carBrands;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return cars.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private CustomTextView tvName;
        private RelativeLayout btnClick;
        private ImageView imgCar;
        private android.widget.RelativeLayout viewPromotion;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            btnClick = itemView.findViewById(R.id.btn_click);
            tvName = itemView.findViewById(R.id.tv_name);
            imgCar = itemView.findViewById(R.id.img_car);
            viewPromotion = itemView.findViewById(R.id.view_promotion);
        }
    }

    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    public interface ItemClickListener {
        void onItemClick(Car car);
    }
}
