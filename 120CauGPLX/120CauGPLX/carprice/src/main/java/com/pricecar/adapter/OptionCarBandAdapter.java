package com.pricecar.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ads.control.customview.CustomTextView;
import com.ads.control.utils.Utils;
import com.pricecar.R;
import com.pricecar.model.CarBrand;
import com.pricecar.model.Province;
import com.rey.material.widget.RelativeLayout;

import java.util.ArrayList;

public class OptionCarBandAdapter extends RecyclerView.Adapter<OptionCarBandAdapter.ViewHolder> {

    private ArrayList<CarBrand> carBrands = new ArrayList<>();
    private CarBrand carBrandSelect = new CarBrand();
    private Context context;
    private ItemClickListener mClickListener;

    public OptionCarBandAdapter(ArrayList<CarBrand> carBrands, CarBrand carBrandSelect, Context context) {
        this.carBrands = carBrands;
        this.context = context;
        this.carBrandSelect = carBrandSelect;
    }

    @NonNull
    @Override
    public OptionCarBandAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        return new OptionCarBandAdapter.ViewHolder(inflater.inflate(R.layout.item_dialog_option, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull OptionCarBandAdapter.ViewHolder holder, int position) {
        CarBrand carBrand = carBrands.get(position);
        holder.tvName.setText(carBrand.getName());
        if (carBrand.getId() == carBrandSelect.getId()) {
            holder.tvName.setTextColor(Utils.getColorFromAttr(context, R.attr.color_text_item_setting_black));
            holder.tvName.setCustomFont(context, context.getResources().getString(R.string.medium));
        } else {
            holder.tvName.setCustomFont(context, context.getResources().getString(R.string.regular));
            holder.tvName.setTextColor(Utils.getColorFromAttr(context, R.attr.color_text_item_setting_dim));
        }
        holder.btnClick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mClickListener != null) {
                    mClickListener.onItemClick(carBrand);
                }
            }
        });
    }

    public void notifyData(ArrayList<CarBrand> carBrands) {
        this.carBrands = carBrands;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return carBrands.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private CustomTextView  tvName;
        private RelativeLayout btnClick;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            btnClick = itemView.findViewById(R.id.btn_click);
            tvName = itemView.findViewById(R.id.tv_name);
        }
    }

    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    public interface ItemClickListener {
        void onItemClick(CarBrand carBrand);
    }
}
