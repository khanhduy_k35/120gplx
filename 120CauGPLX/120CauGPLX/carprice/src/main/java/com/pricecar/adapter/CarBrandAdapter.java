package com.pricecar.adapter;

import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.pricecar.model.CarBrand;
import com.pricecar.activity.CarMainActivity;
import com.pricecar.R;
import com.rey.material.widget.RelativeLayout;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Locale;

public class CarBrandAdapter extends RecyclerView.Adapter<CarBrandAdapter.ViewHolder> {

    private Context context;
    private ArrayList<CarBrand> carBrands;
    private ItemClickListener mClickListener;

    public CarBrandAdapter(ArrayList<CarBrand> carBrands, Context context) {
        this.carBrands = carBrands;
        this.context = context;
    }

    @NonNull
    @Override
    public CarBrandAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        return new CarBrandAdapter.ViewHolder(inflater.inflate(R.layout.item_car_brand, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull CarBrandAdapter.ViewHolder holder, int position) {
        CarBrand carBrand = carBrands.get(position);
        holder.tvBrandName.setText(carBrand.getName());

        Glide.with(context)
                .load(Uri.parse("file:///android_asset/logo/ic_" + carBrand.getName().replace(" ", "_").toLowerCase(Locale.getDefault())) + ".webp")
                .into(holder.imgBrandLogo);
        holder.btnClick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mClickListener != null) {
                    mClickListener.onItemClick(carBrand);
                }
            }
        });
    }

    public void notifyData(ArrayList<CarBrand> carBrands) {
        this.carBrands = carBrands;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return carBrands.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView imgBrandLogo;
        public TextView tvBrandName;
        public RelativeLayout btnClick;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            btnClick = itemView.findViewById(R.id.btn_click);
            imgBrandLogo = itemView.findViewById(R.id.img_brand_logo);
            tvBrandName = itemView.findViewById(R.id.tv_brand_name);
        }
    }

    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    public interface ItemClickListener {
        void onItemClick(CarBrand carBrand);
    }

}
