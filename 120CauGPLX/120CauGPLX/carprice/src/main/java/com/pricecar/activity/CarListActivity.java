package com.pricecar.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.PopupMenu;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.viewpager2.adapter.FragmentStateAdapter;
import androidx.viewpager2.widget.ViewPager2;

import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import com.ads.control.admob.AdmobHelp;
import com.ads.control.customview.CustomTextView;
import com.ads.control.utils.SharedPreferencesUtils;
import com.google.android.material.tabs.TabLayout;
import com.google.android.material.tabs.TabLayoutMediator;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.pricecar.R;
import com.pricecar.fragment.CarListFragment;
import com.pricecar.model.Car;
import com.pricecar.model.VehicleType;

import java.lang.reflect.Type;
import java.util.ArrayList;

public class CarListActivity extends BaseCarActivity {

    private CustomTextView tvTitle;
    private ArrayList<Car> cars = new ArrayList<>();
    private ArrayList<VehicleType> vehicleTypes = new ArrayList<>();
    private int typeSort = 0;
    private SectionsPagerAdapter adapter;
    private ViewPager2 viewPager;
    private TabLayout tab_layout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_car_list);
        AdmobHelp.getInstance().loadBanner(this);
        tvTitle = findViewById(R.id.tv_title);
        cars = getIntent().getExtras().getParcelableArrayList("Car");


        if (cars == null || cars.size() == 0) {
            if (getIntent().getExtras().getBoolean("isFilter", false)){
                filterCar();
            } else {
                Gson gson = new Gson();
                Type listCar = new TypeToken<ArrayList<Car>>() {}.getType();
                cars = gson.fromJson(SharedPreferencesUtils.getString("CarNormal", ""), listCar);
            }
        }

        processGroupCar();
        if (getIntent().getExtras().getString("Title") != null) {
            if (getIntent().getExtras().getString("Title").contains("xe tương ứng")) {
                tvTitle.setText(cars.size() + " xe tương ứng");
            } else {
                tvTitle.setText(getIntent().getExtras().getString("Title"));
            }
        }
        VehicleType all = new VehicleType();
        all.setCars(cars);
        all.setType("Tất cả xe");
        vehicleTypes.add(all);

        ArrayList<String> types = new ArrayList<>();
        for (Car car : cars) {
            if (types.lastIndexOf(car.getVehicle_type()) == -1) {
                if (car.getVehicle_type() != null && !car.getVehicle_type().equals("")) {
                    types.add(car.getVehicle_type());
                }
            }
        }

        for (String type : types) {
            VehicleType vehicleType = new VehicleType();
            vehicleType.setType(type);
            ArrayList<Car> temp = new ArrayList<>();
            for (Car car : cars) {
                if (car.getVehicle_type().equals(type)) {
                    temp.add(car);
                }
            }
            vehicleType.setCars(temp);
            vehicleTypes.add(vehicleType);
        }

        tab_layout = (TabLayout) findViewById(R.id.tab_layout);
        viewPager = (ViewPager2) findViewById(R.id.viewpager);
        adapter = new SectionsPagerAdapter(this, vehicleTypes, typeSort);
        viewPager.setAdapter(adapter);
        new TabLayoutMediator(tab_layout, viewPager,
                new TabLayoutMediator.TabConfigurationStrategy() {
                    @Override
                    public void onConfigureTab(@NonNull TabLayout.Tab tab, int position) {
                        tab.setText(vehicleTypes.get(position).getType() + " (" + vehicleTypes.get(position).getCars().size() + " xe)");
                    }
                }).attach();

        findViewById(R.id.btn_sort).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupMenu popup = new PopupMenu(CarListActivity.this, findViewById(R.id.btn_sort));
                popup.getMenuInflater()
                        .inflate(R.menu.popup_menu_sort, popup.getMenu());
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    public boolean onMenuItemClick(MenuItem item) {
                        if (item.getItemId() == R.id.sort_ascending) {
                            typeSort = 0;
                        } else if (item.getItemId() == R.id.sort_decrease) {
                            typeSort = 1;
                        }
                        adapter = new SectionsPagerAdapter(CarListActivity.this, vehicleTypes, typeSort);
                        viewPager.setAdapter(adapter);
                        return true;
                    }
                });
                popup.show();
            }
        });

        findViewById(R.id.btn_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

    }

    private void filterCar(){
        Gson gson = new Gson();
        Type listCar = new TypeToken<ArrayList<Car>>() {}.getType();
        ArrayList<Car> carsTemp = gson.fromJson(SharedPreferencesUtils.getString("CarNormal", ""), listCar);
        ArrayList<String> carBrandAdd = getIntent().getExtras().getStringArrayList("carBrandAdd");
        ArrayList<String> vehicleTypeAdd = getIntent().getExtras().getStringArrayList("vehicleTypeAdd");
        ArrayList<String> vehicleSegmentAdd = getIntent().getExtras().getStringArrayList("vehicleSegmentAdd");
        ArrayList<String> carSourceAdd = getIntent().getExtras().getStringArrayList("carSourceAdd");
        ArrayList<String> numberSeatAdd = getIntent().getExtras().getStringArrayList("numberSeatAdd");
        ArrayList<String> fuelAdd = getIntent().getExtras().getStringArrayList("fuelAdd");

        ArrayList<Car> newCars = new ArrayList<>();
        for (Car car: carsTemp) {
            boolean isCarBrand = false;
            boolean isVehicleType = false;
            boolean isVehicleSegment = false;
            boolean isCarSource = false;
            boolean isNumberSeat = false;
            boolean isFuel = false;

            if (carBrandAdd.size() == 0) {
                isCarBrand = true;
            } else {
                for (String brand: carBrandAdd) {
                    if (car.getCar_brand().equals(brand)) {
                        isCarBrand = true;
                        break;
                    }
                }
            }

            if (vehicleTypeAdd.size() == 0) {
                isVehicleType = true;
            } else {
                for (String brand: vehicleTypeAdd) {
                    if (car.getVehicle_type().equals(brand)) {
                        isVehicleType = true;
                        break;
                    }
                }
            }

            if (vehicleSegmentAdd.size() == 0) {
                isVehicleSegment = true;
            } else {
                for (String brand: vehicleSegmentAdd) {
                    if (car.getVehicle_segment().equals(brand)) {
                        isVehicleSegment = true;
                        break;
                    }
                }
            }

            if (carSourceAdd.size() == 0) {
                isCarSource = true;
            } else {
                for (String brand: carSourceAdd) {
                    if (car.getCar_source().equals(brand)) {
                        isCarSource = true;
                        break;
                    }
                }
            }

            if (numberSeatAdd.size() == 0) {
                isNumberSeat = true;
            } else {
                for (String brand: numberSeatAdd) {
                    if (car.getNumberSeat().equals(brand)) {
                        isNumberSeat = true;
                        break;
                    }
                }
            }

            if (fuelAdd.size() == 0) {
                isFuel = true;
            } else {
                for (String brand: fuelAdd) {
                    if (car.getFlue().equals(brand)) {
                        isFuel = true;
                        break;
                    }
                }
            }

            if (isCarBrand && isVehicleType && isVehicleSegment && isCarSource && isNumberSeat && isFuel) {
                newCars.add(car);
            }
        }
        cars = newCars;
    }

    private void processGroupCar(){
        ArrayList<Car> newList = new ArrayList<>();
        ArrayList<String> nameCar = new ArrayList<>();
        for (Car car : cars) {
            if (nameCar.lastIndexOf(car.getCar_name()) == -1) {
                nameCar.add(car.getCar_name());
            }
        }

        for (String name : nameCar) {
            ArrayList<Car> temp = new ArrayList<>();
            for (Car car : cars) {
                if (car.getCar_name().equals(name)) {
                    temp.add(car);
                }
            }
            if (temp.size() == 1) {
                newList.add(temp.get(0));
            } else {
                Car carNew = temp.get(0);
                int maxPrice = carNew.getPrice_int();
                int minPrice = carNew.getPrice_int();

                String maxPriceString = carNew.getListed_price();
                String minPriceString = carNew.getListed_price();

                for (Car car : temp) {
                    if (maxPrice < car.getPrice_int() ) {
                        maxPrice = car.getPrice_int();
                        maxPriceString = car.getListed_price();
                    }

                    if (minPrice > car.getPrice_int() && car.getPrice_int() > 0) {
                        minPrice = car.getPrice_int();
                        minPriceString = car.getListed_price();
                    }
                }

                if (maxPrice != minPrice) {
                    carNew.setPrice_range(minPriceString + " - " + maxPriceString);
                }
                newList.add(carNew);
            }

        }
        cars = newList;
    }

    public class SectionsPagerAdapter extends FragmentStateAdapter {

        private ArrayList<VehicleType> vehicleTypes = new ArrayList<>();
        private int typeSort = 0;

        public SectionsPagerAdapter(FragmentActivity fragmentActivity, ArrayList<VehicleType> vehicleTypes, int typeSort) {
            super(fragmentActivity);
            this.vehicleTypes = vehicleTypes;
            this.typeSort = typeSort;
        }

        @Override
        public int getItemCount() {
            return vehicleTypes.size();
        }

        @NonNull
        @Override
        public Fragment createFragment(int position) {
            return CarListFragment.newInstance(vehicleTypes.get(position).getCars(), typeSort);
        }
    }
}