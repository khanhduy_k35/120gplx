package com.pricecar.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;

import androidx.viewpager.widget.PagerAdapter;

import com.bumptech.glide.Glide;
import com.pricecar.R;
import com.rey.material.widget.RelativeLayout;

import java.util.ArrayList;

public class CarDetailThumbAdapter extends PagerAdapter {

    private Context context;
    private ArrayList<String> listThumbImage = new ArrayList<>();
    private CarDetailThumbClick carDetailThumbClick;

    public CarDetailThumbAdapter(ArrayList<String> listThumbImage, Context context) {
        this.listThumbImage = listThumbImage;
        this.context = context;
    }

    @Override
    public int getCount() {
        return listThumbImage.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        FrameLayout frameLayout = (FrameLayout) LayoutInflater.from(container.getContext()).inflate(R.layout.item_image_thumb, null);
        ImageView imageThumb = (ImageView) frameLayout.findViewById(R.id.image_thumb);
        RelativeLayout btnClick = frameLayout.findViewById(R.id.btn_click);
        String url = listThumbImage.get(position);
        Glide.with(context)
                .load(url)
                .placeholder(R.drawable.ic_car_temp)
                .into(imageThumb);
        btnClick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (carDetailThumbClick != null) {
                    carDetailThumbClick.selectItem(url);
                }
            }
        });

        container.addView(frameLayout);
        return frameLayout;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        FrameLayout view = (FrameLayout) object;
        container.removeView(view);
    }

    public void setCarDetailThumbClick (CarDetailThumbClick carDetailThumbClick) {
        this.carDetailThumbClick = carDetailThumbClick;
    }

    public interface CarDetailThumbClick {
        void selectItem(String url);
    }
}
