package com.pricecar.adapter;

import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.content.Context;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ads.control.customview.CustomTextView;
import com.pricecar.R;
import com.pricecar.customview.expandable.ExpandableLayout;
import com.pricecar.customview.expandable.ExpandableLayoutListenerAdapter;
import com.pricecar.customview.expandable.ExpandableLinearLayout;
import com.pricecar.customview.expandable.Utils;
import com.pricecar.model.TechInfo;

import java.util.ArrayList;

public class CarCompareSpecificationsAdapter extends RecyclerView.Adapter<CarCompareSpecificationsAdapter.ViewHolder> {

    private ArrayList<TechInfo> data = new ArrayList<>();
    private SparseBooleanArray expandState = new SparseBooleanArray();
    private Context context;
    private ItemClickListener mClickListener;

    public CarCompareSpecificationsAdapter(ArrayList<TechInfo> data, Context mContext) {
        this.data = data;
        this.context = mContext;
        for (int i = 0; i < data.size(); i++) {
            expandState.append(i, false);
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
        this.context = parent.getContext();
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_specifications, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, @SuppressLint("RecyclerView") int position) {
        final TechInfo item = data.get(position);
        holder.carSpecificationsItemAdapter.notifyData(item.getDetail(), position);
        holder.setIsRecyclable(false);
        holder.expandableLayout.setInRecyclerView(true);
        holder.expandableLayout.setInterpolator(item.getInterpolator());
        holder.expandableLayout.setListener(new ExpandableLayoutListenerAdapter() {
            @Override
            public void onPreOpen() {
                createRotateAnimator(holder.imageArrow, 0f, 180f).start();
                expandState.put(position, true);
            }

            @Override
            public void onPreClose() {
                createRotateAnimator(holder.imageArrow, 180f, 0f).start();
                expandState.put(position, false);
            }
        });

        holder.textView.setText(item.getTitle());
        holder.imageArrow.setVisibility(View.VISIBLE);
        holder.expandableLayout.setExpanded(expandState.get(position));
        holder.imageArrow.setRotation(expandState.get(position) ? 180f : 0f);
        holder.imageArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                onClickButton(holder.expandableLayout);
            }
        });
        holder.lnView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onClickButton(holder.expandableLayout);
            }
        });
    }

    private void onClickButton(final ExpandableLayout expandableLayout) {
        expandableLayout.toggle();
        if (mClickListener != null) {
            mClickListener.onItemClick();
        }
    }

    public void notifyData(ArrayList<TechInfo> data){
        this.data = data;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private CustomTextView textView;
        private LinearLayout lnView;
        private ImageView imageArrow;
        private ExpandableLinearLayout expandableLayout;
        private CarCompareSpecificationsItemAdapter carSpecificationsItemAdapter;
        private RecyclerView recyclerView;

        public ViewHolder(View v) {
            super(v);
            textView = (CustomTextView) v.findViewById(R.id.textView);
            imageArrow = (ImageView) v.findViewById(R.id.image_arrow);
            lnView = (LinearLayout) v.findViewById(R.id.ln_view);
            recyclerView = (RecyclerView) v.findViewById(R.id.recycle_view);
            expandableLayout = (ExpandableLinearLayout) v.findViewById(R.id.expandableLayout);
            recyclerView.setLayoutManager( new LinearLayoutManager(context));
            carSpecificationsItemAdapter = new CarCompareSpecificationsItemAdapter(new ArrayList<>());
            recyclerView.setAdapter(carSpecificationsItemAdapter);
        }
    }

    public ObjectAnimator createRotateAnimator(final View target, final float from, final float to) {
        ObjectAnimator animator = ObjectAnimator.ofFloat(target, "rotation", from, to);
        animator.setDuration(300);
        animator.setInterpolator(Utils.createInterpolator(Utils.LINEAR_INTERPOLATOR));
        return animator;
    }

    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    public interface ItemClickListener {
        void onItemClick();
    }
}
