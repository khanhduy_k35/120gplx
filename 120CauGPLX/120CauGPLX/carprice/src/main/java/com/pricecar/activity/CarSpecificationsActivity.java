package com.pricecar.activity;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;

import com.ads.control.admob.AdmobHelp;
import com.pricecar.R;
import com.pricecar.adapter.CarSpecificationsAdapter;
import com.pricecar.model.Car;
import com.pricecar.model.TechInfo;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;

public class CarSpecificationsActivity extends BaseCarActivity {

    private RecyclerView recycleView;
    private Car car = new Car();
    private ArrayList<TechInfo> techInfo = new ArrayList<>();
    private CarSpecificationsAdapter carSpecificationsAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_car_specifications);
        AdmobHelp.getInstance().loadBanner(this);
        car = getIntent().getExtras().getParcelable("Car");
        recycleView = findViewById(R.id.recycle_view);

        carSpecificationsAdapter = new CarSpecificationsAdapter(techInfo, this);
        recycleView.setLayoutManager(new LinearLayoutManager(this));
        recycleView.setAdapter(carSpecificationsAdapter);

        findViewById(R.id.btn_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        progressCar();
    }

    private void progressCar(){
        techInfo = new ArrayList<>();
        TechInfo base = new TechInfo();
        base.setTitle("Thông số kỹ thuật");
        ArrayList<String> info = new ArrayList<>();
        try {
            JSONObject obj = new JSONObject(car.getTechBase());
            Iterator<String> keys = obj.keys();
            while(keys.hasNext()) {
                String temp = obj.getString(keys.next());
                if (temp.split("::").length > 1) {
                    info.add(temp);
                }
            }
        } catch (Throwable t) {
        }
        base.setDetail(info);
        techInfo.add(base);
        try {
            JSONObject obj = new JSONObject(car.getTech_info());
            Iterator<String> keys = obj.keys();
            while(keys.hasNext()) {
                TechInfo techInfoSub = new TechInfo();
                JSONObject tech = new JSONObject(obj.getString(keys.next()));
                String title = tech.getString("title");
                techInfoSub.setTitle(title);
                String detail = tech.getString("detail");
                JSONObject objDetail = new JSONObject(detail);
                ArrayList<String> infoDetail = new ArrayList<>();
                Iterator<String> keysDetail = objDetail.keys();
                while(keysDetail.hasNext()) {
                    String temp = objDetail.getString(keysDetail.next());
                    if (temp.split("::").length > 1) {
                        infoDetail.add(temp);
                    }
                }
                techInfoSub.setDetail(infoDetail);
                techInfo.add(techInfoSub);
            }
        } catch (Throwable t) {
            t.printStackTrace();
        }
        carSpecificationsAdapter.notifyData(techInfo);
    }

}