package com.pricecar.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.ads.control.customview.CustomTextView;
import com.bumptech.glide.Glide;
import com.pricecar.R;
import com.pricecar.model.Car;
import com.rey.material.widget.RelativeLayout;

import java.util.ArrayList;

public class CarMainAdapter extends RecyclerView.Adapter<CarMainAdapter.ViewHolder> {

    private Context context;
    private ArrayList<Car> cars;
    private boolean isTopCar = false;
    private boolean isSameCar = false;
    private ItemClickListener mClickListener;

    public CarMainAdapter(ArrayList<Car> cars, Context context, boolean isTopCar) {
        this.cars = cars;
        this.context = context;
        this.isTopCar = isTopCar;
    }

    @NonNull
    @Override
    public CarMainAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        return new CarMainAdapter.ViewHolder(inflater.inflate(R.layout.item_car_main, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull CarMainAdapter.ViewHolder holder, int position) {
        Car car = cars.get(position);
        holder.tvName.setText(car.getCar_brand() + " " + car.getCar_name());
        if(isSameCar == true){
            if (car.getListed_price() == null || car.getListed_price().equals("")) {
                holder.tvPrice.setText("Đang cập nhật");
                holder.viewPrice.setVisibility(View.GONE);
            } else {
                holder.tvPrice.setText(car.getListed_price());
                holder.viewPrice.setVisibility(View.VISIBLE);
            }
        } else {
            if (car.getPrice_range() == null || car.getPrice_range().equals("")) {
                holder.tvPrice.setText("Đang cập nhật");
                holder.viewPrice.setVisibility(View.GONE);
            } else {
                holder.tvPrice.setText(car.getPrice_range());
                holder.viewPrice.setVisibility(View.VISIBLE);
            }
        }
        if (car.getNegotiate() == null || car.getNegotiate().equals("")) {
            holder.viewPromotion.setVisibility(View.GONE);
        } else {
            holder.viewPromotion.setVisibility(View.VISIBLE);
        }

        Glide.with(context)
                .load(car.getImg_thumb())
                .placeholder(R.drawable.ic_car_temp)
                .into(holder.imgCar);

        if (isTopCar == false) {
            holder.viewIndex.setVisibility(View.GONE);
            holder.tvTitleNew.setText("Ra mắt: ");
            if (car.getRelease_time() == null || car.getRelease_time().equals("")) {
                holder.tvTime.setText("Đang cập nhật");
                holder.viewTime.setVisibility(View.GONE);
            } else {
                holder.tvTime.setText(car.getRelease_time());
                holder.viewTime.setVisibility(View.VISIBLE);
            }
        } else {
            holder.viewIndex.setVisibility(View.VISIBLE);
            if (position == 0) {
                holder.bgIndex.setCardBackgroundColor(Color.parseColor("#00BC29"));
            } else if (position == 1) {
                holder.bgIndex.setCardBackgroundColor(Color.parseColor("#FE8A49"));
            } else if (position == 2) {
                holder.bgIndex.setCardBackgroundColor(Color.parseColor("#FA015B"));
            } else {
                holder.viewIndex.setVisibility(View.GONE);
            }
            holder.tvTitleNew.setText("Doanh số: ");
            if (car.getSales() == null || car.getSales().equals("")) {
                holder.tvTime.setText("Đang cập nhật");
                holder.viewTime.setVisibility(View.GONE);
            } else {
                holder.tvTime.setText(car.getSales());
                holder.viewTime.setVisibility(View.VISIBLE);
            }
        }

        holder.tvIndex.setText((position + 1) + "");

        holder.btnClick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mClickListener != null) {
                    mClickListener.onItemClick(car);
                }
            }
        });
    }

    public void notifyData(ArrayList<Car> carBrands) {
        this.cars = carBrands;
        notifyDataSetChanged();
    }

    public void notifyData(boolean isSameCar) {
        this.isSameCar = isSameCar;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return cars.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private CardView viewIndex, bgIndex;
        private ImageView imgCar;
        private CustomTextView tvIndex, tvName,tvTime, tvPrice, tvTitleNew;
        private RelativeLayout btnClick, viewPromotion;
        private LinearLayout viewPrice, viewTime;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            btnClick = itemView.findViewById(R.id.btn_click);
            viewIndex = itemView.findViewById(R.id.view_index);
            bgIndex = itemView.findViewById(R.id.bg_index);
            tvIndex = itemView.findViewById(R.id.tv_index);
            tvName = itemView.findViewById(R.id.tv_name);
            tvTime = itemView.findViewById(R.id.tv_time);
            tvPrice = itemView.findViewById(R.id.tv_price);
            tvTitleNew = itemView.findViewById(R.id.tv_title_new);
            imgCar = itemView.findViewById(R.id.img_car);
            viewPromotion = itemView.findViewById(R.id.view_promotion);
            viewPrice = itemView.findViewById(R.id.view_price);
            viewTime = itemView.findViewById(R.id.view_time);
        }
    }

    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    public interface ItemClickListener {
        void onItemClick(Car car);
    }
}
