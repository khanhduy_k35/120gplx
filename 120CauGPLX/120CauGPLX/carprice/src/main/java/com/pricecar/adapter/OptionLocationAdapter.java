package com.pricecar.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ads.control.customview.CustomTextView;
import com.ads.control.utils.Utils;
import com.pricecar.R;
import com.pricecar.model.Car;
import com.pricecar.model.Province;
import com.rey.material.widget.RelativeLayout;

import java.util.ArrayList;

public class OptionLocationAdapter extends RecyclerView.Adapter<OptionLocationAdapter.ViewHolder> {

    private ArrayList<Province> provinces = new ArrayList<>();
    private int provinceSelect = 1;
    private Context context;
    private ItemClickListener mClickListener;

    public OptionLocationAdapter(ArrayList<Province> provinces, int provinceSelect, Context context) {
        this.provinces = provinces;
        this.context = context;
        this.provinceSelect = provinceSelect;
    }

    @NonNull
    @Override
    public OptionLocationAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        return new OptionLocationAdapter.ViewHolder(inflater.inflate(R.layout.item_dialog_option, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull OptionLocationAdapter.ViewHolder holder, int position) {
        Province province = provinces.get(position);
        holder.tvName.setText(province.getName());
        if (province.getId() == provinceSelect) {
            holder.tvName.setTextColor(Utils.getColorFromAttr(context, R.attr.color_text_item_setting_black));
            holder.tvName.setCustomFont(context, context.getResources().getString(R.string.medium));
        } else {
            holder.tvName.setCustomFont(context, context.getResources().getString(R.string.regular));
            holder.tvName.setTextColor(Utils.getColorFromAttr(context, R.attr.color_text_item_setting_dim));
        }
        holder.btnClick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mClickListener != null) {
                    mClickListener.onItemClick(province);
                }
            }
        });
    }

    public void notifyData(ArrayList<Province> provinces) {
        this.provinces = provinces;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return provinces.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private CustomTextView  tvName;
        private RelativeLayout btnClick;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            btnClick = itemView.findViewById(R.id.btn_click);
            tvName = itemView.findViewById(R.id.tv_name);
        }
    }

    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    public interface ItemClickListener {
        void onItemClick(Province province);
    }
}
