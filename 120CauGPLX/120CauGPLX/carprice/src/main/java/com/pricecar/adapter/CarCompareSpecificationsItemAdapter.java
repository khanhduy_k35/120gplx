package com.pricecar.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ads.control.customview.CustomTextView;
import com.pricecar.R;

import java.util.ArrayList;

public class CarCompareSpecificationsItemAdapter extends RecyclerView.Adapter<CarCompareSpecificationsItemAdapter.ViewHolder> {

    private ArrayList<String> info;
    private int indexInfo = 0;

    public CarCompareSpecificationsItemAdapter(ArrayList<String> info) {
        this.info = info;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        return new ViewHolder(inflater.inflate(R.layout.item_specifications_compare_detail, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        String detail = info.get(position);
        holder.tvName.setText(detail.split("::")[0]);
        if (detail.split("::").length > 1) {
            holder.tvDetail.setText(detail.split("::")[1]);
        }
        if (detail.split("::").length > 2) {
            holder.tvDetail2.setText(detail.split("::")[2]);
        }
    }

    public void notifyData(ArrayList<String> info, int indexInfo) {
        this.info = info;
        this.indexInfo = indexInfo;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return info.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private CustomTextView tvName, tvDetail, tvDetail2;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvName = itemView.findViewById(R.id.tv_name);
            tvDetail = itemView.findViewById(R.id.tv_detail);
            tvDetail2 = itemView.findViewById(R.id.tv_detail2);
        }
    }
}
