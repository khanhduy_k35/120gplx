package com.pricecar.service;

import com.pricecar.model.AllInfoAppResponse;
import com.pricecar.model.Car;
import com.pricecar.model.CarDetailResponse;
import com.pricecar.model.VersionResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface APIService {

    @GET("getAppInfo")
    Call<VersionResponse> getVersionApp();

    @GET("getAllInfoApp?")
    Call<AllInfoAppResponse> getAllInfoApp(@Query("version") String version);

    @GET("getCarDetailByID?")
    Call<CarDetailResponse> getCarDetailByID(@Query("id") String id);

}
