package com.pricecar.fragment;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.ads.control.customview.CustomTextView;
import com.ads.control.utils.SharedPreferencesUtils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.pricecar.R;
import com.pricecar.activity.CarDetailActivity;
import com.pricecar.activity.CarListActivity;
import com.pricecar.activity.CarProducerActivity;
import com.pricecar.model.Car;
import com.pricecar.model.CarBrand;
import com.pricecar.model.VehicleSegment;

import java.lang.reflect.Type;
import java.util.ArrayList;

public class CarInfoFragment extends Fragment {

    private CustomTextView tvShowMore, tvIntro;
    private boolean isShowMore = false;
    private Car car = new Car();
    private WebView webIntro;

    public CarInfoFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_car_info, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        tvIntro = view.findViewById(R.id.tv_intro);
        webIntro = view.findViewById(R.id.web_intro);
        tvShowMore = view.findViewById(R.id.tv_show_more);
        webIntro.getSettings().setJavaScriptEnabled(true);

        WebViewClient webViewClient= new WebViewClient(){
            @Override
            public boolean shouldOverrideUrlLoading(WebView  view, String  url){
                if (url.contains("dong-xe")) {
                    Gson gson = new Gson();
                    Type listCar = new TypeToken<ArrayList<Car>>() {}.getType();
                    ArrayList<Car> carNormal = gson.fromJson(SharedPreferencesUtils.getString("CarNormal", ""), listCar);
                    Car carSame = new Car();
                    for (Car temp: carNormal) {
                        if (url.contains(temp.getLink_detail())) {
                            carSame = temp;
                        }
                    }

                    ArrayList<Car> cars = new ArrayList<>();
                    cars.add(carSame);
                    for (Car temp: carNormal) {
                        if (temp.getCar_name().equals(car.getCar_name()) && (car.getId() != temp.getId())) {
                            cars.add(temp);
                        }
                    }
                    if (cars.size() > 0) {
                        Intent intent = new Intent(getActivity(), CarDetailActivity.class);
                        intent.putExtra("Car", cars);
                        startActivity(intent);
                    }
                } else if (url.contains("hang-xe")) {
                    Gson gson = new Gson();
                    Type listBrand = new TypeToken<ArrayList<CarBrand>>() {}.getType();
                    ArrayList<CarBrand> carBrands = gson.fromJson(SharedPreferencesUtils.getString("CarBrand", ""), listBrand);
                    CarBrand carSame = new CarBrand();
                    for (CarBrand temp: carBrands) {
                        if (url.contains(temp.getName().toLowerCase().replace(" ", "-"))) {
                            carSame = temp;
                        }
                    }
                    if (carSame.getId() > 0) {
                        Intent intent = new Intent(getActivity(), CarProducerActivity.class);
                        intent.putExtra("CarBrand", carSame);
                        startActivity(intent);
                    }
                } else if (url.contains("phan-khuc")) {
                    Gson gson = new Gson();
                    Type listBrand = new TypeToken<ArrayList<VehicleSegment>>() {}.getType();
                    ArrayList<VehicleSegment> vehicleSegments = gson.fromJson(SharedPreferencesUtils.getString("Segment", ""), listBrand);
                    VehicleSegment vehicleSegment = new VehicleSegment();
                    for (VehicleSegment temp: vehicleSegments) {
                        if (url.contains(temp.getLink())) {
                            vehicleSegment = temp;
                        }
                    }
                    if (vehicleSegment.getId() > 0) {
                        Type listCar = new TypeToken<ArrayList<Car>>() {}.getType();
                        ArrayList<Car> carNormal = gson.fromJson(SharedPreferencesUtils.getString("CarNormal", ""), listCar);
                        ArrayList<Car> cars = new ArrayList<>();
                        for (Car temp: carNormal) {
                            if (temp.getVehicle_segment().equals(vehicleSegment.getName())) {
                                cars.add(temp);
                            }
                        }
                        Intent intent = new Intent(getActivity(), CarListActivity.class);
                        intent.putExtra("Title", vehicleSegment.getName());
                        intent.putExtra("Car", cars);
                        startActivity(intent);
                    }
                } else {

                }
                return true;
            }
            @Override
            public void onLoadResource(WebView  view, String  url){
            }
        };
        webIntro.setWebViewClient(webViewClient);

        tvShowMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isShowMore = !isShowMore;
                if (isShowMore == true) {
                    tvShowMore.setText("THU GỌN");
                    webIntro.setVisibility(View.VISIBLE);
                    tvIntro.setVisibility(View.GONE);
                } else {
                    tvShowMore.setText("ĐỌC THÊM");
                    webIntro.setVisibility(View.GONE);
                    tvIntro.setVisibility(View.VISIBLE);
                }
                ((CarDetailActivity) getActivity()).reloadSizeViewPager100();
            }
        });
        if (car.getIntroShort() != "") {

            String backgroundColor = SharedPreferencesUtils.isNightMode() ? "#122F3E" : "#fff";
            String textColor = SharedPreferencesUtils.isNightMode() ? "#fff" : "#000";
            String data = "<!DOCTYPE html>\n" +
                    "<html>\n" +
                    "<head>\n" +
                    "    <meta http-equiv=\"Content-Type\" name=\"viewport\" content=\"text/html; charset=UTF-8; width=device-width; initial-scale=1.0\">\n" +
                    "    <title></title>\n" +
                    "\t<style type=\"text/css\">\n" +
                    "    body\n" +
                    "    {\n" +
                    "        padding:0;\n" +
                    "        margin:0;\n" +
                    "        background: "+ backgroundColor + ";\n" +
                    "        color: " + textColor + ";\n" +
                    "    }\n" +
                    "    img {width:100%;}\n" +
                    "    p.Image{\n" +
                    "        font-size: 2em;\n" +
                    "        font-style: italic;\n" +
                    "    }\n" +
                    "    a:link {\n" +
                    "        color: #008af7;\n" +
                    "    }\n" +
                    "</style>\n" +
                    "</head>\n" +
                    "<body>\n" +
                    "\n" +
                    car.getIntroFull() +
                    "\n" +
                    "</body> \n" +
                    "</html>\n";
            webIntro.loadDataWithBaseURL(null,data, "text/html", "UTF-8", null);

            webIntro.setVisibility(View.GONE);
            tvIntro.setVisibility(View.VISIBLE);
            tvIntro.setText(car.getIntroShort());
            ((CarDetailActivity) getActivity()).reloadSizeViewPager100();
        }
    }

    public void setCar(Car car) {
        this.car = car;
        isShowMore = false;
        if (this != null) {
            if (tvShowMore != null) {
                tvShowMore.setText("ĐỌC THÊM");
                webIntro.setVisibility(View.GONE);
                tvIntro.setVisibility(View.VISIBLE);
                tvIntro.setText(car.getIntroShort());
                webIntro.setWebChromeClient(new WebChromeClient());
                String backgroundColor = SharedPreferencesUtils.isNightMode() ? "#122F3E" : "#fff";
                String textColor = SharedPreferencesUtils.isNightMode() ? "#fff" : "#000";
                String data = "<!DOCTYPE html>\n" +
                        "<html>\n" +
                        "<head>\n" +
                        "    <meta http-equiv=\"Content-Type\" name=\"viewport\" content=\"text/html; charset=UTF-8; width=device-width; initial-scale=1.0\">\n" +
                        "    <title></title>\n" +
                        "\t<style type=\"text/css\">\n" +
                        "    body\n" +
                        "    {\n" +
                        "        padding:0;\n" +
                        "        margin:0;\n" +
                        "        background: "+ backgroundColor + ";\n" +
                        "        color: " + textColor + ";\n" +
                        "    }\n" +
                        "    img {width:100%;}\n" +
                        "    p.Image{\n" +
                        "        font-size: 2em;\n" +
                        "        font-style: italic;\n" +
                        "    }\n" +
                        "    a:link {\n" +
                        "        color: #008af7;\n" +
                        "    }\n" +
                        "</style>\n" +
                        "</head>\n" +
                        "<body>\n" +
                        "\n" +
                        car.getIntroFull() +
                        "\n" +
                        "</body> \n" +
                        "</html>\n";
                webIntro.loadDataWithBaseURL(null,data, "text/html", "UTF-8", null);
                ((CarDetailActivity) getActivity()).reloadSizeViewPager100();
            }
        }
    }
}