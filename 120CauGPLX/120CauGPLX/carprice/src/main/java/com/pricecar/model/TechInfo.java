package com.pricecar.model;

import android.animation.TimeInterpolator;

import com.pricecar.customview.expandable.Utils;

import java.util.ArrayList;

public class TechInfo {
    private String title;
    private ArrayList<String> detail;
    private String detailString;
    private TimeInterpolator interpolator;

    public TechInfo() {
        interpolator = Utils.createInterpolator(Utils.DECELERATE_INTERPOLATOR);
    }

    public TimeInterpolator getInterpolator() {
        return interpolator;
    }

    public void setInterpolator(TimeInterpolator interpolator) {
        this.interpolator = interpolator;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public ArrayList<String> getDetail() {
        return detail;
    }

    public String getDetailString() {
        return detailString;
    }

    public void setDetailString(String detailString) {
        this.detailString = detailString;
    }

    public void setDetail(ArrayList<String> detail) {
        this.detail = detail;
    }
}
