package com.pricecar.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ads.control.customview.CustomTextView;
import com.pricecar.R;

import java.util.ArrayList;

public class CarSpecificationsItemAdapter extends RecyclerView.Adapter<CarSpecificationsItemAdapter.ViewHolder> {

    private ArrayList<String> info;
    private int indexInfo = 0;

    public CarSpecificationsItemAdapter(ArrayList<String> info) {
        this.info = info;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        return new ViewHolder(inflater.inflate(R.layout.item_specifications_detail, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        String detail = info.get(position);
        holder.tvName.setText(detail.split("::")[0]);
        if (detail.split("::").length > 1) {
            holder.tvDetail.setText(detail.split("::")[1]);
        }
        if (indexInfo == 0) {
            if (holder.tvName.getText().toString().contains("Động cơ")) {
                holder.imgThumb.setVisibility(View.VISIBLE);
                holder.imgThumb.setImageResource(R.drawable.ic_muscle_stasis);
            } else if (holder.tvName.getText().toString().contains("Hộp số")) {
                holder.imgThumb.setVisibility(View.VISIBLE);
                holder.imgThumb.setImageResource(R.drawable.ic_gear);
            } else if (holder.tvName.getText().toString().contains("Công suất (mã lực)")) {
                holder.imgThumb.setVisibility(View.VISIBLE);
                holder.imgThumb.setImageResource(R.drawable.ic_wattage);
            } else if (holder.tvName.getText().toString().contains("Hệ dẫn động")) {
                holder.imgThumb.setVisibility(View.VISIBLE);
                holder.imgThumb.setImageResource(R.drawable.ic_drive_system);
            } else if (holder.tvName.getText().toString().contains("Mô-men xoắn (Nm)")) {
                holder.imgThumb.setVisibility(View.VISIBLE);
                holder.imgThumb.setImageResource(R.drawable.ic_mo_men);
            } else if (holder.tvName.getText().toString().contains("Số chỗ")) {
                holder.imgThumb.setVisibility(View.VISIBLE);
                holder.imgThumb.setImageResource(R.drawable.ic_number_seat);
            } else {
                holder.imgThumb.setVisibility(View.INVISIBLE);
            }
        } else {
            holder.imgThumb.setVisibility(View.INVISIBLE);
        }
    }

    public void notifyData(ArrayList<String> info, int indexInfo) {
        this.info = info;
        this.indexInfo = indexInfo;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return info.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private CustomTextView tvName, tvDetail;
        private ImageView imgThumb;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvName = itemView.findViewById(R.id.tv_name);
            tvDetail = itemView.findViewById(R.id.tv_detail);
            imgThumb = itemView.findViewById(R.id.img_thumb);
        }
    }
}
