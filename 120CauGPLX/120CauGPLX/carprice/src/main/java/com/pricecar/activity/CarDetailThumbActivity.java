package com.pricecar.activity;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ads.control.admob.AdmobHelp;
import com.ads.control.customview.CustomTextView;
import com.bumptech.glide.Glide;
import com.pricecar.R;
import com.pricecar.adapter.CarDetailThumbItemAdapter;
import com.pricecar.customview.photoview.PhotoView;
import com.pricecar.model.Car;

import java.util.ArrayList;
import java.util.Arrays;

public class CarDetailThumbActivity extends BaseCarActivity {

    private RecyclerView recyclerView;
    private CustomTextView tvTitle;
    private ViewPager viewPager;

    private Car car = new Car();
    private String currentThumb = "";
    private int indexCurrentSelect = 0;
    private ArrayList<String> thumbSmall = new ArrayList<>();
    private ArrayList<String> thumbBig = new ArrayList<>();

    private CarDetailThumbItemAdapter carDetailThumbItemAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_car_detail_thumb);
        AdmobHelp.getInstance().loadBanner(this);
        car = getIntent().getExtras().getParcelable("Car");
        currentThumb = getIntent().getExtras().getString("CurrentThumb");

        thumbSmall = new ArrayList<String>(Arrays.asList(car.getThumbList().split("\\|\\|")));
        thumbBig = new ArrayList<String>(Arrays.asList(car.getBigImgList().split("\\|\\|")));
        indexCurrentSelect = thumbSmall.indexOf(currentThumb);

        recyclerView = findViewById(R.id.recycle_thumb);
        viewPager = findViewById(R.id.view_pager);
        tvTitle = findViewById(R.id.tv_title);

        tvTitle.setText((indexCurrentSelect + 1) + "/" + thumbSmall.size());
        Adapter adapter = new Adapter(CarDetailThumbActivity.this, thumbBig);
        viewPager.setAdapter(adapter);

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                indexCurrentSelect = position;
                if(carDetailThumbItemAdapter != null) {
                    carDetailThumbItemAdapter.notifyData(indexCurrentSelect);
                }
                tvTitle.setText((indexCurrentSelect + 1) + "/" + thumbSmall.size());
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        viewPager.setCurrentItem(indexCurrentSelect);
        carDetailThumbItemAdapter = new CarDetailThumbItemAdapter(thumbSmall, indexCurrentSelect, CarDetailThumbActivity.this);
        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        recyclerView.setAdapter(carDetailThumbItemAdapter);
        carDetailThumbItemAdapter.setClickListener(new CarDetailThumbItemAdapter.ItemClickListener() {
            @Override
            public void onItemClick(int indexSelect) {
                indexCurrentSelect = indexSelect;
                carDetailThumbItemAdapter.notifyData(indexCurrentSelect);
                tvTitle.setText((indexCurrentSelect + 1) + "/" + thumbSmall.size());
                viewPager.setCurrentItem(indexCurrentSelect);
            }
        });

        findViewById(R.id.btn_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    class Adapter extends PagerAdapter {

        private Context mContext;
        private ArrayList<String> thumbBig = new ArrayList<>();

        public Adapter(Context context, ArrayList<String> thumbBig) {
            mContext = context;
            this.thumbBig = thumbBig;
        }

        @Override
        public int getCount() {
            return this.thumbBig.size();
        }

        @Override
        public Object instantiateItem(ViewGroup collection, int position) {
            View view = (View) LayoutInflater.from(mContext).inflate(R.layout.fragment_car_thumb, collection, false);
            collection.addView(view);
            PhotoView imvIntro = view.findViewById(R.id.img_car);
            Glide.with(mContext)
                    .load(thumbBig.get(position))
                    .placeholder(R.drawable.ic_car_temp)
                    .into(imvIntro);
            return view;
        }

        @Override
        public void destroyItem(ViewGroup collection, int position, Object view) {
            collection.removeView((View) view);
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return "";
        }

    }

}