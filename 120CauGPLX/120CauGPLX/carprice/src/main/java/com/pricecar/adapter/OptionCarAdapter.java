package com.pricecar.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ads.control.customview.CustomTextView;
import com.ads.control.utils.Utils;
import com.pricecar.R;
import com.pricecar.model.Car;
import com.pricecar.model.CarBrand;
import com.pricecar.model.Province;
import com.rey.material.widget.RelativeLayout;

import java.util.ArrayList;

public class OptionCarAdapter extends RecyclerView.Adapter<OptionCarAdapter.ViewHolder> {

    private ArrayList<Car> cars = new ArrayList<>();
    private Car carSelect = new Car();
    private Context context;
    private ItemClickListener mClickListener;

    public OptionCarAdapter(ArrayList<Car> cars, Car carSelect, Context context) {
        this.cars = cars;
        this.context = context;
        this.carSelect = carSelect;
    }

    @NonNull
    @Override
    public OptionCarAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        return new OptionCarAdapter.ViewHolder(inflater.inflate(R.layout.item_dialog_option, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull OptionCarAdapter.ViewHolder holder, int position) {
        Car car = cars.get(position);
        holder.tvName.setText(car.getCar_name());
        if (car.getId() == carSelect.getId()) {
            holder.tvName.setTextColor(Utils.getColorFromAttr(context, R.attr.color_text_item_setting_black));
            holder.tvName.setCustomFont(context, context.getResources().getString(R.string.medium));
        } else {
            holder.tvName.setCustomFont(context, context.getResources().getString(R.string.regular));
            holder.tvName.setTextColor(Utils.getColorFromAttr(context, R.attr.color_text_item_setting_dim));
        }
        holder.btnClick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mClickListener != null) {
                    mClickListener.onItemClick(car);
                }
            }
        });
    }

    public void notifyData(ArrayList<Car> cars) {
        this.cars = cars;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return cars.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private CustomTextView  tvName;
        private RelativeLayout btnClick;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            btnClick = itemView.findViewById(R.id.btn_click);
            tvName = itemView.findViewById(R.id.tv_name);
        }
    }

    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    public interface ItemClickListener {
        void onItemClick(Car car);
    }
}
